/**
 * This file is generated! Don't edit it directly.
 * See scripts/templates/assetSymbol.enum.tmpl.ts
 */

export enum ChainAssetSymbol {
  hive = '{{ hive_ticker }}',
  hbd = '{{ hbd_ticker }}',
  vests = '{{ vests_ticker }}',
}

export enum ExternalAssetSymbol {
  btc = 'BTC',
  usd = 'USD',
  hp = 'HP',
}
