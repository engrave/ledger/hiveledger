/* eslint-disable @typescript-eslint/no-var-requires,no-console */

require('dotenv').config();
const { readFileSync, writeFileSync } = require('fs');
const { compile } = require('handlebars');

const inputPath = './scripts/templates/assetSymbol.enum.tmpl.ts';
const outputPath = './src/modules/hive/assetSymbol/assetSymbol.enum.ts';

const template = readFileSync(inputPath);
const generateFile = compile(template.toString());

const context = {
  hive_ticker: process.env.REACT_APP_LIQUID_TICKER,
  hbd_ticker: process.env.REACT_APP_DEBT_TICKER,
  vests_ticker: process.env.REACT_APP_VEST_TICKER,
};

const code = generateFile(context);

writeFileSync(outputPath, code);

console.log('pregenerate - file "assetSymbol.enum.ts" generated');
