// eslint-disable-next-line @typescript-eslint/no-var-requires,import/no-extraneous-dependencies
const webpack = require('webpack');

module.exports = function override(config) {
  // eslint-disable-next-line no-param-reassign
  config.resolve.fallback = {
    buffer: require.resolve('buffer'),
    url: false,
    fs: false,
  };
  config.plugins.push(
    new webpack.ProvidePlugin({
      // @see: https://github.com/facebook/create-react-app/issues/11756
      Buffer: ['buffer', 'Buffer'],
    }),
  );

  return config;
};
