import { FullLoginCard } from 'app/auth/login/fullLoginCard/FullLoginCard';
import { ViewOnlyLoginCard } from 'app/auth/login/viewOnlyLoginCard/ViewOnlyLoginCard';
import { Columns } from 'app/auth/shared/columns/Columns';
import { Header } from 'app/layout/header/Header';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { AppContainer } from 'ui/layout/appContainer/AppContainer';
import { HeaderTitle } from 'ui/typography/headerTitle/HeaderTitle';

export const Login = () => {
  const translate = useTranslator();

  return (
    <>
      <Header />
      <AppContainer>
        <HeaderTitle>{translate('auth.login.title')}</HeaderTitle>

        <Columns>
          <FullLoginCard />
          <ViewOnlyLoginCard />
        </Columns>
      </AppContainer>
    </>
  );
};
