import { Container } from '@mui/material';
import { AssociateAccountCard } from 'app/auth/addAccount/associateAccountCard/AssociateAccountCard';
import { CreateAccountCard } from 'app/auth/addAccount/createAccountCard/CreateAccountCard';
import { Columns } from 'app/auth/shared/columns/Columns';
import { useHtmlTranslator } from 'context/localization/hooks/useHtmlTranslator';
import { Banner } from 'ui/message/banner/Banner';
import { HeaderTitle } from 'ui/typography/headerTitle/HeaderTitle';

export const AddAccount = () => {
  const translate = useHtmlTranslator();

  return (
    <Container fixed>
      <HeaderTitle>{translate('auth.addAccount.title')}</HeaderTitle>
      <Banner>{translate('auth.addAccount.message')}</Banner>
      <Columns>
        <AssociateAccountCard />
        <CreateAccountCard />
      </Columns>
    </Container>
  );
};
