import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Header } from 'app/layout/header/Header';
import { AuthRoute } from 'config/routes';
import { useAuth } from 'context/auth/hooks/useAuth';
import { useToast } from 'hooks/useToast/useToast';

export const Logout = () => {
  const navigate = useNavigate();
  const { showSuccess } = useToast();
  const { logout, isFullyAuthorized } = useAuth();

  useEffect(() => {
    if (isFullyAuthorized) {
      logout();
      showSuccess('auth.logout.success');
    }

    navigate(AuthRoute.login);
  });

  return (
    <>
      <Header />
    </>
  );
};
