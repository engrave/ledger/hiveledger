import { ProxyAccount } from 'app/dashboard/witnesses/proxyAccount/ProxyAccount';
import { ProxyBanner } from 'app/dashboard/witnesses/proxyBanner/ProxyBanner';
import { Ranking } from 'app/dashboard/witnesses/ranking/Ranking';
import { WitnessesBanner } from 'app/dashboard/witnesses/witnessesBanner/WitnessesBanner';
import { AppContainer } from 'ui/layout/appContainer/AppContainer';
import { Content, ContentColumns, Sidebar } from 'ui/layout/contentColumns/ContentColumns';

export const Witnesses = () => {
  return (
    <AppContainer>
      <WitnessesBanner />

      <ContentColumns>
        <Content>
          <Ranking />
        </Content>
        <Sidebar>
          <ProxyBanner />
          <ProxyAccount />
        </Sidebar>
      </ContentColumns>
    </AppContainer>
  );
};
