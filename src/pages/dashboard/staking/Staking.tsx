import { ActiveDelegations } from 'app/dashboard/staking/activeDelegations/ActiveDelegations';
import { DelegateStake } from 'app/dashboard/staking/delegateStake/DelegateStake';
import { DelegationInfo } from 'app/dashboard/staking/delegationInfo/DelegationInfo';
import { ExpiringDelegations } from 'app/dashboard/staking/expiringDelegations/ExpiringDelegations';
import { HivePower } from 'app/dashboard/staking/hivePower/HivePower';
import { ManagePowerDown } from 'app/dashboard/staking/managePowerDown/ManagePowerDown';
import { PowerDown } from 'app/dashboard/staking/powerDown/PowerDown';
import { StakingBanner } from 'app/dashboard/staking/stakingBanner/StakingBanner';
import { StakingRewards } from 'app/dashboard/staking/stakingRewards/StakingRewards';
import { AppContainer } from 'ui/layout/appContainer/AppContainer';
import { Content, ContentColumns, Sidebar } from 'ui/layout/contentColumns/ContentColumns';

export const Staking = () => {
  return (
    <AppContainer>
      <StakingBanner />
      <ContentColumns>
        <Content>
          <HivePower />
          <ManagePowerDown />
          <StakingRewards />
        </Content>
        <Sidebar>
          <DelegateStake />
          <DelegationInfo />
          <ActiveDelegations />
          <ExpiringDelegations />
          <PowerDown />
        </Sidebar>
      </ContentColumns>
    </AppContainer>
  );
};
