import { DelegateResourceCredits } from 'app/dashboard/resourceCredits/delegateResourceCredits/DelegateResourceCredits';
import { DelegationInfo } from 'app/dashboard/resourceCredits/delegationInfo/DelegationInfo';
import { Delegations } from 'app/dashboard/resourceCredits/delegations/Delegations';
import { ResourceCreditsBanner } from 'app/dashboard/resourceCredits/resourceCreditsBanner/ResourceCreditsBanner';
import { Summary } from 'app/dashboard/resourceCredits/summary/Summary';
import { AppContainer } from 'ui/layout/appContainer/AppContainer';
import { Content, ContentColumns, Sidebar } from 'ui/layout/contentColumns/ContentColumns';

export const ResourceCredits = () => {
  return (
    <AppContainer>
      <ResourceCreditsBanner />
      <ContentColumns>
        <Content>
          <Summary />
        </Content>
        <Sidebar>
          <DelegateResourceCredits />
          <DelegationInfo />
          <Delegations />
        </Sidebar>
      </ContentColumns>
    </AppContainer>
  );
};
