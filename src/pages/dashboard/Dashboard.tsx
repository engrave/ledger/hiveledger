import { useEffect } from 'react';
import { Outlet, useNavigate } from 'react-router-dom';
import { Header } from 'app/layout/header/Header';
import { AuthRoute } from 'config/routes';
import { useAuth } from 'context/auth/hooks/useAuth';
import { TransactionController } from 'context/transaction/transactionController/TransactionController';
import { useToast } from 'hooks/useToast/useToast';

export const Dashboard = () => {
  const { isFullyAuthorized } = useAuth();
  const { showError } = useToast();
  const navigate = useNavigate();

  useEffect(() => {
    if (!isFullyAuthorized) {
      navigate(AuthRoute.login);
      showError('errors.needToLogin');
    }
  });

  if (!isFullyAuthorized) {
    return null;
  }

  return (
    <>
      <TransactionController>
        <Header enableCompactMode={true} showNavigation={true} showAccountSwitcher={true} />
        <Outlet />
      </TransactionController>
    </>
  );
};
