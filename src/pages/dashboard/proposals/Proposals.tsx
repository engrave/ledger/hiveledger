import { Outlet } from 'react-router-dom';
import { ProposalsController } from 'context/proposals/proposalsController/ProposalsController';

export const Proposals = () => {
  return (
    <ProposalsController>
      <Outlet />
    </ProposalsController>
  );
};
