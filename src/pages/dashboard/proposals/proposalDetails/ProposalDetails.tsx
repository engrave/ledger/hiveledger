import { useNavigate, useParams } from 'react-router-dom';
import { ProposalContent } from 'app/dashboard/proposals/proposalContent/ProposalContent';
import { ProposalInfobox } from 'app/dashboard/proposals/proposalInfobox/ProposalInfobox';
import { ProposalVoters } from 'app/dashboard/proposals/proposalVoters/ProposalVoters';
import { SupportLevel } from 'app/dashboard/proposals/supportLevel/SupportLevel';
import { DashboardRoute } from 'config/routes';
import { useProposalDetails } from 'context/proposals/hooks/useProposalDetails/useProposalDetails';
import { proposalDetailsParamsSchema } from 'pages/dashboard/proposals/proposalDetails/ProposalDetails.schema';
import { ProposalDetailsParams } from 'pages/dashboard/proposals/proposalDetails/ProposalDetails.types';
import { AppContainer } from 'ui/layout/appContainer/AppContainer';
import { Content, ContentColumns, Sidebar } from 'ui/layout/contentColumns/ContentColumns';
import { SectionLoader } from 'ui/progress/sectionLoader/SectionLoader';

export const ProposalDetails = () => {
  const navigate = useNavigate();
  const params = useParams<ProposalDetailsParams>();

  if (!proposalDetailsParamsSchema.isValidSync(params)) {
    navigate(DashboardRoute.proposals);
  }

  const { proposal, proposalPost, error } = useProposalDetails(Number(params.id));

  if (error) {
    navigate(DashboardRoute.proposals);
    return null;
  }

  if (proposal === null || proposalPost === null) {
    return (
      <AppContainer>
        <SectionLoader height={300} />
      </AppContainer>
    );
  }

  return (
    <AppContainer>
      <ContentColumns>
        <Content>
          <ProposalContent proposal={proposal} post={proposalPost} />
        </Content>
        <Sidebar>
          <ProposalInfobox proposal={proposal} />
          <SupportLevel proposal={proposal} />
          <ProposalVoters proposal={proposal} />
        </Sidebar>
      </ContentColumns>
    </AppContainer>
  );
};
