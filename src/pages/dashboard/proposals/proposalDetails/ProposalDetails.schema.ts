import { number, object } from 'yup';
import { ProposalDetailsParams } from 'pages/dashboard/proposals/proposalDetails/ProposalDetails.types';

export const proposalDetailsParamsSchema = object<ProposalDetailsParams>().shape({
  id: number().integer().required(),
});
