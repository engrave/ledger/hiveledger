import { Budget } from 'app/dashboard/proposals/budget/Budget';
import { ProposalsBanner } from 'app/dashboard/proposals/proposalsBanner/ProposalsBanner';
import { ProxyBanner } from 'app/dashboard/proposals/proxyBanner/ProxyBanner';
import { Ranking } from 'app/dashboard/proposals/ranking/Ranking';
import { ProxyAccount } from 'app/dashboard/witnesses/proxyAccount/ProxyAccount';
import { AppContainer } from 'ui/layout/appContainer/AppContainer';
import { Content, ContentColumns, Sidebar } from 'ui/layout/contentColumns/ContentColumns';

export const ProposalsBrowser = () => {
  return (
    <AppContainer>
      <ProposalsBanner />
      <ContentColumns>
        <Content>
          <Ranking />
        </Content>
        <Sidebar>
          <Budget />
          <ProxyBanner />
          <ProxyAccount />
        </Sidebar>
      </ContentColumns>
    </AppContainer>
  );
};
