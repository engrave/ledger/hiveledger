import { AdvancedBanner } from 'app/dashboard/advanced/advancedBanner/AdvancedBanner';
import { Authorities } from 'app/dashboard/advanced/authorities/Authorities';
import { AppContainer } from 'ui/layout/appContainer/AppContainer';
import { Content, ContentColumns } from 'ui/layout/contentColumns/ContentColumns';

export const Advanced = () => {
  return (
    <AppContainer>
      <AdvancedBanner />

      <ContentColumns>
        <Content>
          <Authorities />
        </Content>
      </ContentColumns>
    </AppContainer>
  );
};
