import { DepositSavings } from 'app/dashboard/savings/depositSavings/DepositSavings';
import { EstimatedReward } from 'app/dashboard/savings/estimatedReward/EstimatedReward';
import { ManageWithdrawals } from 'app/dashboard/savings/manageWithdrawals/ManageWithdrawals';
import { SavingsAprInfo } from 'app/dashboard/savings/savingsAprInfo/SavingsAprInfo';
import { SavingsBanner } from 'app/dashboard/savings/savingsBanner/SavingsBanner';
import { Summary } from 'app/dashboard/savings/summary/Summary';
import { WithdrawSavings } from 'app/dashboard/savings/withdrawSavings/WithdrawSavings';
import { AppContainer } from 'ui/layout/appContainer/AppContainer';
import { Content, ContentColumns, Sidebar } from 'ui/layout/contentColumns/ContentColumns';

export const Savings = () => {
  return (
    <AppContainer>
      <SavingsBanner />
      <ContentColumns>
        <Content>
          <Summary />
          <SavingsAprInfo />
          <EstimatedReward />
        </Content>
        <Sidebar>
          <DepositSavings />
          <WithdrawSavings />
          <ManageWithdrawals />
        </Sidebar>
      </ContentColumns>
    </AppContainer>
  );
};
