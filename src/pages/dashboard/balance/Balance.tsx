import { Container } from '@mui/material';
import { BuyAndSell } from 'app/dashboard/balance/buyAndSell/BuyAndSell';
import { PowerUp } from 'app/dashboard/balance/powerUp/PowerUp';
import { Summary } from 'app/dashboard/balance/summary/Summary';
import { TransactionHistory } from 'app/dashboard/balance/transactionHistory/TransactionHistory';
import { Transfer } from 'app/dashboard/balance/transfer/Transfer';
import { ContentColumns, Content, Sidebar } from 'ui/layout/contentColumns/ContentColumns';

export const Balance = () => {
  return (
    <Container fixed>
      <ContentColumns>
        <Content>
          <Summary />
          <TransactionHistory />
        </Content>
        <Sidebar>
          <BuyAndSell />
          <Transfer />
          <PowerUp />
        </Sidebar>
      </ContentColumns>
    </Container>
  );
};
