import { Accounts } from 'app/dashboard/overview/accounts/Accounts';
import { AssociateAccountButton } from 'app/dashboard/overview/associateAccountButton/AssociateAccountButton';
import { SummaryContainer } from 'app/dashboard/overview/summary/Summary.container';
import { AppContainer } from 'ui/layout/appContainer/AppContainer';

export const Overview = () => {
  return (
    <AppContainer>
      <SummaryContainer />
      <Accounts />
      <AssociateAccountButton />
    </AppContainer>
  );
};
