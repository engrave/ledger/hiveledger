import { Container, Paper } from '@mui/material';

export const NotFound = () => {
  return (
    <Container fixed>
      <Paper sx={{ padding: 3 }}>#404 Not Found</Paper>
    </Container>
  );
};
