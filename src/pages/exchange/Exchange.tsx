import { ExchangeWindow } from 'app/exchange/ExchangeWindow/ExchangeWindow';
import { Header } from 'app/layout/header/Header';
import { AppContainer } from 'ui/layout/appContainer/AppContainer';

export const Exchange = () => {
  return (
    <>
      <Header />
      <AppContainer>
        <ExchangeWindow />
      </AppContainer>
    </>
  );
};
