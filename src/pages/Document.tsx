import { Box } from '@mui/material';
import { Outlet } from 'react-router-dom';
import { AppWrapper } from 'app/layout/appWrapper/AppWrapper';
import { Footer } from 'app/layout/footer/Footer';
import { NavigationLockController } from 'context/navigationLock/navigationLockController/navigationLockController';
import { Toasts } from 'ui/message/toast/Toasts';
import { GlobalProgressBar } from 'ui/progress/globalProgressBar/GlobalProgressBar';
import 'styles/vendor.scss';

export const Document = () => {
  return (
    <NavigationLockController>
      <GlobalProgressBar />
      <Toasts />
      <AppWrapper>
        <Box>
          <Outlet />
        </Box>
        <Footer />
      </AppWrapper>
    </NavigationLockController>
  );
};
