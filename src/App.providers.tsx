import { ThemeProvider } from '@mui/material';
import { QueryClientProvider } from 'react-query';
import { BrowserRouter } from 'react-router-dom';
import { queryClient } from 'api/client';
import { AnalyticsController } from 'context/analytics/analyticsController/AnalyticsController';
import { AssociationController } from 'context/association/associationController/AssociationController';
import { AuthController } from 'context/auth/authController/AuthController';
import { ExchangeController } from 'context/exchange/exchangeController/ExchangeController';
import { HiveController } from 'context/hive/hiveController/HiveController';
import { LedgerController } from 'context/ledger/ledgerController/LedgerController';
import { LocalizationProvider } from 'context/localization/localizationProvider/LocalizationProvider';
import { ProgressProvider } from 'context/progress/progressProvider/ProgressProvider';
import { theme } from 'styles/theme';
import { WrapperProps } from 'types/react';

export const AppProviders = ({ children }: WrapperProps) => {
  return (
    <ThemeProvider theme={theme}>
      <ProgressProvider>
        <QueryClientProvider client={queryClient}>
          <LocalizationProvider>
            <LedgerController>
              <HiveController>
                <ExchangeController>
                  <AuthController>
                    <AssociationController>
                      <BrowserRouter>
                        <AnalyticsController>{children}</AnalyticsController>
                      </BrowserRouter>
                    </AssociationController>
                  </AuthController>
                </ExchangeController>
              </HiveController>
            </LedgerController>
          </LocalizationProvider>
        </QueryClientProvider>
      </ProgressProvider>
    </ThemeProvider>
  );
};
