import { Bignum } from '@hiveio/dhive';

declare module '@hiveio/dhive' {
  interface DynamicGlobalProperties {
    vesting_reward_percent: number;
  }
}

declare module '@hiveio/dhive/lib/chain/rc' {
  interface RCAccount {
    delegated_rc?: Bignum;
    received_delegated_rc?: Bignum;
  }
}

export {};
