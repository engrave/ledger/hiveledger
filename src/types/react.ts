import { ReactNode } from 'react';
import { Styles } from 'styles/theme.types';

export type WrapperProps = {
  children: ReactNode;
};

export type StyledWrapperProps = {
  children: ReactNode;
  sx?: Styles;
};
