import { CssBaseline } from '@mui/material';
import { AppProviders } from 'App.providers';
import { Preloads } from 'app/core/Preloads/Preloads';
import { Routing } from 'app/Routing';
import { ScrollToTop } from 'ui/navigation/scrollToTop/ScrollToTop';

export function App() {
  return (
    <AppProviders>
      <Preloads />
      <CssBaseline />
      <ScrollToTop />
      <Routing />
    </AppProviders>
  );
}
