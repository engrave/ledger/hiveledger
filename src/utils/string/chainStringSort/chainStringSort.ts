export const chainStringSort = (textA: string, textB: string): -1 | 0 | 1 => {
  if (textA < textB) {
    return -1;
  }

  if (textA > textB) {
    return 1;
  }

  return 0;
};
