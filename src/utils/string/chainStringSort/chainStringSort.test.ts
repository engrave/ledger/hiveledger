import { chainStringSort } from 'utils/string/chainStringSort/chainStringSort';

describe('chainStringSort', () => {
  it('sorts strings by ASCII codes', () => {
    const items = ['a', 'b', 'bb', 'Bb', 'ba', 'BA', 'B', 'bA'];
    const properlyOrderedItems = ['B', 'BA', 'Bb', 'a', 'b', 'bA', 'ba', 'bb'];

    const sortedItems = items.sort((a, b) => chainStringSort(a, b));

    expect(sortedItems).toEqual(properlyOrderedItems);
  });
});
