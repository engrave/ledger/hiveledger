export const toSentenceCase = (text: string) => {
  const [firstPart, ...laterParts] = text.split('_');

  const firstPartCapitalized = firstPart.charAt(0).toUpperCase() + firstPart.slice(1);

  return [firstPartCapitalized, ...laterParts].join(' ');
};
