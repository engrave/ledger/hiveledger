import { ReportHandler } from 'web-vitals';

const reportWebVitals = async (onReportEntry?: ReportHandler) => {
  if (onReportEntry) {
    const module = await import('web-vitals');
    const { getCLS, getFID, getFCP, getLCP, getTTFB } = module;

    getCLS(onReportEntry);
    getFID(onReportEntry);
    getFCP(onReportEntry);
    getLCP(onReportEntry);
    getTTFB(onReportEntry);
  }
};

export default reportWebVitals;
