export const downloadContent = (name: string, content: string) => {
  const dataStr = `data:charset=utf-8,${encodeURIComponent(content)}`;

  const anchorNode = document.createElement('a');

  anchorNode.setAttribute('href', dataStr);
  anchorNode.setAttribute('download', name);

  anchorNode.click();
  anchorNode.remove();
};
