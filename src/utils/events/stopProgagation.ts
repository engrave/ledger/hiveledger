import { UIEvent } from 'react';

export const stopPropagation = (event: UIEvent) => {
  event.stopPropagation();
};
