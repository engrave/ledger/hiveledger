import { FormEvent } from 'react';

export const stopSubmitPropagation = (submitHandler: (event: FormEvent<HTMLFormElement>) => Promise<void>) => {
  return (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    event.stopPropagation();

    return submitHandler(event);
  };
};
