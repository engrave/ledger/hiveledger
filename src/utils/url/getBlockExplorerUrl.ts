import { generatePath } from 'react-router-dom';
import { environment } from 'config/environment';

export const getBlockExplorerUrl = (block: number, transactionId: string) => {
  return generatePath(`${environment.blockExplorer.url}/b/:block#:transactionId`, {
    block: block.toString(),
    transactionId,
  });
};
