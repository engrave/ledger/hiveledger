import { convertIntegerString } from 'utils/number/convertIntegerString/convertIntegerString';

describe('convertIntegerString', () => {
  it('should properly convert integer string to float', () => {
    expect(convertIntegerString('100100', 3)).toBe(100.1);
    expect(convertIntegerString('100123', 3)).toBe(100.123);
    expect(convertIntegerString('123', 3)).toBe(0.123);
    expect(convertIntegerString('123', 5)).toBe(0.00123);
    expect(convertIntegerString('123456', -3)).toBe(123456000);
    expect(convertIntegerString('123456', 0)).toBe(123456);
  });
});
