export const convertIntegerString = (integerString: string, precision: number): number => {
  const multiplier = 10 ** precision;

  return parseInt(integerString, 10) / multiplier;
};
