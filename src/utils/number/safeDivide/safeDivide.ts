export const safeDivide = (dividend: number, divider: number) => {
  if (divider === 0) {
    return 0;
  }

  return dividend / divider;
};
