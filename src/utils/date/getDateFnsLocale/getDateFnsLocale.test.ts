import { enUS, fr, frCA } from 'date-fns/locale';
import { getDateFnsLocale } from 'utils/date/getDateFnsLocale/getDateFnsLocale';

describe('getDateFnsLocale', () => {
  it('returns proper locale', () => {
    expect(getDateFnsLocale('fr')).toBe(fr);
    expect(getDateFnsLocale('fr-CA')).toBe(frCA);
    expect(getDateFnsLocale('zz-ZZ')).toBe(enUS);
  });
});
