import * as Locales from 'date-fns/locale';

const isLocaleAvailable = (locale: string): locale is keyof typeof Locales => {
  return Object.keys(Locales).includes(locale);
};

export function getDateFnsLocale(localeName: string): Locale {
  const dateNfsLocaleName = localeName.substring(0, 2) + localeName.substring(3, 5);

  if (isLocaleAvailable(dateNfsLocaleName)) {
    return Locales[dateNfsLocaleName];
  }

  return Locales.enUS;
}
