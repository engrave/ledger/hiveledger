import { parseISO } from 'date-fns';

export const parseChainApiDate = (date: string) => {
  const isoDate = date.endsWith('Z') ? date : `${date}Z`;

  return parseISO(isoDate);
};
