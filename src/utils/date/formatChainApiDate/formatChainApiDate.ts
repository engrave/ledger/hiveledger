import { format } from 'date-fns';
import { zonedTimeToUtc } from 'date-fns-tz';

export const formatChainApiDate = (date: string | Date) => {
  const { timeZone } = Intl.DateTimeFormat().resolvedOptions();
  const utcDate = zonedTimeToUtc(date, timeZone);

  return format(utcDate, "yyyy-MM-dd'T'HH:mm:ss");
};
