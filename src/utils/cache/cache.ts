import NodeCache from 'node-cache';

const globalCache = new NodeCache();

export enum CacheKey {}

const defaultTtl = 30 * 1000;

export const cache = {
  get: <T>(key: CacheKey) => {
    return globalCache.get<T>(key);
  },
  set: <T>(key: CacheKey, value: T, ttl: number = defaultTtl) => {
    return globalCache.set<T>(key, value, ttl);
  },
  delete: (key: CacheKey) => {
    return globalCache.del(key);
  },
  retrieve: async <T>(key: CacheKey, callback: () => T | Promise<T>, force = false, ttl: number = defaultTtl) => {
    if (!globalCache.has(key) || force) {
      globalCache.set<T>(key, await callback(), ttl);
    }

    return globalCache.get<T>(key) as T;
  },
};
