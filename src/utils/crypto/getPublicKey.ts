import { PrivateKey } from '@hiveio/dhive';
import { environment } from 'config/environment';

export const getPublicKey = (privateKeyString: string): string => {
  const privateKey = PrivateKey.fromString(privateKeyString);
  const publicKey = privateKey.createPublic(environment.hive.addressPrefix);

  return publicKey.toString();
};
