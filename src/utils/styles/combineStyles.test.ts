import { Styles } from 'styles/theme.types';
import { combineStyles } from 'utils/styles/combineStyles';

describe('combineStyles', () => {
  it('should merge and overwrite styles', () => {
    const style1: Styles = {
      width: '100px',
      height: '200px',
      color: 'yellow',
    };

    const style2: Styles = {
      color: 'red',
      display: 'grid',
    };

    const style3: Styles = {
      width: '200px',
      color: 'black',
    };

    const combined = combineStyles(style1, style2, style3);

    expect(combined).toEqual({
      width: '200px',
      height: '200px',
      display: 'grid',
      color: 'black',
    });
  });

  it('should merge complex styles recursively', () => {
    const style1: Styles = {
      width: '100px',
      height: '200px',

      '.foobar': {
        color: 'white',
        background: 'red',
      },
    };

    const style2: Styles = {
      width: '300px',
      height: '400px',

      '.foobar': {
        background: 'black',
        zIndex: 9999,
      },
    };

    const combined = combineStyles(style1, style2);

    expect(combined).toEqual({
      width: '300px',
      height: '400px',

      '.foobar': {
        color: 'white',
        background: 'black',
        zIndex: 9999,
      },
    });
  });
});
