import { mergeDeepRight } from 'ramda';
import { Styles } from 'styles/theme.types';

export const combineStyles = (...stylesList: (Styles | undefined)[]): Styles => {
  let stylesCombined = {};

  for (const styles of stylesList) {
    stylesCombined = mergeDeepRight(stylesCombined, styles ?? {});
  }

  return stylesCombined;
};
