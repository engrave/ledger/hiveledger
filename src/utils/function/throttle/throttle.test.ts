import { throttle } from 'utils/function/throttle/throttle';

describe('throttle', () => {
  it('should throttle too fast calls', () => {
    const callback = jest.fn();

    const throttledCallback = throttle(callback, 1000);

    throttledCallback(1);
    throttledCallback(2);
    throttledCallback(3);
    throttledCallback(4);
    throttledCallback(5);

    expect(callback).toBeCalledTimes(1);
    expect(callback).toHaveBeenCalledWith(1);
  });

  it('should call the function after throttling time passed', () => {
    jest.useFakeTimers();
    const callback = jest.fn();

    const throttledCallback = throttle(callback, 1000);

    throttledCallback(1);
    throttledCallback(2);

    jest.advanceTimersByTime(500);

    throttledCallback(3);

    jest.advanceTimersByTime(500);

    throttledCallback(4);
    throttledCallback(5);

    expect(callback).toBeCalledTimes(2);
    expect(callback.mock.calls).toEqual([[1], [4]]);
  });

  it('calls the function with last parameters after throttle if trailer is enabled', () => {
    jest.useFakeTimers();
    const callback = jest.fn();

    const throttledCallback = throttle(callback, 1000, { trailer: true });

    throttledCallback(1);
    throttledCallback(2);

    jest.advanceTimersByTime(500);

    throttledCallback(3);

    jest.advanceTimersByTime(500);

    throttledCallback(4);
    throttledCallback(5);

    expect(callback).toBeCalledTimes(3);
    expect(callback.mock.calls).toEqual([[1], [3], [4]]);
  });

  it('does not run the trailer if there was no call during throttling', () => {
    jest.useFakeTimers();
    const callback = jest.fn();

    const throttledCallback = throttle(callback, 1000, { trailer: true });

    throttledCallback(1);

    jest.advanceTimersByTime(1000);

    throttledCallback(2);
    throttledCallback(3);

    expect(callback).toBeCalledTimes(2);
    expect(callback.mock.calls).toEqual([[1], [2]]);
  });
});
