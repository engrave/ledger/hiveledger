export type ThrottleSettings = {
  trailer: boolean;
};

export const throttle = <P extends unknown[]>(
  callback: (...params: P) => unknown,
  time: number,
  options: Partial<ThrottleSettings> = {},
) => {
  const settings: ThrottleSettings = {
    trailer: false,
    ...options,
  };

  let isThrottling: boolean;
  let lastCall: P;
  let callsCount = 0;

  return (...parameters: P) => {
    lastCall = parameters;
    callsCount += 1;

    if (!isThrottling) {
      callback(...parameters);

      isThrottling = true;

      setTimeout(() => {
        isThrottling = false;

        if (settings.trailer && callsCount > 1) {
          callback(...lastCall);
        }
      }, time);
    }
  };
};
