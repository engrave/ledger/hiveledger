import { useEffect } from 'react';
import ReactGoogleAnalytics4 from 'react-ga4';
import { useLocation } from 'react-router-dom';
import { analytics } from 'config/environment';
import { WrapperProps } from 'types/react';

export const AnalyticsController = ({ children }: WrapperProps) => {
  const { pathname } = useLocation();

  useEffect(() => {
    if (!ReactGoogleAnalytics4.isInitialized && analytics.googleAnalyticsId !== null) {
      ReactGoogleAnalytics4.initialize(analytics.googleAnalyticsId);
    }
  }, []);

  useEffect(() => {
    if (ReactGoogleAnalytics4.isInitialized) {
      ReactGoogleAnalytics4.send({ hitType: 'pageview', page: pathname });
    }
  }, [pathname]);

  return <>{children}</>;
};
