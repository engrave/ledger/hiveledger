import { createContext } from 'react';
import { ExchangeContext } from 'context/exchange/exchangeContext/exchangeContext.types';

const defaultExchangeContext: ExchangeContext = {
  rates: null,
  convertToChain: () => null,
  convertToExternal: () => null,
  convertToComparable: () => ({ usd: null, btc: null }),
};

export const exchangeContext = createContext<ExchangeContext>(defaultExchangeContext);
