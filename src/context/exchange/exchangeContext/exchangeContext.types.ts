import { ComparableEquivalents } from 'modules/exchange/convertAsset/convertAsset.types';
import { ChainExchangeRates, ExchangeChainSymbol, ExchangeExternalSymbol } from 'modules/exchange/exchange';
import { Asset } from 'modules/hive/asset/asset';

export type ExchangeContext = {
  rates: ChainExchangeRates | null;
  convertToChain: <S extends ExchangeChainSymbol>(
    source: Asset<ExchangeExternalSymbol>,
    targetSymbol: S,
  ) => Asset<S> | null;
  convertToExternal: <S extends ExchangeExternalSymbol>(
    source: Asset<ExchangeChainSymbol>,
    targetSymbol: S,
  ) => Asset<S> | null;
  convertToComparable: (source: Asset<ExchangeChainSymbol>) => ComparableEquivalents;
};
