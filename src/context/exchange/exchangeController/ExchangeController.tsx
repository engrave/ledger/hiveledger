import { useCallback } from 'react';
import { useHiveExchange } from 'api/hooks/useHiveExchange/useHiveExchange';
import { exchangeContext } from 'context/exchange/exchangeContext/exchangeContext';
import { ExchangeContext } from 'context/exchange/exchangeContext/exchangeContext.types';
import {
  convertChainAsset,
  convertChainToComparableExternals,
  convertExternalAsset,
} from 'modules/exchange/convertAsset/convertAsset';
import { ExchangeChainSymbol, ExchangeExternalSymbol } from 'modules/exchange/exchange';
import { Asset } from 'modules/hive/asset/asset';
import { WrapperProps } from 'types/react';

export const ExchangeController = ({ children }: WrapperProps) => {
  const { Provider } = exchangeContext;
  const query = useHiveExchange();

  const convertToExternal = useCallback(
    <S extends ExchangeExternalSymbol>(source: Asset<ExchangeChainSymbol>, targetSymbol: S): Asset<S> | null => {
      if (!query.isSuccess) {
        return null;
      }

      return convertChainAsset(source, targetSymbol, query.exchangeRates);
    },
    [query.exchangeRates, query.isSuccess],
  );

  const convertToChain = useCallback(
    <S extends ExchangeChainSymbol>(source: Asset<ExchangeExternalSymbol>, targetSymbol: S): Asset<S> | null => {
      if (!query.isSuccess) {
        return null;
      }

      return convertExternalAsset(source, targetSymbol, query.exchangeRates);
    },
    [query.exchangeRates, query.isSuccess],
  );

  const convertToComparable = useCallback(
    (source: Asset<ExchangeChainSymbol>) => {
      if (!query.isSuccess) {
        return { usd: null, btc: null };
      }

      return convertChainToComparableExternals(source, query.exchangeRates);
    },
    [query.exchangeRates, query.isSuccess],
  );

  const value: ExchangeContext = {
    rates: query.exchangeRates,
    convertToExternal,
    convertToChain,
    convertToComparable,
  };

  return <Provider value={value}>{children}</Provider>;
};
