import { useContext } from 'react';
import { exchangeContext } from 'context/exchange/exchangeContext/exchangeContext';

export const useExchange = () => {
  return useContext(exchangeContext);
};
