import { DynamicGlobalProperties } from '@hiveio/dhive';
import { Client as HiveClient } from '@hiveio/dhive/lib/client';
import { AccountDetails } from 'modules/hive/account/account';

export type SavingWithdrawal = {
  id: string;
  from: string;
  to: string;
  amount: string;
  memo: string;
  request_id: number;
  complete: string;
};

export type HiveContext = {
  hive: HiveClient;
  globalProperties: DynamicGlobalProperties;
  getAccountDetails: (username: string) => Promise<AccountDetails>;
};
