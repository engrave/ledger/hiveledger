import { createContext } from 'react';
import { HiveContext } from 'context/hive/hiveContext/hiveContext.types';

export const hiveContext = createContext<HiveContext | null>(null);
