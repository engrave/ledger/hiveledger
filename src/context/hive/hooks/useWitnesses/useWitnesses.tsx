import { useCallback, useEffect, useState } from 'react';
import { witnessesConfig } from 'config/config';
import { useHive } from 'context/hive/hooks/useHive';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { Witness } from 'modules/hive/witnesses/witnesses';

export const useWitnesses = () => {
  const { hive } = useHive();
  const { start, done } = useGlobalProgressBar();

  const [witnesses, setWitnesses] = useState<Witness[]>([]);
  const [isFetchingCompleted, setFetchingComplete] = useState(false);
  const [isLoading, setLoading] = useState(false);

  const fetchMore = useCallback(async () => {
    start();
    setLoading(true);

    const lastWitness = witnesses.at(-1)?.owner ?? '';
    const perPage = witnesses.length > 0 ? witnessesConfig.perPage + 1 : witnessesConfig.perPage;

    const response: Witness[] = await hive.database.call('get_witnesses_by_vote', [lastWitness, perPage]);
    const moreWitnesses = witnesses.length === 0 ? response : response.slice(1);

    setLoading(false);
    done();

    setWitnesses([...witnesses, ...moreWitnesses]);

    if (moreWitnesses.length < perPage - 1) {
      setFetchingComplete(true);
    }
  }, [done, hive.database, start, witnesses]);

  const reload = useCallback(async () => {
    setFetchingComplete(false);
    setWitnesses([]);
    await fetchMore();
  }, [fetchMore]);

  useEffect(() => {
    if (witnesses.length === 0) {
      fetchMore();
    }
  }, [fetchMore, witnesses.length]);

  const isFirstLoading = isLoading && witnesses.length === 0;

  return { witnesses, isLoading, isFirstLoading, fetchMore, reload, isFetchingCompleted };
};
