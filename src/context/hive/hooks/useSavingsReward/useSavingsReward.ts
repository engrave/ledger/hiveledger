import { add, isPast } from 'date-fns';
import { useMemo } from 'react';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useHive } from 'context/hive/hooks/useHive';
import { getEstimatedSavingsReward } from 'modules/hive/account/getEstimatedSavingsReward/getEstimatedSavingsReward';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { parseChainApiDate } from 'utils/date/parseChainApiDate/parseChainApiDate';

export const useSavingsReward = () => {
  const { globalProperties } = useHive();
  const account = useActiveAccount();

  const estimatedSavingsReward = useMemo(() => {
    return getEstimatedSavingsReward(
      {
        savingsSeconds: Number(account.details.savings_hbd_seconds),
        savingsTimeLastUpdate: account.details.savings_hbd_seconds_last_update,
        savingsHbdBalance: createAsset(account.details.savings_hbd_balance, ChainAssetSymbol.hbd),
      },
      globalProperties.hbd_interest_rate,
    );
  }, [account.details, globalProperties.hbd_interest_rate]);

  const nextPayoutDate = useMemo(() => {
    const lastPayoutUtc = parseChainApiDate(account.details.savings_hbd_last_interest_payment);

    return add(lastPayoutUtc, { days: 30 });
  }, [account.details]);

  const isPayoutAvailable = useMemo(() => {
    return isPast(nextPayoutDate);
  }, [nextPayoutDate]);

  return {
    estimatedSavingsReward,
    nextPayoutDate,
    isPayoutAvailable,
  };
};
