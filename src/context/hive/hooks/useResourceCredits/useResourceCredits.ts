import { useCallback } from 'react';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { AccountResourceCredits, ResourceCreditsDelegation } from 'modules/hive/resourceCredits/resourceCredits';
import { useHive } from '../useHive';

export const useResourceCredits = () => {
  const { hive } = useHive();
  const { start, done } = useGlobalProgressBar();

  const fetchResourceCreditsData = useCallback(
    async (username: string): Promise<AccountResourceCredits> => {
      start();
      const [account] = await hive.rc.findRCAccounts([username]);
      done();

      const mana = hive.rc.calculateRCMana(account);

      return {
        account,
        mana: {
          ...mana,
          current_mana: Math.floor(mana.current_mana),
        },
      };
    },
    [done, hive.rc, start],
  );

  const fetchResourceCreditsDelegations = useCallback(
    async (username: string): Promise<ResourceCreditsDelegation[]> => {
      start();
      const delegations = await hive.call('rc_api', 'list_rc_direct_delegations', {
        start: [username, ''],
        limit: 1000,
      });
      done();

      return delegations.rc_direct_delegations;
    },
    [done, hive, start],
  );

  return { fetchResourceCreditsData, fetchResourceCreditsDelegations };
};
