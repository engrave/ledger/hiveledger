import { useContext } from 'react';
import { hiveContext } from 'context/hive/hiveContext/hiveContext';

export const useHive = () => {
  const context = useContext(hiveContext);

  if (context === null) {
    throw new Error('HiveController is missing!');
  }

  return context;
};
