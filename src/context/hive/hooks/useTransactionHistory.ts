import { useCallback, useEffect, useState } from 'react';
import { transactionsHistory } from 'config/transactions';
import { useHive } from 'context/hive/hooks/useHive';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { useToast } from 'hooks/useToast/useToast';
import { TransactionsHistoryItem } from 'modules/hive/transaction/transaction';

export const useTransactionHistory = (username: string) => {
  const { hive } = useHive();
  const { start, done } = useGlobalProgressBar();
  const { showError } = useToast();

  const [isLoading, setLoading] = useState(false);
  const [isFetchingCompleted, setFetchingComplete] = useState(false);
  const [history, setHistory] = useState<TransactionsHistoryItem[]>([]);

  const loadNextPage = useCallback(async () => {
    start();
    setLoading(true);

    const startItem = history.length > 0 ? history[history.length - 1][0] : -1;
    const limit = startItem > -1 ? Math.min(startItem, transactionsHistory.perPage) : transactionsHistory.perPage;

    let response: TransactionsHistoryItem[];

    try {
      response = await hive.database.getAccountHistory(username, startItem, limit, [
        transactionsHistory.operationFilterHigh,
        transactionsHistory.operationFilterLow,
      ]);
    } catch (error) {
      showError('dashboard.balance.history.fetchingError');
      setLoading(false);
      done();
      return;
    }

    const newItems = history.length === 0 ? response.reverse() : response.reverse().slice(1);

    setHistory([...history, ...newItems]);

    setLoading(false);
    done();

    if (newItems.length === 0) {
      setFetchingComplete(true);
    }
  }, [history, done, hive.database, start, username, showError]);

  useEffect(() => {
    if (history.length === 0 && !isFetchingCompleted && !isLoading) {
      loadNextPage();
    }
  }, [isFetchingCompleted, history, loadNextPage, isLoading]);

  useEffect(() => {
    setHistory([]);
    setFetchingComplete(false);
  }, [username]);

  const isFirstLoading = isLoading && history.length === 0;

  return { isLoading, history, loadNextPage, isFetchingCompleted, isFirstLoading };
};
