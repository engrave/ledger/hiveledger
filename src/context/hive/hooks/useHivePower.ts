import { useCallback } from 'react';
import { useHive } from 'context/hive/hooks/useHive';
import { HpAsset, VestsAsset } from 'modules/hive/asset/asset';
import { calculateHivePower } from 'modules/hive/hivePower/calculateHivePower/calculateHivePower';
import { calculateHivePowerApr } from 'modules/hive/hivePower/calculateHivePowerApr/calculateHivePowerApr';
import { calculateVests } from 'modules/hive/hivePower/calculateVests/calculateVests';

export const useHivePower = () => {
  const { globalProperties } = useHive();

  const getHivePower = useCallback(
    (vests: VestsAsset): HpAsset => {
      return calculateHivePower(vests, globalProperties);
    },
    [globalProperties],
  );

  const getVests = useCallback(
    (asset: HpAsset): VestsAsset => {
      return calculateVests(asset, globalProperties);
    },
    [globalProperties],
  );

  const getHivePowerApr = useCallback(() => {
    return calculateHivePowerApr(globalProperties);
  }, [globalProperties]);

  return { getHivePower, getVests, getHivePowerApr };
};
