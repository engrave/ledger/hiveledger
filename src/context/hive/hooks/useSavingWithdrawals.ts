import { useCallback, useEffect, useState } from 'react';
import { useAuth } from 'context/auth/hooks/useAuth';
import { SavingWithdrawal } from 'context/hive/hiveContext/hiveContext.types';
import { useHive } from 'context/hive/hooks/useHive';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';

export const useSavingWithdrawals = (username: string) => {
  const { hive } = useHive();
  const { activateAccount } = useAuth();
  const { start, done } = useGlobalProgressBar();

  const [isLoading, setLoading] = useState(true);
  const [withdrawals, setWithdrawals] = useState<SavingWithdrawal[]>([]);

  const loadWithdrawals = useCallback(async () => {
    start();
    const response = await hive.database.call('get_savings_withdraw_from', [username]);

    setWithdrawals(response);
    setLoading(false);
    done();
  }, [done, hive, start, username]);

  useEffect(() => {
    loadWithdrawals();
  }, [loadWithdrawals, activateAccount]);

  return { isLoading, withdrawals, refresh: loadWithdrawals };
};
