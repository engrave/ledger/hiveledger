import { useCallback } from 'react';
import { useHive } from 'context/hive/hooks/useHive';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { AccountVestingDelegations, ExpiringVestingDelegation } from 'modules/hive/account/account';
import { formatChainApiDate } from 'utils/date/formatChainApiDate/formatChainApiDate';

export const useVestingDelegations = () => {
  const { hive } = useHive();
  const { start, done } = useGlobalProgressBar();

  const fetchActiveVestingDelegations = useCallback(
    async (username: string) => {
      start();
      const delegations = await hive.database.getVestingDelegations(username);
      done();

      return delegations;
    },
    [done, hive.database, start],
  );

  const fetchExpiringVestingDelegations = useCallback(
    async (username: string): Promise<ExpiringVestingDelegation[]> => {
      start();
      const requestParams = [username, formatChainApiDate(new Date())];
      const delegations = await hive.database.call('get_expiring_vesting_delegations', requestParams);
      done();

      return delegations;
    },
    [done, hive.database, start],
  );

  const fetchVestingDelegations = useCallback(
    async (username: string): Promise<AccountVestingDelegations> => {
      const [active, expiring] = await Promise.all([
        fetchActiveVestingDelegations(username),
        fetchExpiringVestingDelegations(username),
      ]);

      return { active, expiring };
    },
    [fetchActiveVestingDelegations, fetchExpiringVestingDelegations],
  );

  return {
    fetchActiveVestingDelegations,
    fetchExpiringVestingDelegations,
    fetchVestingDelegations,
  };
};
