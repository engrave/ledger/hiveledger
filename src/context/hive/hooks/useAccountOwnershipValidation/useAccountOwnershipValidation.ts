import { useCallback } from 'react';
import { OwnershipValidationResult } from 'context/hive/hooks/useAccountOwnershipValidation/useAccountOwnershipValidation.types';
import { useHive } from 'context/hive/hooks/useHive';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { AccountDetails, AccountError } from 'modules/hive/account/account';
import { checkAccountOwnership } from 'modules/hive/account/checkAccountOwnership/checkAccountOwnership';
import { getPublicKey } from 'utils/crypto/getPublicKey';

export const useAccountOwnershipValidation = () => {
  const { getAccountDetails } = useHive();
  const { start, done } = useGlobalProgressBar();

  return useCallback(
    async (privateKey: string, username: string): Promise<OwnershipValidationResult> => {
      start();
      let publicKey: string;
      let accountDetails: AccountDetails;
      let ownershipCheckResult: true | AccountError = true;

      try {
        publicKey = getPublicKey(privateKey);
        accountDetails = await getAccountDetails(username);

        ownershipCheckResult = checkAccountOwnership(accountDetails, publicKey);
      } finally {
        done();
      }

      if (ownershipCheckResult === true) {
        return {
          isOwned: true,
          publicKey,
          accountDetails,
        };
      }

      return {
        isOwned: false,
        error: ownershipCheckResult,
      };
    },
    [done, getAccountDetails, start],
  );
};
