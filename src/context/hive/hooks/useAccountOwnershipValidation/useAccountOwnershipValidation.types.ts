import { AccountDetails, AccountError } from 'modules/hive/account/account';

export type PositiveOwnershipValidationResult = {
  isOwned: true;
  accountDetails: AccountDetails;
  publicKey: string;
};

export type NegativeOwnershipValidationResult = {
  isOwned: false;
  error: AccountError;
};

export type OwnershipValidationResult = PositiveOwnershipValidationResult | NegativeOwnershipValidationResult;
