import { Client, DynamicGlobalProperties } from '@hiveio/dhive';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { chain } from 'config/config';
import { environment } from 'config/environment';
import { hiveContext } from 'context/hive/hiveContext/hiveContext';
import { HiveContext } from 'context/hive/hiveContext/hiveContext.types';
import { AccountDetails, AccountError } from 'modules/hive/account/account';
import { WrapperProps } from 'types/react';
import { SectionLoader } from 'ui/progress/sectionLoader/SectionLoader';

export const HiveController = ({ children }: WrapperProps) => {
  const { Provider } = hiveContext;
  const [globalProperties, setGlobalProperties] = useState<DynamicGlobalProperties | null>(null);

  const hive = useMemo(() => {
    return new Client(environment.hive.apiUrl, {
      addressPrefix: environment.hive.addressPrefix,
      chainId: environment.hive.chainId,
    });
  }, []);

  const loadGlobalProperties = useCallback(async () => {
    const properties = await hive.database.getDynamicGlobalProperties();
    setGlobalProperties(properties);
  }, [hive.database]);

  const getAccountDetails = useCallback(
    async (username: string): Promise<AccountDetails> => {
      const accounts = await hive.database.getAccounts([username]);

      if (accounts[0]) {
        return accounts[0];
      }

      throw new Error(AccountError.notFound);
    },
    [hive.database],
  );

  useEffect(() => {
    const interval = setInterval(() => loadGlobalProperties(), chain.globalPropertiesRefreshTime);
    loadGlobalProperties();

    return () => clearInterval(interval);
  }, [loadGlobalProperties]);

  if (globalProperties === null) {
    return <SectionLoader />;
  }

  const value: HiveContext = {
    hive,
    globalProperties,
    getAccountDetails,
  };

  return <Provider value={value}>{children}</Provider>;
};
