export enum TransactionError {
  deviceLocked = 'TransactionError/DeviceLocked',
  applicationNotRunning = 'TransactionError/ApplicationNotRunning',
  transactionRejectedByUser = 'TransactionError/TransactionRejectedByUser',
  transactionNotIncludedInBlock = 'TransactionError/TransactionNotIncludedInBlock',
  genericError = 'TransactionError/GenericError',
}
