import LedgerHiveApp from '@engrave/ledger-app-hive';
import { PrivateKey, SignedTransaction, Transaction } from '@hiveio/dhive';
import { StatusCodes } from '@ledgerhq/hw-transport';
import { isTransactionAwaiting, isTransactionConfirmed } from 'app/transaction/transactionModal/helpers';
import { environment } from 'config/environment';
import { useHive } from 'context/hive/hooks/useHive';
import { useLedger } from 'context/ledger/hooks/useLedger/useLedger';
import { TransactionError } from 'context/transaction/hooks/useTransaction/useTransaction.types';
import { isSingleOperation, Operation } from 'modules/hive/operation/operation';
import { createTransaction } from 'modules/hive/transaction/createTransaction/createTransaction';
import { delay } from 'utils/async/delay';
import { logApplicationError } from 'utils/debug/logApplicationError';

function translateError(error: unknown): never {
  if (error instanceof Error && error.name === 'LockedDeviceError') {
    throw new Error(TransactionError.deviceLocked);
  }
  if (error instanceof Error && error.name === 'TransportStatusError') {
    // @ts-expect-error TransportStatusError not typed correctly
    if (error.statusCode === StatusCodes.CONDITIONS_OF_USE_NOT_SATISFIED) {
      throw new Error(TransactionError.transactionRejectedByUser);
    }
    // @ts-expect-error TransportStatusError not typed correctly
    if (error.statusCode === 28161) {
      // 0x6e01 not typed correctly
      throw new Error(TransactionError.applicationNotRunning);
    }
  }
  throw new Error(TransactionError.genericError);
}

export const useTransaction = () => {
  const { globalProperties, hive } = useHive();
  const { connectLedger } = useLedger();

  const getTransaction = (operations: Operation | Operation[]) => {
    if (isSingleOperation(operations)) {
      return createTransaction(globalProperties, [operations]);
    }

    return createTransaction(globalProperties, operations);
  };

  const getTransactionDigest = (transaction: Transaction): string => {
    return LedgerHiveApp.getTransactionDigest(transaction, environment.hive.chainId).toUpperCase();
  };

  const signTransaction = async (transaction: Transaction, path: string) => {
    const connection = await connectLedger();

    try {
      return await connection.signTransaction(path, transaction);
    } catch (error) {
      logApplicationError(error);
      translateError(error);
    } finally {
      await connection.closeDevice();
    }
  };
  const signTransactionHash = async (transaction: Transaction, path: string) => {
    const connection = await connectLedger();

    try {
      return await connection.signTransactionHash(path, transaction);
    } catch (error) {
      logApplicationError(error);
      translateError(error);
    } finally {
      await connection.closeDevice();
    }
  };

  const broadcastSignedTransaction = async (signedTransaction: SignedTransaction) => {
    try {
      return await hive.broadcast.send(signedTransaction);
    } catch (error) {
      logApplicationError(error);
      throw error;
    }
  };

  const broadcastOperation = async (operation: Operation, privateKeyString: string) => {
    try {
      const privateKey = PrivateKey.fromString(privateKeyString);
      return await hive.broadcast.sendOperations([operation], privateKey);
    } catch (error) {
      logApplicationError(error);
      throw error;
    }
  };

  const confirmTransaction = async (id: string) => {
    let transaction = null;

    do {
      await delay(300);
      transaction = await hive.transaction.findTransaction(id);
    } while (isTransactionAwaiting(transaction.status));

    if (isTransactionConfirmed(transaction.status)) {
      return;
    }

    throw new Error(TransactionError.transactionNotIncludedInBlock);
  };

  return {
    getTransaction,
    getTransactionDigest,
    signTransaction,
    signTransactionHash,
    broadcastSignedTransaction,
    broadcastOperation,
    confirmTransaction,
  };
};
