import { useContext } from 'react';
import { transactionContext } from 'context/transaction/transactionContext/transactionContext';

export const useTransactionModal = () => {
  const context = useContext(transactionContext);

  if (context === null) {
    throw new Error('The useTransactionModal hook was used outside TransactionController');
  }

  return context;
};
