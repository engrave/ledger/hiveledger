import { Operation } from 'modules/hive/operation/operation';

export type TransactionContext = {
  runOperation: (operation: Operation | Operation[]) => Promise<void>;
};
