import { createContext } from 'react';
import { TransactionContext } from 'context/transaction/transactionContext/transactionContext.types';

export const transactionContext = createContext<TransactionContext | null>(null);
