import LedgerHiveApp from '@engrave/ledger-app-hive';
import { useCallback, useState } from 'react';
import { HashedTransactionModal } from 'app/transaction/hashedTransactionModal/HashedTransactionModal';
import { TransactionModal } from 'app/transaction/transactionModal/TransactionModal';
import { ErrorCatcher } from 'context/error/errorCatcher/ErrorCatcher';
import { useTransaction } from 'context/transaction/hooks/useTransaction/useTransaction';
import { transactionContext } from 'context/transaction/transactionContext/transactionContext';
import { TransactionContext } from 'context/transaction/transactionContext/transactionContext.types';
import { Operation, Transaction } from 'modules/hive/operation/operation';
import { WrapperProps } from 'types/react';

export const TransactionController = ({ children }: WrapperProps) => {
  const { Provider } = transactionContext;
  const [transaction, setTransaction] = useState<Transaction | null>(null);
  const [isModalOpen, setModalOpen] = useState(false);
  const [resolver, setResolver] = useState<() => void | undefined>();

  const { getTransaction } = useTransaction();

  const runOperation = useCallback(
    (operations: Operation | Operation[]) => {
      return new Promise<void>((resolve) => {
        setResolver(() => resolve);

        const nextTransaction = getTransaction(operations);

        setTransaction(nextTransaction);
        setModalOpen(true);
      });
    },
    [getTransaction],
  );

  const handleClose = useCallback(() => {
    setModalOpen(false);
    setTransaction(null);
    resolver?.();
  }, [resolver]);

  const isDisplayableOnDevice = transaction && LedgerHiveApp.isDisplayableOnDevice(transaction);

  const value: TransactionContext = {
    runOperation,
  };

  return (
    <ErrorCatcher>
      <Provider value={value}>{children}</Provider>

      {transaction && isDisplayableOnDevice && (
        <TransactionModal transaction={transaction} isOpen={isModalOpen} onClose={handleClose} />
      )}
      {transaction && !isDisplayableOnDevice && (
        <HashedTransactionModal transaction={transaction} isOpen={isModalOpen} onClose={handleClose} />
      )}
    </ErrorCatcher>
  );
};
