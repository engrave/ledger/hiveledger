import { useState } from 'react';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useProposalsBudget } from 'context/proposals/hooks/useProposalsBudget/useProposalsBudget';
import { useProposalsRanking } from 'context/proposals/hooks/useProposalsRanking/useProposalsRanking';
import { useProposalVotes } from 'context/proposals/hooks/useProposalVotes/useProposalVotes';
import { proposalsContext } from 'context/proposals/proposalsContext/proposalsContext';
import { ProposalsContext } from 'context/proposals/proposalsContext/proposalsContext.types';
import { getLastFundedProposal } from 'modules/hive/proposals/getLastFundedProposal/getLastFundedProposal';
import { WrapperProps } from 'types/react';

export const ProposalsController = ({ children }: WrapperProps) => {
  const { Provider } = proposalsContext;
  const account = useActiveAccount();

  const [isVotesLoadingEnabled, setVotesLoadingEnabled] = useState(false);

  const { budget } = useProposalsBudget();
  const { proposals, page, ranking, isRankingLoading, isRankingCompleted, fetchPage } = useProposalsRanking(budget);
  const { votes, isVotesListLoading } = useProposalVotes(account, proposals, isVotesLoadingEnabled);

  const isFirstLoading = isRankingLoading && ranking.length === 0;

  const lastFundedProposal = getLastFundedProposal(ranking);

  const value: ProposalsContext = {
    budget,
    proposals,
    ranking,
    page,
    isFirstLoading,
    isRankingLoading,
    isRankingCompleted,
    lastFundedProposal,
    fetchPage,
    votes,
    isVotesListLoading,
    setVotesLoadingEnabled,
  };

  return <Provider value={value}>{children}</Provider>;
};
