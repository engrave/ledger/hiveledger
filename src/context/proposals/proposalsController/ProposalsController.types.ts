import { ProposalDetails } from 'modules/hive/proposals/proposals';

export type ProposalsResponse = {
  proposals: ProposalDetails[];
};

export type ProposalVotes = Record<number, boolean>;
