import { useCallback, useEffect, useState } from 'react';
import { useHive } from 'context/hive/hooks/useHive';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { ProposalVotesResponse } from 'context/proposals/hooks/useProposalVotes/useProposalVotes.types';
import { ProposalsVotes } from 'context/proposals/proposalsContext/proposalsContext.types';
import { Account } from 'modules/hive/account/account';
import { ProposalDetails } from 'modules/hive/proposals/proposals';

export const useProposalVotes = (account: Account, proposals: ProposalDetails[], isEnabled = true) => {
  const { hive } = useHive();
  const { start, done } = useGlobalProgressBar();

  const [votes, setVotes] = useState<ProposalsVotes>({});
  const [isVotesListLoading, setVotesListLoading] = useState(false);

  const getProposalVote = useCallback(
    async (proposalId: number, username: string) => {
      const response: ProposalVotesResponse = await hive.call('database_api', 'list_proposal_votes', {
        start: [proposalId, username],
        limit: 1,
        order: 'by_proposal_voter',
        order_direction: 'ascending',
        status: 'all',
      });

      return response.proposal_votes[0].voter === username;
    },
    [hive],
  );

  const updateVotes = useCallback(async () => {
    if (isVotesListLoading) {
      return;
    }

    start();
    setVotesListLoading(true);

    let nextVotes: ProposalsVotes = {};

    for (const proposal of proposals) {
      const proposalId = proposal.proposal_id;

      if (votes[proposalId] === undefined) {
        // we do it one by one to not break API limits
        const isVoted = await getProposalVote(proposal.proposal_id, account.username);

        nextVotes = { ...nextVotes, [proposalId]: isVoted };
      }
    }

    if (Object.keys(nextVotes).length > 0) {
      setVotes((currentVotes) => ({ ...currentVotes, ...nextVotes }));
    }

    setVotesListLoading(false);
    done();
  }, [account.username, done, getProposalVote, isVotesListLoading, proposals, start, votes]);

  useEffect(() => {
    if (isEnabled) {
      updateVotes();
    }
  }, [updateVotes, isEnabled]);

  useEffect(() => {
    setVotes({});
  }, [account.details]);

  return { votes, isVotesListLoading };
};
