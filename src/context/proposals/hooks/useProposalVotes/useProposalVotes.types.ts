export type ProposalVote = {
  voter: string;
};

export type ProposalVotesResponse = {
  proposal_votes: ProposalVote[];
};
