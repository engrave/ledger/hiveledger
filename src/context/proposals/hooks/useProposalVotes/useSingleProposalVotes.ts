import { useMemo } from 'react';
import { useProposalVotes } from 'context/proposals/hooks/useProposalVotes/useProposalVotes';
import { Account } from 'modules/hive/account/account';
import { ProposalDetails } from 'modules/hive/proposals/proposals';

export const useSingleProposalVotes = (account: Account, proposal: ProposalDetails) => {
  const stableProposalsArray = useMemo(() => {
    return [proposal];
  }, [proposal]);

  const { votes, isVotesListLoading } = useProposalVotes(account, stableProposalsArray);

  return {
    isVoted: votes[proposal.proposal_id] ?? null,
    isVoteLoading: isVotesListLoading,
  };
};
