import { useContext } from 'react';
import { proposalsContext } from 'context/proposals/proposalsContext/proposalsContext';

export const useProposals = () => {
  const context = useContext(proposalsContext);

  if (context === null) {
    throw new Error('The useProposals hooks should be used inside ProposalsController');
  }

  return context;
};
