import { useCallback, useEffect, useState } from 'react';
import { proposalsConfig } from 'config/config';
import { useHive } from 'context/hive/hooks/useHive';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { ProposalsBudget } from 'context/proposals/proposalsContext/proposalsContext.types';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { divideAsset } from 'modules/hive/asset/divideAsset/divideAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export const useProposalsBudget = () => {
  const { start, done } = useGlobalProgressBar();
  const { getAccountDetails } = useHive();

  const [budget, setBudget] = useState<ProposalsBudget | null>(null);
  const [isBudgetLoading, setIsBudgetLoading] = useState(false);

  const loadProposalsBudget = useCallback(async () => {
    start();
    setIsBudgetLoading(true);

    const fundAccountDetails = await getAccountDetails(proposalsConfig.proposalsFundAccount);
    const totalBudget = createAsset(fundAccountDetails.hbd_balance, ChainAssetSymbol.hbd);

    setBudget({
      total: totalBudget,
      daily: divideAsset(totalBudget, 100),
    });

    setIsBudgetLoading(false);
    done();
  }, [done, getAccountDetails, start]);

  useEffect(() => {
    loadProposalsBudget();
  }, [loadProposalsBudget]);

  return { budget, loadProposalsBudget, isBudgetLoading };
};
