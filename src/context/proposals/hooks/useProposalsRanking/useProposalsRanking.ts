import { useCallback, useEffect, useState } from 'react';
import { proposalsConfig } from 'config/config';
import { useHive } from 'context/hive/hooks/useHive';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { ProposalsBudget } from 'context/proposals/proposalsContext/proposalsContext.types';
import { ProposalsResponse } from 'context/proposals/proposalsController/ProposalsController.types';
import { getProposalsRanking } from 'modules/hive/proposals/getProposalsRanking/getProposalsRanking';
import { ProposalDetails, ProposalsRanking } from 'modules/hive/proposals/proposals';

export const useProposalsRanking = (budget: ProposalsBudget | null) => {
  const { hive } = useHive();
  const { start, done } = useGlobalProgressBar();

  const [proposals, setProposals] = useState<ProposalDetails[]>([]);
  const [ranking, setRanking] = useState<ProposalsRanking>([]);
  const [page, setPage] = useState(0);

  const [isRankingLoading, setRankingLoading] = useState(false);
  const [isRankingCompleted, setRankingComplete] = useState(false);

  const fetchPage = useCallback(
    async (reset = false) => {
      if (budget === null) {
        return;
      }

      const currentProposals = reset ? [] : proposals;

      start();
      setRankingLoading(true);

      const lastProposal = currentProposals.at(-1) ?? null;
      const perPage = currentProposals.length > 0 ? proposalsConfig.perPage + 1 : proposalsConfig.perPage;

      const response: ProposalsResponse = await hive.call('database_api', 'list_proposals', {
        start: lastProposal === null ? [-1] : [lastProposal.total_votes, lastProposal.proposal_id],
        limit: perPage,
        order: 'by_total_votes',
        order_direction: 'descending',
        status: 'votable',
      });

      const moreProposals = currentProposals.length > 0 ? response.proposals.slice(1) : response.proposals;
      const nextProposals = [...currentProposals, ...moreProposals];

      setProposals(nextProposals);
      setRanking(getProposalsRanking(nextProposals, budget.daily));

      setRankingLoading(false);
      done();

      setPage((currentPage) => (reset ? 1 : currentPage + 1));

      if (response.proposals.length < perPage) {
        setRankingComplete(true);
      }
    },
    [budget, done, hive, proposals, start],
  );

  useEffect(() => {
    if (budget !== null) {
      fetchPage(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [budget]);

  return { proposals, ranking, page, fetchPage, isRankingLoading, isRankingCompleted };
};
