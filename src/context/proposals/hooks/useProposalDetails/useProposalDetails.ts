import { useCallback, useEffect, useState } from 'react';
import { useHive } from 'context/hive/hooks/useHive';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { useProposals } from 'context/proposals/hooks/useProposals/useProposals';
import { Post } from 'modules/hive/post/post';
import { ProposalDetails, ProposalError } from 'modules/hive/proposals/proposals';

export const useProposalDetails = (proposalId: number) => {
  const { start, done } = useGlobalProgressBar();
  const { hive } = useHive();

  const { proposals } = useProposals();
  const [error, setError] = useState<null | ProposalError>(null);

  const [proposal, setProposal] = useState<ProposalDetails | null>(() => {
    return proposals.find((item) => item.proposal_id === proposalId) ?? null;
  });
  const [isProposalLoading, setProposalLoading] = useState(false);

  const [proposalPost, setProposalPost] = useState<Post | null>(null);
  const [isProposalPostLoading, setProposalPostLoading] = useState(false);

  const fetchProposal = useCallback(async () => {
    if (proposal === null && !isProposalLoading) {
      start();
      setProposalLoading(true);
      setProposalPost(null);

      const response: ProposalDetails[] = await hive.database.call('find_proposals', [[proposalId]]);

      if (response.length === 0) {
        setError(ProposalError.notFound);
        return;
      }

      setProposal(response[0]);

      done();
      setProposalLoading(false);
    }
  }, [done, hive.database, isProposalLoading, proposal, proposalId, start]);

  const fetchProposalPost = useCallback(
    async (proposalDetails: ProposalDetails) => {
      start();
      setProposalPostLoading(true);

      const post: Post = await hive.call('bridge', 'get_post', {
        author: proposalDetails.creator,
        permlink: proposalDetails.permlink,
      });

      setProposalPost(post);

      setProposalPostLoading(false);
      done();
    },
    [done, hive, start],
  );

  useEffect(() => {
    fetchProposal();
  }, [fetchProposal]);

  useEffect(() => {
    setProposal(null);
  }, [proposalId]);

  useEffect(() => {
    if (proposal !== null) {
      fetchProposalPost(proposal);
    }
  }, [fetchProposalPost, proposal]);

  return {
    proposal,
    proposalPost,
    isLoading: isProposalLoading || isProposalPostLoading,
    error,
  };
};
