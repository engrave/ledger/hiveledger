import { ProposalVote } from 'modules/hive/proposals/proposals';

export type ProposalVotesResponse = {
  proposal_votes: ProposalVote[];
};
