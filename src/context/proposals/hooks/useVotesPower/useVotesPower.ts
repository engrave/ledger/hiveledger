import { useCallback, useEffect, useState } from 'react';
import { proposalsConfig } from 'config/config';
import { useHive } from 'context/hive/hooks/useHive';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { ProposalVotesResponse } from 'context/proposals/hooks/useVotesPower/useVotesPower.types';
import { AccountDetails } from 'modules/hive/account/account';
import { getVotePower } from 'modules/hive/hivePower/getVotePower/getVotePower';
import { VotePower } from 'modules/hive/hivePower/hivePower';
import { ProposalDetails } from 'modules/hive/proposals/proposals';

export const useVotesPower = (proposal: ProposalDetails) => {
  const { hive } = useHive();
  const { start, done } = useGlobalProgressBar();

  const [votesPower, setVotesPower] = useState<VotePower[]>([]);

  const [isVotesPowerLoading, setIsVotesPowerLoading] = useState(false);
  const [isVotesPowerCompleted, setIsVotesPowerCompleted] = useState(false);

  const fetchMoreVotesPower = useCallback(async () => {
    start();
    setIsVotesPowerLoading(true);

    const lastVotePower = votesPower.length > 0 ? votesPower.at(-1) : null;
    const votersPerPage = votesPower.length > 0 ? proposalsConfig.votersPerPage + 1 : proposalsConfig.votersPerPage;

    const votesResponse: ProposalVotesResponse = await hive.call('database_api', 'list_proposal_votes', {
      start: [proposal.proposal_id, lastVotePower?.username ?? ''],
      limit: votersPerPage,
      order: 'by_proposal_voter',
      order_direction: 'ascending',
      status: 'all',
    });

    const proposalVotes = votesResponse.proposal_votes.filter((proposalVote) => {
      return proposalVote.proposal.proposal_id === proposal.proposal_id;
    });

    const proposalVotesPage = votesPower.length > 0 ? proposalVotes.slice(1) : proposalVotes;

    const votersNames = proposalVotesPage.map((vote) => vote.voter);

    const votersAccounts: AccountDetails[] = await hive.call('condenser_api', 'get_accounts', [votersNames]);

    const moreVotesPower = votersAccounts.map((account) => getVotePower(account));

    setVotesPower((currentVotesPower) => [...currentVotesPower, ...moreVotesPower]);

    if (proposalVotes.length < votersPerPage) {
      setIsVotesPowerCompleted(true);
    }

    done();
    setIsVotesPowerLoading(false);
  }, [done, hive, proposal.proposal_id, start, votesPower]);

  useEffect(() => {
    if (votesPower.length === 0 && !isVotesPowerLoading) {
      fetchMoreVotesPower();
    }
  }, [fetchMoreVotesPower, isVotesPowerLoading, votesPower.length]);

  useEffect(() => {
    setVotesPower([]);
  }, [proposal]);

  return { votesPower, isVotesPowerLoading, isVotesPowerCompleted, fetchMoreVotesPower };
};
