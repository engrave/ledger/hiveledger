import { createContext } from 'react';
import { ProposalsContext } from 'context/proposals/proposalsContext/proposalsContext.types';

export const proposalsContext = createContext<ProposalsContext | null>(null);
