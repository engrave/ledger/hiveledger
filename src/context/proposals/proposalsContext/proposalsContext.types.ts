import { HbdAsset } from 'modules/hive/asset/asset';
import { ProposalDetails, ProposalsRanking, Proposal } from 'modules/hive/proposals/proposals';

export type ProposalsVotes = Record<string, boolean>;

export type ProposalsBudget = {
  total: HbdAsset;
  daily: HbdAsset;
};

export type ProposalsContext = {
  budget: ProposalsBudget | null;
  proposals: ProposalDetails[];
  ranking: ProposalsRanking;
  page: number;
  lastFundedProposal: Proposal | null;
  fetchPage: (reset?: boolean) => Promise<void>;
  isRankingLoading: boolean;
  isFirstLoading: boolean;
  isRankingCompleted: boolean;
  votes: ProposalsVotes;
  isVotesListLoading: boolean;
  setVotesLoadingEnabled: (enable: boolean) => void;
};
