import {
  AddAccountAction,
  AuthActionType,
  ResetAuthorizationAction,
  SetActiveAccountAction,
  UpdateAccountAction,
} from 'context/auth/authReducer/authReducer.types';
import { Account } from 'modules/hive/account/account';

export const authorizeAccount = (account: Account): AddAccountAction => ({
  type: AuthActionType.addAccount,
  payload: account,
});

export const activateAccount = (account: Account): SetActiveAccountAction => ({
  type: AuthActionType.setActiveAccount,
  payload: account,
});

export const updateAccount = (account: Account): UpdateAccountAction => ({
  type: AuthActionType.updateAccount,
  payload: account,
});

export const resetAuthorization = (): ResetAuthorizationAction => ({
  type: AuthActionType.resetAuthorization,
});
