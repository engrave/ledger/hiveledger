import { Account } from 'modules/hive/account/account';

export type AuthState = {
  isFullyAuthorized: boolean;
  authorizedAccounts: Account[];
  activeAccount: Account | null;
};

export enum AuthActionType {
  addAccount = 'addAccount',
  setActiveAccount = 'setActiveAccount',
  updateAccount = 'updateAccount',
  resetAuthorization = 'resetAuthorization',
}

export type AddAccountAction = {
  type: AuthActionType.addAccount;
  payload: Account;
};

export type SetActiveAccountAction = {
  type: AuthActionType.setActiveAccount;
  payload: Account;
};

export type UpdateAccountAction = {
  type: AuthActionType.updateAccount;
  payload: Account;
};

export type ResetAuthorizationAction = {
  type: AuthActionType.resetAuthorization;
};

export type AuthAction = AddAccountAction | SetActiveAccountAction | ResetAuthorizationAction | UpdateAccountAction;
