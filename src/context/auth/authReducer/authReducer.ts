import { Reducer } from 'react';
import { AuthAction, AuthActionType, AuthState } from 'context/auth/authReducer/authReducer.types';
import { isSame } from 'modules/hive/account/isSame/isSame';

export const authReducer: Reducer<AuthState, AuthAction> = (state, action) => {
  if (action.type === AuthActionType.addAccount) {
    return {
      ...state,
      isFullyAuthorized: true,
      authorizedAccounts: [...state.authorizedAccounts, action.payload],
      activeAccount: state.activeAccount ?? action.payload,
    };
  }
  if (action.type === AuthActionType.setActiveAccount) {
    return {
      ...state,
      activeAccount: action.payload,
    };
  }
  if (action.type === AuthActionType.resetAuthorization) {
    return {
      ...state,
      isFullyAuthorized: false,
      authorizedAccounts: [],
      activeAccount: null,
    };
  }
  if (action.type === AuthActionType.updateAccount) {
    const newAccounts = state.authorizedAccounts.map((account) => {
      return isSame(account, action.payload) ? action.payload : account;
    });

    return {
      ...state,
      authorizedAccounts: newAccounts,
      activeAccount: isSame(state.activeAccount, action.payload) ? action.payload : state.activeAccount,
    };
  }

  return state;
};

export const authReducerDefaultState: AuthState = {
  isFullyAuthorized: false,
  authorizedAccounts: [],
  activeAccount: null,
};
