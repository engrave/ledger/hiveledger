import { useCallback } from 'react';
import { DeviceAccount } from 'context/association/associationContext/associationContext.types';
import { useHive } from 'context/hive/hooks/useHive';
import { useLedger } from 'context/ledger/hooks/useLedger/useLedger';
import { isLedgerInternalStatusError } from 'context/ledger/ledgerController/LedgerController.types';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { AssociationError } from 'modules/hive/accountAssociation/accountAssociation';
import { findUnusedAccountIndex } from 'modules/hive/accountAssociation/findUnusedAccountIndex/findUnusedAccountIndex';
import { AccountUsageChecker } from 'modules/hive/accountAssociation/findUnusedAccountIndex/findUnusedAccountIndex.types';
import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { makePath } from 'modules/hive/accountDiscovery/makePath/makePath';
import { logApplicationError } from 'utils/debug/logApplicationError';

export const useUnusedAccount = () => {
  const { connectLedger } = useLedger();
  const { hive } = useHive();
  const { start, done } = useGlobalProgressBar();

  return useCallback(async (): Promise<DeviceAccount> => {
    start();
    const { getPublicKey, getAccountKey, closeDevice } = await connectLedger();

    const checkAccountUsage: AccountUsageChecker = async (paths) => {
      const foundAccounts: string[] = [];

      for (let index = 0; index < paths.length; index += 1) {
        const publicKey = await getPublicKey(paths[index]);
        const { accounts } = await hive.keys.getKeyReferences([publicKey]);

        foundAccounts.push(...accounts[0]);
      }

      return foundAccounts.length > 0;
    };

    try {
      const accountIndex = await findUnusedAccountIndex(checkAccountUsage);

      const ownerKey = await getAccountKey(makePath(SlipRole.owner, accountIndex));
      const activeKey = await getAccountKey(makePath(SlipRole.active, accountIndex));
      const postingKey = await getAccountKey(makePath(SlipRole.posting, accountIndex));

      return { ownerKey, activeKey, postingKey, accountIndex };
    } catch (error) {
      logApplicationError(error);

      if (isLedgerInternalStatusError(error)) {
        throw new Error(AssociationError.deviceError);
      }

      throw new Error(AssociationError.genericError);
    } finally {
      await closeDevice();
      done();
    }
  }, [connectLedger, done, hive.keys, start]);
};
