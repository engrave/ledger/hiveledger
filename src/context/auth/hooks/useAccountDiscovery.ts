import { groupBy } from 'ramda';
import { useCallback } from 'react';
import { useAccountBuilder } from 'context/auth/hooks/useAccountBuilder';
import { useHive } from 'context/hive/hooks/useHive';
import { PublicKeyGetter } from 'context/ledger/ledgerContext/ledgerContext.types';
import { Account, AccountPublicKey } from 'modules/hive/account/account';
import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { discoverAccounts } from 'modules/hive/accountDiscovery/discoverAccounts/discoverAccounts';
import { DiscoveredAccount } from 'modules/hive/accountDiscovery/discoverAccounts/discoverAccounts.types';

export const useAccountDiscovery = () => {
  const { hive } = useHive();
  const getAccount = useAccountBuilder();

  const getStoredAccounts = useCallback(
    async (getPublicKey: PublicKeyGetter): Promise<DiscoveredAccount[]> => {
      return discoverAccounts(SlipRole.owner, async (path: string) => {
        const publicKey = await getPublicKey(path);
        const { accounts } = await hive.keys.getKeyReferences([publicKey]);

        return { publicKey, usernames: accounts[0] };
      });
    },
    [hive.keys],
  );

  const getAccountsData = useCallback(
    async (discoveredAccount: DiscoveredAccount[]): Promise<Account[]> => {
      const groupedAccounts = groupBy((account) => account.username, discoveredAccount);
      const usernames = Object.keys(groupedAccounts);

      const accounts: Account[] = [];

      for (const username of usernames) {
        const publicKeys = groupedAccounts[username].map<AccountPublicKey>(({ path, publicKey }) => {
          return { path, publicKey };
        });

        const account = await getAccount(username, publicKeys);

        accounts.push(account);
      }

      return accounts;
    },
    [getAccount],
  );

  return { getAccountsData, getStoredAccounts };
};
