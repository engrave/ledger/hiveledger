import { useContext } from 'react';
import { authContext } from 'context/auth/authContext/authContext';

export const useAuth = () => {
  const context = useContext(authContext);

  if (context === null) {
    throw new Error('AuthController is missing!');
  }

  return context;
};
