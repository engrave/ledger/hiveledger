import { useAuth } from 'context/auth/hooks/useAuth';

export const useActiveAccount = () => {
  const { activeAccount } = useAuth();

  if (activeAccount === null) {
    throw new Error('No account is activated!');
  }

  return activeAccount;
};
