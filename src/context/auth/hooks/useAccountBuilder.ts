import { useCallback } from 'react';
import { useVestingDelegations } from 'context/hive/hooks/useDelegations/useVestingDelegations';
import { useHive } from 'context/hive/hooks/useHive';
import { useResourceCredits } from 'context/hive/hooks/useResourceCredits/useResourceCredits';
import { Account, PublicKeysCollection } from 'modules/hive/account/account';
import { getAuthorities } from 'modules/hive/account/getAuthorities/getAuthorities';
import { getAccountWallet } from 'modules/hive/wallet/getAccountWallet/getAccountWallet';

export const useAccountBuilder = () => {
  const { getAccountDetails } = useHive();
  const { fetchVestingDelegations } = useVestingDelegations();
  const { fetchResourceCreditsData, fetchResourceCreditsDelegations } = useResourceCredits();

  return useCallback(
    async (username: string, publicKeys: PublicKeysCollection = []): Promise<Account> => {
      const [details, vestingDelegations, resourceCredits, resourceCreditsDelegations] = await Promise.all([
        getAccountDetails(username),
        fetchVestingDelegations(username),
        fetchResourceCreditsData(username),
        fetchResourceCreditsDelegations(username),
      ]);

      const wallet = getAccountWallet(details);
      const authorities = getAuthorities(details);

      return {
        username,
        wallet,
        details,
        vestingDelegations,
        resourceCredits,
        resourceCreditsDelegations,
        publicKeys,
        authorities,
      };
    },
    [fetchResourceCreditsData, fetchResourceCreditsDelegations, fetchVestingDelegations, getAccountDetails],
  );
};
