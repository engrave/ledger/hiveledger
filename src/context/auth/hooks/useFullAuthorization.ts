import { useCallback } from 'react';
import { AuthorizationError } from 'context/auth/authController/AuthController.types';
import { useAccountDiscovery } from 'context/auth/hooks/useAccountDiscovery';
import { useLedger } from 'context/ledger/hooks/useLedger/useLedger';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { Account } from 'modules/hive/account/account';
import { logApplicationError } from 'utils/debug/logApplicationError';

export const useFullAuthorization = () => {
  const progress = useGlobalProgressBar();
  const { connectLedger } = useLedger();
  const { getStoredAccounts, getAccountsData } = useAccountDiscovery();

  return useCallback(async (): Promise<Account[]> => {
    let discoveredAccounts = [];
    const { closeDevice, getPublicKey } = await connectLedger();

    progress.start();

    try {
      discoveredAccounts = await getStoredAccounts(getPublicKey);
      await closeDevice();
    } catch (error) {
      logApplicationError(error);
      progress.done();

      if (error instanceof Error && (error.name === 'TransportStatusError' || error.name === 'LockedDeviceError')) {
        throw new Error(AuthorizationError.deviceError);
      }
      throw new Error(AuthorizationError.genericError);
    }

    if (discoveredAccounts.length === 0) {
      progress.done();
      throw new Error(AuthorizationError.noAccountFound);
    }

    const accounts = getAccountsData(discoveredAccounts);

    progress.done();

    return accounts;
  }, [connectLedger, progress, getAccountsData, getStoredAccounts]);
};
