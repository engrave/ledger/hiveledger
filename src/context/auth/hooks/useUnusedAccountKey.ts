import { useCallback } from 'react';
import { useHive } from 'context/hive/hooks/useHive';
import { useLedger } from 'context/ledger/hooks/useLedger/useLedger';
import { isLedgerInternalStatusError } from 'context/ledger/ledgerController/LedgerController.types';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { AccountPublicKey } from 'modules/hive/account/account';
import { AssociationError } from 'modules/hive/accountAssociation/accountAssociation';
import { findUnusedKeyIndex } from 'modules/hive/accountAssociation/findUnusedKeyIndex/findUnusedKeyIndex';
import { AccountKeyUsageChecker } from 'modules/hive/accountAssociation/findUnusedKeyIndex/findUnusedKeyIndex.types';
import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { makePath } from 'modules/hive/accountDiscovery/makePath/makePath';
import { logApplicationError } from 'utils/debug/logApplicationError';

export const useUnusedAccountKey = () => {
  const { connectLedger } = useLedger();
  const { hive } = useHive();
  const { start, done } = useGlobalProgressBar();

  return useCallback(
    async (role: SlipRole, accountIndex: number, bannedKeys: string[] = []): Promise<AccountPublicKey> => {
      start();
      const { getPublicKey, getAccountKey, closeDevice } = await connectLedger();

      const checkAccountKeyUsage: AccountKeyUsageChecker = async (path: string) => {
        const publicKey = await getPublicKey(path);

        if (bannedKeys.includes(publicKey)) {
          return true;
        }

        const { accounts } = await hive.keys.getKeyReferences([publicKey]);

        return accounts[0].length > 0;
      };

      try {
        const keyIndex = await findUnusedKeyIndex(checkAccountKeyUsage, { accountIndex, role });

        if (keyIndex === null) {
          throw new Error(AssociationError.noUnusedKeyFound);
        }

        const path = makePath(role, accountIndex, keyIndex);

        return await getAccountKey(path);
      } catch (error) {
        logApplicationError(error);

        if (isLedgerInternalStatusError(error)) {
          throw new Error(AssociationError.deviceError);
        }

        throw new Error(AssociationError.genericError);
      } finally {
        await closeDevice();
        done();
      }
    },
    [connectLedger, done, hive.keys, start],
  );
};
