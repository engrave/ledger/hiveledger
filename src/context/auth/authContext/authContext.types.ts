import { AuthorizationResult } from 'context/auth/authController/AuthController.types';
import { AuthState } from 'context/auth/authReducer/authReducer.types';
import { Account } from 'modules/hive/account/account';

export type AuthContext = AuthState & {
  fullyAuthorize: () => Promise<AuthorizationResult>;
  authorizeAccount: (account: Account) => void;
  activateAccount: (username: string) => void;
  refreshAccounts: () => Promise<void>;
  logout: () => void;
};
