import { createContext } from 'react';
import { AuthContext } from 'context/auth/authContext/authContext.types';

export const authContext = createContext<AuthContext | null>(null);
