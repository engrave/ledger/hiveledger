import { useCallback, useReducer } from 'react';
import { authContext } from 'context/auth/authContext/authContext';
import { AuthContext } from 'context/auth/authContext/authContext.types';
import { AuthorizationError, AuthorizationResult } from 'context/auth/authController/AuthController.types';
import {
  authorizeAccount,
  resetAuthorization,
  activateAccount,
  updateAccount,
} from 'context/auth/authReducer/authActions';
import { authReducer, authReducerDefaultState } from 'context/auth/authReducer/authReducer';
import { useAccountBuilder } from 'context/auth/hooks/useAccountBuilder';
import { useFullAuthorization } from 'context/auth/hooks/useFullAuthorization';
import { useLedger } from 'context/ledger/hooks/useLedger/useLedger';
import { Account } from 'modules/hive/account/account';
import { WrapperProps } from 'types/react';

export const AuthController = ({ children }: WrapperProps) => {
  const { Provider } = authContext;
  const { removeKeysCache } = useLedger();

  const [authState, dispatch] = useReducer(authReducer, authReducerDefaultState);

  const doFullAuthorization = useFullAuthorization();
  const fetchAccount = useAccountBuilder();

  const handleAccountAuthorization = useCallback((account: Account, activate = false) => {
    dispatch(authorizeAccount(account));

    if (activate) {
      dispatch(activateAccount(account));
    }
  }, []);

  const handleAccountActivation = useCallback(
    (username: string) => {
      const account = authState.authorizedAccounts.find((item) => {
        return item.username === username;
      });

      if (!account) {
        throw new Error(AuthorizationError.accountNotAuthorized);
      }

      dispatch(activateAccount(account));
    },
    [authState.authorizedAccounts],
  );

  const handleFullAuthorization = useCallback(async (): Promise<AuthorizationResult> => {
    const accounts = await doFullAuthorization();

    accounts.forEach((account) => {
      dispatch(authorizeAccount(account));
    });

    dispatch(activateAccount(accounts[0]));

    return { accounts, active: accounts[0] };
  }, [doFullAuthorization]);

  const handleAccountRefresh = useCallback(async () => {
    const accountsPromises = authState.authorizedAccounts.map((account) => {
      return fetchAccount(account.username, account.publicKeys);
    });

    const newAccounts = await Promise.all(accountsPromises);

    newAccounts.forEach((account) => {
      dispatch(updateAccount(account));
    });
  }, [authState, fetchAccount]);

  const handleLogout = useCallback(() => {
    removeKeysCache();
    dispatch(resetAuthorization());
  }, [removeKeysCache]);

  const value: AuthContext = {
    ...authState,
    fullyAuthorize: handleFullAuthorization,
    authorizeAccount: handleAccountAuthorization,
    activateAccount: handleAccountActivation,
    refreshAccounts: handleAccountRefresh,
    logout: handleLogout,
  };

  return <Provider value={value}>{children}</Provider>;
};
