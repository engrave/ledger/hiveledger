import { Account } from 'modules/hive/account/account';

export enum AuthorizationError {
  noAccountFound = 'AuthorizationError/NoAccountFound',
  deviceError = 'AuthorizationError/DeviceError',
  accountNotAuthorized = 'AuthorizationError/AccountNotAuthorized',
  genericError = 'AuthorizationError/GenericError',
}

export type AuthorizationResult = {
  accounts: Account[];
  active: Account;
};
