import { useCallback, useState } from 'react';
import { associationContext, AssociationStep } from 'context/association/associationContext/associationContext';
import {
  AssociationContext,
  TargetAccount,
  DeviceAccount,
} from 'context/association/associationContext/associationContext.types';
import { useAccountBuilder } from 'context/auth/hooks/useAccountBuilder';
import { AssociationError } from 'modules/hive/accountAssociation/accountAssociation';
import { WrapperProps } from 'types/react';

export const AssociationController = ({ children }: WrapperProps) => {
  const { Provider } = associationContext;
  const [currentStep, setCurrentStep] = useState<AssociationStep>(AssociationStep.updateAccount);
  const [deviceAccount, setDeviceAccount] = useState<DeviceAccount | null>(null);
  const [targetAccount, setTargetAccount] = useState<TargetAccount | null>(null);

  const fetchAccount = useAccountBuilder();

  const handleStepChange = useCallback((step: AssociationStep) => {
    setCurrentStep(step);
  }, []);

  const handleDeviceAccountChange = useCallback((account: DeviceAccount) => {
    setDeviceAccount(account);
  }, []);

  const handleTargetAccountChange = useCallback((account: TargetAccount) => {
    setTargetAccount(account);
  }, []);

  const handleAssociationCleaUp = useCallback(() => {
    setTargetAccount(null);
    setDeviceAccount(null);
  }, []);

  const getAccountFromAssociation = useCallback(async () => {
    if (targetAccount === null || deviceAccount === null) {
      throw new Error(AssociationError.creatingAccountFailed);
    }

    return fetchAccount(targetAccount.accountDetails.name, [deviceAccount.ownerKey]);
  }, [deviceAccount, fetchAccount, targetAccount]);

  const value: AssociationContext = {
    currentStep,
    setCurrentStep: handleStepChange,
    deviceAccount,
    setDeviceAccount: handleDeviceAccountChange,
    targetAccount,
    setTargetAccount: handleTargetAccountChange,
    cleanUpAssociation: handleAssociationCleaUp,
    getAccountFromAssociation,
  };

  return <Provider value={value}>{children}</Provider>;
};
