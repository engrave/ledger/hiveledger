import { useContext } from 'react';
import { associationContext } from 'context/association/associationContext/associationContext';

export const useAssociation = () => {
  const context = useContext(associationContext);

  if (context === null) {
    throw new Error('Used useAssociation hook without AssociationController');
  }

  return context;
};
