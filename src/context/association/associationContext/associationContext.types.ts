import { AssociationStep } from 'context/association/associationContext/associationContext';
import { Account, AccountDetails, AccountPublicKey } from 'modules/hive/account/account';
import { Keychain } from 'modules/hive/accountAssociation/accountAssociation';

export type TargetAccount = {
  privateKey: string;
  accountDetails: AccountDetails;
};

export type DeviceAccount = Keychain<AccountPublicKey> & {
  accountIndex: number;
};

export type AssociationContext = {
  currentStep: AssociationStep;
  setCurrentStep: (step: AssociationStep) => void;
  deviceAccount: DeviceAccount | null;
  setDeviceAccount: (account: DeviceAccount) => void;
  targetAccount: TargetAccount | null;
  setTargetAccount: (account: TargetAccount) => void;
  cleanUpAssociation: () => void;
  getAccountFromAssociation: () => Promise<Account>;
};
