import { createContext } from 'react';
import { AssociationContext } from './associationContext.types';

export enum AssociationStep {
  requestDeviceKey = 'request_device_key',
  addPrivateKey = 'add_private_key',
  updateAccount = 'update_account',
}

export const associationContext = createContext<AssociationContext | null>(null);
