import { formatDistanceStrict } from 'date-fns';
import { FormatNumberOptions, IntlShape } from 'react-intl';
import { Locale } from 'i18n';
import { Asset } from 'modules/hive/asset/asset';

export type FormattingRules = {
  decimalSeparator: string;
  thousandSeparator: string;
  numberOfDecimals: number;
};

type FormatDistanceOptions = Parameters<typeof formatDistanceStrict>['2'];

export type LocalizationContext = IntlShape & {
  changeLocale: (locale: Locale) => void;
  formatAssetAmount: (amount: Asset, option?: FormatNumberOptions) => string;
  formatAsset: (amount: Asset, option?: FormatNumberOptions) => string;
  formattingRules: FormattingRules;
  formatAdaptableRelativeTime: (target: Date, base?: Date, options?: FormatDistanceOptions) => string;
};
