import { createContext } from 'react';
import { LocalizationContext } from './localizationContext.types';

export const localizationContext = createContext<LocalizationContext | null>(null);
