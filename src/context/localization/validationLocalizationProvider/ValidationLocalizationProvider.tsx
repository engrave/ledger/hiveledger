import { setLocale } from 'yup';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { WrapperProps } from 'types/react';

export const ValidationLocalizationProvider = ({ children }: WrapperProps) => {
  const translate = useTranslator();

  setLocale({
    mixed: {
      required: ({ label }) => translate('validation.mixed.required', { label }),
    },
    number: {
      positive: ({ label }) => translate('validation.number.positive', { label }),
    },
  });

  return <>{children}</>;
};
