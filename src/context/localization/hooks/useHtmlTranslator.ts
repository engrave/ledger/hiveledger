import reactHtmlParser from 'html-react-parser';
import { createElement, Fragment, useCallback } from 'react';
import { TranslationKey, TranslationValues } from 'context/localization/dictionaries/dictionaries';
import { useTranslator } from 'context/localization/hooks/useTranslator';

export type HtmlTranslator = (key: TranslationKey, values?: TranslationValues) => string | JSX.Element | JSX.Element[];

export const useHtmlTranslator = (): HtmlTranslator => {
  const translate = useTranslator();

  return useCallback(
    (key, values = {}) => {
      return createElement(Fragment, {}, reactHtmlParser(translate(key, values)));
    },
    [translate],
  );
};
