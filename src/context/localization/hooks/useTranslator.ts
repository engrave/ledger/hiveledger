import { useCallback } from 'react';
import { TranslationKey, TranslationValues } from 'context/localization/dictionaries/dictionaries';
import { useLocalization } from 'context/localization/hooks/useLocalization';

export type Translator = (key: TranslationKey, values?: TranslationValues) => string;

export const useTranslator = (): Translator => {
  const { formatMessage } = useLocalization();

  return useCallback(
    (key, values = {}) => {
      return formatMessage({ id: key, defaultMessage: key }, values);
    },
    [formatMessage],
  );
};
