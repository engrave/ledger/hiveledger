import { useContext } from 'react';
import { localizationContext } from 'context/localization/localizationContext/localizationContext';

export const useLocalization = () => {
  const localization = useContext(localizationContext);

  if (!localization) {
    throw new Error('The useLocalization hook was used but the LocalizationContext is not defined');
  }

  return localization;
};
