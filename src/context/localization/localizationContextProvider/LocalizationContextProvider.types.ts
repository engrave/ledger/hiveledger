import { ReactNode } from 'react';
import { Locale } from 'i18n';

export type LocalizationContextProviderProps = {
  children: ReactNode;
  onLocaleChange: (locale: Locale) => void;
};
