import { formatDistanceStrict } from 'date-fns';
import { useMemo } from 'react';
import { FormatNumberOptions, useIntl } from 'react-intl';
import { Locale } from 'i18n';
import { Asset } from 'modules/hive/asset/asset';
import { appendSymbol } from 'modules/hive/assetSymbol/appendSymbol/appendSymbol';
import { getPrecision } from 'modules/hive/assetSymbol/getPrecision/getPrecision';
import { getDateFnsLocale } from 'utils/date/getDateFnsLocale/getDateFnsLocale';
import { localizationContext } from '../localizationContext/localizationContext';
import { FormattingRules, LocalizationContext } from '../localizationContext/localizationContext.types';
import { LocalizationContextProviderProps } from './LocalizationContextProvider.types';

export const LocalizationContextProvider = (props: LocalizationContextProviderProps) => {
  const { children, onLocaleChange } = props;

  const intl = useIntl();
  const { Provider } = localizationContext;

  const formattingRules: FormattingRules = useMemo(
    () => ({
      decimalSeparator: intl.formatNumber(12345.6789).match(/345(.*)67/)?.[1] || '.',
      thousandSeparator: intl.formatNumber(12345.6789).match(/12(.*)345/)?.[1] || ',',
      numberOfDecimals: intl.formatNumber(12345.6789).match(/345(\D*)(\d+)$/)?.[2].length || 2,
    }),
    [intl],
  );

  const formatAssetAmount = (asset: Asset, options: FormatNumberOptions = {}) => {
    return intl.formatNumber(asset.amount, {
      minimumFractionDigits: getPrecision(asset.assetSymbol),
      maximumFractionDigits: getPrecision(asset.assetSymbol),
      ...options,
    });
  };

  const formatAsset = (asset: Asset, options: FormatNumberOptions = {}) => {
    return appendSymbol(formatAssetAmount(asset, options), asset.assetSymbol);
  };

  const formatAdaptableRelativeTime = (target: Date, base: Date = new Date(), options = {}) => {
    return formatDistanceStrict(target, base, {
      addSuffix: true,
      locale: getDateFnsLocale(intl.locale),
      ...options,
    });
  };

  const contextValue: LocalizationContext = {
    changeLocale: (newLocale: Locale) => onLocaleChange(newLocale),
    formatAssetAmount,
    formatAsset,
    formattingRules,
    formatAdaptableRelativeTime,
    ...intl,
  };

  return <Provider value={contextValue}>{children}</Provider>;
};
