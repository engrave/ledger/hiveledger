import { useState } from 'react';
import { IntlProvider } from 'react-intl';
import { ValidationLocalizationProvider } from 'context/localization/validationLocalizationProvider/ValidationLocalizationProvider';
import { defaultLocale, Locale } from 'i18n';
import { dictionaries } from '../dictionaries/dictionaries';
import { LocalizationContextProvider } from '../localizationContextProvider/LocalizationContextProvider';
import { LocalizationProviderProps } from './LocalizationPorivder.types';

export const LocalizationProvider = ({ children }: LocalizationProviderProps) => {
  const [locale, setLocale] = useState<Locale>(defaultLocale);

  return (
    <IntlProvider locale={locale} messages={dictionaries[locale]}>
      <LocalizationContextProvider onLocaleChange={(newLocale) => setLocale(newLocale)}>
        <ValidationLocalizationProvider>{children}</ValidationLocalizationProvider>
      </LocalizationContextProvider>
    </IntlProvider>
  );
};
