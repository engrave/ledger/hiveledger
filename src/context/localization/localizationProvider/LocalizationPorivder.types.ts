import { ReactNode } from 'react';

export type LocalizationProviderProps = {
  children: ReactNode;
};
