import { Locale } from 'i18n';

export type Dictionary<K extends string> = Record<K, string>;

export type MultiDictionary<K extends string> = {
  [locale in Locale]?: Dictionary<K>;
};
