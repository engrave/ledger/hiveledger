import { dot } from 'dot-object';
import { PrimitiveType } from 'intl-messageformat';
import { defaultLocale, translations } from 'i18n';
import { ObjectToPath } from 'types/objectToPath';
import { MultiDictionary } from './dictionaries.types';

const defaultDictionary = translations[defaultLocale];
const defaultDictionaryFlattened = dot(defaultDictionary);

export type TranslationKey = ObjectToPath<typeof defaultDictionary>;
export type TranslationValues = Record<string, PrimitiveType>;

export const dictionaries = Object.entries(translations).reduce<MultiDictionary<TranslationKey>>(
  (result, [locale, dictionary]) => {
    return { ...result, [locale]: dot(dictionary) };
  },
  {},
);

export const isTranslationKey = (key: string): key is TranslationKey => {
  return key in defaultDictionaryFlattened;
};
