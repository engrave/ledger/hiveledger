import { createContext } from 'react';

export type ProgressContext = {
  isAnimating: boolean;
  stateKey: number;
  start: () => void;
  done: () => void;
};

export const progressContext = createContext<ProgressContext | null>(null);
