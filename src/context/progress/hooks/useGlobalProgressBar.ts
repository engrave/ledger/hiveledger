import { useContext } from 'react';
import { progressContext } from 'context/progress/progressContext/progressContext';

export const useGlobalProgressBar = () => {
  const context = useContext(progressContext);

  if (context === null) {
    throw new Error('ProgressProvider is missing');
  }

  return context;
};
