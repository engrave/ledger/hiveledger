import { useCallback, useState } from 'react';
import { ProgressContext, progressContext } from 'context/progress/progressContext/progressContext';
import { WrapperProps } from 'types/react';

export const ProgressProvider = ({ children }: WrapperProps) => {
  const { Provider } = progressContext;
  const [tasksCount, setTasksCount] = useState(0);
  const [stateKey, setStateKey] = useState(Date.now());

  const start = useCallback(() => {
    setTasksCount((count) => count + 1);
    setStateKey(Date.now());
  }, []);

  const done = useCallback(() => {
    setTasksCount((count) => Math.max(count - 1, 0));
    setStateKey(Date.now());
  }, []);

  const value: ProgressContext = {
    isAnimating: tasksCount > 0,
    stateKey,
    start,
    done,
  };

  return <Provider value={value}>{children}</Provider>;
};
