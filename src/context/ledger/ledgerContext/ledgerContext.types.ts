import LedgerHiveApp from '@engrave/ledger-app-hive';
import { SignedTransaction, Transaction } from '@hiveio/dhive';
import { KeysStorage } from 'context/ledger/hooks/useKeyStorage/useKeyStorage';
import { AccountPublicKey } from 'modules/hive/account/account';

export type PublicKeyGetter = (path: string, confirm?: boolean) => Promise<string>;
export type AccountKeyGetter = (path: string, confirm?: boolean) => Promise<AccountPublicKey>;
export type TransactionSigner = (path: string, transaction: Transaction) => Promise<SignedTransaction>;
export type DeviceClose = () => Promise<void>;

export type LedgerConnection = {
  device: LedgerHiveApp;
  getPublicKey: PublicKeyGetter;
  getAccountKey: AccountKeyGetter;
  signTransaction: TransactionSigner;
  signTransactionHash: TransactionSigner;
  closeDevice: DeviceClose;
};

export type LedgerContext = {
  connectLedger: () => Promise<LedgerConnection>;
  keysStorage: KeysStorage;
  removeKeysCache: () => void;
};
