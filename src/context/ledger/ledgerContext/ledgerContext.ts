import { createContext } from 'react';
import { LedgerContext } from 'context/ledger/ledgerContext/ledgerContext.types';

export const ledgerContext = createContext<LedgerContext | null>(null);
