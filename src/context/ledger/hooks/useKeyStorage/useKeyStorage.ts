import { useCallback, useRef } from 'react';
import { PublicKeysCollection } from 'modules/hive/account/account';

export type KeysStorage = {
  getPublicKey: (path: string) => string | null;
  setPublicKey: (path: string, publicKey: string) => void;
  clearStorage: () => void;
};

export const useKeyStorage = (): KeysStorage => {
  const cachedKeys = useRef<PublicKeysCollection>([]);

  const getPublicKey = useCallback((path: string) => {
    return cachedKeys.current.find((key) => key.path === path)?.publicKey ?? null;
  }, []);

  const setPublicKey = useCallback((path, publicKey: string) => {
    cachedKeys.current.push({ path, publicKey });
  }, []);

  const clearStorage = useCallback(() => {
    cachedKeys.current = [];
  }, []);

  return {
    getPublicKey,
    setPublicKey,
    clearStorage,
  };
};
