import { useContext } from 'react';
import { ledgerContext } from 'context/ledger/ledgerContext/ledgerContext';

export const useLedger = () => {
  const context = useContext(ledgerContext);

  if (context === null) {
    throw new Error('LedgerController missing');
  }

  return context;
};
