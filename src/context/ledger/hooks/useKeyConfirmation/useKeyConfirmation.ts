import { useLedger } from 'context/ledger/hooks/useLedger/useLedger';
import { isLedgerError } from 'context/ledger/ledgerController/LedgerController.types';
import { logApplicationError } from 'utils/debug/logApplicationError';

export enum KeyConfirmationError {
  cancelled = 'KeyConfirmationError/Cancelled',
  generalError = 'KeyConfirmationError/General',
}

export const useKeyConfirmation = () => {
  const { connectLedger } = useLedger();

  return async (path: string) => {
    const { getPublicKey, closeDevice } = await connectLedger();

    try {
      const publicKey = await getPublicKey(path, true);
      return { publicKey, path };
    } catch (error) {
      logApplicationError(error);

      if (isLedgerError(error, 'CONDITIONS_OF_USE_NOT_SATISFIED')) {
        throw new Error(KeyConfirmationError.cancelled);
      }

      throw new Error(KeyConfirmationError.generalError);
    } finally {
      await closeDevice();
    }
  };
};
