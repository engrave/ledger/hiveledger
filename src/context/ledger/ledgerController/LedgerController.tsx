import LedgerHiveApp from '@engrave/ledger-app-hive';
import Transport from '@ledgerhq/hw-transport';
import TransportWebUsb from '@ledgerhq/hw-transport-webusb';
import { useCallback } from 'react';
import { useKeyStorage } from 'context/ledger/hooks/useKeyStorage/useKeyStorage';
import { ledgerContext } from 'context/ledger/ledgerContext/ledgerContext';
import { LedgerConnection, LedgerContext } from 'context/ledger/ledgerContext/ledgerContext.types';
import { getLedgerConnection } from 'context/ledger/ledgerController/getLedgerConnection/getLedgerConnection';
import { LedgerError } from 'context/ledger/ledgerController/LedgerController.types';
import { WrapperProps } from 'types/react';
import { logApplicationError } from 'utils/debug/logApplicationError';

export const LedgerController = ({ children }: WrapperProps) => {
  const { Provider } = ledgerContext;
  const keysStorage = useKeyStorage();

  const connectLedger = useCallback(async (): Promise<LedgerConnection> => {
    let transport: Transport;
    let hiveApp: LedgerHiveApp;

    try {
      transport = await TransportWebUsb.create();
      hiveApp = new LedgerHiveApp(transport);
    } catch (error) {
      logApplicationError(error);

      if (error instanceof Error && error.name === 'TransportOpenUserCancelled') {
        throw new Error(LedgerError.connectionCancelled);
      }

      throw new Error(LedgerError.genericError);
    }

    return getLedgerConnection(hiveApp, keysStorage);
  }, [keysStorage]);

  const removeKeysCache = useCallback(() => {
    keysStorage.clearStorage();
  }, [keysStorage]);

  const context: LedgerContext = {
    connectLedger,
    keysStorage,
    removeKeysCache,
  };

  return <Provider value={context}>{children}</Provider>;
};
