import { StatusCodes } from '@ledgerhq/hw-transport';

export enum LedgerError {
  connectionCancelled = 'LedgerError/ConnectionCancelled',
  genericError = 'LedgerError/GenericError',
}

export type LedgerInternalStatusError = {
  name: string;
  message: string;
  stack: string;
  statusCode: number;
  statusText: string;
};

export const isLedgerInternalStatusError = (error: unknown): error is LedgerInternalStatusError => {
  const statusError = error as LedgerInternalStatusError;

  if (!Object.values(StatusCodes).includes(statusError.statusCode)) {
    return false;
  }

  return Object.keys(StatusCodes).includes(statusError.statusText);
};

export const isLedgerError = (error: unknown, text: keyof typeof StatusCodes) => {
  if (isLedgerInternalStatusError(error)) {
    return error.statusText === text;
  }

  return false;
};
