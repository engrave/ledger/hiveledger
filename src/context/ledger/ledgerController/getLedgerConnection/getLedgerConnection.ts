import LedgerHiveApp from '@engrave/ledger-app-hive';
import { Transaction } from '@hiveio/dhive';
import { SignedTransaction } from '@hiveio/dhive/lib/chain/transaction';
import { environment } from 'config/environment';
import { KeysStorage } from 'context/ledger/hooks/useKeyStorage/useKeyStorage';
import { LedgerConnection } from 'context/ledger/ledgerContext/ledgerContext.types';

export const getLedgerConnection = (hiveApp: LedgerHiveApp, keysStorage: KeysStorage): LedgerConnection => {
  const getPublicKey = async (app: LedgerHiveApp, path: string, confirm: boolean) => {
    if (!confirm) {
      const publicKey = keysStorage.getPublicKey(path);

      if (publicKey) {
        return publicKey;
      }
    }

    const key = await app.getPublicKey(path, confirm, environment.hive.addressPrefix);

    keysStorage.setPublicKey(key, path);

    return key;
  };

  const getAccountKey = async (hive: LedgerHiveApp, path: string, confirm: boolean) => {
    const publicKey = await getPublicKey(hive, path, confirm);

    return { path, publicKey };
  };

  const signTransaction = async (app: LedgerHiveApp, path: string, transaction: Transaction) => {
    return app.signTransaction(transaction, path, environment.hive.chainId);
  };

  const signTransactionHash = async (
    app: LedgerHiveApp,
    path: string,
    transaction: Transaction,
  ): Promise<SignedTransaction> => {
    const digest = LedgerHiveApp.getTransactionDigest(transaction, environment.hive.chainId);
    const signature = await app.signHash(digest, path);

    return { ...transaction, signatures: [signature] };
  };

  return {
    device: hiveApp,
    getPublicKey: (path: string, confirm = false) => {
      return getPublicKey(hiveApp, path, confirm);
    },
    getAccountKey: (path: string, confirm = false) => {
      return getAccountKey(hiveApp, path, confirm);
    },
    signTransaction: (path: string, transaction: Transaction) => {
      return signTransaction(hiveApp, path, transaction);
    },
    signTransactionHash: (path: string, transaction: Transaction) => {
      return signTransactionHash(hiveApp, path, transaction);
    },
    closeDevice: () => {
      return hiveApp.transport.close();
    },
  };
};
