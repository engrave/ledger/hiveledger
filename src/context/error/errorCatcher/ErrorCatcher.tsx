import { ErrorBoundary } from 'context/error/errorBoundary/ErrorBoundary';
import { useToast } from 'hooks/useToast/useToast';
import { WrapperProps } from 'types/react';

export const ErrorCatcher = (props: WrapperProps) => {
  const { showError } = useToast();

  const handleError = () => {
    showError('errors.generalException');
  };

  return <ErrorBoundary onError={handleError}>{props.children}</ErrorBoundary>;
};
