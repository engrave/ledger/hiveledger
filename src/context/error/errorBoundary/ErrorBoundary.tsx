import { Component, ErrorInfo } from 'react';
import { ErrorBoundaryProps } from 'context/error/errorBoundary/ErrorBoundary.types';

export class ErrorBoundary extends Component<ErrorBoundaryProps> {
  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    this.props.onError(error, errorInfo);
  }

  static getDerivedStateFromError() {
    return {};
  }

  render() {
    return this.props.children;
  }
}
