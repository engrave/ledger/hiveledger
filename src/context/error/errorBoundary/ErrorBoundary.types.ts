import { ErrorInfo, ReactNode } from 'react';

export type ErrorBoundaryProps = {
  children: ReactNode;
  onError: (error: Error, errorInfo: ErrorInfo) => void;
};
