import { useContext } from 'react';
import { navigationLockContext } from 'context/navigationLock/navigationLockContext/navigationLockContext';

export const useNavigationLock = () => {
  const context = useContext(navigationLockContext);

  if (context === null) {
    throw new Error('NavigationLockController is missing');
  }

  return context;
};
