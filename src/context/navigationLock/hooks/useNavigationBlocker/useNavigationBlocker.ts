import type { History, Blocker, Transition } from 'history';
import { useContext, useEffect } from 'react';
import { UNSAFE_NavigationContext as NavigationContext } from 'react-router-dom';

export function useNavigationBlocker(blocker: Blocker, condition = true): void {
  const navigator = useContext(NavigationContext).navigator as History;

  useEffect(() => {
    if (!condition) {
      return;
    }

    const unblock = navigator.block((transition: Transition) => {
      const autoUnblockingTransition = {
        ...transition,
        retry() {
          unblock();
          transition.retry();
        },
      };

      blocker(autoUnblockingTransition);
    });

    // eslint-disable-next-line consistent-return
    return unblock;
  }, [navigator, blocker, condition]);
}
