import { NavigationLock } from 'context/navigationLock/navigationLockController/NavigationLockController.types';

export type NavigationLockContext = {
  isNavigationLocked: boolean;
  addNavigationLock: (lock: NavigationLock) => () => void;
};
