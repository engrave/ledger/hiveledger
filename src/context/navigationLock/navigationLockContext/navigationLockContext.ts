import { createContext } from 'react';
import { NavigationLockContext } from 'context/navigationLock/navigationLockContext/navigationLockContext.types';

export const navigationLockContext = createContext<NavigationLockContext | null>(null);
