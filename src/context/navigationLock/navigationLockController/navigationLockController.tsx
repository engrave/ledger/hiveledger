import { useCallback, useEffect, useReducer } from 'react';
import { useNavigationPrompt } from 'context/navigationLock/hooks/useNavigationPrompt/useNavigationPrompt';
import { navigationLockContext } from 'context/navigationLock/navigationLockContext/navigationLockContext';
import { NavigationLockContext } from 'context/navigationLock/navigationLockContext/navigationLockContext.types';
import { NavigationLock } from 'context/navigationLock/navigationLockController/NavigationLockController.types';
import { navigationLockReducer } from 'context/navigationLock/navigationLockReducer/navigationLockReducer';
import { WrapperProps } from 'types/react';
import { ConfirmationModal } from 'ui/modal/confirmationModal/ConfirmationModal';

export const NavigationLockController = (props: WrapperProps) => {
  const [navigationLocks, dispatchAction] = useReducer(navigationLockReducer, []);

  const isNavigationLocked = navigationLocks.length > 0;

  const { showPrompt, confirmNavigation, cancelNavigation } = useNavigationPrompt(isNavigationLocked);

  const { Provider } = navigationLockContext;

  const handleNavigationConfirmation = () => {
    dispatchAction({ action: 'reset' });
    confirmNavigation();
  };

  const addNavigationLock = useCallback((lock: NavigationLock) => {
    dispatchAction({ action: 'add', lock });

    return () => {
      dispatchAction({ action: 'remove', lock });
    };
  }, []);

  const value: NavigationLockContext = {
    isNavigationLocked,
    addNavigationLock,
  };

  useEffect(() => {
    if (isNavigationLocked) {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      window.onbeforeunload = () => {};
    } else {
      window.onbeforeunload = null;
    }
  }, [isNavigationLocked]);

  return (
    <Provider value={value}>
      <>
        {isNavigationLocked && (
          <ConfirmationModal
            isOpen={showPrompt}
            onConfirm={handleNavigationConfirmation}
            onCancel={cancelNavigation}
            title={navigationLocks[0].title}
          >
            {navigationLocks[0].description}
          </ConfirmationModal>
        )}
        {props.children}
      </>
    </Provider>
  );
};
