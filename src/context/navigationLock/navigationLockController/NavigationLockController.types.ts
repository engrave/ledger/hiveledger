export type NavigationLock = {
  title?: string;
  description: string;
};

export type NavigationLockState = {
  isNavigationLocked: boolean;
  lock: NavigationLock;
};
