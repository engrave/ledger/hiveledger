import { NavigationLock } from 'context/navigationLock/navigationLockController/NavigationLockController.types';

export type NavigationLockAction = {
  action: 'add' | 'remove' | 'reset';
  lock?: NavigationLock;
};

export const navigationLockReducer = (state: NavigationLock[], action: NavigationLockAction) => {
  if (action.action === 'add' && action.lock) {
    return [...state, action.lock];
  }

  if (action.action === 'remove' && action.lock) {
    const locks = [...state];
    const index = locks.indexOf(action.lock);

    if (index !== -1) {
      locks.splice(index, 1);
    }

    return locks;
  }

  if (action.action === 'reset') {
    return [];
  }

  return state;
};
