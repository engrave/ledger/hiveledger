import { HpAsset, VestsAsset } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { sumWallets } from 'modules/hive/wallet/sumWallets/sumWallets';
import { Balance, Wallet } from 'modules/hive/wallet/wallet';

const walletA = {
  liquid: {
    hive: createAsset(100.111, ChainAssetSymbol.hive),
    hbd: createAsset(0, ChainAssetSymbol.hbd),
  },
  savings: {
    hive: createAsset(100.111, ChainAssetSymbol.hive),
    hbd: createAsset(50, ChainAssetSymbol.hbd),
  },
  staked: {
    staked: createAsset(30, ChainAssetSymbol.vests),
  },
} as Wallet;

const walletB = {
  liquid: {
    hive: createAsset(1.999, ChainAssetSymbol.hive),
    hbd: createAsset(500000, ChainAssetSymbol.hbd),
  },
  savings: {
    hive: createAsset(100.111, ChainAssetSymbol.hive),
    hbd: createAsset(60, ChainAssetSymbol.hbd),
  },
  staked: {
    staked: createAsset(50, ChainAssetSymbol.vests),
  },
} as Wallet;

const getHivePower = (vests: VestsAsset): HpAsset => {
  return createAsset(vests.amount * 2, ExternalAssetSymbol.hp);
};

describe('sumWallets', () => {
  it('makes a object with sum of assets in wallet', () => {
    const balance = sumWallets([walletA, walletB], getHivePower);

    expect(balance).toEqual<Balance>({
      hive: createAsset(462.332, ChainAssetSymbol.hive),
      hbd: createAsset(500110, ChainAssetSymbol.hbd),
    });
  });
});
