import { addMultipleAssets } from 'modules/hive/asset/addAsset/addAsset';
import { HpAsset, VestsAsset } from 'modules/hive/asset/asset';
import { castAsset } from 'modules/hive/asset/castAsset/castAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { createEmptyBalance } from 'modules/hive/wallet/createBalance/createBalance';
import { Balance, Wallet } from 'modules/hive/wallet/wallet';

export const sumWallets = (walletsList: Wallet[], getHivePower: (vests: VestsAsset) => HpAsset): Balance => {
  const walletsSum = createEmptyBalance();

  for (const wallet of walletsList) {
    const hivePowerAmount = getHivePower(wallet.staked.staked);
    const hiveAmount = castAsset(hivePowerAmount, ChainAssetSymbol.hive);

    walletsSum.hive = addMultipleAssets(walletsSum.hive, wallet.liquid.hive, wallet.savings.hive, hiveAmount);
    walletsSum.hbd = addMultipleAssets(walletsSum.hbd, wallet.liquid.hbd, wallet.savings.hbd);
  }

  return walletsSum;
};
