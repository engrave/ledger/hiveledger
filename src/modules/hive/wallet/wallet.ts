import { HbdAsset, HiveAsset, VestsAsset } from 'modules/hive/asset/asset';

export type Balance = {
  hive: HiveAsset;
  hbd: HbdAsset;
};

export type FullBalance = {
  hive: HiveAsset;
  hbd: HbdAsset;
  vests: VestsAsset;
};

export type StakedBalance = {
  staked: VestsAsset;
  received: VestsAsset;
  delegated: VestsAsset;
  total: VestsAsset;
};

export type WithdrawsBalance = {
  vestingWithdrawRate: VestsAsset;
  withdrawn: VestsAsset;
  toWithdraw: VestsAsset;
};

export type Wallet = {
  liquid: Balance;
  savings: Balance;
  staked: StakedBalance;
  stakingRewards: FullBalance;
  withdraws: WithdrawsBalance;
};
