import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { createEmptyBalance } from 'modules/hive/wallet/createBalance/createBalance';
import { Balance } from 'modules/hive/wallet/wallet';

describe('createEmptyBalance', () => {
  it('create simple balance object with zero amounts of assets', () => {
    expect(createEmptyBalance()).toEqual<Balance>({
      hive: { amount: 0, assetSymbol: ChainAssetSymbol.hive },
      hbd: { amount: 0, assetSymbol: ChainAssetSymbol.hbd },
    });
  });
});
