import { createEmptyAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { Balance } from 'modules/hive/wallet/wallet';

export const createEmptyBalance = (): Balance => {
  return {
    hive: createEmptyAsset(ChainAssetSymbol.hive),
    hbd: createEmptyAsset(ChainAssetSymbol.hbd),
  };
};
