import { AccountDetails } from 'modules/hive/account/account';
import { VestsAsset } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export const getTotalVests = (account: AccountDetails): VestsAsset => {
  const vesting = createAsset(account.vesting_shares, ChainAssetSymbol.vests);
  const delegated = createAsset(account.delegated_vesting_shares, ChainAssetSymbol.vests);
  const received = createAsset(account.received_vesting_shares, ChainAssetSymbol.vests);

  return createAsset(vesting.amount - delegated.amount + received.amount, ChainAssetSymbol.vests);
};
