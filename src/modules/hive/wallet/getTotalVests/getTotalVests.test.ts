import { AccountDetails } from 'modules/hive/account/account';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getTotalVests } from 'modules/hive/wallet/getTotalVests/getTotalVests';

describe('getTotalVests', () => {
  it('returns calculated number of available vests', () => {
    const account = {
      vesting_shares: `500 ${ChainAssetSymbol.vests}`,
      received_vesting_shares: `200 ${ChainAssetSymbol.vests}`,
      delegated_vesting_shares: `300 ${ChainAssetSymbol.vests}`,
    } as AccountDetails;

    expect(getTotalVests(account)).toEqual({ amount: 400, assetSymbol: ChainAssetSymbol.vests });
  });
});
