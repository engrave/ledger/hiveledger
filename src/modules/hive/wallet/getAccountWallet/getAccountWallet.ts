import { AccountDetails } from 'modules/hive/account/account';
import { createAsset, createFromIntegerString } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getTotalVests } from 'modules/hive/wallet/getTotalVests/getTotalVests';
import { Wallet } from 'modules/hive/wallet/wallet';

export const getAccountWallet = (account: AccountDetails): Wallet => {
  return {
    liquid: {
      hive: createAsset(account.balance, ChainAssetSymbol.hive),
      hbd: createAsset(account.hbd_balance, ChainAssetSymbol.hbd),
    },
    savings: {
      hive: createAsset(account.savings_balance, ChainAssetSymbol.hive),
      hbd: createAsset(account.savings_hbd_balance, ChainAssetSymbol.hbd),
    },
    staked: {
      staked: createAsset(account.vesting_shares, ChainAssetSymbol.vests),
      delegated: createAsset(account.delegated_vesting_shares, ChainAssetSymbol.vests),
      received: createAsset(account.received_vesting_shares, ChainAssetSymbol.vests),
      total: getTotalVests(account),
    },
    stakingRewards: {
      hive: createAsset(account.reward_hive_balance, ChainAssetSymbol.hive),
      hbd: createAsset(account.reward_hbd_balance, ChainAssetSymbol.hbd),
      vests: createAsset(account.reward_vesting_balance, ChainAssetSymbol.vests),
    },
    withdraws: {
      vestingWithdrawRate: createAsset(account.vesting_withdraw_rate, ChainAssetSymbol.vests),
      withdrawn: createFromIntegerString(account.withdrawn, ChainAssetSymbol.vests),
      toWithdraw: createFromIntegerString(account.to_withdraw, ChainAssetSymbol.vests),
    },
  };
};
