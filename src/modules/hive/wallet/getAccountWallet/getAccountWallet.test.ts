import { AccountDetails } from 'modules/hive/account/account';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getAccountWallet } from 'modules/hive/wallet/getAccountWallet/getAccountWallet';
import { Wallet } from 'modules/hive/wallet/wallet';

const { hive, hbd, vests } = ChainAssetSymbol;

describe('getAccountWallet', () => {
  it('converts information from chain into useful data', () => {
    const accountWallet = {
      balance: `100.000 ${hive}`,
      hbd_balance: `0.000 ${hbd}`,
      savings_balance: `1337.000 ${hive}`,
      savings_hbd_balance: `99.999 ${hbd}`,
      vesting_shares: `1000000.000000 ${vests}`,
      received_vesting_shares: `500000.000000 ${vests}`,
      delegated_vesting_shares: `200000.000000 ${vests}`,
      reward_hive_balance: `100.000000 ${hive}`,
      reward_hbd_balance: `200.000000 ${hbd}`,
      reward_vesting_balance: `300.000000 ${vests}`,
      vesting_withdraw_rate: `50.000000 ${vests}`,
      withdrawn: '300000000',
      to_withdraw: '200000000',
    } as AccountDetails;

    const wallet = getAccountWallet(accountWallet);

    expect(wallet).toEqual<Wallet>({
      liquid: {
        hive: { amount: 100, assetSymbol: ChainAssetSymbol.hive },
        hbd: { amount: 0, assetSymbol: ChainAssetSymbol.hbd },
      },
      savings: {
        hive: { amount: 1337, assetSymbol: ChainAssetSymbol.hive },
        hbd: { amount: 99.999, assetSymbol: ChainAssetSymbol.hbd },
      },
      staked: {
        staked: { amount: 1000000, assetSymbol: ChainAssetSymbol.vests },
        received: { amount: 500000, assetSymbol: ChainAssetSymbol.vests },
        delegated: { amount: 200000, assetSymbol: ChainAssetSymbol.vests },
        total: { amount: 1300000, assetSymbol: ChainAssetSymbol.vests },
      },
      stakingRewards: {
        hive: { amount: 100, assetSymbol: ChainAssetSymbol.hive },
        hbd: { amount: 200, assetSymbol: ChainAssetSymbol.hbd },
        vests: { amount: 300, assetSymbol: ChainAssetSymbol.vests },
      },
      withdraws: {
        vestingWithdrawRate: { amount: 50, assetSymbol: ChainAssetSymbol.vests },
        withdrawn: { amount: 300, assetSymbol: ChainAssetSymbol.vests },
        toWithdraw: { amount: 200, assetSymbol: ChainAssetSymbol.vests },
      },
    });
  });
});
