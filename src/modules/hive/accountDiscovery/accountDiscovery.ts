export const slipPurpose = 48;
export const slipNetwork = 13;

export enum SlipRole {
  owner = 0,
  active = 1,
  memo = 3,
  posting = 4,
}

export type Path = {
  slipPurpose: number;
  slipNetwork: number;
  accountRole: SlipRole;
  accountIndex: number;
  keyIndex: number;
};
