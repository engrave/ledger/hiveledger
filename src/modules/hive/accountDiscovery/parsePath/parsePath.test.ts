import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { parsePath } from 'modules/hive/accountDiscovery/parsePath/parsePath';

describe('parsePath', () => {
  it('parser properly valid paths', () => {
    expect(parsePath("m/48'/13'/0'/3'/3'")).toEqual({
      slipPurpose: 48,
      slipNetwork: 13,
      accountRole: SlipRole.owner,
      accountIndex: 3,
      keyIndex: 3,
    });

    expect(parsePath("m/50'/10'/1'/5'/5'")).toEqual({
      slipPurpose: 50,
      slipNetwork: 10,
      accountRole: SlipRole.active,
      accountIndex: 5,
      keyIndex: 5,
    });
  });

  it('returns null for invalid paths', () => {
    expect(parsePath('hello world')).toBe(null);
    expect(parsePath("m/50'/10'/1'/5'")).toBe(null);
    expect(parsePath("m/48'/13'/10'/3'/3'")).toBe(null);
    expect(parsePath("m/48'/13'/1'/b'/3'")).toBe(null);
  });
});
