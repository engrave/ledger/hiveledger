import { number, object } from 'yup';
import { Path, SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';

const pathValidator = object({
  slipPurpose: number().min(0).required(),
  slipNetwork: number().min(0).required(),
  accountRole: number().oneOf(Object.keys(SlipRole).map(Number)).required(),
  accountIndex: number().min(0).required(),
  keyIndex: number().min(0).required(),
});

export const parsePath = (path: string): Path | null => {
  const regex = /m\/(\d+)'\/(\d+)'\/(\d+)'\/(\d+)'\/(\d+)'/;

  const result = path.match(regex);

  if (result !== null) {
    const pathDetails = {
      slipPurpose: Number(result[1]),
      slipNetwork: Number(result[2]),
      accountRole: Number(result[3]),
      accountIndex: Number(result[4]),
      keyIndex: Number(result[5]),
    };

    if (pathValidator.isValidSync(pathDetails)) {
      return pathDetails as Path;
    }
  }

  return null;
};
