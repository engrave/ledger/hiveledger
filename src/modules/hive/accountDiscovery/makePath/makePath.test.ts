import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { makePath } from 'modules/hive/accountDiscovery/makePath/makePath';

describe('makePath', () => {
  it('generates 0/0/0 path for default values', () => {
    expect(makePath()).toBe("m/48'/13'/0'/0'/0'");
  });
  it('generates correct path for given values', () => {
    expect(makePath(SlipRole.owner, 3, 3)).toBe("m/48'/13'/0'/3'/3'");
    expect(makePath(SlipRole.active, 1, 5)).toBe("m/48'/13'/1'/1'/5'");
    expect(makePath(SlipRole.memo, 5, 1)).toBe("m/48'/13'/3'/5'/1'");
  });
});
