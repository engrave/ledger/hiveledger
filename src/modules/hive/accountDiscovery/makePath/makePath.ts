import { slipNetwork, slipPurpose, SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';

export const makePath = (role = SlipRole.owner, accountIndex = 0, keyIndex = 0) => {
  return `m/${slipPurpose}'/${slipNetwork}'/${role}'/${accountIndex}'/${keyIndex}'`;
};
