import { accountDiscovery } from 'config/config';
import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { discoverAccounts } from 'modules/hive/accountDiscovery/discoverAccounts/discoverAccounts';
import { ResolvedReferences } from 'modules/hive/accountDiscovery/discoverAccounts/discoverAccounts.types';

const { maxKeyGap, maxAccountGap } = accountDiscovery;

describe('discoverAccounts', () => {
  it('discovers accounts using given resolver', async () => {
    const resolver = jest.fn<Promise<ResolvedReferences>, [string]>();

    resolver.mockImplementationOnce(() => {
      return Promise.resolve({
        usernames: ['lorem', 'ipsum'],
        publicKey: 'QWERTYUIOP',
      });
    });

    resolver.mockImplementationOnce(() => {
      return Promise.resolve({
        usernames: ['lorem'],
        publicKey: '1234567890',
      });
    });

    resolver.mockImplementationOnce(() => {
      return Promise.resolve({
        usernames: ['ipsum'],
        publicKey: '!@#$%^&*()',
      });
    });

    resolver.mockImplementation((path: string) => {
      return Promise.resolve({
        usernames: [],
        publicKey: `${path}_key`,
      });
    });

    const discoveries = await discoverAccounts(SlipRole.owner, resolver);

    expect(discoveries).toStrictEqual([
      {
        path: "m/48'/13'/0'/0'/0'",
        publicKey: 'QWERTYUIOP',
        username: 'lorem',
      },
      {
        path: "m/48'/13'/0'/0'/0'",
        publicKey: 'QWERTYUIOP',
        username: 'ipsum',
      },
      {
        path: "m/48'/13'/0'/0'/1'",
        publicKey: '1234567890',
        username: 'lorem',
      },
      {
        path: "m/48'/13'/0'/0'/2'",
        publicKey: '!@#$%^&*()',
        username: 'ipsum',
      },
    ]);

    expect(resolver.mock.calls.length).toBe(3 + maxKeyGap + maxAccountGap * maxKeyGap);
  });

  it('calls the resolver to be called properly', async () => {
    const resolver = jest.fn((path: string) => {
      return Promise.resolve({
        usernames: [],
        publicKey: `${path}_key`,
      });
    });

    await discoverAccounts(SlipRole.owner, resolver);

    expect(resolver.mock.calls.length).toBe(maxAccountGap * maxKeyGap);
    expect(resolver.mock.calls).toEqual([
      ["m/48'/13'/0'/0'/0'"],
      ["m/48'/13'/0'/0'/1'"],
      ["m/48'/13'/0'/1'/0'"],
      ["m/48'/13'/0'/1'/1'"],
    ]);
  });
});
