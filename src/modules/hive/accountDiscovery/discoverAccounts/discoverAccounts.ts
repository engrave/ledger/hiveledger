import { accountDiscovery } from 'config/config';
import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { makePath } from 'modules/hive/accountDiscovery/makePath/makePath';
import { ReferenceResolver, DiscoverAccountsOptions, DiscoveredAccount } from './discoverAccounts.types';

const defaultOptions: DiscoverAccountsOptions = {
  maxAccountGap: accountDiscovery.maxAccountGap,
  maxKeyGap: accountDiscovery.maxKeyGap,
};

export const discoverAccounts = async (
  role: SlipRole,
  resolver: ReferenceResolver,
  options: Partial<DiscoverAccountsOptions> = {},
): Promise<DiscoveredAccount[]> => {
  const { maxAccountGap, maxKeyGap } = { ...defaultOptions, ...options };

  const discoveredAccounts: DiscoveredAccount[] = [];

  for (let accountIndex = 0, accountGap = 0; accountGap < maxAccountGap; accountIndex += 1) {
    for (let keyIndex = 0, keyGap = 0; keyGap < maxKeyGap; keyIndex += 1) {
      const path = makePath(role, accountIndex, keyIndex);
      const { publicKey, usernames } = await resolver(path);

      if (usernames.length > 0) {
        accountGap = 0;
        keyGap = 0;

        usernames.forEach((username) => {
          discoveredAccounts.push({ path, publicKey, username });
        });
      } else {
        keyGap += 1;

        if (keyGap === maxKeyGap && keyIndex === maxKeyGap - 1) {
          accountGap += 1;
        }
      }
    }
  }

  return discoveredAccounts;
};
