export type ResolvedReferences = {
  publicKey: string;
  usernames: string[];
};

export type ReferenceResolver = (path: string) => Promise<ResolvedReferences>;

export type DiscoverAccountsOptions = {
  maxAccountGap: number;
  maxKeyGap: number;
};

export type DiscoveredAccount = {
  path: string;
  publicKey: string;
  username: string;
};
