import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { makePath } from 'modules/hive/accountDiscovery/makePath/makePath';

export const generateSlipPaths = (role = SlipRole.owner, accountGap = 5, keyGap = 5) => {
  const paths = [];

  for (let accountIndex = 0; accountIndex < accountGap; accountIndex += 1) {
    for (let keyIndex = 0; keyIndex < keyGap; keyIndex += 1) {
      paths.push(makePath(role, accountIndex, keyIndex));
    }
  }

  return paths;
};
