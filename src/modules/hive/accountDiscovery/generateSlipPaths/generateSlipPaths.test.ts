import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { generateSlipPaths } from 'modules/hive/accountDiscovery/generateSlipPaths/generateSlipPaths';

describe('generateSlipPaths', () => {
  it('should generate 25 paths for default parameters', () => {
    const paths = generateSlipPaths();

    expect(paths.length).toBe(25);
  });

  it('should generate correct paths for given parameters', () => {
    const ownerRolePaths = generateSlipPaths(SlipRole.owner, 2, 3);

    expect(ownerRolePaths).toStrictEqual([
      "m/48'/13'/0'/0'/0'",
      "m/48'/13'/0'/0'/1'",
      "m/48'/13'/0'/0'/2'",
      "m/48'/13'/0'/1'/0'",
      "m/48'/13'/0'/1'/1'",
      "m/48'/13'/0'/1'/2'",
    ]);

    const activeRolePaths = generateSlipPaths(SlipRole.active, 3, 2);

    expect(activeRolePaths).toStrictEqual([
      "m/48'/13'/1'/0'/0'",
      "m/48'/13'/1'/0'/1'",
      "m/48'/13'/1'/1'/0'",
      "m/48'/13'/1'/1'/1'",
      "m/48'/13'/1'/2'/0'",
      "m/48'/13'/1'/2'/1'",
    ]);
  });
});
