import { Asset } from 'modules/hive/asset/asset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { isNaiCode, NaiAsset, NaiCode, NaiError } from 'modules/hive/naiAsset/naiAsset';
import { convertIntegerString } from 'utils/number/convertIntegerString/convertIntegerString';

export const naiCodeMapping = {
  [NaiCode.hive]: ChainAssetSymbol.hive,
  [NaiCode.hbd]: ChainAssetSymbol.hbd,
  [NaiCode.vests]: ChainAssetSymbol.vests,
};

export type NaiMapping = typeof naiCodeMapping;

export const fromNaiFormat = <C extends NaiCode = NaiCode>(naiAsset: NaiAsset<C>): Asset<NaiMapping[C]> => {
  const amount = convertIntegerString(naiAsset.amount.toString(), naiAsset.precision);
  const naiCode = naiAsset.nai;

  if (!isNaiCode(naiCode)) {
    throw new Error(NaiError.invalidNaiCode);
  }

  const assetSymbol = naiCodeMapping[naiCode];

  return { amount, assetSymbol };
};
