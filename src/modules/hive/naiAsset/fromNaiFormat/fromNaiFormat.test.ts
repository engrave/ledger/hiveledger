import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { fromNaiFormat } from 'modules/hive/naiAsset/fromNaiFormat/fromNaiFormat';
import { NaiAsset, NaiCode, NaiError } from 'modules/hive/naiAsset/naiAsset';

describe('fromNaiFormat', () => {
  it('properly converts Nai assets into normal assets', () => {
    const hiveAsset = fromNaiFormat({
      amount: '123456789',
      nai: NaiCode.hive,
      precision: 3,
    });

    const hbdAsset = fromNaiFormat({
      amount: '123456789',
      nai: NaiCode.hbd,
      precision: 3,
    });

    const vestsAsset = fromNaiFormat({
      amount: '123456789',
      nai: NaiCode.vests,
      precision: 3,
    });

    expect(hiveAsset).toEqual({
      amount: 123456.789,
      assetSymbol: ChainAssetSymbol.hive,
    });

    expect(hbdAsset).toEqual({
      amount: 123456.789,
      assetSymbol: ChainAssetSymbol.hbd,
    });

    expect(vestsAsset).toEqual({
      amount: 123456.789,
      assetSymbol: ChainAssetSymbol.vests,
    });
  });

  it('throws for an unknown NAI code', () => {
    const naiAsset: NaiAsset = {
      amount: '123456789',
      nai: '@@_invalid' as NaiCode,
      precision: 3,
    };

    expect(() => fromNaiFormat(naiAsset)).toThrow(NaiError.invalidNaiCode);
  });
});
