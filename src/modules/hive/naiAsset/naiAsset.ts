export enum NaiCode {
  hive = '@@000000021',
  hbd = '@@000000013',
  vests = '@@000000037',
}

export type NaiAsset<C extends NaiCode = NaiCode> = {
  amount: string | number;
  precision: number;
  nai: C | string;
};

export const isNaiCode = (code: string): code is NaiCode => {
  return Object.values(NaiCode).includes(code as NaiCode);
};

export const isNaiAsset = (asset: unknown): asset is NaiAsset => {
  if (asset === null) {
    return false;
  }

  if (typeof asset !== 'object') {
    return false;
  }

  return 'amount' in asset && 'precision' in asset && 'nai' in asset;
};

export enum NaiError {
  invalidNaiCode = 'NaiError/InvalidNaiCode',
  unsupportedAssetType = 'NaiError/UnsupportedAssetType',
}
