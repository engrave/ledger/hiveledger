import { createNaiAsset } from 'modules/hive/naiAsset/createNaiAsset/createNaiAsset';
import { NaiCode } from 'modules/hive/naiAsset/naiAsset';

describe('createNaiAsset', () => {
  it('creates a valid NAI asset', () => {
    const naiHiveAsset = createNaiAsset(21.370001, NaiCode.hive);

    expect(naiHiveAsset).toEqual({
      amount: '21370',
      precision: 3,
      nai: NaiCode.hive,
    });

    const naiVestsAsset = createNaiAsset(21.37001, NaiCode.vests);

    expect(naiVestsAsset).toEqual({
      amount: '21370010',
      precision: 6,
      nai: NaiCode.vests,
    });
  });
});
