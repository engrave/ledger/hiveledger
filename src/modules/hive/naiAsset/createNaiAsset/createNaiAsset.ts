import { getPrecision } from 'modules/hive/assetSymbol/getPrecision/getPrecision';
import { naiCodeMapping } from 'modules/hive/naiAsset/fromNaiFormat/fromNaiFormat';
import { NaiAsset, NaiCode } from 'modules/hive/naiAsset/naiAsset';

export const createNaiAsset = <C extends NaiCode>(amount: number, nai: C): NaiAsset<C> => {
  const assetSymbol = naiCodeMapping[nai];
  const precision = getPrecision(assetSymbol);
  const multiplier = 10 ** precision;

  const naiAmount = (amount * multiplier).toFixed(0);

  return {
    amount: naiAmount,
    precision,
    nai,
  };
};
