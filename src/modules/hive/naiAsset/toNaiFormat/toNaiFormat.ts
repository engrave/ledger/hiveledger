import { Asset } from 'modules/hive/asset/asset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { createNaiAsset } from 'modules/hive/naiAsset/createNaiAsset/createNaiAsset';
import { NaiAsset, NaiCode, NaiError } from 'modules/hive/naiAsset/naiAsset';

export const assetTypeMapping = {
  [ChainAssetSymbol.hive]: NaiCode.hive,
  [ChainAssetSymbol.hbd]: NaiCode.hbd,
  [ChainAssetSymbol.vests]: NaiCode.vests,
} as const;

export type AssetMapping = typeof assetTypeMapping;

export const toNaiFormat = <S extends ChainAssetSymbol>(asset: Asset<S>): NaiAsset<AssetMapping[S]> => {
  const nai = assetTypeMapping[asset.assetSymbol];

  if (nai === undefined) {
    throw new Error(NaiError.unsupportedAssetType);
  }

  return createNaiAsset(asset.amount, nai);
};
