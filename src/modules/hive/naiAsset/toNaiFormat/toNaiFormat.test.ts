import { Asset } from 'modules/hive/asset/asset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { NaiCode, NaiError } from 'modules/hive/naiAsset/naiAsset';
import { toNaiFormat } from 'modules/hive/naiAsset/toNaiFormat/toNaiFormat';

describe('toNaiFormat', () => {
  it('properly converts normal assets into nai assets', () => {
    const hiveNai = toNaiFormat({
      amount: 123456.789,
      assetSymbol: ChainAssetSymbol.hive,
    });

    const hbdNai = toNaiFormat({
      amount: 123456.789,
      assetSymbol: ChainAssetSymbol.hbd,
    });

    const vestsNai = toNaiFormat({
      amount: 123.456789,
      assetSymbol: ChainAssetSymbol.vests,
    });

    expect(hiveNai).toEqual({
      amount: '123456789',
      nai: NaiCode.hive,
      precision: 3,
    });

    expect(hbdNai).toEqual({
      amount: '123456789',
      nai: NaiCode.hbd,
      precision: 3,
    });

    expect(vestsNai).toEqual({
      amount: '123456789',
      nai: NaiCode.vests,
      precision: 6,
    });
  });

  it('throws for an unsupported asset type', () => {
    const asset: Asset = {
      amount: 123456789,
      assetSymbol: ExternalAssetSymbol.usd,
    };

    expect(() => toNaiFormat(asset as Asset<ChainAssetSymbol.hbd>)).toThrow(NaiError.unsupportedAssetType);
  });
});
