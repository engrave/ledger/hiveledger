import { DynamicGlobalProperties } from '@hiveio/dhive';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { transferToVesting } from 'modules/hive/operation/transferToVesting/transferToVesting';
import { createTransaction } from 'modules/hive/transaction/createTransaction/createTransaction';

jest.useFakeTimers().setSystemTime(new Date('2020-01-01T11:11:11Z').getTime());

describe('createTransaction', () => {
  it('create valid transaction object', () => {
    const globalProperties = {
      head_block_number: 3000,
      head_block_id: '03b0b39519a48a19da5f54b00bee15922af48bea',
    } as DynamicGlobalProperties;

    const operationA = transferToVesting({
      from: 'lorem',
      amount: createAsset(23, ChainAssetSymbol.hive),
    });

    const operationB = transferToVesting({
      from: 'ipsum',
      amount: createAsset(100, ChainAssetSymbol.hive),
    });

    expect(createTransaction(globalProperties, [operationA, operationB])).toEqual({
      ref_block_num: 3000,
      ref_block_prefix: 428516377,
      expiration: '2020-01-01T11:12:11',
      operations: [
        [
          'transfer_to_vesting',
          {
            from: 'lorem',
            to: 'lorem',
            amount: `23.000 ${ChainAssetSymbol.hive}`,
          },
        ],
        [
          'transfer_to_vesting',
          {
            from: 'ipsum',
            to: 'ipsum',
            amount: `100.000 ${ChainAssetSymbol.hive}`,
          },
        ],
      ],
      extensions: [],
    });
  });
});
