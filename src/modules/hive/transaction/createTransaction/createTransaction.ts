import { DynamicGlobalProperties } from '@hiveio/dhive';
import { Operation, Transaction } from 'modules/hive/operation/operation';

export const createTransaction = (globalProperties: DynamicGlobalProperties, operations: Operation[]): Transaction => {
  return {
    ref_block_num: globalProperties.head_block_number,
    ref_block_prefix: Buffer.from(globalProperties.head_block_id, 'hex').readUInt32LE(4),
    expiration: new Date(Date.now() + 60 * 1000).toISOString().slice(0, -5),
    operations,
    extensions: [],
  };
};
