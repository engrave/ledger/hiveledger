import { AppliedOperation } from '@hiveio/dhive/lib/chain/operation';

export enum TransactionStatus {
  unknown = 'unknown',
  withinMempool = 'within_mempool',
  withinReversibleBlock = 'within_reversible_block',
  withinIrreversibleBlock = 'within_irreversible_block',
  expiredReversible = 'expired_reversible',
  expiredIrreversible = 'expired_irreversible',
  tooOld = 'too_old',
}

export type TransactionsHistoryItem = [number, AppliedOperation];
