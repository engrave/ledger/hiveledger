import { isValid, parseISO } from 'date-fns';
import { formatInTimeZone } from 'date-fns-tz';

export const serializeParameter = (value: unknown) => {
  if (typeof value === 'string') {
    const parsedDate = parseISO(value);

    if (isValid(parsedDate)) {
      return formatInTimeZone(parsedDate, 'UTC', 'yyyy-MM-dd HH:mm');
    }
  }

  if (typeof value === 'string' || typeof value === 'number') {
    return value.toString();
  }

  return JSON.stringify(value);
};
