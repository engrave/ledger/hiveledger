import { serializeParameter } from 'modules/hive/transaction/serializeParameter/serializeParameter';

describe('serializeParameter', () => {
  it('serializes properly strings', () => {
    expect(serializeParameter('hello world')).toEqual('hello world');
  });

  it('serializes properly numbers', () => {
    expect(serializeParameter(123.456)).toEqual('123.456');
  });

  it('serializes properly ISO dates in the UTC time zone', () => {
    expect(serializeParameter('2022-02-01T11:50:11.000Z')).toEqual('2022-02-01 11:50');
  });

  it('serializes other objects and arrays to JSON', () => {
    expect(serializeParameter({ test: ['hello', 'world'] })).toEqual('{"test":["hello","world"]}');
  });
});
