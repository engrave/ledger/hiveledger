import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getProposalsRanking } from 'modules/hive/proposals/getProposalsRanking/getProposalsRanking';
import {
  ProposalDetails,
  ProposalFundingStatus,
  ProposalsRanking,
  ProposalStatus,
} from 'modules/hive/proposals/proposals';

const proposals = [
  {
    status: ProposalStatus.active,
    daily_pay: `800 ${ChainAssetSymbol.hbd}`,
  },
  {
    status: ProposalStatus.active,
    daily_pay: `100 ${ChainAssetSymbol.hbd}`,
  },
  {
    status: ProposalStatus.inactive,
    daily_pay: `300 ${ChainAssetSymbol.hbd}`,
  },
  {
    status: ProposalStatus.active,
    daily_pay: `600 ${ChainAssetSymbol.hbd}`,
  },
  {
    status: ProposalStatus.active,
    daily_pay: `400 ${ChainAssetSymbol.hbd}`,
  },
  {
    status: ProposalStatus.inactive,
    daily_pay: `50 ${ChainAssetSymbol.hbd}`,
  },
] as unknown as ProposalDetails[];

describe('getProposalsRanking', () => {
  it('should return information about proposals funding', () => {
    const proposalsFunding = getProposalsRanking(proposals, createAsset(1000, ChainAssetSymbol.hbd));

    expect(proposalsFunding).toEqual<ProposalsRanking>([
      {
        details: proposals[0],
        assignedFund: createAsset(800, ChainAssetSymbol.hbd),
        fundStatus: ProposalFundingStatus.funded,
      },
      {
        details: proposals[1],
        assignedFund: createAsset(100, ChainAssetSymbol.hbd),
        fundStatus: ProposalFundingStatus.funded,
      },
      {
        details: proposals[2],
        assignedFund: createAsset(0, ChainAssetSymbol.hbd),
        fundStatus: ProposalFundingStatus.futureFunded,
      },
      {
        details: proposals[3],
        assignedFund: createAsset(100, ChainAssetSymbol.hbd),
        fundStatus: ProposalFundingStatus.funded,
      },
      {
        details: proposals[4],
        assignedFund: createAsset(0, ChainAssetSymbol.hbd),
        fundStatus: ProposalFundingStatus.notFunded,
      },
      {
        details: proposals[5],
        assignedFund: createAsset(0, ChainAssetSymbol.hbd),
        fundStatus: ProposalFundingStatus.notFunded,
      },
    ]);
  });
});
