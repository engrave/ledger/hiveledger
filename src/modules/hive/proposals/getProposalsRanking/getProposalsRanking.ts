import { HbdAsset } from 'modules/hive/asset/asset';
import { createAsset, createEmptyAsset } from 'modules/hive/asset/createAsset/createAsset';
import { isMoreThanAsset } from 'modules/hive/asset/isMoreThanAsset/isMoreThanAsset';
import { subtractAsset } from 'modules/hive/asset/subtractAsset/subtractAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import {
  ProposalDetails,
  ProposalFundingStatus,
  ProposalsRanking,
  ProposalStatus,
} from 'modules/hive/proposals/proposals';

export const getProposalsRanking = (proposals: ProposalDetails[], dailyBudget: HbdAsset) => {
  const ranking: ProposalsRanking = [];

  let remainingBudget: HbdAsset = { ...dailyBudget };

  for (const proposal of proposals) {
    if (proposal.status === ProposalStatus.inactive) {
      ranking.push({
        details: proposal,
        assignedFund: createEmptyAsset(ChainAssetSymbol.hbd),
        fundStatus: remainingBudget.amount > 0 ? ProposalFundingStatus.futureFunded : ProposalFundingStatus.notFunded,
      });

      continue;
    }

    const dailyHbdPay = createAsset(proposal.daily_pay, ChainAssetSymbol.hbd);
    const assignedFund = isMoreThanAsset(remainingBudget, dailyHbdPay) ? dailyHbdPay : remainingBudget;

    ranking.push({
      details: proposal,
      assignedFund,
      fundStatus: assignedFund.amount > 0 ? ProposalFundingStatus.funded : ProposalFundingStatus.notFunded,
    });

    remainingBudget = subtractAsset(remainingBudget, assignedFund);
  }

  return ranking;
};
