import { getLastFundedProposal } from 'modules/hive/proposals/getLastFundedProposal/getLastFundedProposal';
import { ProposalFundingStatus, ProposalsRanking } from 'modules/hive/proposals/proposals';

const ranking = [
  {
    details: { proposal_id: 1 },
    fundStatus: ProposalFundingStatus.funded,
  },
  {
    details: { proposal_id: 2 },
    fundStatus: ProposalFundingStatus.funded,
  },
  {
    details: { proposal_id: 3 },
    fundStatus: ProposalFundingStatus.futureFunded,
  },
  {
    details: { proposal_id: 4 },
    fundStatus: ProposalFundingStatus.funded,
  },
  {
    details: { proposal_id: 5 },
    fundStatus: ProposalFundingStatus.notFunded,
  },
  {
    details: { proposal_id: 6 },
    fundStatus: ProposalFundingStatus.notFunded,
  },
] as ProposalsRanking;

describe('getLastFundedProposal', () => {
  it('returns the last ranking item that gets funds', () => {
    expect(getLastFundedProposal(ranking)).toBe(ranking[3]);
  });
  it('returns null if for some reason no proposals are funded', () => {
    expect(getLastFundedProposal([])).toBe(null);
  });
});
