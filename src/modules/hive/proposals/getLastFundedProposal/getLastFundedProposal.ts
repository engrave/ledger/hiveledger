import { ProposalFundingStatus, ProposalsRanking } from 'modules/hive/proposals/proposals';

export const getLastFundedProposal = (ranking: ProposalsRanking) => {
  const lastFunded = ranking.find((item, index, list) => {
    return list[index + 1]?.fundStatus === ProposalFundingStatus.notFunded;
  });

  return lastFunded || null;
};
