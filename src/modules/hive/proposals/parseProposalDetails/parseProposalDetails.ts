import { differenceInDays } from 'date-fns';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { multiplyAsset } from 'modules/hive/asset/multiplyAsset/multiplyAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { ProposalDetails } from 'modules/hive/proposals/proposals';
import { parseChainApiDate } from 'utils/date/parseChainApiDate/parseChainApiDate';

export const parseProposalDetails = (proposal: ProposalDetails) => {
  const startDate = parseChainApiDate(proposal.start_date);
  const endDate = parseChainApiDate(proposal.end_date);
  const durationInDays = differenceInDays(endDate, startDate);

  const dailyPay = createAsset(proposal.daily_pay, ChainAssetSymbol.hbd);
  const totalPay = multiplyAsset(dailyPay, durationInDays);

  return { startDate, endDate, durationInDays, dailyPay, totalPay };
};
