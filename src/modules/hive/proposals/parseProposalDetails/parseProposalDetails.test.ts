import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { parseProposalDetails } from 'modules/hive/proposals/parseProposalDetails/parseProposalDetails';
import { ProposalDetails } from 'modules/hive/proposals/proposals';

const { hbd } = ChainAssetSymbol;

describe('parseProposalDetails', () => {
  it('returns some data calculated from proposal details', () => {
    const proposal = {
      start_date: '2022-06-15T15:00:00',
      end_date: '2022-06-20T13:00:00',
      daily_pay: `5.000 ${hbd}`,
    } as unknown as ProposalDetails;

    expect(parseProposalDetails(proposal)).toEqual({
      startDate: new Date('2022-06-15T15:00:00Z'),
      endDate: new Date('2022-06-20T13:00:00Z'),
      durationInDays: 4,
      dailyPay: { amount: 5, assetSymbol: hbd },
      totalPay: { amount: 20, assetSymbol: hbd },
    });
  });
});
