import { HbdAsset } from 'modules/hive/asset/asset';
import { NaiAsset, NaiCode } from 'modules/hive/naiAsset/naiAsset';

export enum ProposalStatus {
  active = 'active',
  inactive = 'inactive',
}

export enum ProposalFundingStatus {
  funded = 'funded',
  notFunded = 'notFunded',
  futureFunded = 'futureFunded',
}

export type ProposalDetails = {
  id: number;
  proposal_id: number;
  creator: string;
  receiver: string;
  subject: string;
  permlink: string;
  daily_pay: NaiAsset<NaiCode.hbd>;
  total_votes: string;
  start_date: string;
  end_date: string;
  status: ProposalStatus;
};

export type Proposal = {
  details: ProposalDetails;
  fundStatus: ProposalFundingStatus;
  assignedFund: HbdAsset;
};

export type ProposalsRanking = Proposal[];

export type ProposalVote = {
  id: number;
  proposal: ProposalDetails;
  voter: string;
};

export enum ProposalError {
  notFound = 'not_found',
}
