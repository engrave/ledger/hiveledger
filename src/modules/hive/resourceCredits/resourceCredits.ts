import { Manabar, RCAccount } from '@hiveio/dhive/lib/chain/rc';

export type AccountResourceCredits = {
  account: RCAccount;
  mana: Manabar;
};

export type ResourceCreditsDelegation = {
  from: string;
  to: string;
  delegated_rc: number;
};
