import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getPrecision } from 'modules/hive/assetSymbol/getPrecision/getPrecision';
import { AccountResourceCredits, ResourceCreditsDelegation } from 'modules/hive/resourceCredits/resourceCredits';

export const getMaxDelegation = (
  resourceCredits: AccountResourceCredits,
  existingDelegation: ResourceCreditsDelegation | null = null,
): number => {
  const accountCreationAdjustment = createAsset(
    resourceCredits.account.max_rc_creation_adjustment,
    ChainAssetSymbol.vests,
  );

  const patchedCreationAdjustment = accountCreationAdjustment.amount * 10 ** getPrecision(ChainAssetSymbol.vests);

  const existingDelegationAdjustment = existingDelegation?.delegated_rc ?? 0;

  const maxDelegation =
    resourceCredits.mana.max_mana -
    Number(resourceCredits.account.received_delegated_rc ?? 0) -
    patchedCreationAdjustment +
    existingDelegationAdjustment;

  return Math.min(maxDelegation, resourceCredits.mana.current_mana);
};
