import { getMaxDelegation } from 'modules/hive/resourceCredits/getMaxDelegation/getMaxDelegation';
import { AccountResourceCredits, ResourceCreditsDelegation } from 'modules/hive/resourceCredits/resourceCredits';

describe('getMaxDelegation', () => {
  it('should account for creation adjustment', () => {
    const resourceCredits = {
      mana: {
        max_mana: 10000,
        current_mana: 10000,
      },
      account: {
        max_rc_creation_adjustment: {
          amount: 200,
          precision: 6,
          nai: '@@000000037',
        },
      },
    } as unknown as AccountResourceCredits;

    expect(getMaxDelegation(resourceCredits)).toBe(9800);
  });

  it('should return no more than current_mana', () => {
    const resourceCredits = {
      mana: {
        max_mana: 10000,
        current_mana: 5000,
      },
      account: {
        max_rc_creation_adjustment: {
          amount: 200,
          precision: 6,
          nai: '@@000000037',
        },
      },
    } as unknown as AccountResourceCredits;

    expect(getMaxDelegation(resourceCredits)).toBe(5000);
  });

  it('includes the existing delegation when provided', () => {
    const resourceCredits = {
      mana: {
        max_mana: 10000,
        current_mana: 100000,
      },
      account: {
        max_rc_creation_adjustment: {
          amount: 200,
          precision: 6,
          nai: '@@000000037',
        },
      },
    } as unknown as AccountResourceCredits;

    const delegation: ResourceCreditsDelegation = {
      delegated_rc: 5000,
      from: 'lorem',
      to: 'ipsum',
    };

    expect(getMaxDelegation(resourceCredits, delegation)).toBe(14800);
  });
});
