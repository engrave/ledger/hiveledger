import { RCAccount } from '@hiveio/dhive/lib/chain/rc';
import { getRechargeRate } from 'modules/hive/resourceCredits/getRechargeRate/getRechargeRate';

describe('getRechargeRate', () => {
  it('calculates the recharge rate per day', () => {
    const account = {
      max_rc: '555.5',
    } as RCAccount;

    expect(getRechargeRate(account)).toBe(111.1);
  });
});
