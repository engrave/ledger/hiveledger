import { RCAccount } from '@hiveio/dhive/lib/chain/rc';

export const getRechargeRate = (account: RCAccount) => {
  return Number(account.max_rc) / 5;
};
