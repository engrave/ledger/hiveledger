import { AccountAuthorities, AccountDetails } from 'modules/hive/account/account';
import { getAuthorities } from 'modules/hive/account/getAuthorities/getAuthorities';

describe('getAuthorities', () => {
  it('should transform account details into list of authorities', () => {
    const accountDetails: AccountDetails = {
      owner: {
        weight_threshold: 2,
        account_auths: [
          ['lorem', 1],
          ['ipsum', 2],
        ],
        key_auths: [
          ['aaaaa', 3],
          ['bbbbb', 4],
          ['ccccc', 5],
        ],
      },
      posting: {
        weight_threshold: 3,
        account_auths: [
          ['hello', 10],
          ['world', 20],
        ],
        key_auths: [
          ['xxxxx', 30],
          ['yyyyy', 40],
          ['zzzzz', 50],
        ],
      },
      active: {
        weight_threshold: 4,
        account_auths: [],
        key_auths: [],
      },
    } as unknown as AccountDetails;

    const authorities: AccountAuthorities = {
      owner: {
        threshold: 2,
        accountAuths: [
          { value: 'lorem', weight: 1 },
          { value: 'ipsum', weight: 2 },
        ],
        keyAuths: [
          { value: 'aaaaa', weight: 3 },
          { value: 'bbbbb', weight: 4 },
          { value: 'ccccc', weight: 5 },
        ],
      },
      posting: {
        threshold: 3,
        accountAuths: [
          { value: 'hello', weight: 10 },
          { value: 'world', weight: 20 },
        ],
        keyAuths: [
          { value: 'xxxxx', weight: 30 },
          { value: 'yyyyy', weight: 40 },
          { value: 'zzzzz', weight: 50 },
        ],
      },
      active: {
        threshold: 4,
        accountAuths: [],
        keyAuths: [],
      },
    };

    expect(getAuthorities(accountDetails)).toEqual(authorities);
  });
});
