import { AccountAuthorities, AccountDetails } from 'modules/hive/account/account';

export const getAuthorities = (accountDetails: AccountDetails): AccountAuthorities => {
  return {
    owner: {
      threshold: accountDetails.owner.weight_threshold,
      keyAuths: accountDetails.owner.key_auths.map((auth) => {
        return { value: auth[0].toString(), weight: auth[1] };
      }),
      accountAuths: accountDetails.owner.account_auths.map((auth) => {
        return { value: auth[0], weight: auth[1] };
      }),
    },
    active: {
      threshold: accountDetails.active.weight_threshold,
      keyAuths: accountDetails.active.key_auths.map((auth) => {
        return { value: auth[0].toString(), weight: auth[1] };
      }),
      accountAuths: accountDetails.active.account_auths.map((auth) => {
        return { value: auth[0], weight: auth[1] };
      }),
    },
    posting: {
      threshold: accountDetails.posting.weight_threshold,
      keyAuths: accountDetails.posting.key_auths.map((auth) => {
        return { value: auth[0].toString(), weight: auth[1] };
      }),
      accountAuths: accountDetails.posting.account_auths.map((auth) => {
        return { value: auth[0], weight: auth[1] };
      }),
    },
  };
};
