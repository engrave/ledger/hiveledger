import { Account } from 'modules/hive/account/account';
import { isSame } from 'modules/hive/account/isSame/isSame';

const accountA = {
  username: 'lorem',
} as Account;

const accountB = {
  username: 'lorem',
} as Account;

const accountC = {
  username: 'ipsum',
} as Account;

describe('isSame', () => {
  it('recognizes accounts with the same username', () => {
    expect(isSame(accountA, accountB)).toBe(true);
    expect(isSame(accountB, accountC)).toBe(false);
    expect(isSame(accountA, accountC)).toBe(false);
  });
  it('returns false if one of the account is nullish', () => {
    expect(isSame(accountA, null)).toBe(false);
    expect(isSame(null, accountB)).toBe(false);
  });
});
