import { Account } from 'modules/hive/account/account';

export const isSame = (accountA: Account | null, accountB: Account | null) => {
  if (!accountA || !accountB) {
    return false;
  }

  return accountA.username === accountB.username;
};
