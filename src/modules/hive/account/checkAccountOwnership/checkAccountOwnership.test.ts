import { AccountDetails, AccountError } from 'modules/hive/account/account';
import { checkAccountOwnership } from 'modules/hive/account/checkAccountOwnership/checkAccountOwnership';

describe('checkAccountOwnership', () => {
  it('returns true if the ownership is right', () => {
    const accountDetails = {
      owner: {
        weight_threshold: 1,
        key_auths: [['KEY_LOREM', 1]],
      },
    } as unknown as AccountDetails;

    const result = checkAccountOwnership(accountDetails, 'KEY_LOREM');

    expect(result).toBe(true);
  });

  it('returns InvalidPublicKey error if the public key is not in account details', () => {
    const accountDetails = {
      owner: {
        weight_threshold: 1,
        key_auths: [['KEY_LOREM', 1]],
      },
    } as unknown as AccountDetails;

    const result = checkAccountOwnership(accountDetails, 'KEY_IPSUM');

    expect(result).toBe(AccountError.invalidPublicKey);
  });

  it('returns InsufficientOwnerKeyThreshold if the key weigh is lower than the threshold ', () => {
    const accountDetails = {
      owner: {
        weight_threshold: 1,
        key_auths: [['KEY_LOREM', 0.9]],
      },
    } as unknown as AccountDetails;

    const result = checkAccountOwnership(accountDetails, 'KEY_LOREM');

    expect(result).toBe(AccountError.insufficientOwnerKeyThreshold);
  });
});
