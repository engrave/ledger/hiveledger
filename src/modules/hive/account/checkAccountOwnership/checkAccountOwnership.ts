import { AccountDetails, AccountError } from 'modules/hive/account/account';

export const checkAccountOwnership = (account: AccountDetails, publicKey: string) => {
  const authKey = account.owner.key_auths.find((key) => {
    return key[0] === publicKey;
  });

  if (authKey === undefined) {
    return AccountError.invalidPublicKey;
  }

  if (authKey[1] < account.owner.weight_threshold) {
    return AccountError.insufficientOwnerKeyThreshold;
  }

  return true;
};
