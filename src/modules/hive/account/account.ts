import { ExtendedAccount, VestingDelegation } from '@hiveio/dhive';
import { AccountResourceCredits, ResourceCreditsDelegation } from 'modules/hive/resourceCredits/resourceCredits';
import { Wallet } from 'modules/hive/wallet/wallet';

export enum AccountError {
  invalidPublicKey = 'AccountError/InvalidPublicKey',
  insufficientOwnerKeyThreshold = 'AccountError/InsufficientOwnerKeyThreshold',
  notFound = 'AccountError/NotFound',
}

export type AccountPublicKey = {
  path: string;
  publicKey: string;
};

export type PublicKeysCollection = AccountPublicKey[];

export type AccountDetails = ExtendedAccount;

export type ExpiringVestingDelegation = {
  id: number;
  delegator: string;
  vesting_shares: string;
  expiration: string;
};

export type AccountVestingDelegations = {
  active: VestingDelegation[];
  expiring: ExpiringVestingDelegation[];
};

export enum AuthorityType {
  owner = 'owner',
  active = 'active',
  posting = 'posting',
}

export type AuthorityEntry = {
  value: string;
  weight: number;
};

export type AuthoritiesSet = {
  threshold: number;
  keyAuths: AuthorityEntry[];
  accountAuths: AuthorityEntry[];
};

export type AccountAuthorities = Record<AuthorityType, AuthoritiesSet>;

export type Account = {
  username: string;
  details: AccountDetails;
  wallet: Wallet;
  publicKeys: PublicKeysCollection;
  vestingDelegations: AccountVestingDelegations;
  resourceCredits: AccountResourceCredits;
  resourceCreditsDelegations: ResourceCreditsDelegation[];
  authorities: AccountAuthorities;
};
