import { differenceInSeconds } from 'date-fns';
import { HbdAsset } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { parseChainApiDate } from 'utils/date/parseChainApiDate/parseChainApiDate';

export type SavingsInfo = {
  savingsSeconds: number;
  savingsTimeLastUpdate: string;
  savingsHbdBalance: HbdAsset;
};

const secondsInYear = 60 * 60 * 24 * 365;

export const getEstimatedSavingsReward = (savings: SavingsInfo, hbdInterestRate: number): HbdAsset => {
  const timezonedSavingsTimeLastUpdate = parseChainApiDate(savings.savingsTimeLastUpdate);

  const secondsSinceUpdate = differenceInSeconds(new Date(), timezonedSavingsTimeLastUpdate);

  const pendingSeconds = savings.savingsHbdBalance.amount * secondsSinceUpdate;
  const secondsToPayFor = savings.savingsSeconds / 1000 + pendingSeconds;

  const estimatedPayout = (secondsToPayFor / secondsInYear) * (hbdInterestRate / 10000);

  return createAsset(estimatedPayout, ChainAssetSymbol.hbd);
};
