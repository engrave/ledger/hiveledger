import { getEstimatedSavingsReward } from 'modules/hive/account/getEstimatedSavingsReward/getEstimatedSavingsReward';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

describe('getEstimatedSavingsReward', () => {
  jest.useFakeTimers().setSystemTime(new Date('2022-01-01T01:00:30Z').getTime());

  it('returns estimated savings reward', () => {
    const reward = getEstimatedSavingsReward(
      {
        savingsHbdBalance: createAsset(1000, ChainAssetSymbol.hbd),
        savingsSeconds: 1536000,
        savingsTimeLastUpdate: '2022-01-01T01:00:00',
      },
      10000,
    );

    expect(reward).toEqual({ amount: 0.001, assetSymbol: ChainAssetSymbol.hbd });
  });
});
