export const formatUsername = (name: string) => {
  return `@${name}`;
};
