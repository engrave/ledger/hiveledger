import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';

describe('formatUsername', () => {
  it('adds @ before the name', () => {
    expect(formatUsername('test')).toBe('@test');
  });
});
