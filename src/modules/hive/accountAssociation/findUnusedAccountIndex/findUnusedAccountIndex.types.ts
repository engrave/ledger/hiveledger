export type AccountUsageChecker = (paths: string[]) => Promise<boolean>;

export type FindUnusedAccountOptions = {
  maxKeyGap: number;
};
