import { accountDiscovery } from 'config/config';
import {
  AccountUsageChecker,
  FindUnusedAccountOptions,
} from 'modules/hive/accountAssociation/findUnusedAccountIndex/findUnusedAccountIndex.types';
import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { makePath } from 'modules/hive/accountDiscovery/makePath/makePath';

const defaultOptions: FindUnusedAccountOptions = {
  maxKeyGap: accountDiscovery.maxKeyGap,
};

export const findUnusedAccountIndex = async (
  checkPathUsage: AccountUsageChecker,
  options: Partial<FindUnusedAccountOptions> = {},
): Promise<number> => {
  const { maxKeyGap } = { ...defaultOptions, ...options };

  for (let accountIndex = 0; ; accountIndex += 1) {
    const paths: string[] = [];

    for (let keyIndex = 0; keyIndex < maxKeyGap; keyIndex += 1) {
      const path = makePath(SlipRole.owner, accountIndex, keyIndex);
      paths.push(path);
    }

    const isAccountUsed = await checkPathUsage(paths);

    if (!isAccountUsed) {
      return accountIndex;
    }
  }
};
