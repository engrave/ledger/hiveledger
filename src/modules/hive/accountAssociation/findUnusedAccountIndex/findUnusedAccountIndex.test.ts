import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { makePath } from 'modules/hive/accountDiscovery/makePath/makePath';
import { findUnusedAccountIndex } from './findUnusedAccountIndex';

const accountUsageMapping = {
  [makePath(SlipRole.owner, 0, 0)]: true,
  [makePath(SlipRole.owner, 1, 0)]: true,
  [makePath(SlipRole.owner, 2, 1)]: true,
};

const accountUsageChecker = async (paths: string[]) => {
  const results = paths.map((path) => accountUsageMapping[path] ?? false);
  return results.some((result) => result);
};

describe('findUnusedAccountIndex', () => {
  it('returns index of first unused account', async () => {
    const accountIndex = await findUnusedAccountIndex(accountUsageChecker);

    expect(accountIndex).toBe(3);
  });

  it('checks number of key indexes depending on the maxKeyGap parameter', async () => {
    const accountIndex = await findUnusedAccountIndex(accountUsageChecker, { maxKeyGap: 1 });

    expect(accountIndex).toBe(2);
  });
});
