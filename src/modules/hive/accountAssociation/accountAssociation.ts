export enum AssociationError {
  deviceError = 'AssociationError/DeviceError',
  genericError = 'AssociationError/GenericError',
  creatingAccountFailed = 'AssociationError/CreatingAccountFailed',
  noUnusedKeyFound = 'AssociationError/NoUnusedAccountFound',
}

export type Keychain<T = string> = {
  ownerKey: T;
  postingKey: T;
  activeKey: T;
};
