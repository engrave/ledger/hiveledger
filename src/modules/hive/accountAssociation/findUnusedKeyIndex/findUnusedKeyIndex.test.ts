import { findUnusedKeyIndex } from 'modules/hive/accountAssociation/findUnusedKeyIndex/findUnusedKeyIndex';
import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { makePath } from 'modules/hive/accountDiscovery/makePath/makePath';

const accountUsageMapping = {
  [makePath(SlipRole.owner, 0, 0)]: true,
  [makePath(SlipRole.owner, 0, 1)]: true,
  [makePath(SlipRole.owner, 0, 2)]: true,
  [makePath(SlipRole.active, 1, 0)]: true,
  [makePath(SlipRole.active, 1, 1)]: true,
  [makePath(SlipRole.active, 1, 3)]: true,
  [makePath(SlipRole.posting, 2, 0)]: true,
};

const keyUsageChecker = async (path: string) => {
  return accountUsageMapping[path] ?? false;
};

describe('findUnusedKeyIndex', () => {
  it('returns index of first unused key for given account', async () => {
    const keyIndex1 = await findUnusedKeyIndex(keyUsageChecker);
    const keyIndex2 = await findUnusedKeyIndex(keyUsageChecker, { accountIndex: 1, role: SlipRole.active });
    const keyIndex3 = await findUnusedKeyIndex(keyUsageChecker, { accountIndex: 2, role: SlipRole.posting });

    expect(keyIndex1).toBe(3);
    expect(keyIndex2).toBe(2);
    expect(keyIndex3).toBe(1);
  });
});
