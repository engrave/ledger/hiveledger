import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';

export type AccountKeyUsageChecker = (path: string) => Promise<boolean>;

export type FindUnusedKeyOptions = {
  role: SlipRole;
  accountIndex: number;
};
