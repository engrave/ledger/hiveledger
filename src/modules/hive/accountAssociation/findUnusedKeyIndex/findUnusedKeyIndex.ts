import { accountDiscovery } from 'config/config';
import {
  AccountKeyUsageChecker,
  FindUnusedKeyOptions,
} from 'modules/hive/accountAssociation/findUnusedKeyIndex/findUnusedKeyIndex.types';
import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { makePath } from 'modules/hive/accountDiscovery/makePath/makePath';

const defaultOptions: FindUnusedKeyOptions = {
  role: SlipRole.owner,
  accountIndex: 0,
};

export const findUnusedKeyIndex = async (
  checkPathUsage: AccountKeyUsageChecker,
  options: Partial<FindUnusedKeyOptions> = {},
): Promise<number | null> => {
  const { accountIndex, role } = { ...defaultOptions, ...options };

  for (let keyIndex = 0; keyIndex < accountDiscovery.maxKeySearch; keyIndex += 1) {
    const path = makePath(role, accountIndex, keyIndex);
    const isPathUsed = await checkPathUsage(path);

    if (!isPathUsed) {
      return keyIndex;
    }
  }

  return null;
};
