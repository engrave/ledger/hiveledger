import { DeviceAccount } from 'context/association/associationContext/associationContext.types';
import { Keychain } from 'modules/hive/accountAssociation/accountAssociation';

export type SelectedKeys = {
  ownerKey: boolean;
  activeKey: boolean;
  postingKey: boolean;
};

export type PickedKeys = Partial<Keychain>;

export const pickDeviceAccountKeys = (pickedKeys: SelectedKeys, deviceAccount: DeviceAccount): PickedKeys => {
  const keys: PickedKeys = {};

  if (pickedKeys.ownerKey) {
    keys.ownerKey = deviceAccount.ownerKey.publicKey;
  }
  if (pickedKeys.activeKey) {
    keys.activeKey = deviceAccount.activeKey.publicKey;
  }
  if (pickedKeys.postingKey) {
    keys.postingKey = deviceAccount.postingKey.publicKey;
  }

  return keys;
};
