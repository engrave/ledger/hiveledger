import { DeviceAccount } from 'context/association/associationContext/associationContext.types';
import { pickDeviceAccountKeys } from 'modules/hive/accountAssociation/pickDeviceAccountKeys/pickDeviceAccountKeys';

describe('pickDeviceAccountKeys', () => {
  it('properly picks selected keys', () => {
    const deviceAccount: DeviceAccount = {
      ownerKey: { publicKey: 'OWNER_KEY', path: 'path_1' },
      activeKey: { publicKey: 'ACTIVE_KEY', path: 'path_2' },
      postingKey: { publicKey: 'POSTING_KEY', path: 'path_3' },
      accountIndex: 1,
    };

    const pickOwnerKey = { ownerKey: true, activeKey: false, postingKey: false };
    const pickActiveKey = { ownerKey: false, activeKey: true, postingKey: false };
    const pickPostingKey = { ownerKey: false, activeKey: false, postingKey: true };

    const pickOwnerAndPostingKey = { ownerKey: true, activeKey: false, postingKey: true };
    const pickOwnerAndActiveKey = { ownerKey: true, activeKey: true, postingKey: false };
    const pickPostingAndActiveKey = { ownerKey: false, activeKey: true, postingKey: true };

    const pickAllKeys = { ownerKey: true, activeKey: true, postingKey: true };

    expect(pickDeviceAccountKeys(pickOwnerKey, deviceAccount)).toEqual({
      ownerKey: 'OWNER_KEY',
    });
    expect(pickDeviceAccountKeys(pickActiveKey, deviceAccount)).toEqual({
      activeKey: 'ACTIVE_KEY',
    });
    expect(pickDeviceAccountKeys(pickPostingKey, deviceAccount)).toEqual({
      postingKey: 'POSTING_KEY',
    });

    expect(pickDeviceAccountKeys(pickOwnerAndPostingKey, deviceAccount)).toEqual({
      ownerKey: 'OWNER_KEY',
      postingKey: 'POSTING_KEY',
    });
    expect(pickDeviceAccountKeys(pickOwnerAndActiveKey, deviceAccount)).toEqual({
      ownerKey: 'OWNER_KEY',
      activeKey: 'ACTIVE_KEY',
    });
    expect(pickDeviceAccountKeys(pickPostingAndActiveKey, deviceAccount)).toEqual({
      postingKey: 'POSTING_KEY',
      activeKey: 'ACTIVE_KEY',
    });

    expect(pickDeviceAccountKeys(pickAllKeys, deviceAccount)).toEqual({
      ownerKey: 'OWNER_KEY',
      postingKey: 'POSTING_KEY',
      activeKey: 'ACTIVE_KEY',
    });
  });
});
