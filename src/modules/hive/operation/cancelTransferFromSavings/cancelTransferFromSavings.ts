import { Operation, OperationType } from 'modules/hive/operation/operation';

export type CancelTransferFromSavingsPayload = {
  from: string;
  requestId: number;
};

export const cancelTransferFromSavings = (payload: CancelTransferFromSavingsPayload): Operation => {
  return [
    OperationType.cancelTransferFromSavings,
    {
      from: payload.from,
      request_id: payload.requestId,
    },
  ];
};
