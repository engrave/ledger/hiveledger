import { cancelTransferFromSavings } from 'modules/hive/operation/cancelTransferFromSavings/cancelTransferFromSavings';

describe('cancelTransferFromSavings', () => {
  it('create a cancel transfer from savings operation', () => {
    const operation = cancelTransferFromSavings({
      from: 'lorem',
      requestId: 1647547829,
    });

    expect(operation).toEqual([
      'cancel_transfer_from_savings',
      {
        from: 'lorem',
        request_id: 1647547829,
      },
    ]);
  });
});
