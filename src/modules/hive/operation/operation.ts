export enum OperationType {
  transfer = 'transfer',
  transferToVesting = 'transfer_to_vesting',
  delegateVestingShares = 'delegate_vesting_shares',
  withdrawVesting = 'withdraw_vesting',
  transferToSavings = 'transfer_to_savings',
  transferFromSavings = 'transfer_from_savings',
  cancelTransferFromSavings = 'cancel_transfer_from_savings',
  interest = 'interest',
  fillTransferFromSavings = 'fill_transfer_from_savings',
  fillConvertRequest = 'fill_convert_request',
  claimRewardBalance = 'claim_reward_balance',
  accountUpdate = 'account_update',
  accountWitnessVote = 'account_witness_vote',
  accountWitnessProxy = 'account_witness_proxy',
  updateProposalVotes = 'update_proposal_votes',
  custom = 'custom_json',
}

export type Operation = [OperationType, Record<string, unknown>];

export const isSingleOperation = (data: Operation | Operation[]): data is Operation => {
  return typeof data[0] === 'string' && typeof data[1] === 'object' && data[1] !== null;
};

export type Transaction = {
  ref_block_num: number;
  ref_block_prefix: number;
  expiration: string;
  operations: Operation[];
  extensions: unknown[];
};
