import { HbdAsset, HiveAsset } from 'modules/hive/asset/asset';
import { printAsset } from 'modules/hive/asset/printAsset/printAsset';
import { Operation, OperationType } from 'modules/hive/operation/operation';

export type TransferFromSavingsPayload = {
  username: string;
  amount: HiveAsset | HbdAsset;
};

export const transferFromSavings = (payload: TransferFromSavingsPayload): Operation => {
  return [
    OperationType.transferFromSavings,
    {
      from: payload.username,
      to: payload.username,
      request_id: Math.floor(Date.now() / 1000),
      amount: printAsset(payload.amount),
      memo: '',
    },
  ];
};
