import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { transferFromSavings } from 'modules/hive/operation/transferFromSavings/transferFromSavings';

describe('transferFromSavings', () => {
  it('create a transfer from savings operation', () => {
    jest.useFakeTimers().setSystemTime(1647547829726);

    const operation = transferFromSavings({
      username: 'lorem',
      amount: createAsset(123, ChainAssetSymbol.hbd),
    });

    expect(operation).toEqual([
      'transfer_from_savings',
      {
        from: 'lorem',
        to: 'lorem',
        request_id: 1647547829,
        amount: `123.000 ${ChainAssetSymbol.hbd}`,
        memo: '',
      },
    ]);
  });
});
