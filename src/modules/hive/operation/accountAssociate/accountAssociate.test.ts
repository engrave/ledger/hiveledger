import { accountAssociate } from 'modules/hive/operation/accountAssociate/accountAssociate';

describe('accountAssociate', () => {
  it('create an account update operation', () => {
    const operation = accountAssociate({
      username: 'lorem',
      memoKey: '??????????',
      publicKeys: {
        ownerKey: '1234567890',
        activeKey: 'qwertyuiop',
        postingKey: '!@#$%^&*()',
      },
    });

    expect(operation).toEqual([
      'account_update',
      {
        account: 'lorem',
        owner: {
          weight_threshold: 1,
          account_auths: [],
          key_auths: [['1234567890', 1]],
        },
        active: {
          weight_threshold: 1,
          account_auths: [],
          key_auths: [['qwertyuiop', 1]],
        },
        posting: {
          weight_threshold: 1,
          account_auths: [],
          key_auths: [['!@#$%^&*()', 1]],
        },
        memo_key: '??????????',
        json_metadata: '',
      },
    ]);
  });

  it('updates only given keys', () => {
    const operation1 = accountAssociate({
      username: 'lorem',
      memoKey: '??????????',
      publicKeys: {
        ownerKey: '1234567890',
      },
    });

    expect(operation1).toEqual([
      'account_update',
      {
        account: 'lorem',
        owner: {
          weight_threshold: 1,
          account_auths: [],
          key_auths: [['1234567890', 1]],
        },
        memo_key: '??????????',
        json_metadata: '',
      },
    ]);
  });

  const operation2 = accountAssociate({
    username: 'lorem',
    memoKey: '??????????',
    publicKeys: {
      activeKey: 'qwertyuiop',
    },
  });

  expect(operation2).toEqual([
    'account_update',
    {
      account: 'lorem',
      active: {
        weight_threshold: 1,
        account_auths: [],
        key_auths: [['qwertyuiop', 1]],
      },
      memo_key: '??????????',
      json_metadata: '',
    },
  ]);

  const operation3 = accountAssociate({
    username: 'lorem',
    memoKey: '??????????',
    publicKeys: {
      postingKey: '!@#$%^&*()',
    },
  });

  expect(operation3).toEqual([
    'account_update',
    {
      account: 'lorem',
      posting: {
        weight_threshold: 1,
        account_auths: [],
        key_auths: [['!@#$%^&*()', 1]],
      },
      memo_key: '??????????',
      json_metadata: '',
    },
  ]);
});
