import { Authority } from '@hiveio/dhive/lib/chain/account';
import { Keychain } from 'modules/hive/accountAssociation/accountAssociation';
import { Operation, OperationType } from 'modules/hive/operation/operation';

export type AuthorityFieldsSet = {
  owner?: Authority;
  active?: Authority;
  posting?: Authority;
};

export type AccountAssociatePayload = {
  username: string;
  publicKeys: Partial<Keychain>;
  memoKey: string;
};

export const accountAssociate = (payload: AccountAssociatePayload): Operation => {
  const authorityFields: AuthorityFieldsSet = {};

  if (payload.publicKeys.ownerKey !== undefined) {
    authorityFields.owner = {
      weight_threshold: 1,
      account_auths: [],
      key_auths: [[payload.publicKeys.ownerKey, 1]],
    };
  }

  if (payload.publicKeys.activeKey !== undefined) {
    authorityFields.active = {
      weight_threshold: 1,
      account_auths: [],
      key_auths: [[payload.publicKeys.activeKey, 1]],
    };
  }

  if (payload.publicKeys.postingKey !== undefined) {
    authorityFields.posting = {
      weight_threshold: 1,
      account_auths: [],
      key_auths: [[payload.publicKeys.postingKey, 1]],
    };
  }

  return [
    OperationType.accountUpdate,
    {
      account: payload.username,
      ...authorityFields,
      memo_key: payload.memoKey,
      json_metadata: '',
    },
  ];
};
