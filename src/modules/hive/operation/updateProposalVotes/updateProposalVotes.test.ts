import { updateProposalVotes } from 'modules/hive/operation/updateProposalVotes/updateProposalVotes';

describe('updateProposalVotes', () => {
  it('creates a a update proposal votes operation', () => {
    const operation = updateProposalVotes({
      voter: 'lorem',
      approve: true,
      proposalId: 404,
    });

    expect(operation).toStrictEqual([
      'update_proposal_votes',
      {
        voter: 'lorem',
        proposal_ids: [404],
        approve: true,
        extensions: [],
      },
    ]);
  });
});
