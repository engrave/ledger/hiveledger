import { Operation, OperationType } from 'modules/hive/operation/operation';

export type UpdateProposalVotesPayload = {
  voter: string;
  proposalId: number;
  approve: boolean;
};

export const updateProposalVotes = (payload: UpdateProposalVotesPayload): Operation => {
  return [
    OperationType.updateProposalVotes,
    {
      voter: payload.voter,
      proposal_ids: [payload.proposalId],
      approve: payload.approve,
      extensions: [],
    },
  ];
};
