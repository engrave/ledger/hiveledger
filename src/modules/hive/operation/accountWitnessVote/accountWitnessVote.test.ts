import { accountWitnessVote } from 'modules/hive/operation/accountWitnessVote/accountWitnessVote';

describe('accountWitnessVote', () => {
  it('creates an account witness vote operation', () => {
    const giveVoteOperation = accountWitnessVote({
      witness: 'lorem',
      account: 'ipsum',
      approve: true,
    });

    expect(giveVoteOperation).toEqual([
      'account_witness_vote',
      {
        witness: 'lorem',
        account: 'ipsum',
        approve: true,
      },
    ]);

    const takeVoteOperation = accountWitnessVote({
      witness: 'lorem',
      account: 'ipsum',
      approve: false,
    });

    expect(takeVoteOperation).toEqual([
      'account_witness_vote',
      {
        witness: 'lorem',
        account: 'ipsum',
        approve: false,
      },
    ]);
  });
});
