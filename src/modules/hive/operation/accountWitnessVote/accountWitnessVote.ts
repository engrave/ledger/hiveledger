import { Operation, OperationType } from 'modules/hive/operation/operation';

export type AccountWitnessVotePayload = {
  account: string;
  witness: string;
  approve: boolean;
};

export const accountWitnessVote = (payload: AccountWitnessVotePayload): Operation => {
  return [
    OperationType.accountWitnessVote,
    {
      account: payload.account,
      witness: payload.witness,
      approve: payload.approve,
    },
  ];
};
