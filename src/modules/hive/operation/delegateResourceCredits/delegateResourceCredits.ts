import { Operation, OperationType } from 'modules/hive/operation/operation';

export type DelegateResourceCreditsPayload = {
  delegator: string;
  delegatee: string;
  resourceCredits: number;
};

export const delegateResourceCredits = (payload: DelegateResourceCreditsPayload): Operation => {
  return [
    OperationType.custom,
    {
      required_auths: [],
      required_posting_auths: [payload.delegator],
      id: 'rc',
      json: JSON.stringify([
        'delegate_rc',
        {
          max_rc: payload.resourceCredits,
          delegatees: [payload.delegatee],
          from: payload.delegator,
        },
      ]),
    },
  ];
};
