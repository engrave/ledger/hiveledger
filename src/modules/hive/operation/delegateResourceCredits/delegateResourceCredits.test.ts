import { delegateResourceCredits } from 'modules/hive/operation/delegateResourceCredits/delegateResourceCredits';

describe('delegateResourceCredits', () => {
  it('create a resource credits delegation operation', () => {
    const operation = delegateResourceCredits({
      delegatee: 'lorem',
      delegator: 'ipsum',
      resourceCredits: 500,
    });

    expect(operation).toEqual([
      'custom_json',
      {
        required_auths: [],
        required_posting_auths: ['ipsum'],
        id: 'rc',
        json: JSON.stringify([
          'delegate_rc',
          {
            max_rc: 500,
            delegatees: ['lorem'],
            from: 'ipsum',
          },
        ]),
      },
    ]);
  });
});
