import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { withdrawVesting } from 'modules/hive/operation/withdrawVesting/withdrawVesting';

describe('withdrawVesting', () => {
  it('creates a vesting withdraw operation', () => {
    const operation = withdrawVesting({
      account: 'ipsum',
      vestingShares: createAsset(123, ChainAssetSymbol.vests),
    });

    expect(operation).toEqual([
      'withdraw_vesting',
      {
        account: 'ipsum',
        vesting_shares: `123.000000 ${ChainAssetSymbol.vests}`,
      },
    ]);
  });
});
