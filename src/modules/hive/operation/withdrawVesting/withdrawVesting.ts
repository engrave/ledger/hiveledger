import { VestsAsset } from 'modules/hive/asset/asset';
import { printAsset } from 'modules/hive/asset/printAsset/printAsset';
import { Operation, OperationType } from 'modules/hive/operation/operation';

export type WithdrawVestingPayload = {
  account: string;
  vestingShares: VestsAsset;
};

export const withdrawVesting = (payload: WithdrawVestingPayload): Operation => {
  return [
    OperationType.withdrawVesting,
    {
      account: payload.account,
      vesting_shares: printAsset(payload.vestingShares),
    },
  ];
};
