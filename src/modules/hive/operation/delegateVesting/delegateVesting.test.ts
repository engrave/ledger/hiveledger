import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { delegateVesting } from 'modules/hive/operation/delegateVesting/delegateVesting';

describe('delegateVesting', () => {
  it('create a vesting delegation operation', () => {
    const operation = delegateVesting({
      delegatee: 'lorem',
      delegator: 'ipsum',
      vestingShares: createAsset(123, ChainAssetSymbol.vests),
    });

    expect(operation).toEqual([
      'delegate_vesting_shares',
      {
        delegatee: 'lorem',
        delegator: 'ipsum',
        vesting_shares: `123.000000 ${ChainAssetSymbol.vests}`,
      },
    ]);
  });
});
