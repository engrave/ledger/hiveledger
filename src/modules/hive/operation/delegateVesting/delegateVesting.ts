import { VestsAsset } from 'modules/hive/asset/asset';
import { printAsset } from 'modules/hive/asset/printAsset/printAsset';
import { Operation, OperationType } from 'modules/hive/operation/operation';

export type DelegateVestingPayload = {
  delegator: string;
  delegatee: string;
  vestingShares: VestsAsset;
};

export const delegateVesting = (payload: DelegateVestingPayload): Operation => {
  return [
    OperationType.delegateVestingShares,
    {
      delegator: payload.delegator,
      delegatee: payload.delegatee,
      vesting_shares: printAsset(payload.vestingShares),
    },
  ];
};
