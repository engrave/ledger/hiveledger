import { AuthorityType } from 'modules/hive/account/account';
import { authoritiesUpdate } from 'modules/hive/operation/authoritiesUpdate/authoritiesUpdate';

describe('authoritiesUpdate', () => {
  it('create an authorities update operation with sorted items', () => {
    const operation = authoritiesUpdate({
      username: 'lorem',
      memoKey: '??????????',
      type: AuthorityType.owner,
      authorities: {
        threshold: 5,
        accountAuths: [
          { value: 'lorem', weight: 1 },
          { value: 'ipsum', weight: 2 },
        ],
        keyAuths: [
          { value: 'a', weight: 4 },
          { value: 'b', weight: 5 },
          { value: 'bb', weight: 8 },
          { value: 'Bb', weight: 3 },
          { value: 'ba', weight: 7 },
          { value: 'BA', weight: 2 },
          { value: 'B', weight: 1 },
          { value: 'bA', weight: 6 },
        ],
      },
    });

    expect(operation).toEqual([
      'account_update',
      {
        account: 'lorem',
        owner: {
          weight_threshold: 5,
          account_auths: [
            ['ipsum', 2],
            ['lorem', 1],
          ],
          key_auths: [
            ['B', 1],
            ['BA', 2],
            ['Bb', 3],
            ['a', 4],
            ['b', 5],
            ['bA', 6],
            ['ba', 7],
            ['bb', 8],
          ],
        },
        memo_key: '??????????',
        json_metadata: '',
      },
    ]);
  });
});
