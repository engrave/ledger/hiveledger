import { AuthoritiesSet, AuthorityEntry, AuthorityType } from 'modules/hive/account/account';
import { Operation, OperationType } from 'modules/hive/operation/operation';
import { chainStringSort } from 'utils/string/chainStringSort/chainStringSort';

export type AuthoritiesUpdatePayload = {
  username: string;
  type: AuthorityType;
  authorities: AuthoritiesSet;
  memoKey: string;
};

const formatAuthorities = (authorities: AuthorityEntry[]) => {
  return authorities
    .sort((authA, authB) => {
      return chainStringSort(authA.value, authB.value);
    })
    .map((auth) => {
      return [auth.value, auth.weight];
    });
};

export const authoritiesUpdate = (payload: AuthoritiesUpdatePayload): Operation => {
  return [
    OperationType.accountUpdate,
    {
      account: payload.username,
      [payload.type]: {
        weight_threshold: payload.authorities.threshold,
        account_auths: formatAuthorities(payload.authorities.accountAuths),
        key_auths: formatAuthorities(payload.authorities.keyAuths),
      },
      memo_key: payload.memoKey,
      json_metadata: '',
    },
  ];
};
