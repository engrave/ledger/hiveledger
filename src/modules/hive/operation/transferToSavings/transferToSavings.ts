import { HbdAsset, HiveAsset } from 'modules/hive/asset/asset';
import { printAsset } from 'modules/hive/asset/printAsset/printAsset';
import { Operation, OperationType } from 'modules/hive/operation/operation';

export type TransferToSavingsPayload = {
  username: string;
  amount: HiveAsset | HbdAsset;
};

export const transferToSavings = (payload: TransferToSavingsPayload): Operation => {
  return [
    OperationType.transferToSavings,
    {
      from: payload.username,
      to: payload.username,
      amount: printAsset(payload.amount),
      memo: '',
    },
  ];
};
