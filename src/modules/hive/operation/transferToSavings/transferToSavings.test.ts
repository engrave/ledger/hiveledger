import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { transferToSavings } from 'modules/hive/operation/transferToSavings/transferToSavings';

describe('transferToSavings', () => {
  it('create a transfer to savings operation', () => {
    const operation = transferToSavings({
      username: 'lorem',
      amount: createAsset(123, ChainAssetSymbol.hbd),
    });

    expect(operation).toEqual([
      'transfer_to_savings',
      {
        from: 'lorem',
        to: 'lorem',
        amount: `123.000 ${ChainAssetSymbol.hbd}`,
        memo: '',
      },
    ]);
  });
});
