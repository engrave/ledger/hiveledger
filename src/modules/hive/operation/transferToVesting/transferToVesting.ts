import { HiveAsset } from 'modules/hive/asset/asset';
import { printAsset } from 'modules/hive/asset/printAsset/printAsset';
import { Operation, OperationType } from 'modules/hive/operation/operation';

export type TransferToVestingPayload = {
  from: string;
  amount: HiveAsset;
};

export const transferToVesting = (payload: TransferToVestingPayload): Operation => {
  return [
    OperationType.transferToVesting,
    {
      from: payload.from,
      to: payload.from,
      amount: printAsset(payload.amount),
    },
  ];
};
