import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { transferToVesting } from 'modules/hive/operation/transferToVesting/transferToVesting';

describe('transferToVesting', () => {
  it('create a transfer to vesting operation', () => {
    const operation = transferToVesting({
      from: 'lorem',
      amount: createAsset(123, ChainAssetSymbol.hive),
    });

    expect(operation).toEqual([
      'transfer_to_vesting',
      {
        from: 'lorem',
        to: 'lorem',
        amount: `123.000 ${ChainAssetSymbol.hive}`,
      },
    ]);
  });
});
