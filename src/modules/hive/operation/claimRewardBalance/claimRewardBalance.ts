import { HbdAsset, HiveAsset, VestsAsset } from 'modules/hive/asset/asset';
import { printAsset } from 'modules/hive/asset/printAsset/printAsset';
import { Operation, OperationType } from 'modules/hive/operation/operation';

export type ClaimRewardBalancePayload = {
  account: string;
  hiveReward: HiveAsset;
  hbdReward: HbdAsset;
  vestsReward: VestsAsset;
};

export const claimRewardBalance = (payload: ClaimRewardBalancePayload): Operation => {
  return [
    OperationType.claimRewardBalance,
    {
      account: payload.account,
      reward_hive: printAsset(payload.hiveReward),
      reward_hbd: printAsset(payload.hbdReward),
      reward_vests: printAsset(payload.vestsReward),
    },
  ];
};
