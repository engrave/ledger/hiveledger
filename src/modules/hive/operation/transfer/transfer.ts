import { HbdAsset, HiveAsset } from 'modules/hive/asset/asset';
import { printAsset } from 'modules/hive/asset/printAsset/printAsset';
import { Operation, OperationType } from 'modules/hive/operation/operation';

export type TransferPayload = {
  from: string;
  to: string;
  amount: HiveAsset | HbdAsset;
  memo: string;
};

export const transfer = (payload: TransferPayload): Operation => {
  return [
    OperationType.transfer,
    {
      from: payload.from,
      to: payload.to,
      amount: printAsset(payload.amount),
      memo: payload.memo,
    },
  ];
};
