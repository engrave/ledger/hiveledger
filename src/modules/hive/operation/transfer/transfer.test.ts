import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { transfer } from 'modules/hive/operation/transfer/transfer';

describe('transfer', () => {
  it('create a transfer to vesting operation', () => {
    const operation = transfer({
      from: 'lorem',
      to: 'ipsum',
      amount: createAsset(123, ChainAssetSymbol.hive),
      memo: 'Hello World',
    });

    expect(operation).toEqual([
      'transfer',
      {
        from: 'lorem',
        to: 'ipsum',
        amount: `123.000 ${ChainAssetSymbol.hive}`,
        memo: 'Hello World',
      },
    ]);
  });
});
