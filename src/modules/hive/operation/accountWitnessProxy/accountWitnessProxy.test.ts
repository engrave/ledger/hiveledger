import { accountWitnessProxy } from 'modules/hive/operation/accountWitnessProxy/accountWitnessProxy';

describe('accountWitnessProxy', () => {
  it('create an account witness proxy operation', () => {
    const operation = accountWitnessProxy({
      account: 'lorem',
      proxy: 'ipsum',
    });

    expect(operation).toEqual([
      'account_witness_proxy',
      {
        account: 'lorem',
        proxy: 'ipsum',
      },
    ]);
  });
});
