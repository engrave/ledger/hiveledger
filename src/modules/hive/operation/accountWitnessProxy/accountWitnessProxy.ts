import { Operation, OperationType } from 'modules/hive/operation/operation';

export type AccountWitnessProxyPayload = {
  account: string;
  proxy: string;
};

export const accountWitnessProxy = (payload: AccountWitnessProxyPayload): Operation => {
  return [
    OperationType.accountWitnessProxy,
    {
      account: payload.account,
      proxy: payload.proxy,
    },
  ];
};
