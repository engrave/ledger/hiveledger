import { Asset, AssetError } from 'modules/hive/asset/asset';
import { subtractAsset, subtractMultipleAssets } from 'modules/hive/asset/subtractAsset/subtractAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

const assetHiveA: Asset = {
  amount: 500.555,
  assetSymbol: ChainAssetSymbol.hive,
};
const assetHiveB: Asset = {
  amount: 225.755,
  assetSymbol: ChainAssetSymbol.hive,
};
const assetBitcoin: Asset = {
  amount: 222,
  assetSymbol: ExternalAssetSymbol.btc,
};

describe('subtractAsset', () => {
  it('properly subtracts two assets of the same type', () => {
    expect(subtractAsset(assetHiveA, assetHiveB)).toEqual({
      amount: 274.8,
      assetSymbol: ChainAssetSymbol.hive,
    });
  });

  it('throws when subtracting different asset types', () => {
    expect(() => subtractAsset(assetHiveA, assetBitcoin)).toThrow(AssetError.invalidOperation);
  });
});

describe('subtractMultipleAssets', () => {
  it('properly subtracts multiple assets of the same type', () => {
    expect(subtractMultipleAssets(assetHiveA, assetHiveB, assetHiveB)).toEqual({
      amount: 49.045,
      assetSymbol: ChainAssetSymbol.hive,
    });
  });

  it('throws when one of the asset is of different type', () => {
    expect(() => subtractMultipleAssets(assetHiveA, assetHiveB, assetBitcoin, assetHiveB)).toThrow(
      AssetError.invalidOperation,
    );
  });
});
