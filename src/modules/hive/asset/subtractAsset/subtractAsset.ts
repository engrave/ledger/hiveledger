import { Asset, AssetError } from 'modules/hive/asset/asset';
import { AssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';
import { getPrecision } from 'modules/hive/assetSymbol/getPrecision/getPrecision';

export const subtractAsset = <S extends AssetSymbol>(assetA: Asset<S>, assetB: Asset<S>): Asset<S> => {
  if (assetA.assetSymbol !== assetB.assetSymbol) {
    throw new Error(AssetError.invalidOperation);
  }

  const precision = getPrecision(assetA.assetSymbol);

  return {
    amount: Number((assetA.amount - assetB.amount).toFixed(precision)),
    assetSymbol: assetA.assetSymbol,
  };
};

export const subtractMultipleAssets = <S extends AssetSymbol>(minuend: Asset<S>, ...subtrahends: Asset<S>[]) => {
  if (subtrahends.some((asset) => asset.assetSymbol !== minuend.assetSymbol)) {
    throw new Error(AssetError.invalidOperation);
  }

  let result = { ...minuend };

  for (const asset of subtrahends) {
    result = subtractAsset(result, asset);
  }

  return result;
};
