import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { getLowestAsset } from 'modules/hive/asset/getLowestAsset/getLowestAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

describe('getLowestAsset', () => {
  it('should return the highest asset', () => {
    const assetA = createAsset(100, ChainAssetSymbol.hive);
    const assetB = createAsset(300, ChainAssetSymbol.hive);
    const assetC = createAsset(300, ChainAssetSymbol.hive);

    expect(getLowestAsset(assetA, assetB, assetC)).toStrictEqual(assetA);
    expect(getLowestAsset(assetA, assetC, assetB)).toStrictEqual(assetA);
    expect(getLowestAsset(assetB, assetA, assetC)).toStrictEqual(assetA);
    expect(getLowestAsset(assetB, assetC, assetA)).toStrictEqual(assetA);
    expect(getLowestAsset(assetC, assetA, assetB)).toStrictEqual(assetA);
    expect(getLowestAsset(assetC, assetB, assetA)).toStrictEqual(assetA);
  });
});
