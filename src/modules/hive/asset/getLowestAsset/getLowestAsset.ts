import { Asset, AssetError } from 'modules/hive/asset/asset';
import { compareAssets } from 'modules/hive/asset/compareAssets/compareAssets';
import { AssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';

export const getLowestAsset = <S extends AssetSymbol>(...assets: Asset<S>[]) => {
  if (assets.length === 0) {
    throw new Error(AssetError.invalidOperation);
  }

  if (assets.some((asset) => asset.assetSymbol !== assets[0].assetSymbol)) {
    throw new Error(AssetError.invalidSymbol);
  }

  const sortedAssets = assets.sort((assetA, assetB) => {
    return compareAssets(assetA, assetB);
  });

  return sortedAssets[0];
};
