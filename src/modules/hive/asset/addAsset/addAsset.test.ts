import { addAsset } from 'modules/hive/asset/addAsset/addAsset';
import { Asset, AssetError } from 'modules/hive/asset/asset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

const assetHiveA: Asset = {
  amount: 1234.999,
  assetSymbol: ChainAssetSymbol.hive,
};
const assetHiveB: Asset = {
  amount: 4321.001,
  assetSymbol: ChainAssetSymbol.hive,
};
const assetBitcoin: Asset = {
  amount: 222,
  assetSymbol: ExternalAssetSymbol.btc,
};

describe('addAsset', () => {
  it('properly sums two assets of the same type', () => {
    expect(addAsset(assetHiveA, assetHiveB)).toEqual({
      amount: 5556,
      assetSymbol: ChainAssetSymbol.hive,
    });
  });

  it('throws when summing different asset types', () => {
    expect(() => addAsset(assetHiveA, assetBitcoin)).toThrow(AssetError.invalidOperation);
  });
});
