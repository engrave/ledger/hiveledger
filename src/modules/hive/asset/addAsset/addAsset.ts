import { Asset, AssetError } from 'modules/hive/asset/asset';
import { createEmptyAsset } from 'modules/hive/asset/createAsset/createAsset';
import { AssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';

export const addAsset = <S extends AssetSymbol>(assetA: Asset<S>, assetB: Asset<S>) => {
  if (assetA.assetSymbol !== assetB.assetSymbol) {
    throw new Error(AssetError.invalidOperation);
  }

  return {
    amount: assetA.amount + assetB.amount,
    assetSymbol: assetA.assetSymbol,
  };
};

export const addMultipleAssets = <S extends AssetSymbol>(...assetsList: Asset<S>[]) => {
  if (assetsList.some((asset) => asset.assetSymbol !== assetsList[0].assetSymbol)) {
    throw new Error(AssetError.invalidOperation);
  }

  let assetsSum = createEmptyAsset(assetsList[0].assetSymbol);

  for (const asset of assetsList) {
    assetsSum = addAsset(assetsSum, asset);
  }

  return assetsSum;
};
