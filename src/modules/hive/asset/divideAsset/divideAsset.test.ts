import { AssetError } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { divideAsset } from 'modules/hive/asset/divideAsset/divideAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

describe('divider', () => {
  it('calculates asset divided by a number', () => {
    const hiveSource = createAsset(100, ChainAssetSymbol.hive);
    const hiveTarget = divideAsset(hiveSource, 2);

    expect(hiveTarget).toStrictEqual({ amount: 50, assetSymbol: ChainAssetSymbol.hive });

    const usdSource = createAsset(100, ExternalAssetSymbol.usd);
    const usdTarget = divideAsset(usdSource, 2);

    expect(usdTarget).toStrictEqual({ amount: 50, assetSymbol: ExternalAssetSymbol.usd });
  });

  it('throws error when dividing by zero', () => {
    const source = createAsset(100, ChainAssetSymbol.hive);

    expect(() => divideAsset(source, 0)).toThrow(AssetError.invalidOperation);
  });
});
