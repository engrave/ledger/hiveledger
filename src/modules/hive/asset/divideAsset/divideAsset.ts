import { Asset, AssetError } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { AssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';

export const divideAsset = <S extends AssetSymbol>(asset: Asset<S>, divider: number): Asset<S> => {
  if (divider === 0) {
    throw new Error(AssetError.invalidOperation);
  }

  return createAsset(asset.amount / divider, asset.assetSymbol);
};
