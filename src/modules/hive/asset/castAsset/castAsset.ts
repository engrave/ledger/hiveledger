import { Asset } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { AssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';

export const castAsset = <T extends AssetSymbol>(asset: Asset, targetAssetSymbol: T): Asset<T> => {
  return createAsset(asset.amount, targetAssetSymbol);
};
