import { castAsset } from 'modules/hive/asset/castAsset/castAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

describe('castAsset', () => {
  it('returns asset with the same amount and give symbol', () => {
    const hp = { amount: 45.678, assetSymbol: ExternalAssetSymbol.hp } as const;

    expect(castAsset(hp, ChainAssetSymbol.hive)).toEqual({
      amount: 45.678,
      assetSymbol: ChainAssetSymbol.hive,
    });
  });
});
