import { Asset } from 'modules/hive/asset/asset';
import { getPrecision } from 'modules/hive/assetSymbol/getPrecision/getPrecision';

export const getRoundedAmount = (asset: Asset): number => {
  const precision = getPrecision(asset.assetSymbol);

  return Number(asset.amount.toFixed(precision));
};
