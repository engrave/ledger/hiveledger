import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { getRoundedAmount } from 'modules/hive/asset/getRoundedAmount/getRoundedAmount';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

describe('getRoundedAmount', () => {
  it('should round the amount to precision based on the symbol', () => {
    const hive = createAsset(123.123456789, ChainAssetSymbol.hive);
    const hbd = createAsset(123.123456789, ChainAssetSymbol.hbd);
    const vests = createAsset(123.123456789, ChainAssetSymbol.vests);

    const usd = createAsset(123.123456789, ExternalAssetSymbol.usd);
    const btc = createAsset(123.123456789, ExternalAssetSymbol.btc);
    const hp = createAsset(123.123456789, ExternalAssetSymbol.hp);

    expect(getRoundedAmount(hive)).toBe(123.123);
    expect(getRoundedAmount(hbd)).toBe(123.123);
    expect(getRoundedAmount(vests)).toBe(123.123457);

    expect(getRoundedAmount(usd)).toBe(123.12);
    expect(getRoundedAmount(btc)).toBe(123.12345679);
    expect(getRoundedAmount(hp)).toBe(123.123);
  });
});
