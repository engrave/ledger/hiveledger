import { printAsset } from 'modules/hive/asset/printAsset/printAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

const { hive, hbd, vests } = ChainAssetSymbol;

describe('printAsset', () => {
  it('formats asset to API format', () => {
    const hiveAsset = { amount: 234.005005005, assetSymbol: ChainAssetSymbol.hive };

    expect(printAsset(hiveAsset)).toBe(`234.005 ${hive}`);

    const hbdAsset = { amount: 234.0, assetSymbol: ChainAssetSymbol.hbd };

    expect(printAsset(hbdAsset)).toBe(`234.000 ${hbd}`);

    const vestsAsset = { amount: 234.01, assetSymbol: ChainAssetSymbol.vests };

    expect(printAsset(vestsAsset)).toBe(`234.010000 ${vests}`);

    const usdAsset = { amount: 234.01, assetSymbol: ExternalAssetSymbol.usd };

    expect(printAsset(usdAsset)).toBe('234.01 USD');

    const btcAsset = { amount: 234.01, assetSymbol: ExternalAssetSymbol.btc };

    expect(printAsset(btcAsset)).toBe('234.01000000 BTC');

    const hpAsset = { amount: 234.01, assetSymbol: ExternalAssetSymbol.hp };

    expect(printAsset(hpAsset)).toBe('234.010 HP');
  });

  it('allows to overwrite the precision', () => {
    const hiveAsset = { amount: 234.005005005, assetSymbol: ChainAssetSymbol.hive };

    expect(printAsset(hiveAsset, 0)).toBe(`234 ${hive}`);

    const hbdAsset = { amount: 234.0, assetSymbol: ChainAssetSymbol.hbd };

    expect(printAsset(hbdAsset, 1)).toBe(`234.0 ${hbd}`);

    const vestsAsset = { amount: 234.01, assetSymbol: ChainAssetSymbol.vests };

    expect(printAsset(vestsAsset, 2)).toBe(`234.01 ${vests}`);

    const usdAsset = { amount: 234.01, assetSymbol: ExternalAssetSymbol.usd };

    expect(printAsset(usdAsset, 3)).toBe('234.010 USD');

    const btcAsset = { amount: 234.01, assetSymbol: ExternalAssetSymbol.btc };

    expect(printAsset(btcAsset, 4)).toBe('234.0100 BTC');

    const hpAsset = { amount: 234.01, assetSymbol: ExternalAssetSymbol.hp };

    expect(printAsset(hpAsset, 5)).toBe('234.01000 HP');
  });
});
