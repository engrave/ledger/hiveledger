import { Asset } from 'modules/hive/asset/asset';
import { getPrecision } from 'modules/hive/assetSymbol/getPrecision/getPrecision';

export const printAsset = (asset: Asset, forcedPrecision: number | null = null) => {
  const precision = forcedPrecision ?? getPrecision(asset.assetSymbol);
  const roundedAmount = asset.amount.toFixed(precision);

  return `${roundedAmount} ${asset.assetSymbol}`;
};
