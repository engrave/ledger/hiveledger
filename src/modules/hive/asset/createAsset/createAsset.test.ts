import { Asset } from '@hiveio/dhive';
import { AssetError } from 'modules/hive/asset/asset';
import { createAsset, createEmptyAsset, createFromIntegerString } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { createNaiAsset } from 'modules/hive/naiAsset/createNaiAsset/createNaiAsset';
import { NaiCode } from 'modules/hive/naiAsset/naiAsset';

const { hive, hbd, vests } = ChainAssetSymbol;

describe('createAsset', () => {
  it('can create asset from a number', () => {
    expect(createAsset(123.34, ChainAssetSymbol.hive)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.hive,
    });
    expect(createAsset(123.34, ChainAssetSymbol.hbd)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.hbd,
    });
    expect(createAsset(123.34, ChainAssetSymbol.vests)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.vests,
    });
    expect(createAsset(123.34, ExternalAssetSymbol.usd)).toEqual({
      amount: 123.34,
      assetSymbol: ExternalAssetSymbol.usd,
    });
    expect(createAsset(123.34, ExternalAssetSymbol.btc)).toEqual({
      amount: 123.34,
      assetSymbol: ExternalAssetSymbol.btc,
    });
  });

  it('can create asset from a number passed as a string', () => {
    expect(createAsset('123.34', ChainAssetSymbol.hive)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.hive,
    });
    expect(createAsset('123.34', ChainAssetSymbol.hbd)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.hbd,
    });
    expect(createAsset('123.34', ChainAssetSymbol.vests)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.vests,
    });
    expect(createAsset('123.34', ExternalAssetSymbol.usd)).toEqual({
      amount: 123.34,
      assetSymbol: ExternalAssetSymbol.usd,
    });
    expect(createAsset('123.34', ExternalAssetSymbol.btc)).toEqual({
      amount: 123.34,
      assetSymbol: ExternalAssetSymbol.btc,
    });
  });

  it('can create asset from string', () => {
    expect(createAsset(`123.34 ${hive}`, ChainAssetSymbol.hive)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.hive,
    });
    expect(createAsset(`123.34 ${hbd}`, ChainAssetSymbol.hbd)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.hbd,
    });
    expect(createAsset(`123.34 ${vests}`, ChainAssetSymbol.vests)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.vests,
    });
    expect(createAsset('123.34 USD', ExternalAssetSymbol.usd)).toEqual({
      amount: 123.34,
      assetSymbol: ExternalAssetSymbol.usd,
    });
    expect(createAsset('123.34 BTC', ExternalAssetSymbol.btc)).toEqual({
      amount: 123.34,
      assetSymbol: ExternalAssetSymbol.btc,
    });
  });

  it('can create asset from external library Asset', () => {
    const libraryAsset = new Asset(123.34, ChainAssetSymbol.hive);

    expect(createAsset(libraryAsset, ChainAssetSymbol.hive)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.hive,
    });
  });

  it('can create asset from NAI asset', () => {
    const naiHive = createNaiAsset(123.34, NaiCode.hive);
    const hbdHive = createNaiAsset(123.34, NaiCode.hbd);
    const vestsHive = createNaiAsset(123.34, NaiCode.vests);

    expect(createAsset(naiHive, ChainAssetSymbol.hive)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.hive,
    });
    expect(createAsset(hbdHive, ChainAssetSymbol.hbd)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.hbd,
    });
    expect(createAsset(vestsHive, ChainAssetSymbol.vests)).toEqual({
      amount: 123.34,
      assetSymbol: ChainAssetSymbol.vests,
    });
  });

  it("throws error when symbols don't match", () => {
    expect(() => createAsset('1000 ETH', ExternalAssetSymbol.btc)).toThrow(AssetError.invalidSymbol);
  });

  it('throws error for not finite amounts', () => {
    const infiniteHives = new Asset(Number.POSITIVE_INFINITY, 'HIVE');

    expect(() => createAsset(infiniteHives, ChainAssetSymbol.hive)).toThrow(AssetError.invalidAmount);
  });
});

describe('createEmptyAsset', () => {
  it('make an empty asset of given type', () => {
    expect(createEmptyAsset(ChainAssetSymbol.vests)).toEqual({
      amount: 0,
      assetSymbol: ChainAssetSymbol.vests,
    });

    expect(createEmptyAsset(ExternalAssetSymbol.btc)).toEqual({
      amount: 0,
      assetSymbol: ExternalAssetSymbol.btc,
    });
  });
});

describe('createFromIntegerString', () => {
  it('make an asset from number as string without dot', () => {
    expect(createFromIntegerString('123456', ChainAssetSymbol.hive)).toEqual({
      amount: 123.456,
      assetSymbol: ChainAssetSymbol.hive,
    });

    expect(createFromIntegerString('123456789', ChainAssetSymbol.vests)).toEqual({
      amount: 123.456789,
      assetSymbol: ChainAssetSymbol.vests,
    });

    expect(createFromIntegerString('123456789', ExternalAssetSymbol.btc)).toEqual({
      amount: 1.23456789,
      assetSymbol: ExternalAssetSymbol.btc,
    });
  });
});
