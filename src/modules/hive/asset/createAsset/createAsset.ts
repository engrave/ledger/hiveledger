import { Asset as ChainAsset } from '@hiveio/dhive/lib/chain/asset';
import { Asset, AssetError } from 'modules/hive/asset/asset';
import { AssetSymbol, isChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';
import { getPrecision } from 'modules/hive/assetSymbol/getPrecision/getPrecision';
import { fromNaiFormat } from 'modules/hive/naiAsset/fromNaiFormat/fromNaiFormat';
import { isNaiAsset, NaiAsset } from 'modules/hive/naiAsset/naiAsset';
import { assetTypeMapping } from 'modules/hive/naiAsset/toNaiFormat/toNaiFormat';
import { convertIntegerString } from 'utils/number/convertIntegerString/convertIntegerString';

export const createAsset = <S extends AssetSymbol>(
  source: number | string | ChainAsset | NaiAsset,
  targetSymbol: S,
): Asset<S> => {
  if (typeof source === 'number' || !Number.isNaN(Number(source))) {
    return { amount: Number(source), assetSymbol: targetSymbol };
  }

  if (isNaiAsset(source)) {
    if (isChainAssetSymbol(targetSymbol) && source.nai === assetTypeMapping[targetSymbol]) {
      return fromNaiFormat(source) as Asset<S>;
    }

    throw new Error(AssetError.invalidSymbol);
  }

  const textSource = typeof source === 'object' ? source.toString() : source;

  const [amountText, assetSymbol] = textSource.split(' ');

  const amount = Number.parseFloat(amountText);

  if (!Number.isFinite(amount)) {
    throw new Error(AssetError.invalidAmount);
  }

  if (assetSymbol !== targetSymbol) {
    throw new Error(AssetError.invalidSymbol);
  }

  return { amount, assetSymbol: targetSymbol };
};

export const createEmptyAsset = <S extends AssetSymbol>(assetSymbol: S): Asset<S> => {
  return { amount: 0, assetSymbol };
};

export const createFromIntegerString = <S extends AssetSymbol>(
  amountString: string | number,
  assetSymbol: S,
): Asset<S> => {
  const amount = convertIntegerString(amountString.toString(), getPrecision(assetSymbol));

  return { amount, assetSymbol };
};
