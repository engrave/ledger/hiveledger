import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { multiplyAsset } from 'modules/hive/asset/multiplyAsset/multiplyAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

describe('multiplyAsset', () => {
  it('calculates asset multiplied by a number', () => {
    const hiveSource = createAsset(100, ChainAssetSymbol.hive);
    const hiveTarget = multiplyAsset(hiveSource, 3);

    expect(hiveTarget).toStrictEqual({ amount: 300, assetSymbol: ChainAssetSymbol.hive });

    const usdSource = createAsset(100, ExternalAssetSymbol.usd);
    const usdTarget = multiplyAsset(usdSource, 3);

    expect(usdTarget).toStrictEqual({ amount: 300, assetSymbol: ExternalAssetSymbol.usd });
  });
});
