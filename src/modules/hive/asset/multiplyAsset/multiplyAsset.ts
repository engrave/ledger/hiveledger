import { Asset } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { AssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';

export const multiplyAsset = <S extends AssetSymbol>(asset: Asset<S>, multiplier: number): Asset<S> => {
  return createAsset(asset.amount * multiplier, asset.assetSymbol);
};
