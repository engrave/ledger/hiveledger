import { Asset, AssetError } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { AssetSymbol, isChainAssetSymbol, isExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';
import { fromNaiFormat } from 'modules/hive/naiAsset/fromNaiFormat/fromNaiFormat';
import { isNaiAsset, NaiAsset } from 'modules/hive/naiAsset/naiAsset';

export const parseAsset = <S extends AssetSymbol = AssetSymbol>(asset: string | NaiAsset): Asset<S> => {
  if (isNaiAsset(asset)) {
    return fromNaiFormat(asset) as Asset<S>;
  }

  const [amount, assetSymbol] = asset.split(' ');

  if (isExternalAssetSymbol(assetSymbol) || isChainAssetSymbol(assetSymbol)) {
    return createAsset(amount, assetSymbol) as Asset<S>;
  }

  throw new Error(AssetError.invalidSymbol);
};
