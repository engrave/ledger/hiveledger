import { AssetError } from 'modules/hive/asset/asset';
import { parseAsset } from 'modules/hive/asset/parseAsset/parseAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { createNaiAsset } from 'modules/hive/naiAsset/createNaiAsset/createNaiAsset';
import { NaiCode } from 'modules/hive/naiAsset/naiAsset';

const { hive, hbd, vests } = ChainAssetSymbol;

describe('parseAsset', () => {
  it('properly parses strings', () => {
    expect(parseAsset(`3000 ${hive}`)).toEqual({ amount: 3000, assetSymbol: ChainAssetSymbol.hive });
    expect(parseAsset(`3000 ${hbd}`)).toEqual({ amount: 3000, assetSymbol: ChainAssetSymbol.hbd });
    expect(parseAsset(`3000 ${vests}`)).toEqual({ amount: 3000, assetSymbol: ChainAssetSymbol.vests });

    expect(parseAsset('3000 BTC')).toEqual({ amount: 3000, assetSymbol: ExternalAssetSymbol.btc });
    expect(parseAsset('3000 USD')).toEqual({ amount: 3000, assetSymbol: ExternalAssetSymbol.usd });
    expect(parseAsset('3000 HP')).toEqual({ amount: 3000, assetSymbol: ExternalAssetSymbol.hp });
  });

  it('properly recognizes NAI assets', () => {
    const naiAsset = createNaiAsset(3000, NaiCode.hive);

    expect(parseAsset(naiAsset)).toEqual({ amount: 3000, assetSymbol: ChainAssetSymbol.hive });
  });

  it('throws for invalid strings', () => {
    expect(() => parseAsset('9000 PLN')).toThrow(AssetError.invalidSymbol);
    expect(() => parseAsset('9000')).toThrow(AssetError.invalidSymbol);
  });
});
