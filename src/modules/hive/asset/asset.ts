import { AssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export enum AssetError {
  invalidAmount = 'HiveAssetError/InvalidAmount',
  invalidSymbol = 'HiveAssetError/InvalidSymbol',
  invalidTicker = 'HiveAssetError/InvalidTicker',
  cantConvertAsset = 'HiveAssetError/CantConvertAsset',
  invalidOperation = 'HiveAssetError/InvalidOperation',
}

export type Asset<S extends AssetSymbol = AssetSymbol> = {
  amount: number;
  assetSymbol: S;
};

export type HiveAsset = Asset<ChainAssetSymbol.hive>;
export type HbdAsset = Asset<ChainAssetSymbol.hbd>;
export type VestsAsset = Asset<ChainAssetSymbol.vests>;

export type UsdAsset = Asset<ExternalAssetSymbol.usd>;
export type BtcAsset = Asset<ExternalAssetSymbol.btc>;
export type HpAsset = Asset<ExternalAssetSymbol.hp>;

export type VestsHivePowerPair = [VestsAsset, HpAsset];

export const isVestAsset = (asset: Asset<AssetSymbol>): asset is VestsAsset => {
  return asset.assetSymbol === ChainAssetSymbol.vests;
};
