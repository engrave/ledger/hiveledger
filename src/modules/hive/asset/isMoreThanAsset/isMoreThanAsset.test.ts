import { AssetError } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { isMoreThanAsset } from 'modules/hive/asset/isMoreThanAsset/isMoreThanAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

describe('isMoreThanAsset', () => {
  it('compares two assets of the same type', () => {
    const assetA = createAsset(200, ChainAssetSymbol.hive);
    const assetB = createAsset(100, ChainAssetSymbol.hive);
    const assetC = createAsset(200, ChainAssetSymbol.hive);

    expect(isMoreThanAsset(assetA, assetB)).toBe(true);
    expect(isMoreThanAsset(assetB, assetA)).toBe(false);
    expect(isMoreThanAsset(assetA, assetC)).toBe(false);
  });

  it('throws an error when comparing different assets', () => {
    const assetA = createAsset(200, ChainAssetSymbol.hive);
    const assetB = createAsset(100, ChainAssetSymbol.hbd);

    expect(() => isMoreThanAsset(assetA, assetB)).toThrow(AssetError.invalidSymbol);
  });
});
