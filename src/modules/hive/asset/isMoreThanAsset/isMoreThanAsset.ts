import { Asset, AssetError } from 'modules/hive/asset/asset';
import { compareAssets } from 'modules/hive/asset/compareAssets/compareAssets';
import { AssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';

export const isMoreThanAsset = <S extends AssetSymbol>(thisAsset: Asset<S>, thatAsset: Asset<S>): boolean => {
  if (thisAsset.assetSymbol !== thatAsset.assetSymbol) {
    throw new Error(AssetError.invalidSymbol);
  }

  return compareAssets(thisAsset, thatAsset) === 1;
};
