import { AssetError } from 'modules/hive/asset/asset';
import { compareAssets } from 'modules/hive/asset/compareAssets/compareAssets';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

describe('compareAssets', () => {
  it('allows to compare assets of the same type', () => {
    const assetA = createAsset(200, ChainAssetSymbol.hive);
    const assetB = createAsset(100, ChainAssetSymbol.hive);
    const assetC = createAsset(200, ChainAssetSymbol.hive);

    expect(compareAssets(assetA, assetB)).toBe(1);
    expect(compareAssets(assetB, assetA)).toBe(-1);
    expect(compareAssets(assetA, assetC)).toBe(0);
  });

  it('throws error when assets have different type', () => {
    const assetA = createAsset(200, ChainAssetSymbol.hive);
    const assetB = createAsset(100, ChainAssetSymbol.hbd);

    expect(() => compareAssets(assetA, assetB)).toThrow(AssetError.invalidSymbol);
  });
});
