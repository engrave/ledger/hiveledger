import { Asset, AssetError } from 'modules/hive/asset/asset';
import { AssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';

export const compareAssets = <S extends AssetSymbol>(assetA: Asset<S>, assetB: Asset<S>, reverse = false): number => {
  if (assetA.assetSymbol !== assetB.assetSymbol) {
    throw new Error(AssetError.invalidSymbol);
  }

  const multiplier = reverse ? -1 : 1;

  if (assetA.amount < assetB.amount) {
    return -1 * multiplier;
  }

  if (assetA.amount > assetB.amount) {
    return 1 * multiplier;
  }

  return 0;
};
