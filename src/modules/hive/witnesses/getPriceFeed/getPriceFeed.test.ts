import { ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getPriceFeed } from 'modules/hive/witnesses/getPriceFeed/getPriceFeed';
import { Witness } from 'modules/hive/witnesses/witnesses';

describe('getPriceFeed', () => {
  it('returns the price feed in USD', () => {
    const witness = {
      hbd_exchange_rate: {
        base: '1000 HIVE',
        quote: '500 HBD',
      },
    } as Witness;

    expect(getPriceFeed(witness)).toEqual({ amount: 2, assetSymbol: ExternalAssetSymbol.usd });
  });

  it('returns zero when HBD exchange rate is zero', () => {
    const witness = {
      hbd_exchange_rate: {
        base: '1000 HIVE',
        quote: '0 HBD',
      },
    } as Witness;

    expect(getPriceFeed(witness)).toEqual({ amount: 0, assetSymbol: ExternalAssetSymbol.usd });
  });

  it('returns proper value regardless the asset type in the input', () => {
    const witness = {
      hbd_exchange_rate: {
        base: '1000 HBD',
        quote: '500 HIVE',
      },
    } as Witness;

    expect(getPriceFeed(witness)).toEqual({ amount: 2, assetSymbol: ExternalAssetSymbol.usd });
  });
});
