import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { parseAsset } from 'modules/hive/asset/parseAsset/parseAsset';
import { ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { Witness } from 'modules/hive/witnesses/witnesses';
import { safeDivide } from 'utils/number/safeDivide/safeDivide';

export const getPriceFeed = (witness: Witness) => {
  const hbdExchangeRateBase = parseAsset(witness.hbd_exchange_rate.base);
  const hbdExchangeRateQuote = parseAsset(witness.hbd_exchange_rate.quote);
  const hbdExchangeRatio = safeDivide(hbdExchangeRateBase.amount, hbdExchangeRateQuote.amount);

  return createAsset(hbdExchangeRatio, ExternalAssetSymbol.usd);
};
