export type PostVote = {
  rshares: number;
  voter: string;
};

export type PostStats = {
  hide: boolean;
  gray: boolean;
  total_votes: number;
  flag_weight: number;
};

export type PostMetadata = {
  tags: string[];
  app: string;
  format: string;
};

export type Post = {
  post_id: number;
  author: string;
  permlink: string;
  category: string;
  title: string;
  body: string;
  json_metadata: PostMetadata;
  created: string;
  updated: string;
  depth: number;
  children: number;
  net_rshares: number;
  is_paidout: boolean;
  payout_at: string;
  payout: number;
  pending_payout_value: string;
  author_payout_value: string;
  curator_payout_value: string;
  promoted: string;
  replies: unknown[];
  author_reputation: number;
  stats: PostStats;
  url: string;
  beneficiaries: unknown[];
  max_accepted_payout: string;
  percent_hbd: number;
  active_votes: PostVote[];
  blacklists: string[];
};
