import { AssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

const precisionMap = new Map<AssetSymbol, number>([
  [ExternalAssetSymbol.btc, 8],
  [ChainAssetSymbol.vests, 6],
  [ExternalAssetSymbol.usd, 2],
]);

export const getPrecision = (assetSymbol: AssetSymbol) => {
  return precisionMap.get(assetSymbol) || 3;
};
