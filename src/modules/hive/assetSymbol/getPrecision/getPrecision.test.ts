import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getPrecision } from 'modules/hive/assetSymbol/getPrecision/getPrecision';

describe('getPrecision', () => {
  it('gives proper precision for each supported asset type', () => {
    expect(getPrecision(ChainAssetSymbol.hive)).toBe(3);
    expect(getPrecision(ChainAssetSymbol.hbd)).toBe(3);
    expect(getPrecision(ChainAssetSymbol.vests)).toBe(6);

    expect(getPrecision(ExternalAssetSymbol.usd)).toBe(2);
    expect(getPrecision(ExternalAssetSymbol.btc)).toBe(8);
  });
});
