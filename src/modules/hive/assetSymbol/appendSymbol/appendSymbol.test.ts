import { appendSymbol } from 'modules/hive/assetSymbol/appendSymbol/appendSymbol';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

const { hive, hbd, vests } = ChainAssetSymbol;

describe('appendSymbol', () => {
  it('should append the correct symbol on the correct side', () => {
    expect(appendSymbol(123, ChainAssetSymbol.hive)).toEqual(`123 ${hive}`);
    expect(appendSymbol(123, ChainAssetSymbol.hbd)).toEqual(`123 ${hbd}`);
    expect(appendSymbol(123, ChainAssetSymbol.vests)).toEqual(`123 ${vests}`);
    expect(appendSymbol(123, ExternalAssetSymbol.btc)).toEqual('₿ 123');
    expect(appendSymbol(123, ExternalAssetSymbol.usd)).toEqual('$ 123');
    expect(appendSymbol(123, ExternalAssetSymbol.hp)).toEqual('123 HP');
  });
});
