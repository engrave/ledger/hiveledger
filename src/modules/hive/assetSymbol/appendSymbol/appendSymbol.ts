import { AssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';
import { getSymbolInfo } from 'modules/hive/assetSymbol/getSymbolInfo/getSymbolInfo';

export const appendSymbol = (amount: string | number, assetSymbol: AssetSymbol) => {
  const symbolInfo = getSymbolInfo(assetSymbol);

  if (symbolInfo.prefix) {
    return `${symbolInfo.symbolDisplay} ${amount}`;
  }

  return `${amount} ${symbolInfo.symbolDisplay}`;
};
