import { AssetError } from 'modules/hive/asset/asset';
import { AssetSymbol, isChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol';
import { ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export type SymbolInfo = {
  prefix: boolean;
  suffix: boolean;
  symbolDisplay: string;
};

export const dollarSymbol = '$';
export const bitcoinSymbol = '₿';

export const getSymbolInfo = (assetSymbol: AssetSymbol): SymbolInfo => {
  if (isChainAssetSymbol(assetSymbol)) {
    return { prefix: false, suffix: true, symbolDisplay: assetSymbol };
  }

  if (assetSymbol === ExternalAssetSymbol.btc) {
    return { prefix: true, suffix: false, symbolDisplay: bitcoinSymbol };
  }

  if (assetSymbol === ExternalAssetSymbol.hp) {
    return { prefix: false, suffix: true, symbolDisplay: assetSymbol };
  }

  if (assetSymbol === ExternalAssetSymbol.usd) {
    return { prefix: true, suffix: false, symbolDisplay: dollarSymbol };
  }

  throw new Error(AssetError.invalidSymbol);
};
