import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { bitcoinSymbol, dollarSymbol, getSymbolInfo } from 'modules/hive/assetSymbol/getSymbolInfo/getSymbolInfo';

describe('getSymbolInfo', () => {
  it('gives proper information for each supported asset type', () => {
    expect(getSymbolInfo(ChainAssetSymbol.hive)).toEqual({
      prefix: false,
      suffix: true,
      symbolDisplay: ChainAssetSymbol.hive,
    });

    expect(getSymbolInfo(ChainAssetSymbol.hbd)).toEqual({
      prefix: false,
      suffix: true,
      symbolDisplay: ChainAssetSymbol.hbd,
    });

    expect(getSymbolInfo(ChainAssetSymbol.vests)).toEqual({
      prefix: false,
      suffix: true,
      symbolDisplay: ChainAssetSymbol.vests,
    });

    expect(getSymbolInfo(ExternalAssetSymbol.usd)).toEqual({
      prefix: true,
      suffix: false,
      symbolDisplay: dollarSymbol,
    });

    expect(getSymbolInfo(ExternalAssetSymbol.btc)).toEqual({
      prefix: true,
      suffix: false,
      symbolDisplay: bitcoinSymbol,
    });
  });
});
