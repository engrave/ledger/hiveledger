import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export type AssetSymbol = ChainAssetSymbol | ExternalAssetSymbol;

export const isChainAssetSymbol = (assetSymbol: string): assetSymbol is ChainAssetSymbol => {
  return Object.values<string>(ChainAssetSymbol).includes(assetSymbol);
};

export const isExternalAssetSymbol = (assetSymbol: string): assetSymbol is ExternalAssetSymbol => {
  return Object.values<string>(ExternalAssetSymbol).includes(assetSymbol);
};
