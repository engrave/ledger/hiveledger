import { VestingDelegation } from '@hiveio/dhive';
import { HpAsset, VestsAsset } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getMaxDelegation } from 'modules/hive/hivePower/getMaxDelegation/getMaxDelegation';
import { Wallet } from 'modules/hive/wallet/wallet';

const wallet = {
  staked: {
    staked: createAsset(100, ChainAssetSymbol.vests),
    received: createAsset(1000, ChainAssetSymbol.vests),
    delegated: createAsset(10, ChainAssetSymbol.vests),
  },
  withdraws: {
    toWithdraw: createAsset(50, ChainAssetSymbol.vests),
    withdrawn: createAsset(5, ChainAssetSymbol.vests),
  },
} as Wallet;

const existingDelegation: VestingDelegation = {
  id: 1,
  delegator: 'lorem',
  delegatee: 'ipsum',
  vesting_shares: '500',
  min_delegation_time: '5000',
};

const getHivePower = (asset: VestsAsset): HpAsset => {
  return createAsset(asset.amount * 2, ExternalAssetSymbol.hp);
};

describe('getMaxDelegation', () => {
  it('returns calculated max delegation for new delegatee', () => {
    expect(getMaxDelegation(wallet, getHivePower)).toEqual([
      {
        amount: 45,
        assetSymbol: ChainAssetSymbol.vests,
      },
      {
        amount: 90,
        assetSymbol: ExternalAssetSymbol.hp,
      },
    ]);
  });
  it('returns calculated max delegation for existing delegatee', () => {
    expect(getMaxDelegation(wallet, getHivePower, existingDelegation)).toEqual([
      {
        amount: 545,
        assetSymbol: ChainAssetSymbol.vests,
      },
      {
        amount: 1090,
        assetSymbol: ExternalAssetSymbol.hp,
      },
    ]);
  });
});
