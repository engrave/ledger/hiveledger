import { VestingDelegation } from '@hiveio/dhive';
import { addAsset } from 'modules/hive/asset/addAsset/addAsset';
import { HpAsset, VestsAsset, VestsHivePowerPair } from 'modules/hive/asset/asset';
import { createAsset, createEmptyAsset } from 'modules/hive/asset/createAsset/createAsset';
import { subtractMultipleAssets } from 'modules/hive/asset/subtractAsset/subtractAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { Wallet } from 'modules/hive/wallet/wallet';

export const getMaxDelegation = (
  wallet: Wallet,
  getHivePower: (vests: VestsAsset) => HpAsset,
  existingDelegation: VestingDelegation | null = null,
): VestsHivePowerPair => {
  const toWithdraw = subtractMultipleAssets(wallet.withdraws.toWithdraw, wallet.withdraws.withdrawn);
  const maxExtraDelegation = subtractMultipleAssets(wallet.staked.staked, wallet.staked.delegated, toWithdraw);

  const addition =
    existingDelegation !== null
      ? createAsset(existingDelegation.vesting_shares, ChainAssetSymbol.vests)
      : createEmptyAsset(ChainAssetSymbol.vests);

  const vestsResult = addAsset(maxExtraDelegation, addition);

  return [vestsResult, getHivePower(vestsResult)];
};
