import { VestingDelegation } from '@hiveio/dhive';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { sumDelegations } from 'modules/hive/hivePower/sumDelegations/sumDelegations';

describe('sumDelegations', () => {
  it('adds delegations amounts', () => {
    const delegations: VestingDelegation[] = [
      {
        id: 1,
        delegatee: 'lorem',
        delegator: 'ipsum',
        min_delegation_time: '2022-03-00T11:22:33',
        vesting_shares: '100.000 VESTS',
      },
      {
        id: 2,
        delegatee: 'lorem',
        delegator: 'ipsum',
        min_delegation_time: '2022-03-00T11:22:33',
        vesting_shares: '200.000 VESTS',
      },
      {
        id: 3,
        delegatee: 'lorem',
        delegator: 'ipsum',
        min_delegation_time: '2022-03-00T11:22:33',
        vesting_shares: '300.000 VESTS',
      },
    ];

    expect(sumDelegations(delegations)).toEqual({ amount: 600, assetSymbol: ChainAssetSymbol.vests });
  });
});
