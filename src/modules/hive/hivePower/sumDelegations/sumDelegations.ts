import { VestingDelegation } from '@hiveio/dhive';
import { ExpiringVestingDelegation } from 'modules/hive/account/account';
import { addAsset } from 'modules/hive/asset/addAsset/addAsset';
import { createAsset, createEmptyAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export const sumDelegations = (delegations: VestingDelegation[] | ExpiringVestingDelegation[]) => {
  let sum = createEmptyAsset(ChainAssetSymbol.vests);

  for (const delegation of delegations) {
    sum = addAsset(sum, createAsset(delegation.vesting_shares, ChainAssetSymbol.vests));
  }

  return sum;
};
