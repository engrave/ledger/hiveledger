import { DynamicGlobalProperties } from '@hiveio/dhive';
import { calculateHivePowerApr } from 'modules/hive/hivePower/calculateHivePowerApr/calculateHivePowerApr';

describe('calculateHivePowerApr', () => {
  it('return proper Hive Power Apr', () => {
    const properties = {
      head_block_number: 210000000,
      vesting_reward_percent: 1000,
      virtual_supply: '100000',
      total_vesting_fund_hive: '10000',
    } as DynamicGlobalProperties;

    expect(calculateHivePowerApr(properties)).toBeCloseTo(1.38, 2);
  });

  it('return minimal APR for very late blocks', () => {
    const properties = {
      head_block_number: 2100000000,
      vesting_reward_percent: 1000,
      virtual_supply: '100000',
      total_vesting_fund_hive: '10000',
    } as DynamicGlobalProperties;

    expect(calculateHivePowerApr(properties)).toBe(0.95);
  });
});
