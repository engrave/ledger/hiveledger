import { DynamicGlobalProperties } from '@hiveio/dhive';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export const calculateHivePowerApr = (globalProperties: DynamicGlobalProperties) => {
  const initialInflationRate = 9.5;
  const minimalInflationRate = 0.95;
  const initialBlock = 7000000;

  const decreaseRate = 250000;
  const decreasePercentPerIncrement = 0.01;

  const headBlock = globalProperties.head_block_number;
  const deltaBlocks = headBlock - initialBlock;
  const decreaseIncrements = deltaBlocks / decreaseRate;

  let currentInflationRate = initialInflationRate - decreaseIncrements * decreasePercentPerIncrement;

  if (currentInflationRate < minimalInflationRate) {
    currentInflationRate = minimalInflationRate;
  }

  const vestingRewardPercent = globalProperties.vesting_reward_percent / 10000;
  const virtualSupply = createAsset(globalProperties.virtual_supply, ChainAssetSymbol.hive);
  const totalVestingFunds = createAsset(globalProperties.total_vesting_fund_hive, ChainAssetSymbol.hive);

  return (virtualSupply.amount * currentInflationRate * vestingRewardPercent) / totalVestingFunds.amount;
};
