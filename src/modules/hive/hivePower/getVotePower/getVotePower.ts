import { AccountDetails } from 'modules/hive/account/account';
import { addAsset } from 'modules/hive/asset/addAsset/addAsset';
import { createAsset, createEmptyAsset, createFromIntegerString } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { VotePower } from 'modules/hive/hivePower/hivePower';

export const getVotePower = (accountDetails: AccountDetails): VotePower => {
  const vestingShares = createAsset(accountDetails.vesting_shares, ChainAssetSymbol.vests);

  const proxiedVestingShares = accountDetails.proxied_vsf_votes.reduce((votesSum, vests) => {
    return addAsset(votesSum, createFromIntegerString(vests, ChainAssetSymbol.vests));
  }, createEmptyAsset(ChainAssetSymbol.vests));

  const totalVestingShares = addAsset(vestingShares, proxiedVestingShares);

  return {
    username: accountDetails.name,
    vestingShares,
    proxiedVestingShares,
    totalVestingShares,
  };
};
