import { AccountDetails } from 'modules/hive/account/account';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getVotePower } from 'modules/hive/hivePower/getVotePower/getVotePower';

const { vests } = ChainAssetSymbol;

describe('getVotePower', () => {
  it('returns info about vesting shares and proxied vesting shares', () => {
    const accountDetails = {
      name: 'lorem.ipsum',
      vesting_shares: `40.000 ${vests}`,
      proxied_vsf_votes: [5050000, 6060000, 7070000],
    } as unknown as AccountDetails;

    expect(getVotePower(accountDetails)).toEqual({
      username: 'lorem.ipsum',
      vestingShares: { amount: 40, assetSymbol: vests },
      proxiedVestingShares: { amount: 18.18, assetSymbol: vests },
      totalVestingShares: { amount: 58.18, assetSymbol: vests },
    });
  });
});
