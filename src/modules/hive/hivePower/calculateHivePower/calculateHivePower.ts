import { DynamicGlobalProperties } from '@hiveio/dhive';
import { VestsAsset } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export const calculateHivePower = (vests: VestsAsset, globalProperties: DynamicGlobalProperties) => {
  const totalVestingFundHive = createAsset(globalProperties.total_vesting_fund_hive, ChainAssetSymbol.hive);
  const totalVestingShares = createAsset(globalProperties.total_vesting_shares, ChainAssetSymbol.vests);

  const hivePower = totalVestingFundHive.amount * (vests.amount / totalVestingShares.amount);

  return createAsset(hivePower, ExternalAssetSymbol.hp);
};
