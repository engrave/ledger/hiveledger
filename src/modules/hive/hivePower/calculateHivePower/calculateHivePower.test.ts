import { DynamicGlobalProperties } from '@hiveio/dhive';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { calculateHivePower } from 'modules/hive/hivePower/calculateHivePower/calculateHivePower';

const { hive, vests } = ChainAssetSymbol;

describe('calculateHivePower', () => {
  it('should calculate proper Hive Power', () => {
    const globalProperties = {
      total_vesting_fund_hive: `21 ${hive}`,
      total_vesting_shares: `1000 ${vests}`,
    } as unknown as DynamicGlobalProperties;

    expect(calculateHivePower(createAsset(100, ChainAssetSymbol.vests), globalProperties)).toEqual({
      amount: 2.1,
      assetSymbol: ExternalAssetSymbol.hp,
    });
  });
});
