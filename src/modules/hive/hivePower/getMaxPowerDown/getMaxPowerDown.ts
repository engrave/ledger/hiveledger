import { HpAsset, VestsAsset, VestsHivePowerPair } from 'modules/hive/asset/asset';
import { subtractMultipleAssets } from 'modules/hive/asset/subtractAsset/subtractAsset';
import { Wallet } from 'modules/hive/wallet/wallet';

export const getMaxPowerDown = (wallet: Wallet, getHivePower: (vests: VestsAsset) => HpAsset): VestsHivePowerPair => {
  const awaitingWithdraw = subtractMultipleAssets(wallet.withdraws.toWithdraw, wallet.withdraws.withdrawn);
  const maxVestsPowerDown = subtractMultipleAssets(wallet.staked.staked, wallet.staked.delegated, awaitingWithdraw);

  return [maxVestsPowerDown, getHivePower(maxVestsPowerDown)];
};
