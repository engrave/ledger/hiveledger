import { HpAsset, VestsAsset } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getMaxPowerDown } from 'modules/hive/hivePower/getMaxPowerDown/getMaxPowerDown';
import { Wallet } from 'modules/hive/wallet/wallet';

const wallet = {
  staked: {
    staked: createAsset(100, ChainAssetSymbol.vests),
    received: createAsset(1000, ChainAssetSymbol.vests),
    delegated: createAsset(10, ChainAssetSymbol.vests),
  },
  withdraws: {
    toWithdraw: createAsset(50, ChainAssetSymbol.vests),
    withdrawn: createAsset(5, ChainAssetSymbol.vests),
  },
} as Wallet;

const getHivePower = (asset: VestsAsset): HpAsset => {
  return createAsset(asset.amount * 2, ExternalAssetSymbol.hp);
};

describe('getMaxPowerDown', () => {
  it('returns calculated max power down', () => {
    expect(getMaxPowerDown(wallet, getHivePower)).toEqual([
      {
        amount: 45,
        assetSymbol: ChainAssetSymbol.vests,
      },
      {
        amount: 90,
        assetSymbol: ExternalAssetSymbol.hp,
      },
    ]);
  });
});
