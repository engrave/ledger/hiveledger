import { WithdrawsBalance } from 'modules/hive/wallet/wallet';

export const getRemainingPayouts = (withdraws: WithdrawsBalance) => {
  const { toWithdraw, withdrawn, vestingWithdrawRate } = withdraws;

  return Math.round((toWithdraw.amount - withdrawn.amount) / vestingWithdrawRate.amount);
};
