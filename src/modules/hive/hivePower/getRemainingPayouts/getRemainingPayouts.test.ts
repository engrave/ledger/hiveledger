import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getRemainingPayouts } from 'modules/hive/hivePower/getRemainingPayouts/getRemainingPayouts';
import { WithdrawsBalance } from 'modules/hive/wallet/wallet';

describe('getRemainingPayouts', () => {
  it('should properly calculate number of remaining payouts', () => {
    const withdraws: WithdrawsBalance = {
      toWithdraw: createAsset(800, ChainAssetSymbol.vests),
      withdrawn: createAsset(200, ChainAssetSymbol.vests),
      vestingWithdrawRate: createAsset(199, ChainAssetSymbol.vests),
    };

    expect(getRemainingPayouts(withdraws)).toEqual(3);
  });
});
