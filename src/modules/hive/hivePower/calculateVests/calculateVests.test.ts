import { DynamicGlobalProperties } from '@hiveio/dhive';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { calculateHivePower } from 'modules/hive/hivePower/calculateHivePower/calculateHivePower';
import { calculateVests } from 'modules/hive/hivePower/calculateVests/calculateVests';

const { hive, vests } = ChainAssetSymbol;

describe('calculateVests', () => {
  it('should calculate proper vests amount', () => {
    const globalProperties = {
      total_vesting_fund_hive: `21 ${hive}`,
      total_vesting_shares: `1000 ${vests}`,
    } as unknown as DynamicGlobalProperties;

    expect(calculateVests(createAsset(2.1, ExternalAssetSymbol.hp), globalProperties)).toEqual({
      amount: 100,
      assetSymbol: ChainAssetSymbol.vests,
    });
  });

  it('reverses the calculateHivePower function', () => {
    const globalProperties = {
      total_vesting_fund_hive: `21 ${hive}`,
      total_vesting_shares: `1000 ${vests}`,
    } as unknown as DynamicGlobalProperties;

    const hivePowerAsset = createAsset(2.1, ExternalAssetSymbol.hp);
    const vestsAsset = calculateVests(hivePowerAsset, globalProperties);
    const hivePowerAssetReversed = calculateHivePower(vestsAsset, globalProperties);

    expect(hivePowerAsset).toEqual(hivePowerAssetReversed);
  });
});
