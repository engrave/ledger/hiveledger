import { DynamicGlobalProperties } from '@hiveio/dhive';
import { HpAsset } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export const calculateVests = (hp: HpAsset, globalProperties: DynamicGlobalProperties) => {
  const totalVestingFundHive = createAsset(globalProperties.total_vesting_fund_hive, ChainAssetSymbol.hive);
  const totalVestingShares = createAsset(globalProperties.total_vesting_shares, ChainAssetSymbol.vests);

  const vests = (hp.amount * totalVestingShares.amount) / totalVestingFundHive.amount;

  return createAsset(vests, ChainAssetSymbol.vests);
};
