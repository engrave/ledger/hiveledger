import { VestsAsset } from 'modules/hive/asset/asset';

export type VotePower = {
  username: string;
  vestingShares: VestsAsset;
  proxiedVestingShares: VestsAsset;
  totalVestingShares: VestsAsset;
};
