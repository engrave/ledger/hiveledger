import { Asset } from 'modules/hive/asset/asset';
import { ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export type ComparableEquivalents = {
  usd: Asset<ExternalAssetSymbol.usd> | null;
  btc: Asset<ExternalAssetSymbol.btc> | null;
};
