import {
  convertChainAsset,
  convertChainToComparableExternals,
  convertExternalAsset,
} from 'modules/exchange/convertAsset/convertAsset';
import { ComparableEquivalents } from 'modules/exchange/convertAsset/convertAsset.types';
import { ChainExchangeRates, ExchangeError } from 'modules/exchange/exchange';
import { Asset } from 'modules/hive/asset/asset';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

const rates: ChainExchangeRates = {
  [ChainAssetSymbol.hive]: {
    [ExternalAssetSymbol.usd]: 2,
    [ExternalAssetSymbol.btc]: 0.002,
  },
  [ChainAssetSymbol.hbd]: {
    [ExternalAssetSymbol.usd]: 0.1,
    [ExternalAssetSymbol.btc]: 0.0001,
  },
};

describe('convertChainAsset', () => {
  it('calculates asset value in target asset type', () => {
    const hive2usd = convertChainAsset(createAsset(50.05, ChainAssetSymbol.hive), ExternalAssetSymbol.usd, rates);

    expect(hive2usd).toEqual<Asset>(createAsset(100.1, ExternalAssetSymbol.usd));

    const hbd2usd = convertChainAsset(createAsset(50.05, ChainAssetSymbol.hbd), ExternalAssetSymbol.usd, rates);

    expect(hbd2usd).toEqual<Asset>(createAsset(5.005, ExternalAssetSymbol.usd));
  });

  it('throws error for no supported input assets', () => {
    const unsupportedAsset = createAsset('50.050 VESTS', ChainAssetSymbol.vests);

    expect(() =>
      convertChainAsset(unsupportedAsset as unknown as Asset<ChainAssetSymbol.hive>, ExternalAssetSymbol.usd, rates),
    ).toThrow(ExchangeError.exchangeNotSupported);
  });
});

describe('convertExternalAsset', () => {
  it('calculates asset value in target asset type', () => {
    const usd2hive = convertExternalAsset(
      createAsset('50.050 USD', ExternalAssetSymbol.usd),
      ChainAssetSymbol.hive,
      rates,
    );

    expect(usd2hive).toEqual<Asset>(createAsset(25.025, ChainAssetSymbol.hive));

    const usd2hbd = convertExternalAsset(
      createAsset('50.050 USD', ExternalAssetSymbol.usd),
      ChainAssetSymbol.hbd,
      rates,
    );

    expect(usd2hbd).toEqual<Asset>(createAsset(500.5, ChainAssetSymbol.hbd));
  });

  it('throws error for no supported input assets', () => {
    const unsupportedAsset = createAsset(50.05, ChainAssetSymbol.vests);

    expect(() =>
      convertChainAsset(unsupportedAsset as unknown as Asset<ChainAssetSymbol.hive>, ExternalAssetSymbol.usd, rates),
    ).toThrow(ExchangeError.exchangeNotSupported);
  });
});

describe('convertChainToSupportedExternals', () => {
  it('exchanges chain assets into USD and BTC', () => {
    const result = convertChainToComparableExternals(createAsset(50.05, ChainAssetSymbol.hive), rates);

    expect(result).toEqual<ComparableEquivalents>({
      usd: createAsset('100.1 USD', ExternalAssetSymbol.usd),
      btc: createAsset('0.1001 BTC', ExternalAssetSymbol.btc),
    });
  });

  it('throws error when converting from external asset', () => {
    const externalAsset = createAsset('50.050 BTC', ExternalAssetSymbol.btc);

    expect(() =>
      convertChainToComparableExternals(externalAsset as unknown as Asset<ChainAssetSymbol.hive>, rates),
    ).toThrow(ExchangeError.exchangeNotSupported);
  });

  it('throws error when converting from not supported asset', () => {
    const unsupportedAsset = createAsset('50.050 VESTS', ChainAssetSymbol.vests);

    expect(() => {
      return convertChainToComparableExternals(unsupportedAsset as unknown as Asset<ChainAssetSymbol.hive>, rates);
    }).toThrow(ExchangeError.exchangeNotSupported);
  });
});
