import { ComparableEquivalents } from 'modules/exchange/convertAsset/convertAsset.types';
import {
  ExchangeChainSymbol,
  ExchangeExternalSymbol,
  ChainExchangeRates,
  ExchangeError,
} from 'modules/exchange/exchange';
import { Asset } from 'modules/hive/asset/asset';
import { ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export const convertChainAsset = <S extends ExchangeExternalSymbol>(
  source: Asset<ExchangeChainSymbol>,
  targetSymbol: S,
  rates: ChainExchangeRates,
): Asset<S> => {
  if (!rates[source.assetSymbol] || !rates[source.assetSymbol][targetSymbol]) {
    throw new Error(ExchangeError.exchangeNotSupported);
  }

  const ratio = rates[source.assetSymbol][targetSymbol];

  return { amount: source.amount * ratio, assetSymbol: targetSymbol };
};

export const convertExternalAsset = <S extends ExchangeChainSymbol>(
  source: Asset<ExchangeExternalSymbol>,
  targetSymbol: S,
  rates: ChainExchangeRates,
): Asset<S> => {
  const ratio = 1 / rates[targetSymbol][source.assetSymbol];

  return { amount: source.amount * ratio, assetSymbol: targetSymbol };
};

export const convertChainToComparableExternals = (
  source: Asset<ExchangeChainSymbol>,
  rates: ChainExchangeRates,
): ComparableEquivalents => {
  return {
    usd: convertChainAsset(source, ExternalAssetSymbol.usd, rates),
    btc: convertChainAsset(source, ExternalAssetSymbol.btc, rates),
  };
};
