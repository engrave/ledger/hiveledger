import { CoingeckoCurrency, ExchangeRates } from 'api/actions/getExchangeRates/getExchangeRates.types';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export type ExchangeChainSymbol = ChainAssetSymbol.hive | ChainAssetSymbol.hbd;

export type ExchangeExternalSymbol = ExternalAssetSymbol.btc | ExternalAssetSymbol.usd;

export type CoingeckoSourceCurrency = CoingeckoCurrency.hive | CoingeckoCurrency.hiveDollar;
export type CoingeckoTargetCurrency = CoingeckoCurrency.usd | CoingeckoCurrency.btc;

export type CoingeckoExchangeRates = ExchangeRates<CoingeckoSourceCurrency, CoingeckoTargetCurrency>;
export type ChainExchangeRates = ExchangeRates<ExchangeChainSymbol, ExchangeExternalSymbol>;

export enum ExchangeError {
  sourceSymbolNotSupported = 'ExchangeError/SourceSymbolNotSupported',
  exchangeNotSupported = 'ExchangeError/ExchangeNotSupported',
}
