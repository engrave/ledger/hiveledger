import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { NaiAsset } from 'modules/hive/naiAsset/naiAsset';
import { formatRpcError } from 'modules/ledger/rpcError/formatRpcError/formatRpcError';
import { RpcError } from 'modules/ledger/rpcError/rpcError';

const error: RpcError = {
  name: 'RPCError',
  message: 'Something wen wrong',
  jse_shortmsg: 'Something wen wrong',
  jse_info: {
    code: 500,
    name: 'Some Error',
    message: 'Lorem Ipsum',
    stack: [
      {
        // eslint-disable-next-line no-template-curly-in-string
        format: 'Call ${user} on ${phone} and give him ${amount}',
        data: {
          user: 'John Doe',
          phone: 111222333,
          amount: { amount: 123456789, nai: '@@000000021', precision: 3 } as NaiAsset,
        },
        context: {
          file: 'hello.cpp',
          hostname: 'localhost',
          level: 'error',
          line: 20,
          method: 'isGoat',
          timestamp: '1234567890',
        },
      },
    ],
  },
};

describe('formatRpcError', () => {
  it('return error message and values', () => {
    expect(formatRpcError(error)).toStrictEqual({
      // eslint-disable-next-line no-template-curly-in-string
      message: 'Call ${user} on ${phone} and give him ${amount}',
      values: {
        user: 'John Doe',
        phone: 111222333,
        amount: `123456.789 ${ChainAssetSymbol.hive}`,
      },
    });
  });

  it('should use the name field if format is empty', () => {
    const errorWithoutFormat = { ...error };
    errorWithoutFormat.jse_info.stack[0].format = '';

    expect(formatRpcError(errorWithoutFormat)).toStrictEqual({
      // eslint-disable-next-line no-template-curly-in-string
      message: 'Some Error',
      values: {
        user: 'John Doe',
        phone: 111222333,
        amount: `123456.789 ${ChainAssetSymbol.hive}`,
      },
    });
  });
});
