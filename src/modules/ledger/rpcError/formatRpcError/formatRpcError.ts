import { printAsset } from 'modules/hive/asset/printAsset/printAsset';
import { fromNaiFormat } from 'modules/hive/naiAsset/fromNaiFormat/fromNaiFormat';
import { isNaiAsset } from 'modules/hive/naiAsset/naiAsset';
import { RpcError } from 'modules/ledger/rpcError/rpcError';

export const formatRpcError = (error: RpcError) => {
  const errorData = error.jse_info.stack[0];

  const valuesEntries = Object.entries(errorData.data).map(([key, value]) => {
    const errorKey = key.replace('.', '_');

    if (isNaiAsset(value)) {
      return [errorKey, printAsset(fromNaiFormat(value))];
    }

    return [errorKey, value];
  });

  const formattedValues = Object.fromEntries(valuesEntries);

  const message = errorData.format || error.jse_info.name;

  return { message, values: formattedValues };
};
