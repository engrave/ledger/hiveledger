export type RpcErrorStackItem = {
  format: string;
  data: { [key: string]: unknown };
  context: {
    file: string;
    hostname: string;
    level: string;
    line: number;
    method: string;
    timestamp: string;
  };
};

export interface RpcError extends Error {
  name: 'RPCError';
  jse_info: {
    code: number;
    message: string;
    name: string;
    stack: RpcErrorStackItem[];
  };
  jse_shortmsg: string;
}

export const isRpcError = (error: Error): error is RpcError => {
  return error.name === 'RPCError';
};
