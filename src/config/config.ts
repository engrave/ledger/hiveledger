export const links = {
  engrave: 'https://engrave.dev',
  gitlab: 'https://gitlab.com/engrave',
  hive: 'https://peakd.com/@engrave',
  discord: 'https://discord.gg/8NktdFh',
  buyLedgerNano: 'https://shop.ledger.com?r=d5ee0e9dc3d0',
  createAccount: 'https://signup.hive.io',
};

export const exchange = {
  refreshTime: 60 * 1000,
};

export const chain = {
  globalPropertiesRefreshTime: 60 * 1000,
  minimalSavingsRewardAmount: 0.001,
  rewardClaimTransactionAmount: 0.001,
};

export const accountDiscovery = {
  maxAccountGap: 2,
  maxKeyGap: 2,
  maxKeySearch: 1024,
};

export const witnessesConfig = {
  perPage: 100,
  maxApprovedWitnesses: 30,
  proposedProxyAccount: 'engrave',
};

export const proposalsConfig = {
  perPage: 20,
  proposalsFundAccount: 'hive.fund',
  votersInPreview: 7,
  votersPerPage: 250,
};

export const exchangeWidgetConfig = {
  merchantId: 'vnj4zzcqtgzdz9l7',
};

export const postRendering = {
  baseUrl: 'https://peakd.com/',
  assetsWidth: 640,
  assetsHeight: 480,
};
