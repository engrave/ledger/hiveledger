export enum GeneralRoute {
  root = '/',
  auth = '/auth',
  dashboard = '/dashboard',
  exchange = '/exchange',
}

export enum AuthRoute {
  login = '/auth/login',
  addAccount = '/auth/add',
  logout = '/auth/logout',
}

export enum DashboardRoute {
  overview = '/dashboard',
  balance = '/dashboard/balance',
  staking = '/dashboard/staking',
  savings = '/dashboard/savings',
  proposals = '/dashboard/proposals',
  witnesses = '/dashboard/witnesses',
  resourceCredits = '/dashboard/resource-credits',
  advanced = '/dashboard/advanced',
}

export enum ProposalsRoute {
  browser = '/dashboard/proposals',
  details = '/dashboard/proposals/:id',
}
