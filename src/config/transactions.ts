import { utils } from '@hiveio/dhive';
import { OperationType } from 'modules/hive/operation/operation';

export const parametersOrders: Record<OperationType, string[]> = {
  [OperationType.transfer]: ['from', 'to', 'amount', 'memo'],
  [OperationType.transferToVesting]: ['from', 'to', 'amount'],
  [OperationType.delegateVestingShares]: ['delegator', 'delegatee', 'vesting_shares'],
  [OperationType.withdrawVesting]: ['account', 'vesting_shares'],
  [OperationType.transferToSavings]: ['from', 'to', 'amount', 'memo'],
  [OperationType.transferFromSavings]: ['from', 'request_id', 'to', 'amount', 'memo'],
  [OperationType.cancelTransferFromSavings]: ['from', 'request_id'],
  [OperationType.interest]: ['owner', 'interest'],
  [OperationType.fillTransferFromSavings]: ['from', 'request_id', 'to', 'amount', 'memo'],
  [OperationType.fillConvertRequest]: ['owner', 'requestid', 'amount_in', 'amount_out'],
  [OperationType.claimRewardBalance]: ['account', 'reward_hive', 'reward_hbd', 'reward_vests'],
  [OperationType.accountUpdate]: ['account', 'owner', 'active', 'posting', 'memo_key'],
  [OperationType.accountWitnessVote]: ['account', 'witness', 'approve'],
  [OperationType.accountWitnessProxy]: ['account', 'proxy'],
  [OperationType.updateProposalVotes]: ['voter', 'proposal_ids', 'approve'],
  [OperationType.custom]: ['id', 'required_auths', 'required_posting_auths', 'json'],
};

const [operationFilterHigh, operationFilterLow] = utils.makeBitMaskFilter([
  utils.operationOrders.transfer,
  utils.operationOrders.transfer_to_vesting,
  utils.operationOrders.withdraw_vesting,
  utils.operationOrders.interest,
  utils.operationOrders.transfer_to_savings,
  utils.operationOrders.fill_transfer_from_savings,
  utils.operationOrders.fill_convert_request,
  utils.operationOrders.claim_reward_balance,
]);

export const transactionsHistory = {
  perPage: 100,
  operationFilterHigh,
  operationFilterLow,
};
