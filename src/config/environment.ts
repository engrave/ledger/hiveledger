export const environment = {
  name: process.env.REACT_APP_ENVIRONMENT || '',
  hive: {
    apiUrl: process.env.REACT_APP_HIVE_API || '',
    addressPrefix: process.env.REACT_APP_HIVE_ADDRESS_PREFIX || '',
    chainId: process.env.REACT_APP_HIVE_CHAIN_ID || '',
  },
  coingecko: {
    apiUrl: process.env.REACT_APP_COINGECKO_API || '',
  },
  blockExplorer: {
    url: process.env.REACT_APP_BLOCK_EXPLORER_URL || '',
  },
  ticker: {
    liquid: process.env.REACT_APP_LIQUID_TICKER || 'HIVE',
    debt: process.env.REACT_APP_DEBT_TICKER || 'HBD',
    vests: process.env.REACT_APP_VESTS_TICKER || 'VESTS',
  },
};

export const analytics = {
  googleAnalyticsId: process.env.REACT_APP_GOOGLE_ANALYTICS_ID || null,
};
