import { alpha } from '@mui/material';
import { common } from '@mui/material/colors';
import { createTheme } from '@mui/material/styles';
import { Shadows } from '@mui/material/styles/shadows';

const projectColors = {
  gunmetal: '#525c66',
  paleGrey: '#f0f0f8',
  paleLilac: '#e7e7f1',
  darkGrey: '#212529',
  athensGrey: '#f7f7f8',
  charcoalGrey: '#3d454d',
  pinkishRed: '#e31337',
  lightBlueGrey: '#d0d0d9',
  cerise: '#e31979',
  blueyGrey: '#899aab',
  woodsmoke: '#181c1f',
  jadeGreen: '#00A676',
  mustardYellow: '#d9bd05',
  mischka: '#d0d0d9',
  shamrock: '#31cd87',
  carnation: '#f3595e',
};

const { shadows } = createTheme();

const appShadows: Shadows = [...shadows];
appShadows[2] = '0 3px 4px -1px rgba(0, 0, 0, 0.06);';

export const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1228,
      xl: 1536,
    },
  },
  palette: {
    primary: {
      main: projectColors.pinkishRed,
    },
    secondary: {
      main: projectColors.gunmetal,
    },
    alert: {
      main: projectColors.cerise,
      success: projectColors.shamrock,
      error: projectColors.carnation,
      warning: projectColors.mustardYellow,
    },
    header: {
      main: projectColors.darkGrey,
      separator: projectColors.charcoalGrey,
      contrastText: common.white,
      activeMenu: projectColors.woodsmoke,
    },
    typography: {
      common: projectColors.darkGrey,
      header: projectColors.darkGrey,
      lightText: projectColors.gunmetal,
      callToActionLink: projectColors.cerise,
      errorMessage: projectColors.pinkishRed,
      infoStyles: projectColors.lightBlueGrey,
      addition: projectColors.jadeGreen,
      subtraction: projectColors.pinkishRed,
    },
    ui: {
      separator: projectColors.paleLilac,
      outlinedMenuBorder: projectColors.paleGrey,
      activeElement: projectColors.cerise,
      inactiveElement: projectColors.paleLilac,
      codeBackground: projectColors.paleGrey,
      cardElement: projectColors.athensGrey,
      codeText: projectColors.blueyGrey,
      progressBarOutline: projectColors.lightBlueGrey,
    },
    status: {
      ok: projectColors.jadeGreen,
      alert: projectColors.mustardYellow,
      error: projectColors.pinkishRed,
    },
    background: {
      default: projectColors.paleGrey,
    },
  },
  typography: {
    fontFamily: 'Inter, sans-serif',
    overline: {
      fontSize: '0.625rem',
      fontWeight: 500,
    },
    body2: {
      fontSize: '0.75rem',
      color: projectColors.gunmetal,
    },
    cardTitle: {
      fontSize: '1.125rem',
      fontWeight: 500,
      marginBottom: '20px',
      color: projectColors.darkGrey,
      display: 'block',
    },
  },
  shadows: appShadows,
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        ':root': {
          scrollBehavior: 'smooth',
        },
        a: {
          textDecoration: 'none',
          transition: 'opacity 0.3s',
        },
        'a:hover': {
          opacity: 0.7,
        },
        code: {
          fontFamily: '"Courier", monospace',
          fontWeight: 500,
        },
        '.Toastify': {
          fontSize: '14px',
          '.Toastify__toast-body': {
            padding: '0 20px',
          },
        },
      },
    },
    MuiLink: {
      styleOverrides: {
        root: {
          textDecoration: 'none',
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'none',
          borderRadius: '6px',

          '&:hover': {
            boxShadow: 'none',
          },

          '.MuiLoadingButton-loadingIndicatorStart': {
            position: 'static',
            marginRight: '12px',
          },
          '.MuiLoadingButton-loadingIndicatorEnd': {
            position: 'static',
            marginLeft: '12px',
          },
        },
        contained: {
          minWidth: '100px',
        },
        containedSizeSmall: {
          padding: '5px 15px',
        },
        containedSizeLarge: {
          fontSize: '1rem',
          lineHeight: '1.5em',
          fontWeight: 500,
          padding: '8px 33px 9px',
          minWidth: '120px',
          boxShadow: 'none',
        },
        outlinedPrimary: {
          color: 'black',
          background: 'white',
          borderColor: projectColors.pinkishRed,
        },
        outlinedSizeSmall: {
          lineHeight: '1rem',
          fontSize: '0.875rem',
          fontWeight: 'normal',
          minWidth: '100px',
          height: '30px',
          padding: '5px 15px',
        },
        microText: {
          fontSize: '0.625rem',
          color: projectColors.gunmetal,
          textTransform: 'uppercase',
        },
      },
    },
    MuiAlert: {
      styleOverrides: {
        root: {
          fontWeight: '600',
        },
      },
    },
    MuiModal: {
      styleOverrides: {
        root: {
          '&:not(.MuiMenu-root)': {
            backdropFilter: 'blur(3px)',
          },
        },
      },
    },
    MuiMenuItem: {
      styleOverrides: {
        root: {
          background: 'white',
          '&:hover': {
            background: projectColors.paleGrey,
          },
        },
      },
    },
    MuiDialog: {
      styleOverrides: {
        paper: {
          borderRadius: '10px',
          boxShadow: shadows[2],
        },
      },
    },
    MuiDialogActions: {
      styleOverrides: {
        root: {
          justifyContent: 'center',
          alignItems: 'flex-end',
          gap: 4,
          paddingBottom: '16px',
        },
      },
    },
    MuiDialogTitle: {
      styleOverrides: {
        root: {
          fontSize: '1.125rem',
          fontWeight: 500,
          padding: '20px 32px',
        },
      },
    },
    MuiDialogContent: {
      styleOverrides: {
        root: {
          padding: '0 32px 16px',
          '&:last-child': {
            paddingBottom: '32px',
          },
        },
      },
    },
    MuiDialogContentText: {
      styleOverrides: {
        root: {
          fontSize: '0.875rem',
        },
      },
    },
    MuiFormControl: {
      styleOverrides: {
        root: {
          marginBottom: '1.5rem',
          position: 'relative',
        },
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          background: 'white',
        },
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          position: 'static',
          transform: 'none',
          fontSize: '0.875rem',
          fontWeight: 500,
          color: projectColors.darkGrey,
          marginBottom: '7px',
        },
      },
    },
    MuiSelect: {
      styleOverrides: {
        icon: {
          color: projectColors.paleLilac,
          transition: 'color 0.3s',
        },
      },
    },
    MuiCheckbox: {
      styleOverrides: {
        root: {
          color: 'white',

          '& .MuiTouchRipple-root': {
            color: projectColors.gunmetal,
          },

          '& svg': {
            border: `2px solid ${projectColors.paleGrey}`,
            borderRadius: '4px',
          },

          '&.Mui-checked': {
            color: projectColors.gunmetal,
            '& svg': {
              stroke: projectColors.gunmetal,
              strokeWidth: '1px',
            },
          },

          '&.Mui-disabled': {
            color: projectColors.paleGrey,
            '& svg': {
              background: projectColors.paleGrey,
              border: `2px solid ${projectColors.paleLilac}`,
            },
          },

          '&.Mui-disabled.Mui-checked': {
            color: projectColors.mischka,
            '& svg': {
              stroke: projectColors.mischka,
              strokeWidth: '1px',
              background: projectColors.paleGrey,
              border: `2px solid ${projectColors.paleLilac}`,
            },
          },
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          '& .MuiOutlinedInput-notchedOutline': {
            borderColor: projectColors.lightBlueGrey,
            transition: 'border-color 0.3s',
          },
          '&:hover .MuiOutlinedInput-notchedOutline': {
            borderColor: projectColors.lightBlueGrey,
          },
          '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
            borderWidth: '1px',
            borderColor: projectColors.blueyGrey,
          },
          '&.Mui-focused .MuiSelect-icon': {
            color: projectColors.lightBlueGrey,
          },
          '&.Mui-disabled .MuiOutlinedInput-notchedOutline': {
            borderColor: alpha(projectColors.lightBlueGrey, 0.9),
          },
        },
        input: {
          fontSize: '14px',
          padding: '6px 14px',
          height: 'auto',
        },
        adornedStart: {
          paddingLeft: 0,
        },
      },
    },
    MuiPopover: {
      styleOverrides: {
        root: {
          '& .MuiMenuItem-root': {
            fontSize: '0.875rem',
          },
        },
      },
    },
    MuiFormHelperText: {
      styleOverrides: {
        root: {
          marginLeft: 0,
          marginRight: 0,
          textAlign: 'right',
          color: projectColors.gunmetal,
          width: '100%',

          '& + &': {
            marginTop: 0,
          },
        },
      },
    },
    MuiInputAdornment: {
      styleOverrides: {
        root: {
          background: projectColors.paleGrey,
          height: '33px',
          padding: '0 10px 2px',
          maxHeight: 'none',
          marginRight: 0,
          p: {
            fontSize: '0.875rem',
            color: projectColors.blueyGrey,
          },
        },
      },
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          background: 'white',
          color: projectColors.gunmetal,
          padding: '8px 16px',
          fontSize: '0.75rem',
          boxShadow: shadows[3],
        },
        arrow: {
          color: 'white',
          width: '2em',
          marginLeft: '-0.5em',

          '&:before': {
            boxShadow: shadows[3],
          },
        },
      },
    },
    MuiTableCell: {
      styleOverrides: {
        root: {
          padding: '8px',
          minHeight: '50px',
          borderBottom: `1px solid ${projectColors.paleLilac}`,

          '&:first-of-type': {
            paddingLeft: 0,
          },
          '&:last-of-type': {
            paddingRight: 0,
          },
        },
        head: {
          '&:last-of-type': {
            textAlign: 'right',
          },
        },
      },
    },
    MuiTableBody: {
      styleOverrides: {
        root: {
          '.MuiTableRow-root:last-of-type .MuiTableCell-root': {
            border: 'none',
          },
        },
      },
    },
  },
});
