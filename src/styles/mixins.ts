import { Styles } from 'styles/theme.types';

export const stableTextWidthHack = (attribute = 'title'): Styles => ({
  '&::after': {
    display: 'block',
    content: `attr(${attribute})`,
    fontWeight: '500',
    height: '1px',
    color: 'transparent',
    overflow: 'hidden',
    visibility: 'hidden',
  },
});
