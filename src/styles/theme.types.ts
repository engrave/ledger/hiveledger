import { SxProps } from '@mui/material';
import { Theme } from '@mui/material/styles';
import { CSSProperties } from 'react';

export type Styles = SxProps<Theme>;

declare module '@mui/material/styles' {
  interface Palette {
    alert: {
      main: string;
      success: string;
      error: string;
      warning: string;
    };
    header: Palette['primary'];
    typography: Palette['primary'];
    ui: {
      separator: string;
      outlinedMenuBorder: string;
      activeElement: string;
      inactiveElement: string;
      codeBackground: string;
      cardElement: string;
      codeText: string;
      progressBarOutline: string;
    };
  }
  interface PaletteOptions {
    alert: {
      main: string;
      success: string;
      error: string;
      warning: string;
    };
    header: PaletteOptions['primary'] & {
      separator: string;
      activeMenu: string;
    };
    typography: {
      common: string;
      header: string;
      lightText: string;
      callToActionLink: string;
      errorMessage: string;
      infoStyles: string;
      addition: string;
      subtraction: string;
    };
    ui: {
      separator: string;
      outlinedMenuBorder: string;
      activeElement: string;
      inactiveElement: string;
      codeBackground: string;
      cardElement: string;
      codeText: string;
      progressBarOutline: string;
    };
    status: {
      ok: string;
      alert: string;
      error: string;
    };
  }
  interface TypographyVariants {
    cardTitle: CSSProperties;
  }
  interface TypographyVariantsOptions {
    cardTitle?: CSSProperties;
  }
}

declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    cardTitle: true;
  }
}

declare module '@mui/material/Button/Button' {
  interface ButtonPropsVariantOverrides {
    microText: true;
  }
}

declare module '@mui/material/Button/buttonClasses' {
  interface ButtonClasses {
    microText: string;
  }
}

declare module '@mui/material/SvgIcon/SvgIcon' {
  interface SvgIconPropsColorOverrides {
    microText: string;
    'alert.warning': true;
  }
}
