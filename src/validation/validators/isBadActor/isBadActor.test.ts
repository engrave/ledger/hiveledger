import { TestContext } from 'yup';
import { badActorsList } from 'config/badActors';
import { isBadActor } from 'validation/validators/isBadActor/isBadActor';

const context = { createError: () => ({ error: true }) } as unknown as TestContext;

describe('isBadActor', () => {
  it('return error object for each bad actor', () => {
    const results = badActorsList.map((name) => isBadActor(name, context));

    expect(results.every((item) => item !== true)).toEqual(true);
  });

  it('return true for empty value', () => {
    expect(isBadActor('', context)).toEqual(true);
  });

  it('return true for not bad actor', () => {
    expect(isBadActor(`invalid name but not bad actor`, context)).toEqual(true);
  });
});
