import { TestContext } from 'yup';
import { badActorsList } from 'config/badActors';

export const isBadActor = (value: string | undefined, context: TestContext) => {
  if (!value) {
    return true;
  }

  if (badActorsList.includes(value)) {
    return context.createError();
  }

  return true;
};
