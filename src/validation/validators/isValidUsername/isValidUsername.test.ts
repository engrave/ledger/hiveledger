import { TestContext } from 'yup';
import { isValidUsername } from 'validation/validators/isValidUsername/isValidUsername';

const context = { createError: () => ({ error: true }) } as unknown as TestContext;

describe('isValidUsername', () => {
  it('returns error when username is shorter than 3 characters', () => {
    expect(isValidUsername('dd', context)).toEqual({ error: true });
  });
  it('returns error when username is longer than 16 characters', () => {
    expect(isValidUsername('a2345678901234567', context)).toEqual({ error: true });
  });
  it('returns error when username has a segment shorter than 3 characters', () => {
    expect(isValidUsername('lorem.ip', context)).toEqual({ error: true });
  });
  it('returns error when segment has starting with not a letter', () => {
    expect(isValidUsername('lorem.4ipsum', context)).toEqual({ error: true });
  });
  it('returns error when segment has something other than letters, digits and dashes', () => {
    expect(isValidUsername('l()rem.ipsum', context)).toEqual({ error: true });
  });
  it('returns error when segment ends with not a letter or a digit', () => {
    expect(isValidUsername('lorem-.ipsum', context)).toEqual({ error: true });
  });
  it('returns error when segment contains uppercase letters', () => {
    expect(isValidUsername('Lorem.ipsum', context)).toEqual({ error: true });
  });
  it('returns true for a valid name', () => {
    expect(isValidUsername('l0rem.ip-sum-2.', context)).toEqual({ error: true });
  });
  it('returns true for a valid name', () => {
    expect(isValidUsername('hive------blocks', context)).toEqual(true);
  });
});
