import { string, TestContext } from 'yup';

/**
 * Names must comply with the following grammar (RFC 1035):
 * <domain> ::= <subdomain> | " "
 * <subdomain> ::= <label> | <subdomain> "." <label>
 * <label> ::= <letter> [ [ <ldh-str> ] <let-dig> ]
 * <ldh-str> ::= <let-dig-hyp> | <let-dig-hyp> <ldh-str>
 * <let-dig-hyp> ::= <let-dig> | "-"
 * <let-dig> ::= <letter> | <digit>
 *
 * Which is equivalent to the following:
 *
 * <domain> ::= <subdomain> | " "
 * <subdomain> ::= <label> ("." <label>)*
 * <label> ::= <letter> [ [ <let-dig-hyp>+ ] <let-dig> ]
 * <let-dig-hyp> ::= <let-dig> | "-"
 * <let-dig> ::= <letter> | <digit>
 *
 * I.e. a valid name consists of a dot-separated sequence
 * of one or more labels consisting of the following rules:
 *
 * - Each label is three characters or more
 * - Each label begins with a letter
 * - Each label ends with a letter or digit
 * - Each label contains only letters, digits or hyphens
 *
 * In addition we require the following:
 *
 * - All letters are lowercase
 * - Length is between (inclusive) HIVE_MIN_ACCOUNT_NAME_LENGTH and HIVE_MAX_ACCOUNT_NAME_LENGTH
 */

const generalSchema = string().min(3).max(16);

const segmentSchema = string()
  .min(3) // Each label is three characters or more
  .matches(/^[a-z]/) // Each label begins with a letter
  .matches(/[a-z0-9]$/) // Each label ends with a letter or digit
  .matches(/^[a-z0-9-]*$/); // Each label contains only letters, digits or hyphens

export const isValidUsername = (value: string | undefined, context: TestContext) => {
  if (!value) {
    return context.createError();
  }

  const generalValidationResult = generalSchema.isValidSync(value);

  if (!generalValidationResult) {
    return context.createError();
  }

  const segments = value.split('.');

  const segmentsValidationResults = segments.map((singleFragment) => {
    return segmentSchema.isValidSync(singleFragment);
  });

  if (segmentsValidationResults.some((result) => result === false)) {
    return context.createError();
  }

  return true;
};
