import { TestContext } from 'yup';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { Balance } from 'modules/hive/wallet/wallet';
import { isEnoughAssets } from 'validation/validators/isEnoughAssets/isEnoughAssets';

const context = {
  parent: {
    amount: 123,
    assetHive: ChainAssetSymbol.hive,
    assetHbd: ChainAssetSymbol.hbd,
  },
  createError: () => ({ error: true }),
} as unknown as TestContext;

describe('isEnoughAssets', () => {
  const balance: Balance = {
    hive: createAsset(200, ChainAssetSymbol.hive),
    hbd: createAsset(50, ChainAssetSymbol.hbd),
  };

  it('returns true if the value is less or equal than balance', () => {
    const resultHive = isEnoughAssets('assetHive', balance)(123, context);
    expect(resultHive).toEqual(true);

    const resultHbd = isEnoughAssets('assetHbd', balance)(50, context);
    expect(resultHbd).toEqual(true);
  });

  it('returns error object if the value higher than balance', () => {
    const resultHive = isEnoughAssets('assetHive', balance)(300, context);
    expect(resultHive).toEqual({ error: true });

    const resultHbd = isEnoughAssets('assetHbd', balance)(300, context);
    expect(resultHbd).toEqual({ error: true });
  });
});
