import { TestContext } from 'yup';
import { Asset } from 'modules/hive/asset/asset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { Balance } from 'modules/hive/wallet/wallet';

export type EnoughAssetsMessageParams = {
  value: number;
  available: Asset<ChainAssetSymbol>;
};

export const isEnoughAssets = (assetField: string, balance: Balance) => {
  return (value: number | undefined, context: TestContext) => {
    const availableAmount = context.parent[assetField] === ChainAssetSymbol.hive ? balance.hive : balance.hbd;

    if (!!value && value <= availableAmount.amount) {
      return true;
    }

    return context.createError({
      params: {
        value,
        available: availableAmount,
      },
    });
  };
};
