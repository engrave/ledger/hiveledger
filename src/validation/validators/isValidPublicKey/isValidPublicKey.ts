import { PublicKey } from '@hiveio/dhive';

export const isValidPublicKey = (publicKey: string) => {
  try {
    PublicKey.fromString(publicKey);
  } catch (error) {
    return false;
  }

  return true;
};
