import { PrivateKey } from '@hiveio/dhive';

export const isValidPrivateKey = (privateKey: string) => {
  try {
    PrivateKey.fromString(privateKey);
  } catch (error) {
    return false;
  }

  return true;
};
