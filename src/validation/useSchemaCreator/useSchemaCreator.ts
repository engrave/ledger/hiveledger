import { useLocalization } from 'context/localization/hooks/useLocalization';
import { Translator, useTranslator } from 'context/localization/hooks/useTranslator';
import { LocalizationContext } from 'context/localization/localizationContext/localizationContext.types';

export type SchemaCreatorParams = {
  translate: Translator;
  localization: LocalizationContext;
};

export type SchemaCreator<T> = (params: SchemaCreatorParams) => T;

export const useSchemaCreator = <T>(creator: SchemaCreator<T>): T => {
  const translate = useTranslator();
  const localization = useLocalization();

  return creator({ translate, localization });
};
