import { Styles } from 'styles/theme.types';

export const dialogHorizontalErrorStyles: Styles = {
  fontSize: '0.75rem',
  minHeight: '1.5em',
  lineHeight: '1.5em',
  color: 'typography.errorMessage',
  margin: '0 30px 20px',
};
