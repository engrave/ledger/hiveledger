import { Box } from '@mui/material';
import { WrapperProps } from 'types/react';
import { dialogHorizontalErrorStyles } from 'ui/modal/dialogHorizontalError/DialogHorizontalError.styles';

export const DialogHorizontalError = ({ children }: WrapperProps) => {
  return <Box sx={dialogHorizontalErrorStyles}>{children}</Box>;
};
