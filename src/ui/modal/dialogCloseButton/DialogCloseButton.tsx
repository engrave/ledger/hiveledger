import CloseIcon from '@mui/icons-material/Close';
import { IconButton } from '@mui/material';
import { closeButtonStyles } from 'ui/modal/dialogCloseButton/DialogCloseButton.styles';

export type DialogCloseButtonProps = {
  onClick: () => void;
};

export const DialogCloseButton = (props: DialogCloseButtonProps) => {
  return (
    <IconButton onClick={() => props.onClick()} sx={closeButtonStyles}>
      <CloseIcon />
    </IconButton>
  );
};
