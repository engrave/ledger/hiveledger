import { Styles } from 'styles/theme.types';

export const closeButtonStyles: Styles = {
  position: 'absolute',
  right: 8,
  top: 8,
  color: 'palette.grey.500',
};
