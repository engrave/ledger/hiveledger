import { Box } from '@mui/material';
import { WrapperProps } from 'types/react';
import { dialogVerticalErrorStyles } from 'ui/modal/dialogVerticalError/DialogVerticalError.styles';

export const DialogVerticalError = ({ children }: WrapperProps) => {
  return <Box sx={dialogVerticalErrorStyles}>{children}</Box>;
};
