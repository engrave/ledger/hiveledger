import { Styles } from 'styles/theme.types';

export const dialogVerticalErrorStyles: Styles = {
  textAlign: 'center',
  fontSize: '0.75rem',
  color: 'typography.errorMessage',
  minHeight: '20px',
  marginTop: '10px',
};
