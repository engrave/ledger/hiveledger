import { Styles } from 'styles/theme.types';

export const wrapperStyles: Styles = {
  display: 'flex',
  gap: 3,
  padding: '20px 30px 10px',
  alignItems: 'center',

  '& > :first-of-type': {
    flexGrow: 1,
    padding: 0,
  },
  '& > :last-of-type': {
    padding: 0,
  },
  '.MuiDialogTitle-root + &': {
    paddingTop: 0,
  },
};
