import { Box } from '@mui/material';
import { WrapperProps } from 'types/react';
import { wrapperStyles } from 'ui/modal/dialogHorizontalWrapper/DialogHorizontalWrapper.styles';

export const DialogHorizontalWrapper = ({ children }: WrapperProps) => {
  return <Box sx={wrapperStyles}>{children}</Box>;
};
