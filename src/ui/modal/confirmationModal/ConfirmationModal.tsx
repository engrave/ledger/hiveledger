import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { ConfirmationModalProps } from 'ui/modal/confirmationModal/ConfirmationModal.types';
import { DialogCloseButton } from 'ui/modal/dialogCloseButton/DialogCloseButton';

export const ConfirmationModal = (props: ConfirmationModalProps) => {
  const translate = useTranslator();

  const handleConfirm = () => {
    props.onConfirm();
  };

  const handleCancel = () => {
    props.onCancel();
  };

  return (
    <Dialog open={props.isOpen} onClose={() => handleCancel()} fullWidth={true}>
      {props.title && <DialogTitle>{props.title}</DialogTitle>}
      <DialogCloseButton onClick={() => handleCancel()} />

      <DialogContent>{props.children}</DialogContent>

      <DialogActions>
        <Button variant={'outlined'} onClick={() => handleCancel()} color={'secondary'}>
          {translate('ui.confirmationDialog.cancel')}
        </Button>
        <Button variant={'contained'} onClick={() => handleConfirm()}>
          {translate('ui.confirmationDialog.confirm')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
