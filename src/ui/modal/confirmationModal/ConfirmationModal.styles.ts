import { Styles } from 'styles/theme.types';

export const actionStyles: Styles = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  gap: 2,
  textAlign: 'center',
  marginTop: '30px',
};
