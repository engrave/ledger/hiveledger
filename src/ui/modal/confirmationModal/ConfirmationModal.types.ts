import { ReactNode } from 'react';

export type ConfirmationModalProps = {
  isOpen: boolean;
  onConfirm: () => void;
  onCancel: () => void;
  title?: string;
  children: ReactNode | ReactNode[];
};
