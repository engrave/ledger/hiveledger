import { Box, CircularProgress } from '@mui/material';
import { sectionLoaderStyles } from 'ui/progress/sectionLoader/SectionLoader.styles';

export type SectionLoaderProps = {
  height?: number;
};

export const SectionLoader = (props: SectionLoaderProps) => {
  return (
    <Box sx={sectionLoaderStyles(props.height)}>
      <CircularProgress color="primary" />
    </Box>
  );
};
