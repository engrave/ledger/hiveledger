import { Styles } from 'styles/theme.types';

export const sectionLoaderStyles = (height = 100): Styles => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  minHeight: `${height}px`,
});
