import { Styles } from 'styles/theme.types';

export const progressBarStyles = (animationDuration: number): Styles => ({
  '.MuiLinearProgress-bar1Determinate': {
    transitionDuration: `${animationDuration}ms`,
  },
});

export const containerStyles = (animationDuration: number, isFinished: boolean): Styles => ({
  opacity: isFinished ? 0 : 1,
  pointerEvents: 'none',
  transition: `opacity ${animationDuration}ms linear`,
  position: 'fixed',
  top: 0,
  width: '100vw',
  zIndex: 1500,
});
