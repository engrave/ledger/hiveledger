import { Box, LinearProgress } from '@mui/material';
import { useNProgress } from '@tanem/react-nprogress';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { containerStyles, progressBarStyles } from 'ui/progress/globalProgressBar/GlobalProgressBar.styles';

export const GlobalProgressBar = () => {
  const { isAnimating, stateKey } = useGlobalProgressBar();

  const { animationDuration, isFinished, progress } = useNProgress({
    isAnimating,
    minimum: 0.001,
  });

  return (
    <Box sx={containerStyles(animationDuration, isFinished)}>
      <LinearProgress
        sx={progressBarStyles(animationDuration)}
        value={progress * 100}
        variant="determinate"
        key={stateKey}
      />
    </Box>
  );
};
