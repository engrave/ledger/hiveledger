import { Box, Skeleton } from '@mui/material';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { Styles } from 'styles/theme.types';
import {
  progressBarTitleStyles,
  progressBarStyles,
  progressBarWrapperStyles,
  progressBarInnerBarStyles,
  progressBarLabelStyles,
} from 'ui/progress/progressBar/ProgressBar.styles';
import { combineStyles } from 'utils/styles/combineStyles';

export type ProgressBarProps = {
  title?: string;
  percent: number | null;
  sx?: Styles;
};

export const ProgressBar = (props: ProgressBarProps) => {
  const { formatNumber } = useLocalization();

  return (
    <Box sx={combineStyles(progressBarWrapperStyles, props.sx)}>
      {props.title ? <Box sx={progressBarTitleStyles}>{props.title}</Box> : null}

      <Box sx={progressBarStyles}>
        {props.percent !== null && (
          <>
            <Box sx={progressBarInnerBarStyles(props.percent)} />
            <Box sx={progressBarLabelStyles}>{formatNumber(props.percent, { style: 'percent' })}</Box>
          </>
        )}

        {props.percent === null && <Skeleton height={'10px'} variant={'rectangular'} animation={'wave'} />}
      </Box>
    </Box>
  );
};
