import { Styles } from 'styles/theme.types';

export const progressBarWrapperStyles: Styles = {
  display: 'flex',
  flexDirection: 'column',
  gap: '0.5rem',
};

export const progressBarTitleStyles: Styles = {
  fontSize: '0.625rem',
  color: 'typography.lightText',
  fontWeight: 'normal',
  textTransform: 'uppercase',
  textAlign: 'center',
};

export const progressBarStyles: Styles = {
  outline: (theme) => `1px solid ${theme.palette.ui.progressBarOutline}`,
  border: '2px solid white',
  backgroundColor: 'background.default',
  height: '14px',
  width: '100%',
  margin: 'auto',
  position: 'relative',
};

export const progressBarInnerBarStyles = (percent: number): Styles => ({
  backgroundColor: 'ui.activeElement',
  width: `${percent * 100}%`,
  height: '10px',
  display: percent >= 0.01 ? 'block' : 'none',
});

export const progressBarLabelStyles: Styles = {
  fontSize: '0.625rem',
  color: 'common.white',
  fontWeight: '500',
  position: 'absolute',
  right: '4px',
  top: '-2px',
  mixBlendMode: 'difference',
};
