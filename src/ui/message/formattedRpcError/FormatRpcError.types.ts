import { RpcError } from 'modules/ledger/rpcError/rpcError';

export type FormattedRpcErrorProps = {
  error: RpcError;
};
