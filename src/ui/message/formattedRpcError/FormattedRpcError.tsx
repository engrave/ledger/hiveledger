import { FormattedMessage } from 'react-intl';
import { isTranslationKey } from 'context/localization/dictionaries/dictionaries';
import { formatRpcError } from 'modules/ledger/rpcError/formatRpcError/formatRpcError';
import { FormattedRpcErrorProps } from 'ui/message/formattedRpcError/FormatRpcError.types';
import { TranslatedMessage } from 'ui/message/translatedMessage/TranslatedMessage';

export const FormattedRpcError = (props: FormattedRpcErrorProps) => {
  const { message, values } = formatRpcError(props.error);

  const errorTranslationSlug = `errors.${message}`;

  if (isTranslationKey(errorTranslationSlug)) {
    return <TranslatedMessage id={errorTranslationSlug} values={values} />;
  }

  const normalizedSlug = message.replaceAll('${', '{') || 'errors.genericTransactionError';

  return <FormattedMessage id={normalizedSlug} defaultMessage={normalizedSlug} values={values} />;
};
