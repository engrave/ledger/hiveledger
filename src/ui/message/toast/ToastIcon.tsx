import WarningRoundedIcon from '@mui/icons-material/WarningRounded';
import { useTheme } from '@mui/material';
import { Theme, TypeOptions } from 'react-toastify/dist/types';
import { ReactComponent as ErrorIcon } from 'assets/images/icons/error.svg';
import { ReactComponent as OkIcon } from 'assets/images/icons/ok.svg';

export type ToastIconProps = {
  theme: Theme;
  type: TypeOptions;
};

export const ToastIcon = (props: ToastIconProps) => {
  const theme = useTheme();

  if (props.type === 'success') {
    return <OkIcon />;
  }

  if (props.type === 'error') {
    return <ErrorIcon />;
  }

  if (props.type === 'warning') {
    return <WarningRoundedIcon htmlColor={theme.palette.alert.warning} />;
  }

  return null;
};
