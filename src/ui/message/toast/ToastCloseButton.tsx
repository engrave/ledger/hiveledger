import { Box } from '@mui/material';
import { ReactComponent as CloseIcon } from 'assets/images/icons/close.svg';

export const ToastCloseButton = () => {
  return (
    <Box className={'Toastify__close-button'}>
      <CloseIcon />
    </Box>
  );
};
