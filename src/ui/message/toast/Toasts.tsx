import { Box } from '@mui/material';
import { Slide, ToastContainer } from 'react-toastify';
import { ToastCloseButton } from './ToastCloseButton';
import { ToastIcon } from './ToastIcon';
import { toastsStyles } from './Toasts.styles';

export const Toasts = () => {
  return (
    <Box
      component={ToastContainer}
      hideProgressBar
      transition={Slide}
      sx={toastsStyles}
      closeButton={<ToastCloseButton />}
      width={'408px'}
      icon={ToastIcon}
    />
  );
};
