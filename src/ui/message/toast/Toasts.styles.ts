import { Styles } from '../../../styles/theme.types';

export const toastsStyles: Styles = {
  '& .Toastify__toast': {
    display: 'block',
    padding: 2.5,
    borderRadius: '6px',
    boxShadow: (theme) => theme.shadows[2],

    '&.Toastify__toast-theme--light': {
      color: 'header.main',
      background: 'white',
    },

    '&.Toastify__toast--success': {
      borderLeft: (theme) => `4px solid ${theme.palette.alert.success}`,
    },

    '&.Toastify__toast--error': {
      borderLeft: (theme) => `4px solid ${theme.palette.alert.error}`,
    },

    '&.Toastify__toast--warning': {
      borderLeft: (theme) => `4px solid ${theme.palette.alert.warning}`,
    },
  },
  '& .Toastify__toast-body': {
    margin: 0,
    padding: 0,
    fontSize: '1rem',
    fontWeight: '500',
    fontFamily: (theme) => theme.typography.fontFamily,
  },
  '& .Toastify__toast-icon': {
    marginInlineEnd: '15px',
  },
  '& .Toastify__close-button': {
    position: 'absolute',
    top: '15px',
    right: '15px',
    lineHeight: 0,

    '& > svg': {
      width: '8px',
      height: '8px',
    },
  },
};
