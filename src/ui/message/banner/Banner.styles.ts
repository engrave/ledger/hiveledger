import { Styles } from 'styles/theme.types';

export const bannerStyles: Styles = {
  padding: '34px 50px',
  marginBottom: '30px',
  fontSize: '14px',
};

export const textContainerStyles: Styles = {
  padding: '4px 0 4px 26px',
  borderColor: 'alert.main',
  borderStyle: 'solid',
  borderWidth: '0 0 0 4px',
};
