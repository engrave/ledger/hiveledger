import { Box, Paper } from '@mui/material';
import { WrapperProps } from 'types/react';
import { bannerStyles, textContainerStyles } from 'ui/message/banner/Banner.styles';

export const Banner = ({ children }: WrapperProps) => {
  return (
    <Paper square elevation={0} sx={bannerStyles}>
      <Box sx={textContainerStyles}>{children}</Box>
    </Paper>
  );
};
