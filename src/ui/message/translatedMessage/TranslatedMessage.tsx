import { TranslationKey, TranslationValues } from 'context/localization/dictionaries/dictionaries';
import { useTranslator } from 'context/localization/hooks/useTranslator';

export type TranslatedMessageProps = {
  id: TranslationKey;
  values?: TranslationValues;
};

export const TranslatedMessage = (props: TranslatedMessageProps) => {
  const translate = useTranslator();

  return <>{translate(props.id, props.values)}</>;
};
