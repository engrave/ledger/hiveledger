import { Styles } from 'styles/theme.types';
import { HeadsUpPosition } from 'ui/message/headsUp/HeadsUp';

export const headsUpStyles = (position: HeadsUpPosition): Styles => ({
  borderWidth: '0 0 0 4px',
  borderStyle: 'solid',
  borderColor: 'alert.main',
  margin: position === HeadsUpPosition.top ? '0 0 10px' : '-20px 0 30px',
  padding: '8px 20px 8px 0',
  fontSize: '0.75rem',
  display: 'flex',
  alignItems: 'center',
});

export const alertIconStyles: Styles = {
  minWidth: '60px',
  height: '46px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  '& > svg': {
    width: '24px',
    height: '24px',
  },
};
