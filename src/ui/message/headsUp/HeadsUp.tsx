import { Box, Paper } from '@mui/material';
import { ReactNode } from 'react';
import { ReactComponent as AlertIcon } from 'assets/images/icons/alert.svg';
import { alertIconStyles, headsUpStyles } from 'ui/message/headsUp/HeadsUp.styles';

export enum HeadsUpPosition {
  top = 'top',
  bottom = 'bottom',
}

export type HeadsUpProps = {
  children: ReactNode;
  position?: HeadsUpPosition;
};

export const HeadsUp = ({ children, position = HeadsUpPosition.bottom }: HeadsUpProps) => {
  return (
    <Paper elevation={0} square sx={headsUpStyles(position)}>
      <Box sx={alertIconStyles}>
        <AlertIcon />
      </Box>
      <Box>{children}</Box>
    </Paper>
  );
};
