import { Box, TooltipProps as MuiTooltipProps, Tooltip as MuiTooltip } from '@mui/material';
import { ElementType, ReactElement } from 'react';
import * as React from 'react';

export type TooltipProps = Omit<MuiTooltipProps, 'children'> & {
  wrap?: ElementType;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  children: ReactElement | string;
};

export const Tooltip = (props: TooltipProps) => {
  const { wrap, ...tooltipProps } = props;

  if (wrap) {
    return (
      <MuiTooltip placement={'top'} arrow={true} {...tooltipProps}>
        <Box component={wrap}>{props.children}</Box>
      </MuiTooltip>
    );
  }

  if (typeof props.children === 'string') {
    return (
      <MuiTooltip placement={'top'} arrow={true} {...tooltipProps}>
        <span>{props.children}</span>
      </MuiTooltip>
    );
  }

  return (
    <MuiTooltip placement={'top'} arrow={true} {...tooltipProps}>
      {props.children}
    </MuiTooltip>
  );
};
