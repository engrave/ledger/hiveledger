import { IconButton, Tooltip, TooltipProps } from '@mui/material';
import { ReactComponent as HelpIcon } from 'assets/images/icons/help.svg';

export type HelpTooltipProps = Omit<TooltipProps, 'children'>;

export const HelpTooltip = (props: HelpTooltipProps) => {
  return (
    <Tooltip placement={'top'} {...props}>
      <IconButton disableRipple>
        <HelpIcon />
      </IconButton>
    </Tooltip>
  );
};
