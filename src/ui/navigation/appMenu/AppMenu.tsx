import { Box, Link } from '@mui/material';
import { createContext, useContext } from 'react';
import { Link as RouterLink, useMatch } from 'react-router-dom';
import { appMenuItemStyles, menuLinkStyles, appMenuStyles } from 'ui/navigation/appMenu/AppMenu.styles';
import { AppMenuContext, AppMenuItemProps, AppMenuProps } from 'ui/navigation/appMenu/AppMenu.types';

export const appMenuContext = createContext<AppMenuContext>({ compact: false });

export const AppMenuItem = ({ children, to }: AppMenuItemProps) => {
  const context = useContext(appMenuContext);
  const match = useMatch(to);
  const isActive = match !== null;

  return (
    <Box component={'li'} sx={appMenuItemStyles}>
      <Link to={to} component={RouterLink} sx={menuLinkStyles(isActive, context.compact)} title={children}>
        {children}
      </Link>
    </Box>
  );
};

export const AppMenu = ({ children, compact }: AppMenuProps) => {
  const AppContextProvider = appMenuContext.Provider;

  return (
    <AppContextProvider value={{ compact }}>
      <Box component={'ul'} sx={appMenuStyles}>
        {children}
      </Box>
    </AppContextProvider>
  );
};
