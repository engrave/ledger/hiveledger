import { stableTextWidthHack } from 'styles/mixins';
import { Styles } from 'styles/theme.types';

export const appMenuStyles = {
  listStyle: 'none',
  margin: 0,
  padding: 0,
  display: 'inline-flex',
  gap: 0.5,
};

export const menuLinkStyles = (isActive: boolean, isCompact: boolean): Styles => ({
  display: 'block',
  lineHeight: '1rem',
  padding: isCompact ? '11px 13px 12px' : '11px 20px 12px',
  backgroundColor: isActive && !isCompact ? 'header.activeMenu' : 'transparent',
  fontSize: isCompact ? '0.875rem' : '1rem',
  fontWeight: isActive && !isCompact ? '500' : 'normal',
  color: 'common.white',
  borderRadius: '6px',
  textAlign: 'center',
  ...stableTextWidthHack(),
  transition: 'opacity 0.3s, padding 0.15s',
});

export const appMenuItemStyles: Styles = {
  marginRight: 0,
};
