import { ReactNode } from 'react';

export type AppMenuContext = {
  compact: boolean;
};

export type AppMenuProps = {
  children: ReactNode[];
  compact: boolean;
};

export type AppMenuItemProps = {
  children: string;
  to: string;
};
