import { ReactNode } from 'react';

export enum StatusDotColor {
  ok = 'status.ok',
  alert = 'status.alert',
  error = 'status.error',
}

export type StatusDotProps = {
  label: ReactNode;
  color: StatusDotColor;
  tooltip?: string;
};
