import { Box } from '@mui/material';
import { dotIconStyles, statusDotStyles } from 'ui/label/statusDot/StatusDot.styles';
import { StatusDotProps } from 'ui/label/statusDot/StatusDot.types';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';

export const StatusDot = (props: StatusDotProps) => {
  return (
    <Tooltip title={props.tooltip ?? ''} sx={statusDotStyles} wrap={Box}>
      <>
        <Box sx={dotIconStyles(props.color)} />
        {props.label}
      </>
    </Tooltip>
  );
};
