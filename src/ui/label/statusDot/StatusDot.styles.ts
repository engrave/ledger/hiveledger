import { Styles } from 'styles/theme.types';
import { StatusDotColor } from 'ui/label/statusDot/StatusDot.types';

export const statusDotStyles: Styles = {
  display: 'flex',
  alignItems: 'center',
  gap: 0.5,
  fontSize: '0.625rem',
  fontWeight: 500,
  color: 'typography.lightText',
  lineHeight: 0,
  height: '1rem',
  cursor: 'default',
};

export const dotIconStyles = (color: StatusDotColor): Styles => ({
  width: '6px',
  height: '6px',
  borderRadius: '50%',
  backgroundColor: color,
});
