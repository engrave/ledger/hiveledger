import { DefaultRenderer } from '@hiveio/content-renderer';
import { Box } from '@mui/material';
import { useEffect, useMemo, useRef } from 'react';
import { postRendering } from 'config/config';
import { postContentStyles } from 'ui/post/postContent/PostContent.styles';
import { PostContentProps } from 'ui/post/postContent/PostContent.types';

export const PostContent = (props: PostContentProps) => {
  const body = useRef<string>('');

  const renderer = useMemo(() => {
    return new DefaultRenderer({
      baseUrl: postRendering.baseUrl,
      breaks: true,
      skipSanitization: false,
      allowInsecureScriptTags: false,
      addNofollowToLinks: true,
      doNotShowImages: false,
      ipfsPrefix: '',
      assetsHeight: postRendering.assetsHeight,
      assetsWidth: postRendering.assetsWidth,
      imageProxyFn: (url: string) => url,
      usertagUrlFn: (account: string) => `${postRendering.baseUrl}@${account}`,
      hashtagUrlFn: (hashtag: string) => `${postRendering.baseUrl}trending/${hashtag}`,
      isLinkSafeFn: () => true,
    });
  }, []);

  useEffect(() => {
    body.current = renderer.render(props.content);
  }, [props.content, renderer]);

  return <Box dangerouslySetInnerHTML={{ __html: body.current }} sx={postContentStyles} />;
};
