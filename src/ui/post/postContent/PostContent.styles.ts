import { Styles } from 'styles/theme.types';

export const postContentStyles: Styles = {
  wordWrap: 'break-word',
  img: {
    maxWidth: '100%',
  },
  code: {
    backgroundColor: 'ui.codeBackground',
    fontSize: '0.9rem',
    padding: '2px 6px',
    borderRadius: '3px',
    display: 'inline-block',
  },
  'pre > code': {
    width: '100%',
  },
};
