export type StepProps = {
  isActive: boolean;
  name: string;
  description: string;
};
