import { Styles } from 'styles/theme.types';

export const stepperStyles: Styles = {
  display: 'grid',
  gridAutoColumns: 'minmax(0, 1fr)',
  gridAutoFlow: 'column',
  gap: '30px',
  marginBottom: '40px',
};

export const stepWrapperStyles = (isActive: boolean): Styles => ({
  borderWidth: '2px 0 0 0',
  borderStyle: 'solid',
  borderColor: isActive ? 'ui.activeElement' : 'ui.inactiveElement',
  paddingTop: '11px',
});

export const stepNameStyles = (isActive: boolean): Styles => ({
  fontSize: '0.6875rem',
  fontWeight: 600,
  color: isActive ? 'ui.activeElement' : 'secondary.main',
  textTransform: 'uppercase',
});

export const stepDescriptionStyles = (isActive: boolean): Styles => ({
  fontSize: '0.6875rem',
  fontWeight: 'normal',
  color: isActive ? 'typography.common' : 'secondary.main',
});
