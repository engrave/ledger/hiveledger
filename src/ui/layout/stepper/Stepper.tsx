import { Box } from '@mui/material';
import { WrapperProps } from 'types/react';
import {
  stepDescriptionStyles,
  stepNameStyles,
  stepperStyles,
  stepWrapperStyles,
} from 'ui/layout/stepper/Stepper.styles';
import { StepProps } from 'ui/layout/stepper/Stepper.types';

export const Step = ({ isActive, name, description }: StepProps) => {
  return (
    <Box sx={stepWrapperStyles(isActive)}>
      <Box sx={stepNameStyles(isActive)}>{name}</Box>
      <Box sx={stepDescriptionStyles(isActive)}>{description}</Box>
    </Box>
  );
};

export const Stepper = ({ children }: WrapperProps) => {
  return <Box sx={stepperStyles}>{children}</Box>;
};
