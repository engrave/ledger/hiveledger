import { Container } from '@mui/material';
import { WrapperProps } from 'types/react';
import { appContainerStyles } from 'ui/layout/appContainer/AppContainer.styles';

export const AppContainer = (props: WrapperProps) => {
  return (
    <Container fixed sx={appContainerStyles}>
      {props.children}
    </Container>
  );
};
