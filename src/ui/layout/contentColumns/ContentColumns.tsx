import { Box } from '@mui/material';
import { WrapperProps } from 'types/react';
import { contentColumnsStyles, contentStyles, sidebarStyles } from 'ui/layout/contentColumns/ContentColumns.styles';

export const ContentColumns = ({ children }: WrapperProps) => {
  return <Box sx={contentColumnsStyles}>{children}</Box>;
};

export const Content = ({ children }: WrapperProps) => {
  return (
    <Box component={'main'} sx={contentStyles}>
      {children}
    </Box>
  );
};

export const Sidebar = ({ children }: WrapperProps) => {
  return (
    <Box component={'aside'} sx={sidebarStyles}>
      {children}
    </Box>
  );
};
