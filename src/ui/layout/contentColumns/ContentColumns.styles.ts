import { Styles } from 'styles/theme.types';

export const contentColumnsStyles: Styles = {
  display: 'grid',
  gridTemplateColumns: {
    lg: '765px 385px',
    xs: '1fr',
  },
  gridTemplateAreas: {
    lg: '"content sidebar"',
    xs: '"content" "sidebar"',
  },
  gap: '30px',
};

export const contentStyles: Styles = {
  gridArea: 'content',
};

export const sidebarStyles: Styles = {
  gridArea: 'sidebar',
};
