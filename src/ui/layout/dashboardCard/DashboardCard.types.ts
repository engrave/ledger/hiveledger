import { ReactNode } from 'react';
import { Styles } from 'styles/theme.types';

export type DashboardCardProps = {
  children: ReactNode;
  sx?: Styles;
};

export type DashboardCardTitleProps = {
  children: ReactNode;
  sx?: Styles;
};
