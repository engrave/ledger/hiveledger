import { Paper, Typography } from '@mui/material';
import { dashboardCardStyles } from 'ui/layout/dashboardCard/DashboardCard.styles';
import { DashboardCardProps, DashboardCardTitleProps } from 'ui/layout/dashboardCard/DashboardCard.types';
import { combineStyles } from 'utils/styles/combineStyles';

export const DashboardCardTitle = (props: DashboardCardTitleProps) => {
  return (
    <Typography variant={'cardTitle'} component={'h2'} sx={props.sx}>
      {props.children}
    </Typography>
  );
};

export const DashboardCard = (props: DashboardCardProps) => {
  return (
    <Paper elevation={2} square sx={combineStyles(dashboardCardStyles, props.sx)}>
      {props.children}
    </Paper>
  );
};
