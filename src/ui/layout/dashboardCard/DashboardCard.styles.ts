import { Styles } from 'styles/theme.types';

export const dashboardCardStyles: Styles = {
  padding: '22px 30px 30px',
  position: 'relative',
  marginBottom: '30px',
};
