import { Box, ButtonBase, Paper, PaperProps, Typography } from '@mui/material';
import { WrapperProps } from 'types/react';
import {
  buttonStyles,
  cardStyles,
  contentStyles,
  iconStyles,
  titleStyles,
} from 'ui/layout/optionCard/OptionCard.styles';

export const OptionCard = ({ children, onClick }: PaperProps) => {
  return (
    <ButtonBase sx={buttonStyles}>
      <Paper elevation={2} sx={cardStyles} square onClick={onClick}>
        {children}
      </Paper>
    </ButtonBase>
  );
};

export const OptionCardIcon = ({ children }: WrapperProps) => {
  return <Box sx={iconStyles}>{children}</Box>;
};

export const OptionCardTitle = ({ children }: WrapperProps) => {
  return (
    <Typography component={'h2'} sx={titleStyles}>
      {children}
    </Typography>
  );
};

export const OptionCardContent = ({ children }: WrapperProps) => {
  return (
    <Typography component={'p'} variant={'body2'} sx={contentStyles}>
      {children}
    </Typography>
  );
};
