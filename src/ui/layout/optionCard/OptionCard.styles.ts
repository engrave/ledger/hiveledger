import { Styles } from 'styles/theme.types';

export const buttonStyles: Styles = {
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'stretch',
};

export const cardStyles: Styles = {
  paddingY: 4,
  paddingX: 4,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  gap: '10px',
  cursor: 'pointer',
  width: '100%',
};

export const iconStyles: Styles = {
  paddingTop: 2,
  paddingBottom: 1,
};

export const titleStyles: Styles = {
  fontSize: '1.25rem',
  fontWeight: 'bold',
  color: 'typography.header',
};

export const contentStyles: Styles = {
  textAlign: 'center',

  a: {
    color: 'typography.callToActionLink',
  },
};
