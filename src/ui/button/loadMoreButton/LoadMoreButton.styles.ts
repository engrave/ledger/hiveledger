import { Styles } from 'styles/theme.types';

export const loadMoreButtonStyles: Styles = {
  width: '46px',
  height: '46px',
  margin: 'auto',
  display: 'flex',
};
