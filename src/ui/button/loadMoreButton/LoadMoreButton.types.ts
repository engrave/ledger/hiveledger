import { Styles } from 'styles/theme.types';

export type LoadMoreButtonProps = {
  isLoading: boolean;
  onClick: () => void;
  sx?: Styles;
};
