import { CircularProgress, IconButton } from '@mui/material';
import { ReactComponent as ChevronDownIcon } from 'assets/images/icons/chevronDown.svg';
import { loadMoreButtonStyles } from 'ui/button/loadMoreButton/LoadMoreButton.styles';
import { LoadMoreButtonProps } from 'ui/button/loadMoreButton/LoadMoreButton.types';
import { combineStyles } from 'utils/styles/combineStyles';

export const LoadMoreButton = (props: LoadMoreButtonProps) => {
  return (
    <IconButton disabled={props.isLoading} sx={combineStyles(loadMoreButtonStyles, props.sx)} onClick={props.onClick}>
      {props.isLoading && <CircularProgress size={30} />}
      {!props.isLoading && <ChevronDownIcon />}
    </IconButton>
  );
};
