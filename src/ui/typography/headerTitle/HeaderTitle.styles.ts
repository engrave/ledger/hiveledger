import { Styles } from 'styles/theme.types';

export const headerTitleStyles: Styles = {
  fontSize: '2rem',
  fontWeight: 'bold',
  lineHeight: '40px',
  marginTop: '10px',
  textAlign: 'center',
  marginBottom: 5,
  color: 'header.contrastText',
};
