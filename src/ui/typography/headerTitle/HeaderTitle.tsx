import { Box } from '@mui/material';
import { WrapperProps } from 'types/react';
import { headerTitleStyles } from 'ui/typography/headerTitle/HeaderTitle.styles';

export const HeaderTitle = ({ children }: WrapperProps) => {
  return <Box sx={headerTitleStyles}>{children}</Box>;
};
