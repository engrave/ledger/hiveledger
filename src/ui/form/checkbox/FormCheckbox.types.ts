import { FieldError, Path } from 'react-hook-form';
import { Control } from 'react-hook-form/dist/types/form';
import { Styles } from 'styles/theme.types';

export type FormCheckboxProps<T, Context extends object> = {
  name: Path<T>;
  label?: string;
  control: Control<T, Context>;
  error?: FieldError;
  helperText?: string;
  disabled?: boolean;
  sx?: Styles;
};
