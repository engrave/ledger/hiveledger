import { FormControl, FormControlLabel, FormHelperText } from '@mui/material';
import * as React from 'react';
import { Controller } from 'react-hook-form';
import { Control } from 'react-hook-form/dist/types/form';
import { BaseCheckbox } from 'ui/form/checkbox/BaseCheckbox';
import { FormCheckboxProps } from 'ui/form/checkbox/FormCheckbox.types';
import { formControlStyles } from 'ui/form/Form.styles';
import { combineStyles } from 'utils/styles/combineStyles';

export const FormCheckbox = <T, C extends object>(props: FormCheckboxProps<T, C>) => {
  const { name, control, label, helperText, error, disabled = false } = props;

  const hasError = !!error;
  const hasMessage = hasError || !!helperText;

  return (
    <Controller
      control={control as Control<T, object>}
      name={name}
      render={({ field: { value, onChange } }) => (
        <FormControl error={hasError} sx={combineStyles(formControlStyles(hasMessage), props.sx)} disabled={disabled}>
          {label && <FormControlLabel label={label} control={<BaseCheckbox value={value} onChange={onChange} />} />}
          {!label && <BaseCheckbox value={value} onChange={onChange} />}

          {helperText && <FormHelperText>{helperText}</FormHelperText>}
          {hasError && <FormHelperText>{error.message}</FormHelperText>}
        </FormControl>
      )}
    />
  );
};
