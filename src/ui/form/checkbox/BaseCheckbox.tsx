import CheckRoundedIcon from '@mui/icons-material/CheckRounded';
import SquareRoundedIcon from '@mui/icons-material/SquareRounded';
import { Checkbox } from '@mui/material';
import * as React from 'react';

export type BaseCheckboxProps = {
  onChange?: (event: unknown) => void;
  value: unknown;
  onClick?: () => void;
  disabled?: boolean;
};

export const BaseCheckbox = ({ onChange, value, onClick, disabled }: BaseCheckboxProps) => {
  return (
    <Checkbox
      onChange={onChange}
      checked={Boolean(value)}
      color={'secondary'}
      icon={<SquareRoundedIcon />}
      checkedIcon={<CheckRoundedIcon stroke={'4px'} />}
      onClick={() => onClick?.()}
      disabled={disabled}
    />
  );
};
