import { Path } from 'react-hook-form';
import { Control } from 'react-hook-form/dist/types/form';
import { Props as InputMaskProps } from 'react-input-mask';

export type FormMaskedTextProps<T> = {
  name: Path<T>;
  label: string;
  control: Control<T>;
  variant?: 'standard' | 'filled' | 'outlined';
  helperText?: string;
  maskProps: InputMaskProps;
};
