import { TextField, FormControl, InputLabel } from '@mui/material';
import * as React from 'react';
import { Controller } from 'react-hook-form';
import InputMask from 'react-input-mask';
import { FormMaskedTextProps } from 'ui/form/maskedText/FormMaskedText.types';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const FormMaskedText = <T extends Record<string, any>>({
  name,
  control,
  label,
  helperText,
  maskProps,
  variant = 'outlined',
}: FormMaskedTextProps<T>) => {
  return (
    <Controller
      control={control}
      name={name}
      render={({ field: { onChange, value } }) => (
        <InputMask {...maskProps} value={value} onChange={onChange}>
          {(inputProps: React.InputHTMLAttributes<HTMLInputElement>) => (
            <FormControl variant={variant} fullWidth>
              <InputLabel shrink={true}>{label}</InputLabel>
              <TextField
                variant={variant}
                helperText={helperText}
                onChange={inputProps.onChange}
                value={inputProps.value}
              />
            </FormControl>
          )}
        </InputMask>
      )}
    />
  );
};
