import { Styles } from 'styles/theme.types';

export const availableAmountLinkStyles: Styles = {
  textDecoration: 'none',
  transition: 'opacity 0.3s',
  cursor: 'pointer',

  ':hover': {
    opacity: 0.7,
  },
};
