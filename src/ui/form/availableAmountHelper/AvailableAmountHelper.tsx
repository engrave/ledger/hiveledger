import { Box } from '@mui/material';
import { Path } from 'react-hook-form';
import { UnpackNestedValue, UseFormReturn } from 'react-hook-form/dist/types/form';
import { FieldPathValue } from 'react-hook-form/dist/types/path';
import { FormattedMessage } from 'react-intl';
import { TranslationKey } from 'context/localization/dictionaries/dictionaries';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { Asset } from 'modules/hive/asset/asset';
import { getRoundedAmount } from 'modules/hive/asset/getRoundedAmount/getRoundedAmount';
import { availableAmountLinkStyles } from 'ui/form/availableAmountHelper/AvailableAmountHelper.styles';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';

export type AvailableAmountHelperProps<T, N extends Path<T>> = {
  asset?: Asset;
  value?: number;
  target: N;
  form: UseFormReturn<T, object>;
  slug?: TranslationKey;
  tooltip?: string;
};

export const AvailableAmountHelper = <T, N extends Path<T>>(props: AvailableAmountHelperProps<T, N>) => {
  const { formatAssetAmount, formatNumber } = useLocalization();

  const slug: TranslationKey = props.slug ?? 'ui.availableAmountHelper.availableAmount';

  const handleAmountSet = () => {
    const value = props.asset ? getRoundedAmount(props.asset) : props.value ?? 0;

    props.form.setValue(props.target, value as UnpackNestedValue<FieldPathValue<T, N>>);
    props.form.trigger(props.target);
  };

  if (typeof props.asset === 'undefined' && typeof props.value === 'undefined') {
    return null;
  }

  return (
    <FormattedMessage
      id={slug}
      defaultMessage={slug}
      values={{
        amount: (
          <Tooltip title={props.tooltip ?? ''}>
            <Box component={'span'} onClick={handleAmountSet} sx={availableAmountLinkStyles}>
              {props.asset ? formatAssetAmount(props.asset) : formatNumber(props.value || 0)}
            </Box>
          </Tooltip>
        ),
      }}
    />
  );
};
