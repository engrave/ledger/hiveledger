import { FieldError, Path } from 'react-hook-form';
import { Control } from 'react-hook-form/dist/types/form';

export type FormTextProps<T, Context extends object> = {
  name: Path<T>;
  label?: string;
  placeholder?: string;
  control: Control<T, Context>;
  variant?: 'standard' | 'filled' | 'outlined';
  error?: FieldError;
  helperText?: string;
  prefix?: string;
  disabled?: boolean;
  type?: 'text' | 'number' | 'password';
  min?: number;
  max?: number;
  step?: number;
};
