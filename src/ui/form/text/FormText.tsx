import { FormControl, InputLabel, FormHelperText, OutlinedInput, InputAdornment } from '@mui/material';
import * as React from 'react';
import { Controller } from 'react-hook-form';
import { Control } from 'react-hook-form/dist/types/form';
import { formControlStyles } from 'ui/form/Form.styles';
import { FormTextProps } from 'ui/form/text/FormText.types';

export const FormText = <T, C extends object>(props: FormTextProps<T, C>) => {
  const hasError = Boolean(props.error);
  const hasMessage = Boolean(props.error) || Boolean(props.helperText);

  return (
    <Controller
      control={props.control as Control<T, object>}
      name={props.name}
      render={({ field: { onChange, value } }) => (
        <FormControl
          variant={props.variant ?? 'outlined'}
          fullWidth
          error={hasError}
          sx={formControlStyles(hasMessage)}
          disabled={props.disabled ?? false}
        >
          {props.label && <InputLabel shrink={true}>{props.label}</InputLabel>}
          <OutlinedInput
            type={props.type ?? 'text'}
            onChange={(event) => (props.type === 'number' ? onChange(Number(event.target.value)) : onChange(event))}
            value={value}
            placeholder={props.placeholder}
            startAdornment={props.prefix && <InputAdornment position="start">{props.prefix}</InputAdornment>}
            inputProps={{
              min: props.min,
              max: props.max,
              step: props.step,
            }}
          />
          {props.helperText && <FormHelperText>{props.helperText}</FormHelperText>}
          {props.error && <FormHelperText>{props.error.message}</FormHelperText>}
        </FormControl>
      )}
    />
  );
};
