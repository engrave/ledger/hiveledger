import { Alert } from '@mui/material';
import { formAlertStyles } from 'ui/form/alert/FormAlert.styles';
import { FormAlertProps } from 'ui/form/alert/FormAlert.types';

export const FormAlert = ({ children, severity }: FormAlertProps) => {
  return (
    <Alert severity={severity} sx={formAlertStyles}>
      {children}
    </Alert>
  );
};
