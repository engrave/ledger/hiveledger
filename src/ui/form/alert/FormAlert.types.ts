import { AlertColor } from '@mui/material/Alert/Alert';
import { ReactNode } from 'react';

export type FormAlertProps = {
  children: ReactNode;
  severity: AlertColor;
};
