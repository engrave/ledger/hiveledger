import { Styles } from 'styles/theme.types';

export const helpTextStylesList: Styles = {
  display: 'flex',
  justifyContent: 'end',
  gap: '16px',
};
