import { Box } from '@mui/material';
import { WrapperProps } from 'types/react';
import { helpTextStylesList } from 'ui/form/helpTextList/HelpTextList.styles';

export const HelpTextList = ({ children }: WrapperProps) => {
  return (
    <Box component={'span'} sx={helpTextStylesList}>
      {children}
    </Box>
  );
};
