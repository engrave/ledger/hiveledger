import { MenuItem, FormControl, InputLabel, Select, FormHelperText } from '@mui/material';
import { Controller, FieldPath, FieldPathValue } from 'react-hook-form';
import { Control } from 'react-hook-form/dist/types/form';
import { formControlStyles } from 'ui/form/Form.styles';
import { DropdownOption, FormSelectProps } from 'ui/form/select/FormSelect.types';

export const formSelectEmptyValue = '$__empty__';

export const toSelectOptions = (texts: string[], formatLabel?: (label: string) => string): DropdownOption[] => {
  return texts.map((item) => ({
    label: formatLabel ? formatLabel(item) : item,
    value: item,
  }));
};

export const FormSelect = <T, C extends object, N extends FieldPath<T>>(props: FormSelectProps<T, C, N>) => {
  const { name, control, label, options, disabled = false, helperText, onChange, error } = props;

  const hasError = error !== undefined;
  const hasMessage = helperText !== undefined || hasError;

  return (
    <Controller
      control={control as Control<T, object>}
      name={name}
      render={({ field: { onChange: onSelectChange, value } }) => (
        <FormControl variant="outlined" fullWidth error={hasError} sx={formControlStyles(hasMessage)}>
          <InputLabel shrink={true}>{label}</InputLabel>
          <Select
            onChange={(event) => {
              onSelectChange(event);
              onChange?.(event.target.value as FieldPathValue<T, N>);
            }}
            value={value}
            name={name}
            variant={'outlined'}
            displayEmpty
            MenuProps={{ elevation: 1 }}
            disabled={disabled}
          >
            {options.map((option: DropdownOption, index) => {
              return (
                <MenuItem key={index} value={option.value || ''}>
                  {option.label}
                </MenuItem>
              );
            })}
          </Select>
          {helperText && <FormHelperText>{helperText}</FormHelperText>}
          {hasError && <FormHelperText error>{error.message}</FormHelperText>}
        </FormControl>
      )}
    />
  );
};
