import { ReactNode } from 'react';
import { FieldError, FieldPath, FieldPathValue } from 'react-hook-form';
import { Control } from 'react-hook-form/dist/types/form';

export type DropdownOption<T extends string = string> = {
  label: string;
  value: T | ReadonlyArray<T>;
};

export type FormSelectProps<T, C extends object, N extends FieldPath<T> = FieldPath<T>> = {
  name: N;
  label: string;
  control: Control<T, C>;
  options: DropdownOption[];
  disabled?: boolean;
  helperText?: ReactNode;
  error?: FieldError;
  onChange?: (value: FieldPathValue<T, N>) => void;
};
