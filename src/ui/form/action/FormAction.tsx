import { Box } from '@mui/material';
import { WrapperProps } from 'types/react';
import { formActionStyles } from 'ui/form/action/FormAction.styles';

export const FormAction = ({ children }: WrapperProps) => {
  return <Box sx={formActionStyles}>{children}</Box>;
};
