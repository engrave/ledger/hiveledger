import { Styles } from 'styles/theme.types';

export const formActionStyles: Styles = {
  display: 'flex',
  flexDirection: 'row-reverse',
  justifyContent: 'space-between',
};
