import { Styles } from 'styles/theme.types';

export const accountKeyCheckboxStyles = (isChecked: boolean): Styles => ({
  display: 'flex',
  alignItems: 'flex-end',

  '& > :first-of-type': {
    flexGrow: 1,
    opacity: isChecked ? 1 : 0.5,
    transition: 'opacity 0.3s',
  },
});

export const checkboxStyles: Styles = {
  '&, &:last-of-type': {
    marginBottom: '0.6rem',
  },
};
