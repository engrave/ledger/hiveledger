import { Box } from '@mui/material';
import { useWatch } from 'react-hook-form';
import { TranslationKey } from 'context/localization/dictionaries/dictionaries';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { ActionParameter, ActionParameterLabel, ActionParameterValue } from 'ui/data/actionParameter/ActionParameter';
import { accountKeyCheckboxStyles, checkboxStyles } from 'ui/form/accountKeyCheckbox/AccountKeyCheckbox.styles';
import { FormCheckbox } from 'ui/form/checkbox/FormCheckbox';
import { FormCheckboxProps } from 'ui/form/checkbox/FormCheckbox.types';

export type SupportedSlipRoles = SlipRole.owner | SlipRole.active | SlipRole.posting;

export type AccountKeyCheckboxProps<T> = FormCheckboxProps<T, object> & {
  accountKey: string;
  role: SupportedSlipRoles;
  path: string;
};

const roleLabelMapping: Record<SupportedSlipRoles, TranslationKey> = {
  [SlipRole.owner]: 'ui.accountKeyCheckbox.owner',
  [SlipRole.active]: 'ui.accountKeyCheckbox.active',
  [SlipRole.posting]: 'ui.accountKeyCheckbox.posting',
};

export const AccountKeyCheckbox = <T,>(props: AccountKeyCheckboxProps<T>) => {
  const translate = useTranslator();

  const isChecked = Boolean(useWatch({ control: props.control, name: props.name }));

  const { accountKey, role, path, ...checkboxProps } = props;

  return (
    <Box sx={accountKeyCheckboxStyles(isChecked)}>
      <ActionParameter>
        <ActionParameterLabel>{translate(roleLabelMapping[role], { path })}</ActionParameterLabel>
        <ActionParameterValue>{accountKey}</ActionParameterValue>
      </ActionParameter>
      <FormCheckbox {...checkboxProps} sx={checkboxStyles} />
    </Box>
  );
};
