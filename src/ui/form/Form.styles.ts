import { Styles } from 'styles/theme.types';

export const formControlStyles = (hasMessage: boolean): Styles => ({
  marginBottom: hasMessage ? '1px' : '1.5rem',

  '&:last-of-type': {
    marginBottom: '1.375rem',
  },
});
