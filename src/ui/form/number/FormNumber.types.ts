import { InputBaseComponentProps } from '@mui/material/InputBase/InputBase';
import { ReactNode } from 'react';
import { FieldError, Path } from 'react-hook-form';
import { Control } from 'react-hook-form/dist/types/form';
import { NumberFormatPropsBase } from 'react-number-format';

export type FormattedInputProps = NumberFormatPropsBase<Record<string, unknown>> & InputBaseComponentProps;

export type FormNumberProps<T, C extends object> = {
  name: Path<T>;
  label?: string;
  control: Control<T, C>;
  variant?: 'standard' | 'filled' | 'outlined';
  helperText?: string | ReactNode;
  format?: NumberFormatPropsBase<Record<string, unknown>>;
  error?: FieldError;
};
