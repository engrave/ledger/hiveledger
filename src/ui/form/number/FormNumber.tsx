import { FormControl, FormHelperText, InputLabel, OutlinedInput } from '@mui/material';
import { FormEvent, forwardRef } from 'react';
import * as React from 'react';
import { Controller } from 'react-hook-form';
import { Control } from 'react-hook-form/dist/types/form';
import NumberFormat from 'react-number-format';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { formControlStyles } from 'ui/form/Form.styles';
import { FormattedInputProps, FormNumberProps } from 'ui/form/number/FormNumber.types';

const FormattedInput = forwardRef((props: FormattedInputProps, ref) => {
  const { onChange, defaultValue, ...other } = props;
  const { formattingRules } = useLocalization();

  return (
    <NumberFormat
      thousandSeparator={formattingRules.thousandSeparator}
      decimalSeparator={formattingRules.decimalSeparator}
      allowEmptyFormatting={true}
      isNumericString
      {...other}
      defaultValue={Array.isArray(defaultValue) ? defaultValue[0] : defaultValue}
      getInputRef={ref}
      onValueChange={(values, why) => {
        if (why.source === 'prop') {
          return;
        }

        onChange?.({
          target: {
            name: props.name,
            value: values.value,
          },
        } as unknown as FormEvent<HTMLInputElement>);
      }}
    />
  );
});

export const FormNumber = <T, C extends object>(props: FormNumberProps<T, C>) => {
  const { name, control, label, helperText, format, variant = 'outlined', error } = props;

  const hasError = error !== undefined;
  const hasMessage = hasError || !!helperText;

  return (
    <Controller
      control={control as Control<T, object>}
      name={name}
      render={({ field: { onChange, value } }) => (
        <FormControl variant={variant} fullWidth error={hasError} sx={formControlStyles(hasMessage)}>
          {label && <InputLabel shrink={true}>{label}</InputLabel>}
          <OutlinedInput
            inputComponent={FormattedInput}
            inputProps={{ ...format }}
            onChange={(event) => {
              onChange(event);
            }}
            value={value}
          />
          {helperText && <FormHelperText>{helperText}</FormHelperText>}
          {hasError && <FormHelperText>{error.message}</FormHelperText>}
        </FormControl>
      )}
    />
  );
};
