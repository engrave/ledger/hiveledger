import { Box } from '@mui/material';
import { StyledWrapperProps, WrapperProps } from 'types/react';
import {
  infoboxGridStyles,
  infoboxParameterHeaderStyles,
  infoboxParameterLabelStyles,
  infoboxParameterStyles,
  infoboxParameterValueStyles,
  infoboxStyles,
} from 'ui/data/infobox/Infobox.styles';
import { combineStyles } from 'utils/styles/combineStyles';

export const InfoboxParameterHeader = ({ children }: WrapperProps) => {
  return <Box sx={infoboxParameterHeaderStyles}>{children}</Box>;
};

export const InfoboxParameterLabel = ({ children }: WrapperProps) => {
  return <Box sx={infoboxParameterLabelStyles}>{children}</Box>;
};

export const InfoboxParameterValue = ({ children, sx }: StyledWrapperProps) => {
  return <Box sx={combineStyles(infoboxParameterValueStyles, sx)}>{children}</Box>;
};

export const InfoboxParameter = ({ children }: WrapperProps) => {
  return <Box sx={infoboxParameterStyles}>{children}</Box>;
};

export const InfoboxGrid = ({ children }: WrapperProps) => {
  return <Box sx={infoboxGridStyles}>{children}</Box>;
};

export const Infobox = ({ children, sx }: StyledWrapperProps) => {
  return <Box sx={combineStyles(infoboxStyles, sx)}>{children}</Box>;
};
