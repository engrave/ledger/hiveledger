import { Styles } from 'styles/theme.types';

export const infoboxStyles: Styles = {
  display: 'grid',
  gridTemplateColumns: '1fr',
  gap: '1rem 0',
  marginBottom: '1.5rem',
};

export const infoboxGridStyles: Styles = {
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
  gap: '1rem 0',
};

export const infoboxParameterStyles: Styles = {
  display: 'flex',
  flexDirection: 'column',
  gap: '0.4rem',
};

export const infoboxParameterHeaderStyles: Styles = {
  fontSize: '0.875rem',
  fontWeight: 500,
  color: 'black',
};
export const infoboxParameterLabelStyles: Styles = {
  fontSize: '0.875rem',
  fontWeight: 500,
};

export const infoboxParameterValueStyles: Styles = {
  fontSize: '0.875rem',
  fontWeight: 400,
  color: 'typography.lightText',
  display: 'flex',
  gap: '0.5rem',
  alignItems: 'center',
};
