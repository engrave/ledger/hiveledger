import { Box } from '@mui/material';
import { WrapperProps } from 'types/react';
import {
  expandableValueHeadingStyles,
  expandableValueStyles,
  labelStyles,
  parameterStyles,
  valueStyles,
} from 'ui/data/actionParameter/ActionParameter.styles';
import { ExpandableValueProps } from 'ui/data/actionParameter/ActionParameter.types';

export const ActionParameterLabel = (props: WrapperProps) => {
  return <Box sx={labelStyles}>{props.children}</Box>;
};

export const ActionParameterValue = (props: WrapperProps) => {
  return <Box sx={valueStyles}>{props.children}</Box>;
};

export const ActionParameterExpandableValue = (props: ExpandableValueProps) => {
  return (
    <Box sx={expandableValueStyles}>
      <details>
        <summary>
          <Box sx={expandableValueHeadingStyles}>{props.heading}</Box>
        </summary>
        <pre>{props.children}</pre>
      </details>
    </Box>
  );
};

export const ActionParameter = (props: WrapperProps) => {
  return <Box sx={parameterStyles}>{props.children}</Box>;
};
