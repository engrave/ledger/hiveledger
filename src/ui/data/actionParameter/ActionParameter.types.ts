import { ReactNode } from 'react';

export type ExpandableValueProps = {
  children: ReactNode;
  heading: string;
};
