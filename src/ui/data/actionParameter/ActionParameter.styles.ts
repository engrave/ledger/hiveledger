import detailsCloseIcon from 'assets/images/icons/detailsClose.svg';
import detailsOpenIcon from 'assets/images/icons/detailsOpen.svg';
import { Styles } from 'styles/theme.types';

export const parameterStyles: Styles = {
  marginBottom: '20px',
};

export const labelStyles: Styles = {
  fontSize: '0.875rem',
  fontWeight: 500,
  marginBottom: '7px',
};

export const valueStyles: Styles = {
  fontSize: '0.875rem',
  fontWeight: 500,
  fontFamily: '"Courier", monospace',
  color: 'secondary.main',
  wordBreak: 'break-all',
};

export const expandableValueStyles: Styles = {
  ...valueStyles,
  marginBottom: 1,

  details: {
    summary: {
      cursor: 'pointer',
      listStyleImage: `url(${detailsCloseIcon})`,
      lineHeight: '1rem',
      '::marker': {
        marginRight: '10px',
      },
    },
    '&[open] summary': {
      listStyleImage: `url(${detailsOpenIcon})`,
    },
  },

  pre: {
    backgroundColor: 'ui.codeBackground',
    color: 'ui.codeText',
    fontSize: '0.8rem',
    padding: '8px 8px 8px 40px',
    marginY: 1,
    borderRadius: '3px',
    lineHeight: 1.2,
  },
};

export const expandableValueHeadingStyles: Styles = {
  paddingLeft: 0.7,
  display: 'inline-block',
  position: 'relative',
  top: '-1px',
};
