import { Box, Skeleton, Typography } from '@mui/material';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { getSymbolInfo } from 'modules/hive/assetSymbol/getSymbolInfo/getSymbolInfo';
import { WrapperProps } from 'types/react';
import {
  assetConversionsStyles,
  assetConversionValueStyles,
  assetFootnoteStyles,
  assetLabelStyles,
  assetStyles,
  assetSymbolStyles,
  assetValueRowStyles,
} from 'ui/data/asset/Asset.styles';
import {
  AmountValueProps,
  AssetConversionSize,
  AssetConversionsProps,
  AssetConversionValueProps,
  AssetLabelProps,
  AssetLabelVariant,
  AssetValueSize,
} from 'ui/data/asset/Asset.types';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';

export const AssetValue = (props: AmountValueProps) => {
  const { asset, sign, size = AssetValueSize.medium, color = 'typography.common' } = props;
  const { formatAssetAmount } = useLocalization();

  if (asset === null) {
    return (
      <Box sx={assetStyles(size, color)}>
        <Skeleton variant="text" width={150} height={45} />
      </Box>
    );
  }

  const { prefix, suffix, symbolDisplay } = getSymbolInfo(asset.assetSymbol);

  return (
    <Tooltip title={props.tooltip ?? ''}>
      <Box sx={assetStyles(size, color)}>
        {prefix && <Typography sx={assetSymbolStyles(size)}>{symbolDisplay}</Typography>}
        {sign ? `${sign} ` : ''}
        {formatAssetAmount(asset)}
        {suffix && <Typography sx={assetSymbolStyles(size)}>{symbolDisplay}</Typography>}
      </Box>
    </Tooltip>
  );
};

export const AssetValuesRow = ({ children }: WrapperProps) => {
  return <Box sx={assetValueRowStyles}>{children}</Box>;
};

export const AssetLabel = ({ children, variant = AssetLabelVariant.primary }: AssetLabelProps) => {
  return <Box sx={assetLabelStyles(variant)}>{children}</Box>;
};

export const AssetFootnote = ({ children }: WrapperProps) => {
  return <Box sx={assetFootnoteStyles}>{children}</Box>;
};

export const AssetConversionValue = ({ asset, size }: AssetConversionValueProps) => {
  const { formatAssetAmount } = useLocalization();
  const { prefix, suffix, symbolDisplay } = getSymbolInfo(asset.assetSymbol);

  return (
    <Box sx={assetConversionValueStyles(size)}>
      {prefix && symbolDisplay} {formatAssetAmount(asset)} {suffix && symbolDisplay}
    </Box>
  );
};

export const AssetConversions = ({ conversions, size = AssetConversionSize.medium }: AssetConversionsProps) => {
  return (
    <Box sx={assetConversionsStyles(size)}>
      {conversions?.map((convertedAsset, index) => (
        <Box key={index}>
          {convertedAsset === null && (
            <Skeleton variant="text" width={80} height={size === AssetConversionSize.medium ? 16 : 12} />
          )}
          {convertedAsset !== null && <AssetConversionValue asset={convertedAsset} size={size} />}
        </Box>
      ))}
    </Box>
  );
};

export const Asset = ({ children }: WrapperProps) => {
  return <Box>{children}</Box>;
};
