import { Styles } from 'styles/theme.types';
import { AssetConversionSize, AssetLabelVariant, AssetValueSize } from 'ui/data/asset/Asset.types';

const assetValueSizeMapping = {
  [AssetValueSize.mini]: {
    asset: {
      fontSize: '1.375rem',
      fontWeight: 600,
    },
    assetSymbol: {
      fontSize: '1rem',
      fontWeight: 'normal',
    },
  },
  [AssetValueSize.small]: {
    asset: {
      fontSize: '1.625rem',
      fontWeight: 600,
    },
    assetSymbol: {
      fontSize: '1.125rem',
      fontWeight: 'normal',
    },
  },
  [AssetValueSize.medium]: {
    asset: {
      fontSize: '1.875rem',
      fontWeight: 500,
    },
    assetSymbol: {
      fontSize: '1.25rem',
      fontWeight: 'normal',
    },
  },
  [AssetValueSize.big]: {
    asset: {
      fontSize: '2rem',
      fontWeight: 600,
    },
    assetSymbol: {
      fontSize: '1.25rem',
      fontWeight: 'normal',
    },
  },
};

const assetLabelVariantMapping: Record<AssetLabelVariant, Styles> = {
  [AssetLabelVariant.header]: {
    color: 'typography.lightText',
    fontSize: '0.75rem',
    fontWeight: 500,
    marginBottom: '0',
    textTransform: 'uppercase',
  },
  [AssetLabelVariant.primary]: {
    color: 'typography.header',
    fontWeight: 600,
    marginBottom: '13px',
  },
  [AssetLabelVariant.secondary]: {
    color: 'typography.lightText',
    fontWeight: 500,
    marginBottom: '11px',
  },
};
export const assetStyles = (size: AssetValueSize, color: string): Styles => ({
  color,
  display: 'flex',
  alignItems: 'baseline',
  gap: 0.5,
  ...assetValueSizeMapping[size].asset,
});

export const assetSymbolStyles = (size: AssetValueSize): Styles => ({
  textTransform: 'uppercase',
  color: 'typography.common',
  ...assetValueSizeMapping[size].assetSymbol,
});

export const assetValueRowStyles: Styles = {
  display: 'flex',
  gap: 3.5,
};

export const assetLabelStyles = (variant: AssetLabelVariant): Styles => ({
  fontSize: '0.875rem',
  ...assetLabelVariantMapping[variant],
});

export const assetFootnoteStyles: Styles = {
  fontSize: '0.75rem',
  fontWeight: 500,
  color: 'typography.lightText',
  textTransform: 'uppercase',
};

export const assetConversionsStyles = (size: AssetConversionSize): Styles => ({
  display: 'flex',
  gap: 1,
  alignItems: 'center',
  lineHeight: '1rem',
  marginTop: size === AssetConversionSize.medium ? '0.375rem' : '0.15rem',
});

export const assetConversionValueStyles = (size: AssetConversionSize): Styles => ({
  color: 'typography.lightText',
  fontWeight: 500,
  fontSize: size === AssetConversionSize.medium ? '0.875rem' : '0.75rem',
});
