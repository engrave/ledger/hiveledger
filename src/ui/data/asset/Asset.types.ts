import { ReactElement, ReactNode } from 'react';
import { Asset } from 'modules/hive/asset/asset';

export enum AssetValueSize {
  mini = 'mini',
  small = 'small',
  medium = 'medium',
  big = 'big',
}

export enum AssetLabelVariant {
  header = 'header',
  primary = 'primary',
  secondary = 'secondary',
}

export type AssetLabelProps = {
  children: ReactNode;
  variant?: AssetLabelVariant;
};

export type AmountValueProps = {
  asset: Asset | null;
  size?: AssetValueSize;
  color?: string;
  sign?: '+' | '-';
  tooltip?: string | ReactElement;
};

export enum AssetConversionSize {
  small = 'small',
  medium = 'medium',
}

export type AssetConversionsProps = {
  conversions?: (Asset | null)[];
  size?: AssetConversionSize;
};

export type AssetConversionValueProps = {
  asset: Asset;
  size: AssetConversionSize;
};
