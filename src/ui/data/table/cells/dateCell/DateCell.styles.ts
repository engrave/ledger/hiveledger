import { Styles } from 'styles/theme.types';

export const dateCellStyles: Styles = {
  fontSize: '10px',
  color: 'typography.lightText',
  lineHeight: '1rem',
};
