import { TableCell } from '@mui/material';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { dateCellStyles } from 'ui/data/table/cells/dateCell/DateCell.styles';

export type DateCellProps = {
  timestamp: string;
};

export const DateCell = ({ timestamp }: DateCellProps) => {
  const { formatDate, formatTime } = useLocalization();

  return (
    <TableCell sx={dateCellStyles}>
      {formatDate(timestamp, { dateStyle: 'short' })}
      <br />
      {formatTime(timestamp, { timeStyle: 'medium', hourCycle: 'h23' })}
    </TableCell>
  );
};
