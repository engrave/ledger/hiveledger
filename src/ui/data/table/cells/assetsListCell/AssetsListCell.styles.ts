import { Styles } from 'styles/theme.types';

export const assetsListCellStyles: Styles = {
  color: 'typography.common',
  fontWeight: 600,
  fontSize: '12px',
};

export const assetsWrapperStyles: Styles = {
  display: 'flex',
  alignItems: 'center',
  gap: '16px',

  '.MuiTableCell-root:last-of-type > &': {
    justifyContent: 'end',
  },
};
