import { Box, TableCell } from '@mui/material';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { Asset, isVestAsset } from 'modules/hive/asset/asset';
import { printAsset } from 'modules/hive/asset/printAsset/printAsset';
import { assetsListCellStyles, assetsWrapperStyles } from 'ui/data/table/cells/assetsListCell/AssetsListCell.styles';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';

export type AssetsListCellProps = {
  assets: Asset[];
};

export const AssetsListCell = ({ assets }: AssetsListCellProps) => {
  const { getHivePower } = useHivePower();

  return (
    <TableCell sx={assetsListCellStyles}>
      <Box sx={assetsWrapperStyles}>
        {assets.map((item, index) => {
          if (isVestAsset(item)) {
            return (
              <Tooltip title={printAsset(item)} wrap={'span'} key={index}>
                <span>{printAsset(getHivePower(item))}</span>
              </Tooltip>
            );
          }
          return <span key={index}>{printAsset(item)}</span>;
        })}
      </Box>
    </TableCell>
  );
};
