import { Styles } from 'styles/theme.types';

export const assetCellStyles: Styles = {
  color: 'typography.common',
  fontWeight: 600,
  fontSize: '1rem',
};
