import { TableCell } from '@mui/material';
import { ReactElement } from 'react';
import { Asset } from 'modules/hive/asset/asset';
import { printAsset } from 'modules/hive/asset/printAsset/printAsset';
import { assetCellStyles } from 'ui/data/table/cells/assetCell/AssetCell.styles';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';

export type AssetsCellProps = {
  asset: Asset;
  tooltip?: ReactElement | string;
};

export const AssetCell = ({ asset, tooltip }: AssetsCellProps) => {
  if (tooltip) {
    return (
      <TableCell sx={assetCellStyles}>
        <Tooltip title={tooltip}>{printAsset(asset)}</Tooltip>
      </TableCell>
    );
  }

  return <TableCell sx={assetCellStyles}>{printAsset(asset)}</TableCell>;
};
