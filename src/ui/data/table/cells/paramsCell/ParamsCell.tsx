import { Operation } from '@hiveio/dhive';
import { Box, TableCell } from '@mui/material';
import { Fragment } from 'react';
import { parametersOrders } from 'config/transactions';
import { OperationType } from 'modules/hive/operation/operation';
import { serializeParameter } from 'modules/hive/transaction/serializeParameter/serializeParameter';
import { paramsCellStyles } from 'ui/data/table/cells/paramsCell/ParamsCell.styles';
import { toSentenceCase } from 'utils/string/toSentenceCase';

export type ParamsCellProps = {
  operation: Operation;
};

export const ParamsCell = ({ operation }: ParamsCellProps) => {
  const operationType = operation[0];
  const operationParams = operation[1];

  const order = parametersOrders[operationType as OperationType] || Object.keys(operationParams);

  return (
    <TableCell sx={paramsCellStyles}>
      <strong>{operationType}</strong>

      <Box>
        {order.map((param, index) => (
          <Fragment key={index}>
            {toSentenceCase(param)}: {serializeParameter(operationParams[param])}
            <br />
          </Fragment>
        ))}
      </Box>
    </TableCell>
  );
};
