import { TableCell } from '@mui/material';
import { textCellStyles } from 'ui/data/table/cells/textCell/TextCell.styles';
import { TextCellFontSize, TextCellFontWeight, TextCellProps } from 'ui/data/table/cells/textCell/TextCell.types';

export const TextCell = ({
  children,
  size = TextCellFontSize.normal,
  weight = TextCellFontWeight.bold,
}: TextCellProps) => {
  return <TableCell sx={textCellStyles(size, weight)}>{children}</TableCell>;
};
