import { Styles } from 'styles/theme.types';
import { TextCellFontSize, TextCellFontWeight } from 'ui/data/table/cells/textCell/TextCell.types';

export const textCellStyles = (size: TextCellFontSize, weight: TextCellFontWeight): Styles => ({
  color: 'typography.common',
  fontSize: size === TextCellFontSize.big ? '1rem' : '0.875rem',
  fontWeight: weight === TextCellFontWeight.bold ? 500 : 400,
});
