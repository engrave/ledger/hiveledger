import { ReactNode } from 'react';

export enum TextCellFontSize {
  big = 'big',
  normal = 'normal',
}

export enum TextCellFontWeight {
  bold = 'bold',
  normal = 'normal',
}

export type TextCellProps = {
  size?: TextCellFontSize;
  weight?: TextCellFontWeight;
  children: ReactNode;
};
