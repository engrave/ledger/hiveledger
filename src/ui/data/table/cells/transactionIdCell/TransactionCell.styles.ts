import { Styles } from 'styles/theme.types';

export const transactionIdCellStyles: Styles = {
  fontWeight: 500,

  '& > a': {
    color: 'typography.common',
  },
};

export const linkIconStyles: Styles = {
  marginRight: '5px',
};
