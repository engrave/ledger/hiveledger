import { Box, TableCell } from '@mui/material';
import { ReactComponent as LinkIcon } from 'assets/images/icons/link.svg';
import { transactionIdCellStyles, linkIconStyles } from 'ui/data/table/cells/transactionIdCell/TransactionCell.styles';
import { getBlockExplorerUrl } from 'utils/url/getBlockExplorerUrl';

export type TransactionIdCellProps = {
  id: string;
  block: number;
};

export const TransactionIdCell = ({ id, block }: TransactionIdCellProps) => {
  const shortId = id.substring(0, 7);

  return (
    <TableCell sx={transactionIdCellStyles}>
      <a href={getBlockExplorerUrl(block, id)} target={'_blank'} rel="noopener noreferrer">
        <Box sx={linkIconStyles} component={'span'}>
          <LinkIcon />
        </Box>
        {shortId}
      </a>
    </TableCell>
  );
};
