import { IconButton, TableCell, Tooltip } from '@mui/material';
import { IconButtonProps } from '@mui/material/IconButton/IconButton';
import { iconButtonCellStyles } from 'ui/data/table/cells/iconButtonCell/IconButtonCell.styles';

export type IconButtonCellProps = IconButtonProps & {
  tooltip: string;
};

export const IconButtonCell = ({ children, tooltip, ...props }: IconButtonCellProps) => {
  return (
    <TableCell sx={iconButtonCellStyles}>
      <Tooltip title={tooltip} placement={'top'} arrow>
        <IconButton {...props}>{children}</IconButton>
      </Tooltip>
    </TableCell>
  );
};
