import { Styles } from 'styles/theme.types';

export const iconButtonCellStyles: Styles = {
  display: 'flex',
  alignItems: 'center',

  '&:last-of-type': {
    justifyContent: 'end',
  },

  '& > button': {
    width: '32px',
    height: '32px',
  },
};
