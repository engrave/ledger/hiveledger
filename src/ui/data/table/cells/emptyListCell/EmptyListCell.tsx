import { TableCell } from '@mui/material';
import { ReactNode } from 'react';
import { emptyListCellStyles } from 'ui/data/table/cells/emptyListCell/EmptyListCell.styles';

export type EmptyListCellProps = {
  children: ReactNode;
  colSpan: number;
};

export const EmptyListCell = ({ children, colSpan }: EmptyListCellProps) => {
  return (
    <TableCell colSpan={colSpan} sx={emptyListCellStyles}>
      {children}
    </TableCell>
  );
};
