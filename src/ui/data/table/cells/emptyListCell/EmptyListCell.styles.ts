import { Styles } from 'styles/theme.types';

export const emptyListCellStyles: Styles = {
  textAlign: 'center',
  color: 'typography.lightText',
  height: '50px',
};
