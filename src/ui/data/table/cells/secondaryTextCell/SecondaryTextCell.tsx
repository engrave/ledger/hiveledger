import { TableCell } from '@mui/material';
import { WrapperProps } from 'types/react';
import { textCellStyles } from 'ui/data/table/cells/secondaryTextCell/SecondaryTextCell.styles';

export const SecondaryTextCell = ({ children }: WrapperProps) => {
  return <TableCell sx={textCellStyles}>{children}</TableCell>;
};
