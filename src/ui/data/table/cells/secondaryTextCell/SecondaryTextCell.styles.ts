import { Styles } from 'styles/theme.types';

export const textCellStyles: Styles = {
  color: 'typography.lightText',
  fontSize: '12px',
};
