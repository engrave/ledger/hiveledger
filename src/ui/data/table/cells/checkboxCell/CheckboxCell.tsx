import { TableCell } from '@mui/material';
import { Styles } from 'styles/theme.types';
import { BaseCheckbox } from 'ui/form/checkbox/BaseCheckbox';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';

export type CheckboxCellProps = {
  sx?: Styles;
  isDisabled?: boolean;
  tooltip?: string;
  value: boolean;
  onClick?: () => void;
};

export const CheckboxCell = (props: CheckboxCellProps) => {
  const { sx, isDisabled = false, tooltip = '', value, onClick } = props;

  return (
    <TableCell sx={sx}>
      <Tooltip title={tooltip} arrow={true} placement={'top'} wrap={'span'}>
        <BaseCheckbox value={value} disabled={isDisabled} onClick={onClick} />
      </Tooltip>
    </TableCell>
  );
};
