import { Styles } from 'styles/theme.types';

export const numberCellStyles: Styles = {
  color: 'typography.common',
  fontWeight: 600,
  fontSize: '1rem',
};
