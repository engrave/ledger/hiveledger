import { TableCell } from '@mui/material';
import { useIntl } from 'react-intl';
import { numberCellStyles } from 'ui/data/table/cells/numberCell/NumberCell.styles';

export type NumberCellProps = {
  value: number;
};

export const NumberCell = ({ value }: NumberCellProps) => {
  const { formatNumber } = useIntl();

  return <TableCell sx={numberCellStyles}>{formatNumber(value)}</TableCell>;
};
