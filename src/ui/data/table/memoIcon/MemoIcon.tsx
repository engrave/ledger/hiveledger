import { Box, Tooltip } from '@mui/material';
import { ReactComponent as CommentIcon } from 'assets/images/icons/comment.svg';
import { memoIconStyles } from 'ui/data/table/memoIcon/MemoIcon.styles';

export type MemoIconProps = {
  title: string;
};

export const MemoIcon = ({ title }: MemoIconProps) => {
  return (
    <Tooltip placement={'top'} title={title} arrow={true}>
      <Box component={'span'} sx={memoIconStyles}>
        <CommentIcon />
      </Box>
    </Tooltip>
  );
};
