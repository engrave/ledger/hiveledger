import { Styles } from 'styles/theme.types';

export const memoIconStyles: Styles = {
  marginLeft: '5px',
  position: 'relative',
  top: '2px',
};
