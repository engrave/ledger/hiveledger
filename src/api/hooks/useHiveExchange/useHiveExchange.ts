import { useQuery } from 'react-query';
import { getExchangeRates } from 'api/actions/getExchangeRates/getExchangeRates';
import { CoingeckoCurrency } from 'api/actions/getExchangeRates/getExchangeRates.types';
import { exchange } from 'config/config';
import { ChainExchangeRates, CoingeckoExchangeRates } from 'modules/exchange/exchange';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export const getChainExchange = (rates: CoingeckoExchangeRates): ChainExchangeRates => {
  const hiveRates = {
    [ExternalAssetSymbol.usd]: rates.hive.usd,
    [ExternalAssetSymbol.btc]: rates.hive.btc,
  };

  const hiveDollarRates = {
    [ExternalAssetSymbol.usd]: rates.hive_dollar.usd,
    [ExternalAssetSymbol.btc]: rates.hive_dollar.btc,
  };

  return {
    [ChainAssetSymbol.hive]: hiveRates,
    [ChainAssetSymbol.hbd]: hiveDollarRates,
  };
};

export const useHiveExchange = () => {
  const query = useQuery<CoingeckoExchangeRates>(
    'hiveExchange',
    () => {
      return getExchangeRates(
        [CoingeckoCurrency.hive, CoingeckoCurrency.hiveDollar],
        [CoingeckoCurrency.usd, CoingeckoCurrency.btc],
      );
    },
    {
      refetchInterval: exchange.refreshTime,
    },
  );

  if (query.isSuccess) {
    return { ...query, exchangeRates: getChainExchange(query.data) };
  }

  return { ...query, exchangeRates: null };
};
