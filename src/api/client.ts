import axios from 'axios';
import { QueryClient } from 'react-query';

export const axiosInstance = axios.create({
  timeout: 30000,
});

export const queryClient = new QueryClient();
