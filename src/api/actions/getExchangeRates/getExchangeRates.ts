import { CoingeckoCurrency, ExchangeRates } from 'api/actions/getExchangeRates/getExchangeRates.types';
import { axiosInstance } from 'api/client';
import { environment } from 'config/environment';

export const getExchangeRates = async <S extends CoingeckoCurrency, T extends CoingeckoCurrency>(
  sources: readonly S[],
  targets: readonly T[],
): Promise<ExchangeRates<S, T>> => {
  const response = await axiosInstance.request<ExchangeRates<S, T>>({
    method: 'GET',
    url: `${environment.coingecko.apiUrl}/simple/price`,
    params: {
      ids: sources.join(','),
      vs_currencies: targets.join(','),
    },
  });

  return response.data;
};
