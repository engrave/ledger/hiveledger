export enum CoingeckoCurrency {
  hive = 'hive',
  hiveDollar = 'hive_dollar',
  usd = 'usd',
  btc = 'btc',
}

export type AssetExchangeRate<T extends string> = {
  [K in T]: number;
};

export type ExchangeRates<S extends string, T extends string> = {
  [K in S]: AssetExchangeRate<T>;
};
