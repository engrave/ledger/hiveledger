/* eslint-disable @typescript-eslint/no-shadow */
import { english } from 'i18n/english';

export enum Locale {
  english = 'en',
}

export const defaultLocale: Locale = Locale.english;

export const translations = {
  [Locale.english]: english,
};
