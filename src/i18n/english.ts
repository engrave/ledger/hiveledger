/* eslint-disable no-template-curly-in-string */
export const english = {
  logo: 'Ledger',
  header: {
    signOut: 'Sign out',
  },
  navigation: {
    dashboard: 'Dashboard',
    balance: 'Balance',
    staking: 'Staking',
    savings: 'Savings',
    proposals: 'Proposals',
    witnesses: 'Witnesses',
    resourceCredits: 'Resource Credits',
    advanced: 'Advanced',
  },
  footer: {
    developedBy: 'Developed by',
    discord: 'Discord',
    gitlab: 'GitLab',
    hive: 'Hive',
  },
  auth: {
    login: {
      title: 'How would you like to access your assets?',
      fullLogin: {
        header: 'Full access with Ledger Nano S/X',
        description:
          'Use your hardware wallet to access or associate your current Hive account. <br/>' +
          'We will find your account automatically or ask you to provide username.',
        supportUs: 'Support us by buying one',
      },
      viewOnlyLogin: {
        header: 'Username - view only mode',
        description:
          'Use this option if you only want to view account details without the possibility to <br/>' +
          'broadcast transactions. You will be able to associate this account with your device later on.',
      },
      fullLoginModal: {
        title: 'Connect your ledger',
        description: 'Please connect your ledger device and open Hive application.',
        connect: 'Connect',
        retry: 'Retry',
        errors: {
          connectionCancelled: 'Error: connection was cancelled',
          deviceProblem: 'Error: check if Ledger is unlocked and the Hive app is open',
          authorizationError: 'Error: authorization process failed',
        },
        authorized: "Authorized as '<strong>'{name}'</strong>'",
        noAccountFound: 'No account found on your Ledger',
        waitForAuthorization: 'Please wait for authorization to finish',
        failed: 'Authorization failed',
      },
    },
    addAccount: {
      title: 'We could not find any account associated with this device',
      message: `
        None of the keys derived from your ledger device could be found associated with existing Hive username.<br/>
        You can associate your existing account which will replace all of your keys with keys derived from your device.
      `,
      associateAccount: {
        header: 'Associate existing account',
        description: `
          Use this option if you already have an account and would like 
          to protect it with hardware wallet by replacing your current keys.
        `,
        success: 'Account update succeed! You were automatically signed in',
      },
      associateAccountModal: {
        steps: {
          step1: {
            name: 'Step 1',
            description: 'Request new owner key',
          },
          step2: {
            name: 'Step 2',
            description: 'Verify ownership',
          },
          step3: {
            name: 'Step 3',
            description: 'Replace current keys',
          },
        },
        requestDeviceKey: {
          message: `
            Start with requesting and confirming unused key on your ledger device. 
            This key will replace your current owner key in the last step.
          `,
          parameters: {
            publicKey: 'New owner public key:',
            path: 'Derivation path:',
          },
          requestNewKey: 'Request new key',
          confirm: 'Confirm on ledger',
          continue: 'Continue',
          errors: {
            cancelled: 'Error: process was canceled by the user',
            deviceError: 'Error: check if Ledger is unlocked and the Hive app is open',
          },
          failed: 'Action failed',
        },
        addPrivateKey: {
          message: `
            Enter your username and your current owner private key. 
            It will be used to verify account ownership and to update the account in the next step.
          `,
          form: {
            username: 'Username',
            privateKey: 'Current owner private key',
            privateKeyValidationLabel: 'Private key',
          },
          errors: {
            invalidPrivateKey: 'Error: Private key is not valid for this account',
            insufficientOwnerKeyThreshold: 'Error: Insufficient owner key threshold, please enter another key',
            accountNotFound: 'Error: account not found',
            genericError: 'Error: account ownership validation failed',
          },
          continue: 'Continue',
          failed: 'Adding new key failed',
        },
        updateAccount: {
          message: `
            To ultimately secure your account, replace all your current keys with a new ones received from your device. 
            You may skip posting key to keep social abilities with your current key. Select key roles you want to replace.
          `,
          update: 'Update account',
          errors: {
            deviceLocked: 'Please unlock your device and try again',
            applicationNotRunning: 'Please open Hive app on your device and try again',
            transactionRejected: 'Transaction was rejected by user',
            transactionNotAccepted: 'Transaction was not accepted in the chain',
            failed: 'Account update failed',
          },
        },
      },
      createAccount: {
        header: 'Create new account',
        description: `
          Create a brand new account. This might take a while because 
          accounts aren’t free and someone need to pay for creating it.
        `,
      },
    },
    logout: {
      success: 'You were successfully logout',
    },
  },
  dashboard: {
    overview: {
      summary: {
        totalLiquid: 'Total {asset}',
        totalDebt: 'Total {asset}',
        estimatedWalletValue: 'Estimated wallet value',
        refresh: 'Refresh',
        refreshError: "We couldn't load account data",
      },
      accounts: {
        accountNo: 'Account #{index}',
        liquidAndSavings: 'Liquid and savings',
        staked: 'Staked',
        activate: 'Activate',
        activated: 'Activated',
      },
      associateAccountButton: {
        label: 'Associate another account',
        success: 'Account update succeed!',
      },
    },
    balance: {
      summary: {
        title: 'Liquid balance',
      },
      history: {
        title: 'Transaction history',
        table: {
          transactionId: 'Transaction ID',
          date: 'Date',
          operation: 'Operation',
          amount: 'Amount',
        },
        emptyHistory: 'Your history seems to be empty',
        fetchingError: 'Failed to fetch transactions. Please try again.',
      },
      buyAndSell: {
        title: 'Buy / Sell',
        action: 'Buy/sell HIVE',
        disclaimer: 'Powered by Changelly',
      },
      powerUp: {
        title: 'Power up',
        help: `
          Hive Power is staked HIVE token and requires 3 months (13 payments, one per week) 
          to convert back to HIVE. It will give you governance and voting power, 
          and it will increase your balance at a rate of {apr}% over a year.
        `,
        form: {
          asset: 'Asset',
          amount: 'Amount',
          powerUp: 'Power up',
        },
      },
      transfer: {
        title: 'Transfer',
        help: 'Power Up help message',
        form: {
          to: 'To',
          toPlaceholder: 'username',
          asset: 'Asset',
          amount: 'Amount',
          memo: 'Memo',
          send: 'Send',
        },
      },
    },
    staking: {
      banner: `
        Stake your HIVE to increase your balance at an APR of approximately {apr}%. 
        Hive power gives you also larger voting power, more resource credits and governance 
        abilities on Hive blockchain.
      `,
      hivePower: {
        title: 'Hive power',
        balances: {
          staked: 'Staked balance',
          received: 'Received delegation',
          delegated: 'Delegated',
          total: 'Total power',
        },
      },
      managePowerDown: {
        title: 'Manage power down',
        table: {
          toWithdraw: 'To withdraw',
          withdrawn: 'Withdrawn',
          nextPayout: 'Next payout',
          amount: 'Amount',
          remaining: 'Remaining',
        },
        cancelTooltip: 'Click to cancel power down',
      },
      stakingRewards: {
        title: 'Reward balance',
        claim: 'Claim',
      },
      delegateStake: {
        title: 'Delegate stake',
        help: `
          You can delegate your stake to other users to increase their voting power. 
          You can cancel delegation at any time but it will take 5 days to get back your stake. 
          During that time your previously delegated Hive Power will be in limbo state.
          `,
        form: {
          delegatee: 'To',
          delegateePlaceholder: 'username',
          amount: 'Amount',
          delegate: 'Delegate',
        },
        delegationUpdate: 'This will replace the existing delegation for {username} of {current}',
      },
      delegationInfo: `
        Delegation does not impact your APR nor governance abilities. 
        Delegate to @engrave to help improving the platform.
      `,
      activeDelegations: {
        title: 'Manage delegations',
        table: {
          to: 'To',
          amount: 'Amount',
        },
        cancelButton: 'Click to cancel delegation',
      },
      expiringDelegations: {
        title: 'Expiring delegations',
        table: {
          amount: 'Amount',
          expiring: 'Expiring',
        },
      },
      powerDown: {
        title: 'Power down',
        form: {
          amount: 'Amount',
          powerDown: 'Power down',
        },
      },
    },
    savings: {
      banner: `
        Keep your funds safe and generate passive income. 
        Savings balance is subject to 3 day withdraw waiting period.
      `,
      summary: {
        title: 'Savings balance',
      },
      savingsAprInfo: ` 
        Moving liquid HBD to your Savings account will generate a {hbdApr}% APR as defined by the witnesses.
      `,
      estimatedReward: {
        title: 'Estimated savings reward',
        claim: 'Claim',
        claimInTime: 'Claim {inTime}',
      },
      depositSavings: {
        title: 'Deposit to savings',
        form: {
          asset: 'Asset',
          amount: 'Amount',
          deposit: 'Deposit',
        },
      },
      withdrawSavings: {
        title: 'Withdraw from savings',
        form: {
          asset: 'Asset',
          amount: 'Amount',
          withdraw: 'Withdraw',
        },
      },
      manageWithdrawals: {
        title: 'Manage withdrawals',
        table: {
          amount: 'Amount',
          date: 'Date',
        },
        cancelTooltip: 'Cancel this withdrawal',
      },
    },
    proposals: {
      banner: `
        The Decentralized Hive Fund (DHF) is an on chain decentralized autonomous system that allows 
        users to submit proposals for funding and vote on which proposals should be funded.
        Funds are paid out every hour to the proposals that get the most votes until the daily budget is drained.
      `,
      ranking: {
        insufficientVotes: 'Insufficient votes',
        card: {
          status: {
            active: {
              label: 'Active',
              tooltip: 'The proposal will receive payment in the next round',
            },
            inactive: {
              label: 'Inactive',
              tooltip: 'The proposal will not be funded in the next round due to insufficient vote',
            },
            upcoming: {
              label: 'Upcoming',
              tooltip: 'The proposal will start in the future',
            },
          },
          proposalId: '#{id}',
          totalVotes: 'Total votes: {votes}',
          durationWithTimeLeft: '{duration} ({timeLeft} left)',
          durationAndWhen: '{duration} (in {when})',
          hbdDaily: '{amount} daily',
          hbdTotal: '{amount} in total',
          creator: "by '<strong>'{creator}'</strong>'",
          creatorWithReceiver: "by '<strong>'{creator}'</strong>' with receiver '<strong>'{receiver}'</strong>'",
          proxyEnabled: "You can't vote because there is a proxy set to vote on behalf of you",
        },
      },
      budget: {
        title: 'Budget',
        totalBudget: 'Total budget',
        dailyBudget: 'Daily budget',
      },
      proxyExplanation: {
        proxyPropose: `
          Don’t know who to vote on? Set '<strong>'{account}'</strong>' as 
          a proxy account and we will vote on your behalf.
        `,
      },
      infobox: {
        title: 'Proposal #{proposalId}',
        creator: 'Creator',
        receiver: 'Receiver',
        totalPayout: 'Total payout',
        dailyPayout: 'Daily payout',
        startDate: 'Start date',
        endDate: 'End date',
      },
      supportLevel: {
        title: 'Support level',
        requiredToFund: 'Required to get funded',
        totalVotes: 'Total votes',
        progress: 'Progress',
        proxyEnabled: "You can't vote because there is a proxy set to vote on behalf of you",
      },
      voters: {
        title: 'Voters',
        power: '{votingPower}',
        powerWithProxies: '{votingPower} + {proxiedVotingPower} (proxy)',
        showMore: 'Show more',
        modalTitle: 'Voters',
      },
    },
    witnesses: {
      banner: `
        Block producers aka “witnesses” are responsible for operating consensus nodes and maintaining the network.
        The top 20 witnesses decide on blockchain properties and future development. You can and you should vote for your favorite witnesses.
      `,
      ranking: {
        table: {
          rank: 'Rank',
          witness: 'Witness',
          votes: 'Votes',
          version: 'Version',
          apr: 'APR',
          acFee: 'AC Fee',
          priceFeed: 'Price Feed',
          vote: 'Vote',
        },
        position: '#{rank}',
        version: 'v{version}',
        maxApprovals: 'You can only vote at max {max} witnesses',
        proxyEnabled: "You can't vote because there is a proxy set to vote on behalf of you",
      },
      proxyExplanation: {
        proxyPropose: `
          Don’t know who to vote on? Set '<strong>'{account}'</strong>' as 
          a proxy account and we will vote on your behalf.
        `,
        currentVotes: 'Your votes: {current}/{max}',
      },
      proxy: {
        title: 'Proxy Account',
        form: {
          proxy: 'Username:',
        },
        activateProxy: 'Set proxy',
        deactivateProxy: 'Deactivate',
        proxyStatus: "Your voting settings will be the same as for the '<strong>'{proxy}'</strong>' account.",
      },
    },
    resourceCredits: {
      banner: `
        Resource Credits are credits given to each account based on how much Hive Power it has, which get spent whenever
        a user transacts with the Hive blockchain. RCs regenerate over a 5 days period. If an account doesn't have
        sufficient credits, the transaction will not be allowed to occur.
      `,
      summary: {
        title: 'Resource Credits',
        maxMana: 'max_mana',
        currentMana: 'current_mana',
        delegated: 'delegated_rc',
        receivedDelegation: 'received_delegated_rc',
        rechargeRate: "Your recharge rate is '<strong>'{amount}'</strong>' per day.",
      },
      delegationInfo: `
        If you won't utilize your Resource Credits, you can delegate them to other accounts.
        Delegate to @engrave to help improving the platform`,
      delegateResourceCredits: {
        title: 'Delegate RC',
        help: `
          You can delegate your current RC (current_mana) to another account. If you want to delegate more, you will need to wait until it is regenerated.
          Cancelled delegations will not be refunded and will need to regenerate until you can spend it. 
          `,
        form: {
          delegatee: 'To',
          delegateePlaceholder: 'username',
          amount: 'Amount',
          delegate: 'Delegate',
        },
        delegationUpdate: 'This will replace the existing delegation for {username} of {current}',
      },
      delegations: {
        title: 'Manage delegations',
        table: {
          to: 'To',
          amount: 'Amount',
        },
        cancelButton: 'Click to cancel delegation',
      },
    },
    advanced: {
      banner: `
        The Hive blockchain identifies three distinct authority roles: Owner, Active, and Posting.  
        These roles can be configured using either Public keys or account authorities. 
        The weight threshold is a critical parameter, as it determines the cumulative weight necessary to sign and broadcast a transaction. 
        Every key and account is assigned a specific weight, which is then evaluated against the threshold value.
      `,
      authorities: {
        ownerTitle: 'Owner Authorities',
        activeTitle: 'Active Authorities',
        postingTitle: 'Posting Authorities',
      },
      authoritiesEditor: {
        threshold: {
          label: 'Threshold:',
        },
        keyAuths: {
          label: 'Key auths',
          publicKeyColumn: 'Public Key',
          weightColumn: 'Weight',
          addPublicKeyButton: 'Add Public Key',
          delete: {
            tooltip: 'Click to delete',
            title: 'Key authority deletion',
            description: 'Are you sure you want to delete this authority?',
          },
        },
        addPublicKeyModal: {
          title: 'Enter public key',
          publicKey: 'Public key',
          save: 'Add public key',
          getFromLedger: 'Get from Ledger',
          errors: {
            cancelled: 'Operation cancelled by te user',
            deviceError: 'Error: check if Ledger is unlocked and the Hive app is open',
          },
        },
        accountAuths: {
          label: 'Account auths',
          accountColumn: 'Account',
          weightColumn: 'Weight',
          addAccountButton: 'Add Account',
          delete: {
            tooltip: 'Click to delete',
            title: 'Account authority deletion',
            description: 'Are you sure you want to delete the "{username}" authority?',
          },
        },
        addAccountModal: {
          title: 'Enter account username',
          username: 'Username',
          save: 'Add account',
        },
        saveButton: 'Save changes',
        navigationLock: {
          title: 'Unsaved changes',
          description: 'There are unsaved change in this form. Are you sure you want to leave?',
        },
      },
    },
  },
  exchange: {
    loadingError: "Can't load widget",
  },
  transaction: {
    transactionModal: {
      title: 'Confirm transaction',
      signingKeyPath: 'Signing key path',
      operation: 'Operation',
      operations: 'Operations',
      hash: 'Hash',
      button: {
        signIn: 'Sign with ledger',
        confirmOnDevice: 'Confirm on device',
        broadcasting: 'Broadcasting',
        downloadJson: 'Save as JSON',
      },
      errors: {
        transactionInProgress: 'Please wait for the transaction to be confirmed',
        deviceLocked: 'Please unlock your device and try again',
        applicationNotRunning: 'Please open Hive app on your device and try again',
        transactionRejected: 'Transaction was rejected by user',
        transactionNotAccepted: 'Transaction was rejected by chain',
        failed: 'Failed to broadcast transaction',
        hashSigningDisabled:
          'Hash sign policy is not enabled on the device. Please enable it in the Hive app settings.',
      },
      completed: 'Transaction confirmed',
    },
    description: {
      fillConvertRequest: 'Converted {amount}',
      transferFrom: 'Received from {username}',
      transferTo: 'Sent to {username}',
      transferToSavings: 'Transfer to savings',
      transferToSavingsFrom: 'Transfer to savings from {username}',
      transferToVesting: 'Powered up',
      transferToVestingFrom: 'Powered up from {username}',
      fillTransferFromSavings: 'Savings withdrawal completed',
      fillTransferFromSavingsFrom: 'Savings withdrawal completed from {username}',
      interest: 'Claimed savings account interest',
      claimRewards: 'Claimed rewards',
    },
  },
  errors: {
    generalException: 'An error occurred! Please try again later.',
    needToLogin: 'You need to authorize',
    genericTransactionError: 'Failed to broadcast transaction',
    'op.vesting_shares >= min_delegation: Account must delegate a minimum of ${v}':
      'Account must delegate a minimum of {v}',
    transaction_expiration_exception: 'The transaction expired, please close the modal and try again.',
  },
  validation: {
    custom: {
      enoughAssets: 'Amount needs to be {available} or less',
      invalidUsername: 'Invalid username',
      badActor: 'This username is known scammer',
      invalidPrivateKey: 'This is not a valid private key',
      invalidPublicKey: 'This is not a valid public key',
    },
    mixed: {
      required: 'This field is required',
    },
    number: {
      positive: '{label} has to be higher than zero',
    },
  },
  ui: {
    availableAmountHelper: {
      availableAmount: 'Available: {amount}',
    },
    accountKeyCheckbox: {
      owner: 'Owner key ({path})',
      active: 'Active key ({path})',
      posting: 'Posting key ({path})',
    },
    confirmationDialog: {
      confirm: 'Confirm',
      cancel: 'Cancel',
    },
  },
};
