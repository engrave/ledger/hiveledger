import { ReactComponent as PlusIcon } from 'assets/images/icons/plus.svg';
import { links } from 'config/config';
import { useHtmlTranslator } from 'context/localization/hooks/useHtmlTranslator';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { OptionCard, OptionCardContent, OptionCardIcon, OptionCardTitle } from 'ui/layout/optionCard/OptionCard';

export const CreateAccountCard = () => {
  const translate = useTranslator();
  const translateHtml = useHtmlTranslator();

  const handleNewAccountCreation = () => {
    window.open(links.createAccount, '_blank');
  };

  return (
    <>
      <OptionCard onClick={() => handleNewAccountCreation()}>
        <OptionCardIcon>
          <PlusIcon />
        </OptionCardIcon>
        <OptionCardTitle>{translate('auth.addAccount.createAccount.header')}</OptionCardTitle>
        <OptionCardContent>{translateHtml('auth.addAccount.createAccount.description')}</OptionCardContent>
      </OptionCard>
    </>
  );
};
