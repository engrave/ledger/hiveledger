import { Dialog, DialogContent } from '@mui/material';
import { ReactElement, useEffect } from 'react';
import { CSSTransition, SwitchTransition } from 'react-transition-group';
import {
  contentAnimationDuration,
  dialogContentStyles,
} from 'app/auth/addAccount/associateAccountModal/AssociateAcoountModa.styles';
import { AssociationStepper } from 'app/auth/addAccount/associateAccountModal/associationStepper/AssociationStepper';
import { AddPrivateKey } from 'app/auth/addAccount/associateAccountModal/steps/addPrivateKey/AddPrivateKey';
import { RequestDeviceKey } from 'app/auth/addAccount/associateAccountModal/steps/requestDeviceKey/RequestDeviceKey';
import { UpdateAccount } from 'app/auth/addAccount/associateAccountModal/steps/updateAccount/UpdateAccount';
import { AssociationStep } from 'context/association/associationContext/associationContext';
import { useAssociation } from 'context/association/hooks/useAssociation';
import { Account } from 'modules/hive/account/account';

export type AssociateAccountModalProps = {
  open: boolean;
  onComplete: (account: Account) => void;
  onCancel: () => void;
};

export const AssociateAccountModal = (props: AssociateAccountModalProps) => {
  const { currentStep, setCurrentStep, cleanUpAssociation, getAccountFromAssociation } = useAssociation();

  const handleComplete = async () => {
    props.onComplete(await getAccountFromAssociation());
    cleanUpAssociation();
    setCurrentStep(AssociationStep.requestDeviceKey);
  };

  const handleCancel = () => {
    props.onCancel();
    cleanUpAssociation();
    setCurrentStep(AssociationStep.requestDeviceKey);
  };

  useEffect(() => {
    return () => cleanUpAssociation();
  }, [cleanUpAssociation]);

  const renderMap: Record<AssociationStep, ReactElement> = {
    [AssociationStep.requestDeviceKey]: <RequestDeviceKey />,
    [AssociationStep.addPrivateKey]: <AddPrivateKey />,
    [AssociationStep.updateAccount]: <UpdateAccount onComplete={handleComplete} />,
  };

  return (
    <Dialog open={props.open} onClose={handleCancel}>
      <DialogContent sx={dialogContentStyles}>
        <AssociationStepper />

        <SwitchTransition>
          <CSSTransition
            key={currentStep}
            classNames={'dialog-transition'}
            unmountOnExit
            timeout={contentAnimationDuration}
          >
            {renderMap[currentStep]}
          </CSSTransition>
        </SwitchTransition>
      </DialogContent>
    </Dialog>
  );
};
