import { AssociationStep } from 'context/association/associationContext/associationContext';

export const stepsOrder = [
  AssociationStep.requestDeviceKey,
  AssociationStep.addPrivateKey,
  AssociationStep.updateAccount,
];

export type UpdateAccountProps = {
  onComplete: () => void;
};
