import { Styles } from 'styles/theme.types';

export const contentAnimationDuration = 200;

export const dialogContentStyles: Styles = {
  width: '600x',
  minHeight: '460px',
  padding: '30px 30px 20px',
  fontSize: '0.875rem',
  display: 'flex',
  flexDirection: 'column',

  '& > .dialog-transition-exit': {
    opacity: 1,
  },
  '& > .dialog-transition-exit-active': {
    opacity: 0,
  },
  '& > .dialog-transition-enter': {
    opacity: 0,
  },
  '& > .dialog-transition-enter-active': {
    opacity: 1,
  },
  '& > .dialog-transition-enter-active, & > .dialog-transition-exit-active': {
    transition: `opacity ${contentAnimationDuration}ms`,
  },
};

export const stepWrapperStyles: Styles = {
  display: 'flex',
  flexDirection: 'column',
  flexGrow: 1,
};

export const stepContentStyles: Styles = {
  flexGrow: 1,
};

export const stepMessageStyles: Styles = {
  marginBottom: '24px',
};

export const stepActionStyles: Styles = {
  button: {
    fontSize: '0.875rem',
    fontWeight: 600,
  },
};
