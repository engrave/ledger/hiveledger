import { boolean, object } from 'yup';
import { UpdateAccountForm } from 'app/auth/addAccount/associateAccountModal/steps/updateAccount/UpdateAccount.types';

export const updateAccountSchema = object<UpdateAccountForm>({
  ownerKey: boolean().isTrue().required(),
  activeKey: boolean().required(),
  postingKey: boolean().required(),
}).required();
