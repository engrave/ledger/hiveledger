import { ReactNode } from 'react';
import { TransactionError } from 'context/transaction/hooks/useTransaction/useTransaction.types';
import { isRpcError } from 'modules/ledger/rpcError/rpcError';
import { FormattedRpcError } from 'ui/message/formattedRpcError/FormattedRpcError';
import { TranslatedMessage } from 'ui/message/translatedMessage/TranslatedMessage';

export const getErrorMessage = (error: unknown): ReactNode => {
  if (error instanceof Error) {
    if (error.message === TransactionError.deviceLocked) {
      return <TranslatedMessage id={'auth.addAccount.associateAccountModal.updateAccount.errors.deviceLocked'} />;
    }

    if (error.message === TransactionError.applicationNotRunning) {
      return (
        <TranslatedMessage id={'auth.addAccount.associateAccountModal.updateAccount.errors.applicationNotRunning'} />
      );
    }

    if (error.message === TransactionError.transactionRejectedByUser) {
      return (
        <TranslatedMessage id={'auth.addAccount.associateAccountModal.updateAccount.errors.transactionRejected'} />
      );
    }
    if (error.message === TransactionError.transactionNotIncludedInBlock) {
      return (
        <TranslatedMessage id={'auth.addAccount.associateAccountModal.updateAccount.errors.transactionNotAccepted'} />
      );
    }

    if (isRpcError(error)) {
      return <FormattedRpcError error={error} />;
    }
  }

  return <TranslatedMessage id={'auth.addAccount.associateAccountModal.updateAccount.errors.failed'} />;
};
