export type UpdateAccountForm = {
  ownerKey: boolean;
  activeKey: boolean;
  postingKey: boolean;
};
