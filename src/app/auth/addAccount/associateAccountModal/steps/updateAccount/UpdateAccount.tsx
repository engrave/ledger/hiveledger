import { yupResolver } from '@hookform/resolvers/yup';
import { LoadingButton } from '@mui/lab';
import { Box, DialogActions } from '@mui/material';
import { ReactNode, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { UpdateAccountProps } from 'app/auth/addAccount/associateAccountModal/AssociateAccountModal.types';
import {
  stepActionStyles,
  stepContentStyles,
  stepMessageStyles,
  stepWrapperStyles,
} from 'app/auth/addAccount/associateAccountModal/AssociateAcoountModa.styles';
import { getErrorMessage } from 'app/auth/addAccount/associateAccountModal/steps/updateAccount/helpers';
import { updateAccountSchema } from 'app/auth/addAccount/associateAccountModal/steps/updateAccount/UpdateAccount.schema';
import { UpdateAccountForm } from 'app/auth/addAccount/associateAccountModal/steps/updateAccount/UpdateAccount.types';
import { AssociationStep } from 'context/association/associationContext/associationContext';
import { useAssociation } from 'context/association/hooks/useAssociation';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { useTransaction } from 'context/transaction/hooks/useTransaction/useTransaction';
import { useToast } from 'hooks/useToast/useToast';
import { pickDeviceAccountKeys } from 'modules/hive/accountAssociation/pickDeviceAccountKeys/pickDeviceAccountKeys';
import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { accountAssociate } from 'modules/hive/operation/accountAssociate/accountAssociate';
import { AccountKeyCheckbox } from 'ui/form/accountKeyCheckbox/AccountKeyCheckbox';
import { DialogVerticalError } from 'ui/modal/dialogVerticalError/DialogVerticalError';

export const UpdateAccount = (props: UpdateAccountProps) => {
  const translate = useTranslator();
  const { broadcastOperation, confirmTransaction } = useTransaction();
  const { deviceAccount, targetAccount, setCurrentStep } = useAssociation();

  const { showError } = useToast();
  const { start, done } = useGlobalProgressBar();

  const [isWorking, setWorking] = useState(false);
  const [errorMessage, setErrorMessage] = useState<ReactNode>(null);

  useEffect(() => {
    if (deviceAccount === null || targetAccount === null) {
      setCurrentStep(AssociationStep.requestDeviceKey);
    }
  }, [setCurrentStep, deviceAccount, targetAccount]);

  const form = useForm<UpdateAccountForm>({
    defaultValues: {
      ownerKey: true,
      activeKey: true,
      postingKey: true,
    },
    resolver: yupResolver(updateAccountSchema),
  });

  if (deviceAccount === null || targetAccount === null) {
    return null;
  }

  const handleUpdateAccountFormSubmit = async (values: UpdateAccountForm) => {
    const updateAccountOperation = accountAssociate({
      username: targetAccount.accountDetails.name,
      memoKey: targetAccount.accountDetails.memo_key,
      publicKeys: pickDeviceAccountKeys(values, deviceAccount),
    });

    start();
    setWorking(true);

    try {
      const transaction = await broadcastOperation(updateAccountOperation, targetAccount.privateKey);
      await confirmTransaction(transaction.id);
    } catch (error) {
      setErrorMessage(getErrorMessage(error));
      showError('auth.addAccount.associateAccountModal.updateAccount.errors.failed');
      return;
    } finally {
      setWorking(false);
      done();
    }

    props.onComplete();
  };

  return (
    <Box sx={stepWrapperStyles}>
      <Box sx={stepContentStyles}>
        <Box sx={stepMessageStyles}>{translate('auth.addAccount.associateAccountModal.updateAccount.message')}</Box>
        <Box>
          <form onSubmit={form.handleSubmit(handleUpdateAccountFormSubmit)} id={'update_account_form'}>
            <AccountKeyCheckbox
              name={'ownerKey'}
              control={form.control}
              role={SlipRole.owner}
              path={deviceAccount.ownerKey.path}
              accountKey={deviceAccount.ownerKey.publicKey}
              disabled
            />
            <AccountKeyCheckbox
              name={'activeKey'}
              control={form.control}
              role={SlipRole.active}
              path={deviceAccount.activeKey.path}
              accountKey={deviceAccount.activeKey.publicKey}
            />
            <AccountKeyCheckbox
              name={'postingKey'}
              control={form.control}
              role={SlipRole.posting}
              path={deviceAccount.postingKey.path}
              accountKey={deviceAccount.postingKey.publicKey}
            />
          </form>
        </Box>

        <DialogActions sx={stepActionStyles}>
          <LoadingButton
            loading={isWorking}
            disabled={isWorking || !form.formState.isValid}
            type={'submit'}
            form={'update_account_form'}
            variant={'contained'}
            size={'small'}
          >
            {translate('auth.addAccount.associateAccountModal.updateAccount.update')}
          </LoadingButton>
        </DialogActions>
      </Box>
      <DialogVerticalError>{errorMessage}</DialogVerticalError>
    </Box>
  );
};
