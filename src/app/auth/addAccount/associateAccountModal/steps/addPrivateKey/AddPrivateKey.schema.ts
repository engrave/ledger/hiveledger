import { object, string } from 'yup';
import { SchemaCreatorParams } from 'validation/useSchemaCreator/useSchemaCreator';
import { isBadActor } from 'validation/validators/isBadActor/isBadActor';
import { isValidPrivateKey } from 'validation/validators/isValidPrivateKey/isValidPrivateKey';
import { isValidUsername } from 'validation/validators/isValidUsername/isValidUsername';

export const createAddPrivateKeySchema = ({ translate }: SchemaCreatorParams) => {
  return object({
    username: string()
      .required()
      .test({
        test: isValidUsername,
        message: translate('validation.custom.invalidUsername'),
      })
      .test({
        test: isBadActor,
        message: translate('validation.custom.badActor'),
      })
      .label(translate('auth.addAccount.associateAccountModal.addPrivateKey.form.username')),
    privateKey: string()
      .required()
      .length(51)
      .test({
        test: isValidPrivateKey,
        message: translate('validation.custom.invalidPrivateKey'),
      })
      .label(translate('auth.addAccount.associateAccountModal.addPrivateKey.form.privateKeyValidationLabel')),
  }).required();
};
