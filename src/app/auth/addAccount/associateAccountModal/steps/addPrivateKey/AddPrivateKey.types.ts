export type AddPrivateKeyForm = {
  username: string;
  privateKey: string;
};
