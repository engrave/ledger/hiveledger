import { yupResolver } from '@hookform/resolvers/yup';
import { LoadingButton } from '@mui/lab';
import { Box, DialogActions } from '@mui/material';
import { ReactNode, useState } from 'react';
import { useForm } from 'react-hook-form';
import {
  stepActionStyles,
  stepContentStyles,
  stepMessageStyles,
  stepWrapperStyles,
} from 'app/auth/addAccount/associateAccountModal/AssociateAcoountModa.styles';
import { createAddPrivateKeySchema } from 'app/auth/addAccount/associateAccountModal/steps/addPrivateKey/AddPrivateKey.schema';
import { AddPrivateKeyForm } from 'app/auth/addAccount/associateAccountModal/steps/addPrivateKey/AddPrivateKey.types';
import { getErrorMessage } from 'app/auth/addAccount/associateAccountModal/steps/addPrivateKey/helpers';
import { AssociationStep } from 'context/association/associationContext/associationContext';
import { useAssociation } from 'context/association/hooks/useAssociation';
import { useAccountOwnershipValidation } from 'context/hive/hooks/useAccountOwnershipValidation/useAccountOwnershipValidation';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { FormText } from 'ui/form/text/FormText';
import { DialogVerticalError } from 'ui/modal/dialogVerticalError/DialogVerticalError';
import { logApplicationError } from 'utils/debug/logApplicationError';
import { useSchemaCreator } from 'validation/useSchemaCreator/useSchemaCreator';

export const AddPrivateKey = () => {
  const translate = useTranslator();
  const { setCurrentStep, setTargetAccount } = useAssociation();

  const [isWorking, setWorking] = useState(false);
  const [errorMessage, setErrorMessage] = useState<ReactNode>(null);

  const validateAccountOwnership = useAccountOwnershipValidation();
  const addPrivateKeySchema = useSchemaCreator(createAddPrivateKeySchema);

  const form = useForm<AddPrivateKeyForm>({
    defaultValues: {
      username: '',
      privateKey: '',
    },
    mode: 'onChange',
    resolver: yupResolver(addPrivateKeySchema),
  });

  const handleAddNewKeyFormSubmit = async (values: AddPrivateKeyForm) => {
    setWorking(true);

    try {
      const result = await validateAccountOwnership(values.privateKey, values.username);

      if (result.isOwned) {
        setTargetAccount({
          accountDetails: result.accountDetails,
          privateKey: values.privateKey,
        });
        setCurrentStep(AssociationStep.updateAccount);
      } else {
        setErrorMessage(getErrorMessage(result.error));
        setWorking(false);
      }
    } catch (error) {
      logApplicationError(error);
      setErrorMessage(getErrorMessage(error));
      setWorking(false);
    }
  };

  return (
    <Box sx={stepWrapperStyles}>
      <Box sx={stepContentStyles}>
        <Box sx={stepMessageStyles}>{translate('auth.addAccount.associateAccountModal.addPrivateKey.message')}</Box>

        <form onSubmit={form.handleSubmit(handleAddNewKeyFormSubmit)} id={'add_private_key_form'}>
          <Box>
            <FormText
              name={'username'}
              label={translate('auth.addAccount.associateAccountModal.addPrivateKey.form.username')}
              control={form.control}
              error={form.formState.errors.username}
              prefix={'@'}
            />
            <FormText
              name={'privateKey'}
              label={translate('auth.addAccount.associateAccountModal.addPrivateKey.form.privateKey')}
              control={form.control}
              error={form.formState.errors.privateKey}
            />
          </Box>
        </form>
      </Box>

      <DialogActions sx={stepActionStyles}>
        <LoadingButton
          loading={isWorking}
          disabled={isWorking || !form.formState.isValid}
          type={'submit'}
          form={'add_private_key_form'}
          variant={'contained'}
          size={'small'}
        >
          {translate('auth.addAccount.associateAccountModal.addPrivateKey.continue')}
        </LoadingButton>
      </DialogActions>

      <DialogVerticalError>{errorMessage}</DialogVerticalError>
    </Box>
  );
};
