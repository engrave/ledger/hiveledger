import { ReactNode } from 'react';
import { AccountError } from 'modules/hive/account/account';
import { isRpcError } from 'modules/ledger/rpcError/rpcError';
import { FormattedRpcError } from 'ui/message/formattedRpcError/FormattedRpcError';
import { TranslatedMessage } from 'ui/message/translatedMessage/TranslatedMessage';

export const getErrorMessage = (error: unknown): ReactNode => {
  if (error instanceof Error) {
    if (error.message === AccountError.notFound) {
      return <TranslatedMessage id={'auth.addAccount.associateAccountModal.addPrivateKey.errors.accountNotFound'} />;
    }

    if (error.message === AccountError.invalidPublicKey) {
      return <TranslatedMessage id={'auth.addAccount.associateAccountModal.addPrivateKey.errors.invalidPrivateKey'} />;
    }

    if (error.message === AccountError.insufficientOwnerKeyThreshold) {
      return (
        <TranslatedMessage
          id={'auth.addAccount.associateAccountModal.addPrivateKey.errors.insufficientOwnerKeyThreshold'}
        />
      );
    }

    if (isRpcError(error)) {
      return <FormattedRpcError error={error} />;
    }
  }

  return <TranslatedMessage id={'auth.addAccount.associateAccountModal.addPrivateKey.errors.genericError'} />;
};
