export enum ViewState {
  idle = 'idle',
  requestingNewKey = 'requesting_new_key',
  awaitingConfirmation = 'awaiting_confirmation',
  confirmed = 'confirmed',
}
