import SaveIcon from '@mui/icons-material/Save';
import { LoadingButton } from '@mui/lab';
import { Box, Button, DialogActions } from '@mui/material';
import { ReactNode, useState } from 'react';
import {
  stepActionStyles,
  stepContentStyles,
  stepMessageStyles,
  stepWrapperStyles,
} from 'app/auth/addAccount/associateAccountModal/AssociateAcoountModa.styles';
import { getErrorMessage } from 'app/auth/addAccount/associateAccountModal/steps/requestDeviceKey/helpers';
import { ViewState } from 'app/auth/addAccount/associateAccountModal/steps/requestDeviceKey/RequestDeviceKey.types';
import { AssociationStep } from 'context/association/associationContext/associationContext';
import { useAssociation } from 'context/association/hooks/useAssociation';
import { useUnusedAccount } from 'context/auth/hooks/useUnusedAccount';
import { useKeyConfirmation } from 'context/ledger/hooks/useKeyConfirmation/useKeyConfirmation';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useToast } from 'hooks/useToast/useToast';
import { AccountPublicKey } from 'modules/hive/account/account';
import { ActionParameter, ActionParameterLabel, ActionParameterValue } from 'ui/data/actionParameter/ActionParameter';
import { DialogVerticalError } from 'ui/modal/dialogVerticalError/DialogVerticalError';
import { logApplicationError } from 'utils/debug/logApplicationError';

export const RequestDeviceKey = () => {
  const translate = useTranslator();
  const { showError } = useToast();
  const { setCurrentStep, setDeviceAccount } = useAssociation();

  const [currentState, setCurrentState] = useState<ViewState>(ViewState.idle);
  const [unconfirmedAccountKey, setUnconfirmedAccountKey] = useState<AccountPublicKey | null>(null);
  const [errorMessage, setErrorMessage] = useState<ReactNode>(null);

  const findUnusedAccount = useUnusedAccount();
  const confirmPublicKey = useKeyConfirmation();

  const handleRequestingNewOwnerKey = async () => {
    setCurrentState(ViewState.requestingNewKey);
    setErrorMessage(null);

    try {
      setUnconfirmedAccountKey(null);
      const unusedAccount = await findUnusedAccount();

      setCurrentState(ViewState.awaitingConfirmation);
      setUnconfirmedAccountKey(unusedAccount.ownerKey);

      await confirmPublicKey(unusedAccount.ownerKey.path);
      setDeviceAccount(unusedAccount);

      setCurrentState(ViewState.confirmed);
    } catch (error) {
      setCurrentState(ViewState.idle);
      setErrorMessage(getErrorMessage(error));
      showError('auth.addAccount.associateAccountModal.requestDeviceKey.failed');
      logApplicationError(error);
    }
  };

  const handleContinue = () => {
    setCurrentStep(AssociationStep.addPrivateKey);
  };

  return (
    <Box sx={stepWrapperStyles}>
      <Box sx={stepContentStyles}>
        <Box sx={stepMessageStyles}>{translate('auth.addAccount.associateAccountModal.requestDeviceKey.message')}</Box>

        {unconfirmedAccountKey && (
          <Box>
            <ActionParameter>
              <ActionParameterLabel>
                {translate('auth.addAccount.associateAccountModal.requestDeviceKey.parameters.publicKey')}
              </ActionParameterLabel>
              <ActionParameterValue>{unconfirmedAccountKey.publicKey}</ActionParameterValue>
            </ActionParameter>
            <ActionParameter>
              <ActionParameterLabel>
                {translate('auth.addAccount.associateAccountModal.requestDeviceKey.parameters.path')}
              </ActionParameterLabel>
              <ActionParameterValue>{unconfirmedAccountKey.path}</ActionParameterValue>
            </ActionParameter>
          </Box>
        )}
      </Box>

      <DialogActions sx={stepActionStyles}>
        {[ViewState.idle, ViewState.requestingNewKey].includes(currentState) && (
          <LoadingButton
            variant={'contained'}
            size={'small'}
            loading={currentState === ViewState.requestingNewKey}
            disabled={currentState === ViewState.requestingNewKey}
            onClick={handleRequestingNewOwnerKey}
          >
            {translate('auth.addAccount.associateAccountModal.requestDeviceKey.requestNewKey')}
          </LoadingButton>
        )}
        {currentState === ViewState.awaitingConfirmation && (
          <LoadingButton
            variant={'contained'}
            size={'small'}
            loading={true}
            disabled={true}
            loadingPosition={'start'}
            startIcon={<SaveIcon />}
          >
            {translate('auth.addAccount.associateAccountModal.requestDeviceKey.confirm')}
          </LoadingButton>
        )}
        {currentState === ViewState.confirmed && (
          <Button variant={'contained'} size={'small'} onClick={() => handleContinue()}>
            {translate('auth.addAccount.associateAccountModal.requestDeviceKey.continue')}
          </Button>
        )}
      </DialogActions>
      <DialogVerticalError>{errorMessage}</DialogVerticalError>
    </Box>
  );
};
