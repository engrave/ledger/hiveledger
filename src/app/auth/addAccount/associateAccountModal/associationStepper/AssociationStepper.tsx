import { stepsOrder } from 'app/auth/addAccount/associateAccountModal/AssociateAccountModal.types';
import { useAssociation } from 'context/association/hooks/useAssociation';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { Step, Stepper } from 'ui/layout/stepper/Stepper';

export const AssociationStepper = () => {
  const translate = useTranslator();
  const { currentStep } = useAssociation();

  const currentStepIndex = stepsOrder.indexOf(currentStep);

  return (
    <Stepper>
      <Step
        isActive={currentStepIndex >= 0}
        name={translate('auth.addAccount.associateAccountModal.steps.step1.name')}
        description={translate('auth.addAccount.associateAccountModal.steps.step1.description')}
      />
      <Step
        isActive={currentStepIndex >= 1}
        name={translate('auth.addAccount.associateAccountModal.steps.step2.name')}
        description={translate('auth.addAccount.associateAccountModal.steps.step2.description')}
      />
      <Step
        isActive={currentStepIndex >= 2}
        name={translate('auth.addAccount.associateAccountModal.steps.step3.name')}
        description={translate('auth.addAccount.associateAccountModal.steps.step3.description')}
      />
    </Stepper>
  );
};
