import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { AssociateAccountModal } from 'app/auth/addAccount/associateAccountModal/AssociateAccountModal';
import { ReactComponent as UserIcon } from 'assets/images/icons/user.svg';
import { DashboardRoute } from 'config/routes';
import { useAuth } from 'context/auth/hooks/useAuth';
import { useHtmlTranslator } from 'context/localization/hooks/useHtmlTranslator';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useToast } from 'hooks/useToast/useToast';
import { Account } from 'modules/hive/account/account';
import { OptionCard, OptionCardContent, OptionCardIcon, OptionCardTitle } from 'ui/layout/optionCard/OptionCard';

export const AssociateAccountCard = () => {
  const translate = useTranslator();
  const translateHtml = useHtmlTranslator();
  const { showSuccess } = useToast();
  const navigate = useNavigate();
  const { authorizeAccount } = useAuth();

  const [isModalOpen, setModalOpen] = useState(false);

  const handleAssociateExistingAccount = () => {
    setModalOpen(true);
  };

  const handleAssociationComplete = (account: Account) => {
    setModalOpen(false);
    showSuccess('auth.addAccount.associateAccount.success');
    authorizeAccount(account);
    navigate(DashboardRoute.overview);
  };

  const handleAssociationCancel = () => {
    setModalOpen(false);
  };

  return (
    <>
      <AssociateAccountModal
        open={isModalOpen}
        onComplete={handleAssociationComplete}
        onCancel={handleAssociationCancel}
      />

      <OptionCard onClick={() => handleAssociateExistingAccount()}>
        <OptionCardIcon>
          <UserIcon />
        </OptionCardIcon>
        <OptionCardTitle>{translate('auth.addAccount.associateAccount.header')}</OptionCardTitle>
        <OptionCardContent>{translateHtml('auth.addAccount.associateAccount.description')}</OptionCardContent>
      </OptionCard>
    </>
  );
};
