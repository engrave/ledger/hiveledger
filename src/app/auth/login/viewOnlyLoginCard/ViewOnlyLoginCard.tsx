import reactHtmlParser from 'html-react-parser';
import { ReactComponent as UserIcon } from 'assets/images/icons/user.svg';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { OptionCard, OptionCardContent, OptionCardIcon, OptionCardTitle } from 'ui/layout/optionCard/OptionCard';

export const ViewOnlyLoginCard = () => {
  const translate = useTranslator();

  const startUsernameLogin = () => {
    // to be implemented...
  };

  return (
    <OptionCard onClick={() => startUsernameLogin()}>
      <OptionCardIcon>
        <UserIcon />
      </OptionCardIcon>
      <OptionCardTitle>{translate('auth.login.viewOnlyLogin.header')}</OptionCardTitle>
      <OptionCardContent>{reactHtmlParser(translate('auth.login.viewOnlyLogin.description'))}</OptionCardContent>
    </OptionCard>
  );
};
