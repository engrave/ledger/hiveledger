export type FullLoginModalProps = {
  open: boolean;
  onClose: () => void;
};
