import { ReactNode } from 'react';
import { AuthorizationError } from 'context/auth/authController/AuthController.types';
import { LedgerError } from 'context/ledger/ledgerController/LedgerController.types';
import { isRpcError } from 'modules/ledger/rpcError/rpcError';
import { FormattedRpcError } from 'ui/message/formattedRpcError/FormattedRpcError';
import { TranslatedMessage } from 'ui/message/translatedMessage/TranslatedMessage';

export const getErrorMessage = (error: unknown): ReactNode => {
  if (error instanceof Error) {
    if (error.message === LedgerError.connectionCancelled) {
      return <TranslatedMessage id={'auth.login.fullLoginModal.errors.connectionCancelled'} />;
    }

    if (error.message === AuthorizationError.deviceError) {
      return <TranslatedMessage id={'auth.login.fullLoginModal.errors.deviceProblem'} />;
    }

    if (isRpcError(error)) {
      return <FormattedRpcError error={error} />;
    }
  }

  return <TranslatedMessage id={'auth.login.fullLoginModal.errors.authorizationError'} />;
};
