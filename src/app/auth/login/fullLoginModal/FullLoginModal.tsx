import LoadingButton from '@mui/lab/LoadingButton';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';
import { ReactNode, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { FullLoginModalProps } from 'app/auth/login/fullLoginModal/FullLoginModal.types';
import { getErrorMessage } from 'app/auth/login/fullLoginModal/helpers';
import { AuthRoute, DashboardRoute } from 'config/routes';
import { AuthorizationError } from 'context/auth/authController/AuthController.types';
import { useAuth } from 'context/auth/hooks/useAuth';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useToast } from 'hooks/useToast/useToast';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { DialogHorizontalError } from 'ui/modal/dialogHorizontalError/DialogHorizontalError';
import { DialogHorizontalWrapper } from 'ui/modal/dialogHorizontalWrapper/DialogHorizontalWrapper';
import { logApplicationError } from 'utils/debug/logApplicationError';

export const FullLoginModal = (props: FullLoginModalProps) => {
  const translate = useTranslator();
  const { showSuccess, showError, showWarning } = useToast();
  const navigate = useNavigate();
  const { fullyAuthorize } = useAuth();
  const [errorMessage, setErrorMessage] = useState<ReactNode>(null);
  const [isAuthorizing, setAuthorizing] = useState(false);

  const startLegerConnection = async () => {
    setAuthorizing(true);
    setErrorMessage(null);

    try {
      const { active } = await fullyAuthorize();

      showSuccess('auth.login.fullLoginModal.authorized', { name: formatUsername(active.username) });
      navigate(DashboardRoute.overview);
    } catch (error) {
      if (error instanceof Error && error.message === AuthorizationError.noAccountFound) {
        showWarning('auth.login.fullLoginModal.noAccountFound');
        navigate(AuthRoute.addAccount);
      } else {
        showError('auth.login.fullLoginModal.failed');
        setErrorMessage(getErrorMessage(error));
      }

      setAuthorizing(false);
      logApplicationError(error);
    }
  };

  const handleModalClose = () => {
    if (isAuthorizing) {
      showWarning('auth.login.fullLoginModal.waitForAuthorization');
    } else {
      props.onClose();
      setTimeout(() => setErrorMessage(null), 100);
    }
  };

  return (
    <Dialog open={props.open} onClose={handleModalClose} maxWidth={'xs'}>
      <DialogTitle>{translate('auth.login.fullLoginModal.title')}</DialogTitle>
      <DialogHorizontalWrapper>
        <DialogContent>
          <DialogContentText>{translate('auth.login.fullLoginModal.description')}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <LoadingButton
            onClick={() => startLegerConnection()}
            variant={'contained'}
            loading={isAuthorizing}
            disabled={isAuthorizing}
          >
            {errorMessage === null
              ? translate('auth.login.fullLoginModal.connect')
              : translate('auth.login.fullLoginModal.retry')}
          </LoadingButton>
        </DialogActions>
      </DialogHorizontalWrapper>
      <DialogHorizontalError>{errorMessage}</DialogHorizontalError>
    </Dialog>
  );
};
