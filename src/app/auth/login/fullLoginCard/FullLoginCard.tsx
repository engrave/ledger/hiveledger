import { useState } from 'react';
import { FullLoginModal } from 'app/auth/login/fullLoginModal/FullLoginModal';
import { ReactComponent as LedgerIcon } from 'assets/images/icons/ledger.svg';
import { links } from 'config/config';
import { useHtmlTranslator } from 'context/localization/hooks/useHtmlTranslator';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { OptionCard, OptionCardContent, OptionCardIcon, OptionCardTitle } from 'ui/layout/optionCard/OptionCard';
import { stopPropagation } from 'utils/events/stopProgagation';

export const FullLoginCard = () => {
  const translate = useTranslator();
  const translateHtml = useHtmlTranslator();
  const [isModalOpen, setModalOpen] = useState(false);

  return (
    <>
      <FullLoginModal open={isModalOpen} onClose={() => setModalOpen(false)} />
      <OptionCard onClick={() => setModalOpen(true)}>
        <OptionCardIcon>
          <LedgerIcon />
        </OptionCardIcon>
        <OptionCardTitle>{translate('auth.login.fullLogin.header')}</OptionCardTitle>
        <OptionCardContent>
          <a href={links.buyLedgerNano} target={'_blank'} rel="noreferrer" onClick={stopPropagation}>
            {translate('auth.login.fullLogin.supportUs')}
          </a>
        </OptionCardContent>
        <OptionCardContent>{translateHtml('auth.login.fullLogin.description')}</OptionCardContent>
      </OptionCard>
    </>
  );
};
