import { Styles } from 'styles/theme.types';

export const columnsStyles: Styles = {
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
  alignItems: 'stretch',
  justifyContent: 'start',
  gap: 6,
};
