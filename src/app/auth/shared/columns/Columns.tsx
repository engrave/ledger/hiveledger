import { Box } from '@mui/material';
import { columnsStyles } from 'app/auth/shared/columns/Columns.styles';
import { WrapperProps } from 'types/react';

export const Columns = ({ children }: WrapperProps) => {
  return <Box sx={columnsStyles}>{children}</Box>;
};
