import { Styles } from 'styles/theme.types';

export const headerBackgroundStyles = (compact: boolean): Styles => ({
  position: compact ? 'fixed' : 'absolute',
  width: '100%',
  backgroundColor: 'header.main',
  height: compact ? '80px' : '280px',
  transition: `height 0.15s`,
});

export const headerStyles = (withNavigation: boolean, isCompactModeEnabled: boolean, compact: boolean): Styles => ({
  position: isCompactModeEnabled ? 'fixed' : 'absolute',
  zIndex: 1000,
  width: '100%',
  backgroundColor: 'header.main',
  paddingTop: compact ? 0 : 4,
  marginBottom: withNavigation ? '-60px' : '-130px',
  transition: `padding-top 0.15s`,
});

export const headerContainerStyles = (compact: boolean): Styles => ({
  backgroundColor: 'header.main',
  height: compact ? '80px' : 'auto',
  overflow: 'hidden',
  transition: `height 0.15s`,
});

export const headerSpacerStyles = (withNavigation: boolean): Styles => ({
  height: withNavigation ? '220px' : '150px',
});

export const logoSectionStyles: Styles = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  height: '80px',
  marginBottom: 1,
};

export const accountSwitcherWrapperStyles: Styles = {
  marginBottom: '10px',
};

export const dividerStyles = (compact: boolean): Styles => ({
  borderColor: 'header.separator',
  marginBottom: '28px',
  opacity: compact ? 0 : 1,
  transition: `opacity 0.15s`,
});

export const navigationWrapperStyles = (compact: boolean): Styles => ({
  position: 'relative',
  display: 'inline-block',
  top: compact ? '-96px' : 0,
  left: compact ? '180px' : 0,
  transition: `top 0.15s, left 0.15s`,
});
