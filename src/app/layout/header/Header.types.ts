export type HeaderProps = {
  enableCompactMode?: boolean;
  showNavigation?: boolean;
  showAccountSwitcher?: boolean;
};
