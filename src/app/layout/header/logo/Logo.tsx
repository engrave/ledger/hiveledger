import { Box, Link } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import { environmentNameStyles, logoWrapperStyles } from 'app/layout/header/logo/Logo.styles';
import { LogoProps } from 'app/layout/header/logo/Logo.types';
import { ReactComponent as HiveLogo } from 'assets/images/logo/hiveLedger.svg';
import { environment } from 'config/environment';
import { DashboardRoute, GeneralRoute } from 'config/routes';
import { useAuth } from 'context/auth/hooks/useAuth';

export const Logo = ({ compact }: LogoProps) => {
  const { isFullyAuthorized } = useAuth();

  const path = isFullyAuthorized ? DashboardRoute.overview : GeneralRoute.root;

  const shouldDisplayEnvironment = environment.name && environment.name !== 'mainnet';

  return (
    <Link sx={logoWrapperStyles(compact)} to={path} component={RouterLink}>
      <HiveLogo />
      {shouldDisplayEnvironment && <Box sx={environmentNameStyles}>{environment.name}</Box>}
    </Link>
  );
};
