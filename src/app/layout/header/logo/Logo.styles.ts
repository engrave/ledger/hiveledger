import { Styles } from 'styles/theme.types';

export const logoWrapperStyles = (compact: boolean): Styles => ({
  position: 'relative',
  transition: `opacity 0.3s, height 0.15s, width 0.15s`,
  height: compact ? '30px' : '40px',
  width: compact ? '147px' : '196px',

  svg: {
    height: '100%',
    width: '100%',
  },
});

export const logoStyles: Styles = {};

export const environmentNameStyles: Styles = {
  position: 'absolute',
  textTransform: 'uppercase',
  fontWeight: 'bolder',
  right: 0,
  bottom: '-16px',
  transform: 'translateZ(1px)',
};
