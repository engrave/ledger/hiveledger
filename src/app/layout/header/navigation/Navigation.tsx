import { NavigationProps } from 'app/layout/header/navigation/Navigation.types';
import { DashboardRoute } from 'config/routes';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { AppMenu, AppMenuItem } from 'ui/navigation/appMenu/AppMenu';

export const Navigation = (props: NavigationProps) => {
  const translate = useTranslator();

  return (
    <AppMenu compact={props.compact}>
      <AppMenuItem to={DashboardRoute.overview}>{translate('navigation.dashboard')}</AppMenuItem>
      <AppMenuItem to={DashboardRoute.balance}>{translate('navigation.balance')}</AppMenuItem>
      <AppMenuItem to={DashboardRoute.staking}>{translate('navigation.staking')}</AppMenuItem>
      <AppMenuItem to={DashboardRoute.savings}>{translate('navigation.savings')}</AppMenuItem>
      <AppMenuItem to={DashboardRoute.witnesses}>{translate('navigation.witnesses')}</AppMenuItem>
      <AppMenuItem to={DashboardRoute.proposals}>{translate('navigation.proposals')}</AppMenuItem>
      <AppMenuItem to={DashboardRoute.resourceCredits}>{translate('navigation.resourceCredits')}</AppMenuItem>
      <AppMenuItem to={DashboardRoute.advanced}>{translate('navigation.advanced')}</AppMenuItem>
    </AppMenu>
  );
};
