import { Box, Container, Divider } from '@mui/material';
import { AccountSwitcher } from 'app/layout/header/accountSwitcher/AccountSwitcher';
import {
  accountSwitcherWrapperStyles,
  dividerStyles,
  headerBackgroundStyles,
  headerContainerStyles,
  headerSpacerStyles,
  headerStyles,
  logoSectionStyles,
  navigationWrapperStyles,
} from 'app/layout/header/Header.styles';
import { HeaderProps } from 'app/layout/header/Header.types';
import { Logo } from 'app/layout/header/logo/Logo';
import { Navigation } from 'app/layout/header/navigation/Navigation';
import { useScrollAnimation } from 'hooks/useScrollAnimation/useScrollAnimation';

export const Header = (props: HeaderProps) => {
  const { enableCompactMode = false, showNavigation = false, showAccountSwitcher = false } = props;

  const { isAnimatedIn: isCompactModeTriggered } = useScrollAnimation({ threshold: 20 });

  const inCompactMode = enableCompactMode && isCompactModeTriggered;

  return (
    <>
      <Box sx={headerBackgroundStyles(inCompactMode)} />

      <Box sx={headerStyles(showNavigation, enableCompactMode, inCompactMode)} className={'mui-fixed'}>
        <Container fixed sx={headerContainerStyles(inCompactMode)}>
          <Box sx={logoSectionStyles}>
            <Logo compact={inCompactMode} />

            {showAccountSwitcher && (
              <Box sx={accountSwitcherWrapperStyles}>
                <AccountSwitcher />
              </Box>
            )}
          </Box>

          <Divider sx={dividerStyles(inCompactMode)} />

          {showNavigation && (
            <Box sx={navigationWrapperStyles(inCompactMode)}>
              <Navigation compact={inCompactMode} />
            </Box>
          )}
        </Container>
      </Box>

      <Box sx={headerSpacerStyles(showNavigation)}></Box>
    </>
  );
};
