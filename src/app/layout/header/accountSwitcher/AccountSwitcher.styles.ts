import { Styles } from 'styles/theme.types';

export const accountSwitcherStyles: Styles = {
  position: 'relative',
  padding: 0,
};

export const accountSwitcherUsernameStyles: Styles = {
  color: '#ffffff',
  fontWeight: 'normal',
  fontSize: '1rem',
};

export const accountSwitcherProgressStyles = (progress: number): Styles => ({
  position: 'absolute',
  left: 0,
  right: 0,
  bottom: '-2px',

  '&:before, &:after': {
    position: 'absolute',
    content: "' '",
    display: 'block',
    height: '3px',
    width: '100%',
    backgroundColor: 'common.white',
  },

  '&:after': {
    width: `${progress}%`,
    backgroundColor: 'ui.activeElement',
  },
});

export const accountSwitcherMenuStyles: Styles = {
  ul: {
    padding: 0,
    border: (theme) => `solid 1px ${theme.palette.ui.outlinedMenuBorder}`,
  },
  li: {
    borderBottom: (theme) => `1px solid ${theme.palette.ui.outlinedMenuBorder}`,
    padding: '9px 18px',
    minWidth: '200px',
  },
};
