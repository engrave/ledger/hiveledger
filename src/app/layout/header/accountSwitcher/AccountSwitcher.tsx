import { Box, Button, Menu, MenuItem } from '@mui/material';
import { usePopupState, bindTrigger, bindMenu } from 'material-ui-popup-state/hooks';
import { useNavigate } from 'react-router-dom';
import {
  accountSwitcherMenuStyles,
  accountSwitcherStyles,
  accountSwitcherProgressStyles,
  accountSwitcherUsernameStyles,
} from 'app/layout/header/accountSwitcher/AccountSwitcher.styles';
import { AuthRoute } from 'config/routes';
import { useAuth } from 'context/auth/hooks/useAuth';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useToast } from 'hooks/useToast/useToast';
import { Account } from 'modules/hive/account/account';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';

export const AccountSwitcher = () => {
  const { authorizedAccounts, activeAccount, activateAccount } = useAuth();
  const translate = useTranslator();
  const navigate = useNavigate();

  const menuState = usePopupState({ variant: 'popover', popupId: 'account-switcher-menu' });

  if (activeAccount === null) {
    return null;
  }

  const resourceCreditsProgress = activeAccount.resourceCredits.mana.percentage / 100;

  const handleActiveAccountChange = (account: Account) => {
    menuState.close();
    activateAccount(account.username);
  };

  const handleLogout = () => {
    menuState.close();
    navigate(AuthRoute.logout);
  };

  return (
    <>
      <Button sx={accountSwitcherStyles} {...bindTrigger(menuState)}>
        <Box sx={accountSwitcherUsernameStyles}>{formatUsername(activeAccount.username)}</Box>
        <Box sx={accountSwitcherProgressStyles(resourceCreditsProgress)} />
      </Button>

      <Menu
        {...bindMenu(menuState)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: -16,
          horizontal: 'right',
        }}
        elevation={2}
        sx={accountSwitcherMenuStyles}
      >
        {authorizedAccounts.map((account, index) => (
          <MenuItem onClick={() => handleActiveAccountChange(account)} key={index}>
            {formatUsername(account.username)}
          </MenuItem>
        ))}
        <MenuItem onClick={() => handleLogout()}>{translate('header.signOut')}</MenuItem>
      </Menu>
    </>
  );
};
