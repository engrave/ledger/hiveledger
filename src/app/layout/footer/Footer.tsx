import { Box, Container, Typography } from '@mui/material';
import { creditsStyles, footerStyles, profilesStyles } from 'app/layout/footer/Footer.styles';
import { ReactComponent as DiscordIcon } from 'assets/images/icons/discord.svg';
import { ReactComponent as GitLabIcon } from 'assets/images/icons/gitlab.svg';
import { ReactComponent as HiveIcon } from 'assets/images/icons/hive.svg';
import { ReactComponent as EngraveLogo } from 'assets/images/logo/engrave.svg';
import { links } from 'config/config';
import { useTranslator } from 'context/localization/hooks/useTranslator';

export const Footer = () => {
  const translate = useTranslator();

  return (
    <Container fixed>
      <Box sx={footerStyles}>
        <Box sx={creditsStyles}>
          <Typography variant={'overline'}>{translate('footer.developedBy')}</Typography>
          <a href={links.engrave} target={'_blank'} rel="noreferrer">
            <EngraveLogo />
          </a>
        </Box>
        <Box sx={profilesStyles}>
          <a href={links.discord} target={'_blank'} rel="noreferrer" title={translate('footer.discord')}>
            <DiscordIcon />
          </a>
          <a href={links.gitlab} target={'_blank'} rel="noreferrer" title={translate('footer.gitlab')}>
            <GitLabIcon />
          </a>
          <a href={links.hive} target={'_blank'} rel="noreferrer" title={translate('footer.hive')}>
            <HiveIcon />
          </a>
        </Box>
      </Box>
    </Container>
  );
};
