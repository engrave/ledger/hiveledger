import { Styles } from 'styles/theme.types';

export const footerStyles: Styles = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  height: '72px',
};

export const creditsStyles: Styles = {
  display: 'flex',
  alignItems: 'center',
  gap: 1,
};

export const profilesStyles: Styles = {
  display: 'flex',
  alignItems: 'center',
  gap: 2,

  '& > a': {
    '&:hover': {
      opacity: 0.7,
    },
  },
};
