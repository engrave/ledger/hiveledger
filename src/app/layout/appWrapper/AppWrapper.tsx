import { Box } from '@mui/material';
import { appWrapperStyles } from 'app/layout/appWrapper/AppWrapper.styles';
import { WrapperProps } from 'types/react';

export const AppWrapper = ({ children }: WrapperProps) => {
  return <Box sx={appWrapperStyles}>{children}</Box>;
};
