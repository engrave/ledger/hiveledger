import { Styles } from 'styles/theme.types';

export const appWrapperStyles: Styles = {
  display: 'flex',
  flexDirection: 'column',
  minHeight: '100vh',

  '& > *:first-of-type': {
    flex: '1 0 auto',
  },
  '& > *:last-of-type': {
    flexShrink: 0,
  },
};
