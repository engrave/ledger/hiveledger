import { Styles } from 'styles/theme.types';

export const currentVotesStyles: Styles = {
  textTransform: 'uppercase',
  fontSize: '0.625rem',
  fontWeight: 600,
  color: 'typography.common',
  letterSpacing: '0.17px',
  marginTop: '8px',
  display: 'flex',
  alignItems: 'center',
  gap: '5px',
};
