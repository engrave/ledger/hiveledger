import { Box } from '@mui/material';
import { ReactComponent as VoteIcon } from 'assets/images/icons/vote.svg';
import { witnessesConfig } from 'config/config';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useHtmlTranslator } from 'context/localization/hooks/useHtmlTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { HeadsUp, HeadsUpPosition } from 'ui/message/headsUp/HeadsUp';
import { currentVotesStyles } from './ProxyBanner.styles';

export const ProxyBanner = () => {
  const translate = useHtmlTranslator();
  const { details } = useActiveAccount();

  const isProxySet = details.proxy !== '';

  if (isProxySet) {
    return null;
  }

  return (
    <HeadsUp position={HeadsUpPosition.top}>
      {translate('dashboard.witnesses.proxyExplanation.proxyPropose', {
        account: formatUsername(witnessesConfig.proposedProxyAccount),
      })}

      <Box sx={currentVotesStyles}>
        <VoteIcon />
        {translate('dashboard.witnesses.proxyExplanation.currentVotes', {
          current: details.witness_votes.length,
          max: witnessesConfig.maxApprovedWitnesses,
        })}
      </Box>
    </HeadsUp>
  );
};
