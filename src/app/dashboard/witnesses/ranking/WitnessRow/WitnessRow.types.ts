import { Witness } from 'modules/hive/witnesses/witnesses';

export enum DisablementReason {
  maxApprovals = 'max_approvals',
  proxyEnabled = 'proxy_enabled',
  none = 'none',
}

export type WitnessRowProps = {
  witness: Witness;
  rank: number;
  isApproved: boolean;
  onApprovalChange: (witness: Witness, approve: boolean) => void;
  disablementReason?: DisablementReason;
};
