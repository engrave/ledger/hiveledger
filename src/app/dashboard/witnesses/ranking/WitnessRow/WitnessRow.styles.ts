import { Styles } from 'styles/theme.types';

export const witnessLinkStyles: Styles = {
  color: 'typography.common',
};

export const voteCellStyles: Styles = {
  textAlign: 'right',
  '& > *': {
    position: 'relative',
    right: '-8px',
  },
};
