import { Box, TableRow } from '@mui/material';
import { memo } from 'react';
import { voteCellStyles, witnessLinkStyles } from 'app/dashboard/witnesses/ranking/WitnessRow/WitnessRow.styles';
import { DisablementReason, WitnessRowProps } from 'app/dashboard/witnesses/ranking/WitnessRow/WitnessRow.types';
import { ReactComponent as LinkIcon } from 'assets/images/icons/link.svg';
import { witnessesConfig } from 'config/config';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { createAsset, createFromIntegerString } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getPriceFeed } from 'modules/hive/witnesses/getPriceFeed/getPriceFeed';
import { CheckboxCell } from 'ui/data/table/cells/checkboxCell/CheckboxCell';
import { TextCell } from 'ui/data/table/cells/textCell/TextCell';
import { TextCellFontSize, TextCellFontWeight } from 'ui/data/table/cells/textCell/TextCell.types';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';

export const WitnessRow = memo((props: WitnessRowProps) => {
  const { witness, rank, isApproved, onApprovalChange, disablementReason = DisablementReason.none } = props;

  const translate = useTranslator();
  const { getHivePower } = useHivePower();
  const { formatNumber, formatAsset } = useLocalization();

  const aprPercent = witness.props.hbd_interest_rate / 100 / 100;
  const accountCreationFee = createAsset(witness.props.account_creation_fee, ChainAssetSymbol.hive);
  const priceFeed = getPriceFeed(witness);

  const votes = createFromIntegerString(witness.votes, ChainAssetSymbol.vests);
  const votesPower = getHivePower(createFromIntegerString(witness.votes, ChainAssetSymbol.vests));

  return (
    <TableRow>
      <TextCell weight={TextCellFontWeight.normal} size={TextCellFontSize.big}>
        {translate('dashboard.witnesses.ranking.position', { rank })}
      </TextCell>

      <TextCell>
        <Box component={'a'} href={witness.url} target={'_blank'} rel={'noreferrer'} sx={witnessLinkStyles}>
          {formatUsername(witness.owner)} <LinkIcon />
        </Box>
      </TextCell>

      <TextCell weight={TextCellFontWeight.normal}>
        <Tooltip title={formatAsset(votes, { notation: 'compact' })}>
          {formatAsset(votesPower, { notation: 'compact', minimumFractionDigits: 0, maximumFractionDigits: 0 })}
        </Tooltip>
      </TextCell>

      <TextCell weight={TextCellFontWeight.normal}>
        {translate('dashboard.witnesses.ranking.version', { version: witness.running_version })}
      </TextCell>

      <TextCell weight={TextCellFontWeight.normal}>{formatNumber(aprPercent, { style: 'percent' })}</TextCell>

      <TextCell weight={TextCellFontWeight.normal}>
        {formatAsset(accountCreationFee, { minimumFractionDigits: 0, maximumFractionDigits: 0 })}
      </TextCell>

      <TextCell>{formatAsset(priceFeed, { minimumFractionDigits: 3, maximumFractionDigits: 3 })}</TextCell>

      {disablementReason === DisablementReason.none && (
        <CheckboxCell sx={voteCellStyles} value={isApproved} onClick={() => onApprovalChange(witness, !isApproved)} />
      )}
      {disablementReason === DisablementReason.maxApprovals && (
        <CheckboxCell
          sx={voteCellStyles}
          value={false}
          isDisabled={true}
          tooltip={translate('dashboard.witnesses.ranking.maxApprovals', {
            max: witnessesConfig.maxApprovedWitnesses,
          })}
        />
      )}
      {disablementReason === DisablementReason.proxyEnabled && (
        <CheckboxCell
          sx={voteCellStyles}
          value={false}
          isDisabled={true}
          tooltip={translate('dashboard.witnesses.ranking.proxyEnabled')}
        />
      )}
    </TableRow>
  );
});
