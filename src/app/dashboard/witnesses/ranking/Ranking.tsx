import { Box, Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';
import { useCallback } from 'react';
import { WitnessRow } from 'app/dashboard/witnesses/ranking/WitnessRow/WitnessRow';
import { DisablementReason } from 'app/dashboard/witnesses/ranking/WitnessRow/WitnessRow.types';
import { witnessesConfig } from 'config/config';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useWitnesses } from 'context/hive/hooks/useWitnesses/useWitnesses';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { accountWitnessVote } from 'modules/hive/operation/accountWitnessVote/accountWitnessVote';
import { Witness } from 'modules/hive/witnesses/witnesses';
import { LoadMoreButton } from 'ui/button/loadMoreButton/LoadMoreButton';
import { DashboardCard } from 'ui/layout/dashboardCard/DashboardCard';
import { SectionLoader } from 'ui/progress/sectionLoader/SectionLoader';

export const Ranking = () => {
  const translate = useTranslator();
  const { details, username } = useActiveAccount();
  const { witnesses, isLoading, isFirstLoading, isFetchingCompleted, fetchMore } = useWitnesses();
  const { runOperation } = useTransactionModal();

  const handleWitnessChange = useCallback(
    async (witness: Witness, approve: boolean) => {
      await runOperation(
        accountWitnessVote({
          account: username,
          witness: witness.owner,
          approve,
        }),
      );
    },
    [runOperation, username],
  );

  const approvedWitnesses = details.witness_votes;

  const getDisablementReason = (isActive: boolean) => {
    if (details.proxy !== '') {
      return DisablementReason.proxyEnabled;
    }
    if (!isActive && approvedWitnesses.length >= witnessesConfig.maxApprovedWitnesses) {
      return DisablementReason.maxApprovals;
    }
    return DisablementReason.none;
  };

  return (
    <DashboardCard>
      {isFirstLoading && <SectionLoader />}

      {!isFirstLoading && (
        <Box>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{translate('dashboard.witnesses.ranking.table.rank')}</TableCell>
                <TableCell>{translate('dashboard.witnesses.ranking.table.witness')}</TableCell>
                <TableCell>{translate('dashboard.witnesses.ranking.table.votes')}</TableCell>
                <TableCell>{translate('dashboard.witnesses.ranking.table.version')}</TableCell>
                <TableCell>{translate('dashboard.witnesses.ranking.table.apr')}</TableCell>
                <TableCell>{translate('dashboard.witnesses.ranking.table.acFee')}</TableCell>
                <TableCell>{translate('dashboard.witnesses.ranking.table.priceFeed')}</TableCell>
                <TableCell>{translate('dashboard.witnesses.ranking.table.vote')}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {witnesses.map((witness, index) => {
                const isApproved = approvedWitnesses.includes(witness.owner);

                return (
                  <WitnessRow
                    witness={witness}
                    rank={index + 1}
                    isApproved={isApproved}
                    onApprovalChange={handleWitnessChange}
                    disablementReason={getDisablementReason(isApproved)}
                    key={index}
                  />
                );
              })}
            </TableBody>
          </Table>

          {!isFetchingCompleted && <LoadMoreButton isLoading={isLoading} onClick={() => fetchMore()} />}
        </Box>
      )}
    </DashboardCard>
  );
};
