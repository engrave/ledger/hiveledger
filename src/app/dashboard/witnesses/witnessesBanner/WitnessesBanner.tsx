import { useTranslator } from 'context/localization/hooks/useTranslator';
import { Banner } from 'ui/message/banner/Banner';

export const WitnessesBanner = () => {
  const translate = useTranslator();

  return <Banner>{translate('dashboard.witnesses.banner')}</Banner>;
};
