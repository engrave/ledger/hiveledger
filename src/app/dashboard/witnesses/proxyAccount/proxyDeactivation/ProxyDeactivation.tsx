import { Box, Button } from '@mui/material';
import { proxyStatusStyles } from 'app/dashboard/witnesses/proxyAccount/proxyDeactivation/ProxyDeactivation.styles';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useHtmlTranslator } from 'context/localization/hooks/useHtmlTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { accountWitnessProxy } from 'modules/hive/operation/accountWitnessProxy/accountWitnessProxy';
import { FormAction } from 'ui/form/action/FormAction';

export const ProxyDeactivation = () => {
  const translate = useHtmlTranslator();
  const { details, username } = useActiveAccount();
  const { runOperation } = useTransactionModal();

  const handleProxyDeactivation = async () => {
    await runOperation(
      accountWitnessProxy({
        account: username,
        proxy: '',
      }),
    );
  };

  return (
    <Box>
      <Box sx={proxyStatusStyles}>
        {translate('dashboard.witnesses.proxy.proxyStatus', { proxy: formatUsername(details.proxy) })}
      </Box>

      <FormAction>
        <Button variant={'contained'} size={'small'} onClick={() => handleProxyDeactivation()}>
          {translate('dashboard.witnesses.proxy.deactivateProxy')}
        </Button>
      </FormAction>
    </Box>
  );
};
