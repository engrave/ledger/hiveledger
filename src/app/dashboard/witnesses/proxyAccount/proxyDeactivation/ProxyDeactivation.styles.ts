import { Styles } from 'styles/theme.types';

export const proxyStatusStyles: Styles = {
  fontSize: '0.875rem',
  marginBottom: '25px',
};
