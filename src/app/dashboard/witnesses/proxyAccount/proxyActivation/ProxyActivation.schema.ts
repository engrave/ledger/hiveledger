import { object, ObjectSchema, string } from 'yup';
import { ProxyActivationForm } from 'app/dashboard/witnesses/proxyAccount/proxyActivation/ProxyActivation.types';
import { SchemaCreatorParams } from 'validation/useSchemaCreator/useSchemaCreator';
import { isBadActor } from 'validation/validators/isBadActor/isBadActor';
import { isValidUsername } from 'validation/validators/isValidUsername/isValidUsername';

export const createProxyActivationSchema = ({ translate }: SchemaCreatorParams) => {
  return (): ObjectSchema<ProxyActivationForm> => {
    return object({
      proxy: string()
        .required()
        .test({
          test: isValidUsername,
          message: translate('validation.custom.invalidUsername'),
        })
        .test({
          test: isBadActor,
          message: translate('validation.custom.badActor'),
        })
        .label(translate('dashboard.witnesses.proxy.form.proxy')),
    }).required();
  };
};
