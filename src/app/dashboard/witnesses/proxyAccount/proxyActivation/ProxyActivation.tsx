import { yupResolver } from '@hookform/resolvers/yup/dist/yup';
import { Button } from '@mui/material';
import { useForm } from 'react-hook-form';
import { createProxyActivationSchema } from 'app/dashboard/witnesses/proxyAccount/proxyActivation/ProxyActivation.schema';
import { ProxyActivationForm } from 'app/dashboard/witnesses/proxyAccount/proxyActivation/ProxyActivation.types';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { accountWitnessProxy } from 'modules/hive/operation/accountWitnessProxy/accountWitnessProxy';
import { FormAction } from 'ui/form/action/FormAction';
import { FormText } from 'ui/form/text/FormText';
import { useSchemaCreator } from 'validation/useSchemaCreator/useSchemaCreator';

export const ProxyActivation = () => {
  const translate = useTranslator();
  const { username } = useActiveAccount();
  const { runOperation } = useTransactionModal();

  const formSchema = useSchemaCreator(createProxyActivationSchema);

  const form = useForm<ProxyActivationForm>({
    defaultValues: { proxy: '' },
    mode: 'onChange',
    resolver: yupResolver(formSchema()),
  });

  const handleProxyActivation = async (data: ProxyActivationForm) => {
    await runOperation(
      accountWitnessProxy({
        account: username,
        proxy: data.proxy,
      }),
    );
    form.reset();
  };

  return (
    <form onSubmit={form.handleSubmit(handleProxyActivation)}>
      <FormText
        name={'proxy'}
        label={translate('dashboard.witnesses.proxy.form.proxy')}
        control={form.control}
        error={form.formState.errors.proxy}
        prefix={'@'}
      />

      <FormAction>
        <Button type={'submit'} variant={'contained'} size={'small'} disabled={!form.formState.isValid}>
          {translate('dashboard.witnesses.proxy.activateProxy')}
        </Button>
      </FormAction>
    </form>
  );
};
