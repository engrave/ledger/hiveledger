import { ProxyActivation } from 'app/dashboard/witnesses/proxyAccount/proxyActivation/ProxyActivation';
import { ProxyDeactivation } from 'app/dashboard/witnesses/proxyAccount/proxyDeactivation/ProxyDeactivation';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export const ProxyAccount = () => {
  const translate = useTranslator();
  const { details } = useActiveAccount();

  const isProxySet = details.proxy !== '';

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.witnesses.proxy.title')}</DashboardCardTitle>

      {isProxySet ? <ProxyDeactivation /> : <ProxyActivation />}
    </DashboardCard>
  );
};
