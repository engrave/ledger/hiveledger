import { Box, Button } from '@mui/material';
import { useState } from 'react';
import { AssociateAccountModal } from 'app/auth/addAccount/associateAccountModal/AssociateAccountModal';
import { buttonWrapperStyles } from 'app/dashboard/overview/associateAccountButton/AssociateAccountButton.styles';
import { useAuth } from 'context/auth/hooks/useAuth';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useToast } from 'hooks/useToast/useToast';
import { Account } from 'modules/hive/account/account';

export const AssociateAccountButton = () => {
  const translate = useTranslator();
  const [isModalOpen, setModalOpen] = useState(false);
  const { showSuccess } = useToast();
  const { refreshAccounts, authorizeAccount } = useAuth();

  const handleAssociationComplete = (account: Account) => {
    setModalOpen(false);
    showSuccess('dashboard.overview.associateAccountButton.success');
    authorizeAccount(account);
    refreshAccounts();
  };

  const handleAssociationCancel = () => {
    setModalOpen(false);
  };

  return (
    <>
      <AssociateAccountModal
        open={isModalOpen}
        onComplete={handleAssociationComplete}
        onCancel={handleAssociationCancel}
      />

      <Box sx={buttonWrapperStyles}>
        <Button variant={'contained'} size={'large'} onClick={() => setModalOpen(true)}>
          {translate('dashboard.overview.associateAccountButton.label')}
        </Button>
      </Box>
    </>
  );
};
