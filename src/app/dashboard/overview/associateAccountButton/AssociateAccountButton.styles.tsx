import { Styles } from 'styles/theme.types';

export const buttonWrapperStyles: Styles = {
  marginTop: '30px',
  marginBottom: '40px',
  display: 'flex',
  justifyContent: 'flex-end',
};
