import ReloadIcon from '@mui/icons-material/Autorenew';
import { LoadingButton } from '@mui/lab';
import { Box } from '@mui/material';
import { buttonWrapperStyles, summaryStyles } from 'app/dashboard/overview/summary/Summary.styles';
import { SummaryProps } from 'app/dashboard/overview/summary/Summary.types';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { Asset, AssetConversions, AssetLabel, AssetValue } from 'ui/data/asset/Asset';
import { DashboardCard } from 'ui/layout/dashboardCard/DashboardCard';

export const Summary = (props: SummaryProps) => {
  const translate = useTranslator();
  const { totalBalance, conversions, estimatedAccountValue, handleAccountsRefresh, isRefreshing } = props;

  return (
    <DashboardCard sx={summaryStyles}>
      <Asset>
        <AssetLabel>{translate('dashboard.overview.summary.totalLiquid', { asset: ChainAssetSymbol.hive })}</AssetLabel>
        <AssetValue asset={totalBalance.hive} />
        <AssetConversions conversions={[conversions.hive.usd, conversions.hive.btc]} />
      </Asset>
      <Asset>
        <AssetLabel>{translate('dashboard.overview.summary.totalDebt', { asset: ChainAssetSymbol.hbd })}</AssetLabel>
        <AssetValue asset={totalBalance.hbd} />
        <AssetConversions conversions={[conversions.hbd.usd, conversions.hbd.btc]} />
      </Asset>
      <Asset>
        <AssetLabel>
          {translate('dashboard.overview.summary.estimatedWalletValue', { asset: ChainAssetSymbol.vests })}
        </AssetLabel>
        <AssetValue asset={estimatedAccountValue} />
      </Asset>
      <Box sx={buttonWrapperStyles}>
        <LoadingButton
          variant={'outlined'}
          size={'large'}
          startIcon={<ReloadIcon />}
          disabled={isRefreshing}
          loading={isRefreshing}
          onClick={handleAccountsRefresh}
        >
          {translate('dashboard.overview.summary.refresh')}
        </LoadingButton>
      </Box>
    </DashboardCard>
  );
};
