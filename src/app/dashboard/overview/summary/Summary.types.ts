import { ComparableEquivalents } from 'modules/exchange/convertAsset/convertAsset.types';
import { Asset } from 'modules/hive/asset/asset';
import { Balance } from 'modules/hive/wallet/wallet';

export type SummaryProps = {
  totalBalance: Balance;
  conversions: {
    hive: ComparableEquivalents;
    hbd: ComparableEquivalents;
  };
  estimatedAccountValue: Asset | null;
  handleAccountsRefresh: () => Promise<void>;
  isRefreshing: boolean;
};
