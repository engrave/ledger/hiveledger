import { Styles } from 'styles/theme.types';

export const summaryStyles: Styles = {
  padding: '30px',
  display: 'grid',
  gridTemplateColumns: '2fr 2fr 2fr 1fr',
};

export const buttonWrapperStyles: Styles = {
  justifySelf: 'self-end',
  alignSelf: 'center',
};
