import { useMemo, useState } from 'react';
import { Summary } from 'app/dashboard/overview/summary/Summary';
import { useAuth } from 'context/auth/hooks/useAuth';
import { useExchange } from 'context/exchange/hooks/useExchange';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useToast } from 'hooks/useToast/useToast';
import { addAsset } from 'modules/hive/asset/addAsset/addAsset';
import { sumWallets } from 'modules/hive/wallet/sumWallets/sumWallets';
import { logApplicationError } from 'utils/debug/logApplicationError';

export const SummaryContainer = () => {
  const { authorizedAccounts } = useAuth();
  const { getHivePower } = useHivePower();

  const wallets = authorizedAccounts.map((account) => account.wallet);

  const { showError } = useToast();

  const { convertToComparable } = useExchange();
  const { refreshAccounts } = useAuth();

  const [isRefreshing, setRefreshing] = useState(false);

  const totalBalance = useMemo(() => {
    return sumWallets(wallets, getHivePower);
  }, [wallets, getHivePower]);

  const conversions = useMemo(() => {
    return {
      hive: convertToComparable(totalBalance.hive),
      hbd: convertToComparable(totalBalance.hbd),
    };
  }, [convertToComparable, totalBalance.hbd, totalBalance.hive]);

  const estimatedAccountValue = useMemo(() => {
    if (conversions.hive.usd === null || conversions.hbd.usd === null) {
      return null;
    }
    return addAsset(conversions.hive.usd, conversions.hbd.usd);
  }, [conversions.hbd.usd, conversions.hive.usd]);

  const handleAccountsRefresh = async () => {
    setRefreshing(true);

    try {
      await refreshAccounts();
    } catch (error) {
      logApplicationError(error);
      showError('dashboard.overview.summary.refreshError');
    }

    setRefreshing(false);
  };

  return (
    <Summary
      totalBalance={totalBalance}
      conversions={conversions}
      estimatedAccountValue={estimatedAccountValue}
      handleAccountsRefresh={handleAccountsRefresh}
      isRefreshing={isRefreshing}
    />
  );
};
