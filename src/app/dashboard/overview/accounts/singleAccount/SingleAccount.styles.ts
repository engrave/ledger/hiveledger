import { Styles } from 'styles/theme.types';

export const accountInfoStyles: Styles = {
  paddingTop: '4px',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
};
