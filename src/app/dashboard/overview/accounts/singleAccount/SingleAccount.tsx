import { Box, Button } from '@mui/material';
import { accountInfoStyles } from 'app/dashboard/overview/accounts/singleAccount/SingleAccount.styles';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { Account } from 'modules/hive/account/account';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { addAsset } from 'modules/hive/asset/addAsset/addAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { Asset, AssetLabel, AssetValue, AssetValuesRow } from 'ui/data/asset/Asset';
import { AssetValueSize } from 'ui/data/asset/Asset.types';

export type SingleAccountProps = {
  account: Account;
  isActive: boolean;
  onActivate: () => void;
  index: number;
};

export const SingleAccount = (props: SingleAccountProps) => {
  const translate = useTranslator();
  const { formatAsset } = useLocalization();

  const { getHivePower } = useHivePower();

  const { account, isActive, onActivate, index } = props;
  const username = formatUsername(account.username);

  const handleAccountActivation = () => {
    onActivate();
  };

  const totalHive = addAsset(account.wallet.liquid.hive, account.wallet.savings.hive);
  const totalHbd = addAsset(account.wallet.liquid.hbd, account.wallet.savings.hbd);
  const totalHivePower = getHivePower(account.wallet.staked.staked);

  return (
    <>
      <Asset>
        <AssetLabel>{translate('dashboard.overview.accounts.accountNo', { index })}</AssetLabel>
        <Box sx={accountInfoStyles}>
          {username}
          <Button variant={'outlined'} size={'small'} disabled={isActive} onClick={handleAccountActivation}>
            {isActive && translate('dashboard.overview.accounts.activated')}
            {!isActive && translate('dashboard.overview.accounts.activate')}
          </Button>
        </Box>
      </Asset>
      <Asset>
        <AssetLabel>
          {translate('dashboard.overview.accounts.liquidAndSavings', { asset: ChainAssetSymbol.hive })}
        </AssetLabel>
        <AssetValuesRow>
          <AssetValue asset={totalHive} size={AssetValueSize.small} />
          <AssetValue asset={totalHbd} size={AssetValueSize.small} />
        </AssetValuesRow>
      </Asset>
      <Asset>
        <AssetLabel>{translate('dashboard.overview.accounts.staked', { asset: ChainAssetSymbol.hive })}</AssetLabel>
        <AssetValuesRow>
          <AssetValue
            asset={totalHivePower}
            tooltip={formatAsset(account.wallet.staked.staked)}
            size={AssetValueSize.small}
          />
        </AssetValuesRow>
      </Asset>
    </>
  );
};
