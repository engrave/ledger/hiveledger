import { accountsStyles } from 'app/dashboard/overview/accounts/Accounts.styles';
import { SingleAccount } from 'app/dashboard/overview/accounts/singleAccount/SingleAccount';
import { useAuth } from 'context/auth/hooks/useAuth';
import { isSame } from 'modules/hive/account/isSame/isSame';
import { DashboardCard } from 'ui/layout/dashboardCard/DashboardCard';

export const Accounts = () => {
  const { authorizedAccounts, activeAccount, activateAccount } = useAuth();

  return (
    <DashboardCard sx={accountsStyles}>
      {authorizedAccounts.map((account, index) => (
        <SingleAccount
          account={account}
          isActive={isSame(account, activeAccount)}
          onActivate={() => activateAccount(account.username)}
          index={index + 1}
          key={index}
        />
      ))}
    </DashboardCard>
  );
};
