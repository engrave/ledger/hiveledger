import { Styles } from 'styles/theme.types';

export const accountsStyles: Styles = {
  display: 'grid',
  gridTemplateColumns: '1fr 1.5fr 0fr',
  paddingY: '0',

  '& > *': {
    borderBottom: 1,
    borderColor: 'ui.separator',
    paddingY: '30px',
    paddingRight: 8,

    '&:nth-of-type(3n)': {
      paddingRight: 0,
    },
    '&:nth-last-of-type(-n+3)': {
      borderBottom: 0,
    },
  },
};
