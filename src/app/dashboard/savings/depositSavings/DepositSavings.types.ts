import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export type DepositSavingsForm = {
  asset: ChainAssetSymbol.hive | ChainAssetSymbol.hbd;
  amount: number;
};
