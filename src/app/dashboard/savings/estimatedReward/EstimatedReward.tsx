import { Box, Button } from '@mui/material';
import { actionAreaStyles, cardStyles } from 'app/dashboard/savings/estimatedReward/EstimatedReward.styles';
import { chain } from 'config/config';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useExchange } from 'context/exchange/hooks/useExchange';
import { useSavingsReward } from 'context/hive/hooks/useSavingsReward/useSavingsReward';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { cancelTransferFromSavings } from 'modules/hive/operation/cancelTransferFromSavings/cancelTransferFromSavings';
import { transferFromSavings } from 'modules/hive/operation/transferFromSavings/transferFromSavings';
import { Asset, AssetConversions, AssetValue } from 'ui/data/asset/Asset';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export const EstimatedReward = () => {
  const translate = useTranslator();
  const { formatAdaptableRelativeTime } = useLocalization();
  const { runOperation } = useTransactionModal();

  const account = useActiveAccount();

  const { estimatedSavingsReward, nextPayoutDate, isPayoutAvailable } = useSavingsReward();
  const { convertToComparable } = useExchange();

  if (estimatedSavingsReward.amount < chain.minimalSavingsRewardAmount) {
    return null;
  }

  const conversions = convertToComparable(estimatedSavingsReward);

  const handleClaim = () => {
    const transferOperation = transferFromSavings({
      amount: createAsset(chain.rewardClaimTransactionAmount, ChainAssetSymbol.hbd),
      username: account.username,
    });

    const transferCancellationOperation = cancelTransferFromSavings({
      requestId: transferOperation[1].request_id as number,
      from: account.username,
    });

    runOperation([transferOperation, transferCancellationOperation]);
  };

  return (
    <DashboardCard sx={cardStyles}>
      <DashboardCardTitle>{translate('dashboard.savings.estimatedReward.title')}</DashboardCardTitle>

      <Asset>
        <AssetValue asset={estimatedSavingsReward} />
        <AssetConversions conversions={[conversions.usd, conversions.btc]} />
      </Asset>

      <Box sx={actionAreaStyles}>
        {isPayoutAvailable && (
          <Button variant={'contained'} size={'small'} onClick={handleClaim}>
            {translate('dashboard.savings.estimatedReward.claim')}
          </Button>
        )}

        {!isPayoutAvailable && (
          <Button variant={'contained'} size={'small'} disabled>
            {translate('dashboard.savings.estimatedReward.claimInTime', {
              inTime: formatAdaptableRelativeTime(nextPayoutDate),
            })}
          </Button>
        )}
      </Box>
    </DashboardCard>
  );
};
