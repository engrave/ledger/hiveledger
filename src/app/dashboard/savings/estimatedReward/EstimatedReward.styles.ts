import { Styles } from 'styles/theme.types';

export const cardStyles: Styles = {
  width: '50%',
};

export const actionAreaStyles: Styles = {
  marginTop: '27px',
};
