import { Styles } from 'styles/theme.types';

export const assetsWrapperStyles: Styles = {
  display: 'grid',
  gridTemplateColumns: '2fr 2fr',
};

export const titleStyles: Styles = {
  marginBottom: '16px',
};
