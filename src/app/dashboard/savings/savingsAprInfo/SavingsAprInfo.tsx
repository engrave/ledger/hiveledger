import { useHive } from 'context/hive/hooks/useHive';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { HeadsUp } from 'ui/message/headsUp/HeadsUp';

export const SavingsAprInfo = () => {
  const translate = useTranslator();
  const { formatNumber } = useLocalization();
  const { globalProperties } = useHive();

  const hbdApr = formatNumber(globalProperties.hbd_interest_rate / 100, {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });

  return <HeadsUp>{translate('dashboard.savings.savingsAprInfo', { hbdApr })}</HeadsUp>;
};
