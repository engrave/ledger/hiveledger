import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export type WithdrawSavingsForm = {
  asset: ChainAssetSymbol.hive | ChainAssetSymbol.hbd;
  amount: number;
};
