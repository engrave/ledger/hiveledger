import { number, object, string } from 'yup';
import { DepositSavingsForm } from 'app/dashboard/savings/depositSavings/DepositSavings.types';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { Balance } from 'modules/hive/wallet/wallet';
import { SchemaCreatorParams } from 'validation/useSchemaCreator/useSchemaCreator';
import { EnoughAssetsMessageParams, isEnoughAssets } from 'validation/validators/isEnoughAssets/isEnoughAssets';

export const createWithdrawSavingsSchema = ({ translate, localization }: SchemaCreatorParams) => {
  return (balance: Balance) => {
    return object<DepositSavingsForm>({
      asset: string<ChainAssetSymbol>()
        .required()
        .oneOf([ChainAssetSymbol.hive, ChainAssetSymbol.hbd])
        .label(translate('dashboard.savings.depositSavings.form.asset')),
      amount: number<number>()
        .required()
        .positive()
        .test({
          test: isEnoughAssets('asset', balance),
          message: (params: EnoughAssetsMessageParams) => {
            return translate('validation.custom.enoughAssets', {
              ...params,
              available: localization.formatAssetAmount(params.available),
            });
          },
        })
        .label(translate('dashboard.savings.depositSavings.form.amount')),
    }).required();
  };
};
