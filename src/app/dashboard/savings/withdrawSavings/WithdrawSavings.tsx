import { yupResolver } from '@hookform/resolvers/yup';
import { Button } from '@mui/material';
import { useForm } from 'react-hook-form';
import { createWithdrawSavingsSchema } from 'app/dashboard/savings/withdrawSavings/WithdrawSavings.schema';
import { WithdrawSavingsForm } from 'app/dashboard/savings/withdrawSavings/WithdrawSavings.types';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { transferFromSavings } from 'modules/hive/operation/transferFromSavings/transferFromSavings';
import { FormAction } from 'ui/form/action/FormAction';
import { AvailableAmountHelper } from 'ui/form/availableAmountHelper/AvailableAmountHelper';
import { FormNumber } from 'ui/form/number/FormNumber';
import { FormSelect } from 'ui/form/select/FormSelect';
import { DropdownOption } from 'ui/form/select/FormSelect.types';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { useSchemaCreator } from 'validation/useSchemaCreator/useSchemaCreator';

export const WithdrawSavings = () => {
  const translate = useTranslator();
  const { wallet, username } = useActiveAccount();
  const { runOperation } = useTransactionModal();

  const withdrawSavingsSchema = useSchemaCreator(createWithdrawSavingsSchema);

  const form = useForm<WithdrawSavingsForm>({
    defaultValues: {
      asset: ChainAssetSymbol.hive,
      amount: 0,
    },
    mode: 'onChange',
    resolver: yupResolver(withdrawSavingsSchema(wallet.savings)),
  });

  const [asset] = form.watch(['asset', 'amount']);
  const availableAssets = asset === ChainAssetSymbol.hive ? wallet.savings.hive : wallet.savings.hbd;

  const assetOptions: DropdownOption[] = [
    { label: ChainAssetSymbol.hive, value: ChainAssetSymbol.hive },
    { label: ChainAssetSymbol.hbd, value: ChainAssetSymbol.hbd },
  ];

  const handleWithdrawSavingsFormSubmit = async (data: WithdrawSavingsForm) => {
    await runOperation(transferFromSavings({ amount: createAsset(data.amount, data.asset), username }));
    form.reset();
  };

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.savings.withdrawSavings.title')}</DashboardCardTitle>

      <form onSubmit={form.handleSubmit(handleWithdrawSavingsFormSubmit)}>
        <FormSelect
          name={'asset'}
          control={form.control}
          label={translate('dashboard.savings.withdrawSavings.form.asset')}
          options={assetOptions}
          helperText={<AvailableAmountHelper asset={availableAssets} form={form} target={'amount'} />}
          error={form.formState.errors.asset}
          onChange={() => {
            if (form.getValues('amount') > 0) {
              form.trigger('amount');
            }
          }}
        />
        <FormNumber
          name={'amount'}
          control={form.control}
          label={translate('dashboard.savings.withdrawSavings.form.amount')}
          format={{
            decimalScale: 3,
            fixedDecimalScale: true,
          }}
          error={form.formState.errors.amount}
        />
        <FormAction>
          <Button type={'submit'} variant={'contained'} size={'small'} disabled={!form.formState.isValid}>
            {translate('dashboard.savings.withdrawSavings.form.withdraw')}
          </Button>
        </FormAction>
      </form>
    </DashboardCard>
  );
};
