import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';
import { titleStyles } from 'app/dashboard/savings/manageWithdrawals/ManageWithdrawals.styles';
import { WithdrawalRow } from 'app/dashboard/savings/manageWithdrawals/withdrawalRow/WithdrawalRow';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { SavingWithdrawal } from 'context/hive/hiveContext/hiveContext.types';
import { useSavingWithdrawals } from 'context/hive/hooks/useSavingWithdrawals';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { cancelTransferFromSavings } from 'modules/hive/operation/cancelTransferFromSavings/cancelTransferFromSavings';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export const ManageWithdrawals = () => {
  const translate = useTranslator();
  const { username } = useActiveAccount();
  const { runOperation } = useTransactionModal();

  const { isLoading, withdrawals, refresh } = useSavingWithdrawals(username);

  const handleWithdrawalCancel = async (withdrawal: SavingWithdrawal) => {
    await runOperation(cancelTransferFromSavings({ from: username, requestId: withdrawal.request_id }));
    await refresh();
  };

  if (isLoading || withdrawals.length === 0) {
    return null;
  }

  return (
    <DashboardCard>
      <DashboardCardTitle sx={titleStyles}>{translate('dashboard.savings.manageWithdrawals.title')}</DashboardCardTitle>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>{translate('dashboard.savings.manageWithdrawals.table.amount')}</TableCell>
            <TableCell>{translate('dashboard.savings.manageWithdrawals.table.date')}</TableCell>
            <TableCell>&nbsp;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {withdrawals.map((withdrawal, index) => (
            <WithdrawalRow withdrawal={withdrawal} onCancel={() => handleWithdrawalCancel(withdrawal)} key={index} />
          ))}
        </TableBody>
      </Table>
    </DashboardCard>
  );
};
