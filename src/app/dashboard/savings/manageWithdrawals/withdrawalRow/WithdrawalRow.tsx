import { TableRow, Tooltip } from '@mui/material';
import { WithdrawalRowProps } from 'app/dashboard/savings/manageWithdrawals/withdrawalRow/WithdrawalRow.types';
import { ReactComponent as TrashcanIcon } from 'assets/images/icons/trashcan.svg';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { parseAsset } from 'modules/hive/asset/parseAsset/parseAsset';
import { AssetCell } from 'ui/data/table/cells/assetCell/AssetCell';
import { IconButtonCell } from 'ui/data/table/cells/iconButtonCell/IconButtonCell';
import { TextCell } from 'ui/data/table/cells/textCell/TextCell';

export const WithdrawalRow = ({ withdrawal, onCancel }: WithdrawalRowProps) => {
  const { formatAdaptableRelativeTime, formatDate } = useLocalization();
  const translate = useTranslator();

  return (
    <TableRow>
      <AssetCell asset={parseAsset(withdrawal.amount)} />
      <TextCell>
        <Tooltip title={formatDate(withdrawal.complete, { dateStyle: 'long', timeStyle: 'medium' })}>
          <span>{formatAdaptableRelativeTime(new Date(withdrawal.complete))}</span>
        </Tooltip>
      </TextCell>
      <IconButtonCell
        onClick={() => onCancel()}
        tooltip={translate('dashboard.savings.manageWithdrawals.cancelTooltip')}
      >
        <TrashcanIcon />
      </IconButtonCell>
    </TableRow>
  );
};
