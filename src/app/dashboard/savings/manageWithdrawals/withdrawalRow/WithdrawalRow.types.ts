import { SavingWithdrawal } from 'context/hive/hiveContext/hiveContext.types';

export type WithdrawalRowProps = {
  withdrawal: SavingWithdrawal;
  onCancel: () => void;
};
