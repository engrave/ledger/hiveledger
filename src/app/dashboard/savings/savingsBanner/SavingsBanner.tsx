import { useTranslator } from 'context/localization/hooks/useTranslator';
import { Banner } from 'ui/message/banner/Banner';

export const SavingsBanner = () => {
  const translate = useTranslator();

  return <Banner>{translate('dashboard.savings.banner')}</Banner>;
};
