import { Box } from '@mui/material';
import { assetsWrapperStyles, titleStyles } from 'app/dashboard/balance/summary/Summary.styles';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useExchange } from 'context/exchange/hooks/useExchange';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { Asset, AssetConversions, AssetValue } from 'ui/data/asset/Asset';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export const Summary = () => {
  const translate = useTranslator();
  const { wallet } = useActiveAccount();
  const { convertToComparable } = useExchange();

  const hiveConversions = convertToComparable(wallet.liquid.hive);
  const hbdConversions = convertToComparable(wallet.liquid.hbd);

  return (
    <DashboardCard>
      <DashboardCardTitle sx={titleStyles}>{translate('dashboard.balance.summary.title')}</DashboardCardTitle>

      <Box sx={assetsWrapperStyles}>
        <Asset>
          <AssetValue asset={wallet.liquid.hive} />
          <AssetConversions conversions={[hiveConversions.usd, hiveConversions.btc]} />
        </Asset>
        <Asset>
          <AssetValue asset={wallet.liquid.hbd} />
          <AssetConversions conversions={[hbdConversions.usd, hbdConversions.btc]} />
        </Asset>
      </Box>
    </DashboardCard>
  );
};
