import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export type PowerUpForm = {
  asset: ChainAssetSymbol.hive;
  amount: number;
};
