import { Styles } from 'styles/theme.types';

export const helpTooltipStyles: Styles = {
  position: 'absolute',
  top: '17px',
  right: '22px',
};
