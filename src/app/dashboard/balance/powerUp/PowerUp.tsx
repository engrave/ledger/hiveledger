import { yupResolver } from '@hookform/resolvers/yup';
import { Box, Button } from '@mui/material';
import { useForm } from 'react-hook-form';
import { number, object, ObjectSchema, string } from 'yup';
import { helpTooltipStyles } from 'app/dashboard/balance/powerUp/PowerUp.styles';
import { PowerUpForm } from 'app/dashboard/balance/powerUp/PowerUp.types';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { transferToVesting } from 'modules/hive/operation/transferToVesting/transferToVesting';
import { FormAction } from 'ui/form/action/FormAction';
import { AvailableAmountHelper } from 'ui/form/availableAmountHelper/AvailableAmountHelper';
import { FormNumber } from 'ui/form/number/FormNumber';
import { FormSelect } from 'ui/form/select/FormSelect';
import { DropdownOption } from 'ui/form/select/FormSelect.types';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { HelpTooltip } from 'ui/popover/helpTooltip/HelpTooltip';

export const PowerUp = () => {
  const translate = useTranslator();
  const { formatNumber } = useLocalization();
  const { getHivePowerApr } = useHivePower();
  const { wallet, username } = useActiveAccount();
  const { runOperation } = useTransactionModal();

  const formSchema: ObjectSchema<PowerUpForm> = object({
    asset: string<ChainAssetSymbol.hive>()
      .oneOf([ChainAssetSymbol.hive])
      .required()
      .label(translate('dashboard.balance.powerUp.form.asset')),
    amount: number()
      .positive()
      .max(wallet.liquid.hive.amount)
      .required()
      .label(translate('dashboard.balance.powerUp.form.amount')),
  }).required();

  const form = useForm<PowerUpForm>({
    defaultValues: {
      asset: ChainAssetSymbol.hive,
      amount: 0,
    },
    mode: 'onChange',
    resolver: yupResolver(formSchema),
  });

  const assetOptions: DropdownOption[] = [{ label: ChainAssetSymbol.hive, value: ChainAssetSymbol.hive }];

  const handlePowerUpFormSubmit = async (data: PowerUpForm) => {
    await runOperation(transferToVesting({ amount: createAsset(data.amount, data.asset), from: username }));
    form.reset();
  };

  const apr = formatNumber(getHivePowerApr(), { maximumFractionDigits: 2 });

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.balance.powerUp.title')}</DashboardCardTitle>

      <HelpTooltip title={translate('dashboard.balance.powerUp.help', { apr })} sx={helpTooltipStyles} />
      <form onSubmit={form.handleSubmit(handlePowerUpFormSubmit)}>
        <Box>
          <FormSelect
            name={'asset'}
            control={form.control}
            label={translate('dashboard.balance.powerUp.form.asset')}
            options={assetOptions}
            helperText={<AvailableAmountHelper asset={wallet.liquid.hive} form={form} target={'amount'} />}
            error={form.formState.errors.asset}
            disabled
          />
          <FormNumber
            name={'amount'}
            control={form.control}
            label={translate('dashboard.balance.powerUp.form.amount')}
            format={{
              decimalScale: 3,
              fixedDecimalScale: true,
            }}
            error={form.formState.errors.amount}
          />
        </Box>

        <FormAction>
          <Button type={'submit'} variant={'contained'} size={'small'} disabled={!form.formState.isValid}>
            {translate('dashboard.balance.powerUp.form.powerUp')}
          </Button>
        </FormAction>
      </form>
    </DashboardCard>
  );
};
