import { TableCell, TableRow } from '@mui/material';
import { TransactionRowProps } from 'app/dashboard/balance/transactionHistory/TransactionHistory.types';
import { DateCell } from 'ui/data/table/cells/dateCell/DateCell';
import { ParamsCell } from 'ui/data/table/cells/paramsCell/ParamsCell';
import { TransactionIdCell } from 'ui/data/table/cells/transactionIdCell/TransactionIdCell';

export const DefaultRow = ({ transaction }: TransactionRowProps) => {
  return (
    <TableRow>
      <TransactionIdCell id={transaction.trx_id} block={transaction.block} />
      <DateCell timestamp={transaction.timestamp} />
      <ParamsCell operation={transaction.op} />
      <TableCell />
    </TableRow>
  );
};
