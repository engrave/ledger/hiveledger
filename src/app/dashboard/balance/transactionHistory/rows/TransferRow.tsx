import { TableRow } from '@mui/material';
import { TransactionRowProps } from 'app/dashboard/balance/transactionHistory/TransactionHistory.types';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { parseAsset } from 'modules/hive/asset/parseAsset/parseAsset';
import { AssetsListCell } from 'ui/data/table/cells/assetsListCell/AssetsListCell';
import { DateCell } from 'ui/data/table/cells/dateCell/DateCell';
import { SecondaryTextCell } from 'ui/data/table/cells/secondaryTextCell/SecondaryTextCell';
import { TransactionIdCell } from 'ui/data/table/cells/transactionIdCell/TransactionIdCell';
import { MemoIcon } from 'ui/data/table/memoIcon/MemoIcon';

export const TransferRow = ({ transaction }: TransactionRowProps) => {
  const translate = useTranslator();
  const { username } = useActiveAccount();

  const toAccount = transaction.op['1'].to || '';
  const fromAccount = transaction.op['1'].from || '';

  const isSender = fromAccount === username;

  const amount = parseAsset(transaction.op['1'].amount);
  const { memo } = transaction.op['1'];

  return (
    <TableRow>
      <TransactionIdCell id={transaction.trx_id} block={transaction.block} />
      <DateCell timestamp={transaction.timestamp} />
      <SecondaryTextCell>
        {isSender &&
          translate('transaction.description.transferTo', {
            username: formatUsername(toAccount),
          })}
        {!isSender &&
          translate('transaction.description.transferFrom', {
            username: formatUsername(fromAccount),
          })}
        {memo && <MemoIcon title={transaction.op['1'].memo} />}
      </SecondaryTextCell>
      <AssetsListCell assets={[amount]} />
    </TableRow>
  );
};
