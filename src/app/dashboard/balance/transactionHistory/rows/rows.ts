import { TransferRow } from 'app/dashboard/balance/transactionHistory/rows/TransferRow';
import { OperationType } from 'modules/hive/operation/operation';
import { ClaimRewardBalanceRow } from './ClaimRewardBalanceRow';
import { FillConvertRequestRow } from './FillConvertRequestRow';
import { FillTransferFromSavingsRow } from './FillTransferFromSavingsRow';
import { InterestRow } from './InterestRow';
import { TransferToSavingsRow } from './TransferToSavingsRow';
import { TransferToVestingRow } from './TransferToVestingRow';

export const rowRenderer = {
  [OperationType.transfer]: TransferRow,
  [OperationType.interest]: InterestRow,
  [OperationType.transferToSavings]: TransferToSavingsRow,
  [OperationType.fillTransferFromSavings]: FillTransferFromSavingsRow,
  [OperationType.transferToVesting]: TransferToVestingRow,
  [OperationType.fillConvertRequest]: FillConvertRequestRow,
  [OperationType.claimRewardBalance]: ClaimRewardBalanceRow,
};

export const hasCustomRenderer = (type: string | number | symbol): type is keyof typeof rowRenderer => {
  return Object.keys(rowRenderer).includes(type.toString());
};
