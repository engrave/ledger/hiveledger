import { TableRow } from '@mui/material';
import { TransactionRowProps } from 'app/dashboard/balance/transactionHistory/TransactionHistory.types';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { parseAsset } from 'modules/hive/asset/parseAsset/parseAsset';
import { AssetsListCell } from 'ui/data/table/cells/assetsListCell/AssetsListCell';
import { DateCell } from 'ui/data/table/cells/dateCell/DateCell';
import { SecondaryTextCell } from 'ui/data/table/cells/secondaryTextCell/SecondaryTextCell';
import { TransactionIdCell } from 'ui/data/table/cells/transactionIdCell/TransactionIdCell';

export const TransferToVestingRow = ({ transaction }: TransactionRowProps) => {
  const translate = useTranslator();
  const { username } = useActiveAccount();

  const toAccount = transaction.op['1'].to || '';
  const fromAccount = transaction.op['1'].from || '';

  const selfPowerUp = fromAccount === username;

  const amount = parseAsset(transaction.op['1'].amount);

  return (
    <TableRow>
      <TransactionIdCell id={transaction.trx_id} block={transaction.block} />
      <DateCell timestamp={transaction.timestamp} />
      <SecondaryTextCell>
        {selfPowerUp &&
          translate('transaction.description.transferToVesting', {
            username: formatUsername(toAccount),
          })}
        {!selfPowerUp &&
          translate('transaction.description.transferToVestingFrom', {
            username: formatUsername(fromAccount),
          })}
      </SecondaryTextCell>
      <AssetsListCell assets={[amount]} />
    </TableRow>
  );
};
