import { TableRow } from '@mui/material';
import { TransactionRowProps } from 'app/dashboard/balance/transactionHistory/TransactionHistory.types';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { parseAsset } from 'modules/hive/asset/parseAsset/parseAsset';
import { AssetsListCell } from 'ui/data/table/cells/assetsListCell/AssetsListCell';
import { DateCell } from 'ui/data/table/cells/dateCell/DateCell';
import { SecondaryTextCell } from 'ui/data/table/cells/secondaryTextCell/SecondaryTextCell';
import { TransactionIdCell } from 'ui/data/table/cells/transactionIdCell/TransactionIdCell';
import { MemoIcon } from 'ui/data/table/memoIcon/MemoIcon';

export const TransferToSavingsRow = ({ transaction }: TransactionRowProps) => {
  const translate = useTranslator();
  const { username } = useActiveAccount();

  const { memo, to, from } = transaction.op['1'];
  const amount = parseAsset(transaction.op['1'].amount);
  const selfTransfer = from === username;

  return (
    <TableRow>
      <TransactionIdCell id={transaction.trx_id} block={transaction.block} />
      <DateCell timestamp={transaction.timestamp} />
      <SecondaryTextCell>
        {selfTransfer &&
          translate('transaction.description.transferToSavings', {
            username: formatUsername(to),
          })}
        {!selfTransfer &&
          translate('transaction.description.transferToSavingsFrom', {
            username: formatUsername(from),
          })}
        {memo && <MemoIcon title={transaction.op['1'].memo} />}
      </SecondaryTextCell>
      <AssetsListCell assets={[amount]} />
    </TableRow>
  );
};
