import { TableRow } from '@mui/material';
import { TransactionRowProps } from 'app/dashboard/balance/transactionHistory/TransactionHistory.types';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { parseAsset } from 'modules/hive/asset/parseAsset/parseAsset';
import { AssetsListCell } from 'ui/data/table/cells/assetsListCell/AssetsListCell';
import { DateCell } from 'ui/data/table/cells/dateCell/DateCell';
import { SecondaryTextCell } from 'ui/data/table/cells/secondaryTextCell/SecondaryTextCell';
import { TransactionIdCell } from 'ui/data/table/cells/transactionIdCell/TransactionIdCell';
import { printAsset } from '../../../../../modules/hive/asset/printAsset/printAsset';

export const FillConvertRequestRow = ({ transaction }: TransactionRowProps) => {
  const translate = useTranslator();

  const amountIn = parseAsset(transaction.op['1'].amount_in);
  const amountOut = parseAsset(transaction.op['1'].amount_out);

  return (
    <TableRow>
      <TransactionIdCell id={transaction.trx_id} block={transaction.block} />
      <DateCell timestamp={transaction.timestamp} />
      <SecondaryTextCell>
        {translate('transaction.description.fillConvertRequest', {
          amount: printAsset(amountIn),
        })}
      </SecondaryTextCell>
      <AssetsListCell assets={[amountOut]} />
    </TableRow>
  );
};
