import { TableRow } from '@mui/material';
import { TransactionRowProps } from 'app/dashboard/balance/transactionHistory/TransactionHistory.types';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { AssetsListCell } from 'ui/data/table/cells/assetsListCell/AssetsListCell';
import { DateCell } from 'ui/data/table/cells/dateCell/DateCell';
import { SecondaryTextCell } from 'ui/data/table/cells/secondaryTextCell/SecondaryTextCell';
import { TransactionIdCell } from 'ui/data/table/cells/transactionIdCell/TransactionIdCell';
import { parseAsset } from '../../../../../modules/hive/asset/parseAsset/parseAsset';

export const InterestRow = ({ transaction }: TransactionRowProps) => {
  const translate = useTranslator();
  const amount = parseAsset(transaction.op['1'].interest);
  return (
    <TableRow>
      <TransactionIdCell id={transaction.trx_id} block={transaction.block} />
      <DateCell timestamp={transaction.timestamp} />
      <SecondaryTextCell>{translate('transaction.description.interest')}</SecondaryTextCell>
      <AssetsListCell assets={[amount]} />
    </TableRow>
  );
};
