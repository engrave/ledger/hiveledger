import { TableRow } from '@mui/material';
import { TransactionRowProps } from 'app/dashboard/balance/transactionHistory/TransactionHistory.types';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { AssetsListCell } from 'ui/data/table/cells/assetsListCell/AssetsListCell';
import { DateCell } from 'ui/data/table/cells/dateCell/DateCell';
import { SecondaryTextCell } from 'ui/data/table/cells/secondaryTextCell/SecondaryTextCell';
import { TransactionIdCell } from 'ui/data/table/cells/transactionIdCell/TransactionIdCell';

export const ClaimRewardBalanceRow = ({ transaction }: TransactionRowProps) => {
  const translate = useTranslator();

  const rewardHive = createAsset(transaction.op['1'].reward_hive, ChainAssetSymbol.hive);
  const rewardHbd = createAsset(transaction.op['1'].reward_hbd, ChainAssetSymbol.hbd);
  const rewardVests = createAsset(transaction.op['1'].reward_vests, ChainAssetSymbol.vests);

  const rewardedAssets = [rewardHive, rewardHbd, rewardVests].filter((asset) => asset.amount > 0);

  return (
    <TableRow>
      <TransactionIdCell id={transaction.trx_id} block={transaction.block} />
      <DateCell timestamp={transaction.timestamp} />
      <SecondaryTextCell>{translate('transaction.description.claimRewards')}</SecondaryTextCell>
      <AssetsListCell assets={rewardedAssets} />
    </TableRow>
  );
};
