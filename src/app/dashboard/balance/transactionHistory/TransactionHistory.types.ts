import { AppliedOperation } from '@hiveio/dhive/lib/chain/operation';

export type TransactionRowProps = {
  transaction: AppliedOperation;
};
