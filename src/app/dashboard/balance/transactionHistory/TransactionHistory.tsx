import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';
import { memo } from 'react';
import { titleStyles } from 'app/dashboard/balance/summary/Summary.styles';
import { DefaultRow } from 'app/dashboard/balance/transactionHistory/rows/DefaultRow';
import { hasCustomRenderer, rowRenderer } from 'app/dashboard/balance/transactionHistory/rows/rows';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTransactionHistory } from 'context/hive/hooks/useTransactionHistory';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { LoadMoreButton } from 'ui/button/loadMoreButton/LoadMoreButton';
import { EmptyListCell } from 'ui/data/table/cells/emptyListCell/EmptyListCell';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { SectionLoader } from 'ui/progress/sectionLoader/SectionLoader';

export const TransactionHistory = () => {
  const translate = useTranslator();
  const { username } = useActiveAccount();
  const { history, isLoading, loadNextPage, isFetchingCompleted, isFirstLoading } = useTransactionHistory(username);

  return (
    <DashboardCard>
      <DashboardCardTitle sx={titleStyles}>{translate('dashboard.balance.history.title')}</DashboardCardTitle>

      {isFirstLoading && <SectionLoader height={91} />}

      {!isFirstLoading && (
        <>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{translate('dashboard.balance.history.table.transactionId')}</TableCell>
                <TableCell>{translate('dashboard.balance.history.table.date')}</TableCell>
                <TableCell>{translate('dashboard.balance.history.table.operation')}</TableCell>
                <TableCell>{translate('dashboard.balance.history.table.amount')}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {history.map((transaction) => {
                const operationType = transaction[1].op['0'];
                const Row = hasCustomRenderer(operationType) ? memo(rowRenderer[operationType]) : memo(DefaultRow);

                return <Row transaction={transaction[1]} key={transaction[0]} />;
              })}

              {history.length === 0 && (
                <EmptyListCell colSpan={4}>{translate('dashboard.balance.history.emptyHistory')}</EmptyListCell>
              )}
            </TableBody>
          </Table>

          {!isFetchingCompleted && <LoadMoreButton isLoading={isLoading} onClick={() => loadNextPage()} />}
        </>
      )}
    </DashboardCard>
  );
};
