import { yupResolver } from '@hookform/resolvers/yup';
import { Button } from '@mui/material';
import { useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { createTransferSchema } from 'app/dashboard/balance/transfer/Transfer.schema';
import { TransferForm } from 'app/dashboard/balance/transfer/Transfer.types';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useExchange } from 'context/exchange/hooks/useExchange';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { transfer } from 'modules/hive/operation/transfer/transfer';
import { FormAction } from 'ui/form/action/FormAction';
import { AvailableAmountHelper } from 'ui/form/availableAmountHelper/AvailableAmountHelper';
import { HelpTextList } from 'ui/form/helpTextList/HelpTextList';
import { FormNumber } from 'ui/form/number/FormNumber';
import { FormSelect } from 'ui/form/select/FormSelect';
import { DropdownOption } from 'ui/form/select/FormSelect.types';
import { FormText } from 'ui/form/text/FormText';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { useSchemaCreator } from 'validation/useSchemaCreator/useSchemaCreator';

export const Transfer = () => {
  const translate = useTranslator();
  const { formatAsset } = useLocalization();
  const { wallet, username } = useActiveAccount();
  const { convertToComparable } = useExchange();
  const { runOperation } = useTransactionModal();

  const getTransferSchema = useSchemaCreator(createTransferSchema);
  const formSchema = getTransferSchema(wallet.liquid);

  const form = useForm<TransferForm>({
    defaultValues: {
      to: '',
      asset: ChainAssetSymbol.hive,
      amount: 0,
      memo: '',
    },
    mode: 'onChange',
    resolver: yupResolver(formSchema),
  });

  const [asset, amount] = form.watch(['asset', 'amount']);
  const availableAssets = asset === ChainAssetSymbol.hive ? wallet.liquid.hive : wallet.liquid.hbd;

  const handleTransferFormSubmit = async (data: TransferForm) => {
    await runOperation(
      transfer({
        amount: createAsset(data.amount, data.asset),
        from: username,
        to: data.to,
        memo: data.memo,
      }),
    );
    form.reset();
  };

  const assetOptions: DropdownOption[] = [
    { label: ChainAssetSymbol.hive, value: ChainAssetSymbol.hive },
    { label: ChainAssetSymbol.hbd, value: ChainAssetSymbol.hbd },
  ];

  const formattedConversions = useMemo(() => {
    const conversions = convertToComparable(createAsset(amount, asset));

    if (conversions.btc === null || conversions.usd === null) {
      return { btc: null, usd: null };
    }

    return {
      btc: formatAsset(conversions.btc),
      usd: formatAsset(conversions.usd),
    };
  }, [amount, asset, convertToComparable, formatAsset]);

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.balance.transfer.title')}</DashboardCardTitle>

      <form onSubmit={form.handleSubmit(handleTransferFormSubmit)}>
        <FormText
          name={'to'}
          label={translate('dashboard.balance.transfer.form.to')}
          placeholder={translate('dashboard.balance.transfer.form.toPlaceholder')}
          control={form.control}
          error={form.formState.errors.to}
          prefix={'@'}
        />
        <FormSelect
          name={'asset'}
          control={form.control}
          label={translate('dashboard.balance.powerUp.form.asset')}
          options={assetOptions}
          helperText={<AvailableAmountHelper asset={availableAssets} form={form} target={'amount'} />}
          error={form.formState.errors.asset}
          onChange={() => {
            if (form.getValues('amount') > 0) {
              form.trigger('amount');
            }
          }}
        />
        <FormNumber
          name={'amount'}
          control={form.control}
          label={translate('dashboard.balance.powerUp.form.amount')}
          format={{
            decimalScale: 3,
            fixedDecimalScale: true,
          }}
          error={form.formState.errors.amount}
          helperText={
            <HelpTextList>
              <span>{formattedConversions.usd}</span>
              <span>{formattedConversions.btc}</span>
            </HelpTextList>
          }
        />
        <FormText
          name={'memo'}
          label={translate('dashboard.balance.transfer.form.memo')}
          control={form.control}
          error={form.formState.errors.memo}
        />
        <FormAction>
          <Button type={'submit'} variant={'contained'} size={'small'} disabled={!form.formState.isValid}>
            {translate('dashboard.balance.transfer.form.send')}
          </Button>
        </FormAction>
      </form>
    </DashboardCard>
  );
};
