import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';

export type TransferForm = {
  to: string;
  asset: ChainAssetSymbol.hive | ChainAssetSymbol.hbd;
  amount: number;
  memo: string;
};
