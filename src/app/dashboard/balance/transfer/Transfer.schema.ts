import { number, object, ObjectSchema, string } from 'yup';
import { TransferForm } from 'app/dashboard/balance/transfer/Transfer.types';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { Balance } from 'modules/hive/wallet/wallet';
import { SchemaCreatorParams } from 'validation/useSchemaCreator/useSchemaCreator';
import { isBadActor } from 'validation/validators/isBadActor/isBadActor';
import { EnoughAssetsMessageParams, isEnoughAssets } from 'validation/validators/isEnoughAssets/isEnoughAssets';
import { isValidUsername } from 'validation/validators/isValidUsername/isValidUsername';

export const createTransferSchema = ({ translate, localization }: SchemaCreatorParams) => {
  return (balance: Balance): ObjectSchema<TransferForm> => {
    return object({
      to: string()
        .required()
        .test({
          test: isValidUsername,
          message: translate('validation.custom.invalidUsername'),
        })
        .test({
          test: isBadActor,
          message: translate('validation.custom.badActor'),
        })
        .label(translate('dashboard.balance.transfer.form.to')),
      asset: string<ChainAssetSymbol>()
        .required()
        .oneOf([ChainAssetSymbol.hive, ChainAssetSymbol.hbd])
        .label(translate('dashboard.balance.transfer.form.asset')),
      amount: number<number>()
        .required()
        .positive()
        .test({
          test: isEnoughAssets('asset', balance),
          message: (params: EnoughAssetsMessageParams) => {
            return translate('validation.custom.enoughAssets', {
              ...params,
              available: localization.formatAssetAmount(params.available),
            });
          },
        })
        .label(translate('dashboard.balance.powerUp.form.amount')),
      memo: string().max(512).label(translate('dashboard.balance.transfer.form.memo')).default(''),
    }).required();
  };
};
