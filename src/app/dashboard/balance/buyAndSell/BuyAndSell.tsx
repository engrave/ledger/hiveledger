import { Box, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { contentStyles, disclaimerStyles } from 'app/dashboard/balance/buyAndSell/BuyAndSell.styles';
import { GeneralRoute } from 'config/routes';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export const BuyAndSell = () => {
  const translate = useTranslator();
  const navigation = useNavigate();

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.balance.buyAndSell.title')}</DashboardCardTitle>

      <Box sx={contentStyles}>
        <Button onClick={() => navigation(GeneralRoute.exchange)} variant={'contained'}>
          {translate('dashboard.balance.buyAndSell.action')}
        </Button>

        <Box sx={disclaimerStyles}>{translate('dashboard.balance.buyAndSell.disclaimer')}</Box>
      </Box>
    </DashboardCard>
  );
};
