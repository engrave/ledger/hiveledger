import { Styles } from 'styles/theme.types';

export const contentStyles: Styles = {
  display: 'flex',
  gap: 2,
  alignItems: 'center',
};

export const disclaimerStyles: Styles = {
  fontSize: '0.75rem',
  color: 'typography.lightText',
};
