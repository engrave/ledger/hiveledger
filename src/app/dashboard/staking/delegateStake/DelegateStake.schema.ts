import { number, object, ObjectSchema, string } from 'yup';
import { DelegationForm } from 'app/dashboard/staking/delegateStake/DelegateStake.types';
import { HpAsset } from 'modules/hive/asset/asset';
import { getRoundedAmount } from 'modules/hive/asset/getRoundedAmount/getRoundedAmount';
import { SchemaCreatorParams } from 'validation/useSchemaCreator/useSchemaCreator';
import { isBadActor } from 'validation/validators/isBadActor/isBadActor';
import { isValidUsername } from 'validation/validators/isValidUsername/isValidUsername';

export const createStakeDelegationSchema = ({ translate }: SchemaCreatorParams) => {
  return (maxDelegation: HpAsset): ObjectSchema<DelegationForm> => {
    return object({
      delegatee: string()
        .required()
        .test({
          test: isValidUsername,
          message: translate('validation.custom.invalidUsername'),
        })
        .test({
          test: isBadActor,
          message: translate('validation.custom.badActor'),
        })
        .label(translate('dashboard.staking.delegateStake.form.delegatee')),
      amount: number<number>()
        .required()
        .positive()
        .max(getRoundedAmount(maxDelegation))
        .label(translate('dashboard.staking.delegateStake.form.amount')),
    }).required();
  };
};
