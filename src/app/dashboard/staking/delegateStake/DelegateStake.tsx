import { VestingDelegation } from '@hiveio/dhive';
import { yupResolver } from '@hookform/resolvers/yup';
import { Box, Button } from '@mui/material';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { helpTooltipStyles } from 'app/dashboard/balance/powerUp/PowerUp.styles';
import { createStakeDelegationSchema } from 'app/dashboard/staking/delegateStake/DelegateStake.schema';
import { DelegationForm } from 'app/dashboard/staking/delegateStake/DelegateStake.types';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol, ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getMaxDelegation } from 'modules/hive/hivePower/getMaxDelegation/getMaxDelegation';
import { delegateVesting } from 'modules/hive/operation/delegateVesting/delegateVesting';
import { FormAction } from 'ui/form/action/FormAction';
import { FormAlert } from 'ui/form/alert/FormAlert';
import { AvailableAmountHelper } from 'ui/form/availableAmountHelper/AvailableAmountHelper';
import { FormNumber } from 'ui/form/number/FormNumber';
import { FormText } from 'ui/form/text/FormText';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { HelpTooltip } from 'ui/popover/helpTooltip/HelpTooltip';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';
import { useSchemaCreator } from 'validation/useSchemaCreator/useSchemaCreator';

export const DelegateStake = () => {
  const translate = useTranslator();
  const { formatAsset } = useLocalization();

  const [existingDelegation, setExisingDelegation] = useState<VestingDelegation | null>(null);

  const { getVests, getHivePower } = useHivePower();
  const { username, wallet, vestingDelegations } = useActiveAccount();
  const { runOperation } = useTransactionModal();

  const formSchema = useSchemaCreator(createStakeDelegationSchema);

  const [maxVestsDelegation, maxPowerDelegation] = getMaxDelegation(wallet, getHivePower, existingDelegation);

  const form = useForm<DelegationForm>({
    defaultValues: { delegatee: '', amount: 0 },
    mode: 'onChange',
    resolver: yupResolver(formSchema(maxPowerDelegation)),
  });

  const [currentDelegatee, currentAmount] = form.watch(['delegatee', 'amount']);

  const existingDelegationShares =
    existingDelegation !== null ? createAsset(existingDelegation.vesting_shares, ChainAssetSymbol.vests) : null;

  useEffect(() => {
    const matchingDelegation = vestingDelegations.active.find((delegation) => {
      return delegation.delegatee === currentDelegatee;
    });

    setExisingDelegation(matchingDelegation ?? null);
  }, [currentDelegatee, vestingDelegations.active]);

  useEffect(() => {
    if (currentAmount > 0) {
      form.trigger('amount');
    }
  }, [existingDelegation, form, currentAmount]);

  const handleDelegationFormSubmit = async (data: DelegationForm) => {
    await runOperation(
      delegateVesting({
        vestingShares: getVests(createAsset(data.amount, ExternalAssetSymbol.hp)),
        delegator: username,
        delegatee: data.delegatee,
      }),
    );
    form.reset();
  };

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.staking.delegateStake.title')}</DashboardCardTitle>
      <HelpTooltip title={translate('dashboard.staking.delegateStake.help')} sx={helpTooltipStyles} />

      <form onSubmit={form.handleSubmit(handleDelegationFormSubmit)}>
        <Box>
          <FormText
            name={'delegatee'}
            label={translate('dashboard.staking.delegateStake.form.delegatee')}
            placeholder={translate('dashboard.staking.delegateStake.form.delegateePlaceholder')}
            control={form.control}
            error={form.formState.errors.delegatee}
            prefix={'@'}
          />
          <FormNumber
            name={'amount'}
            control={form.control}
            label={translate('dashboard.staking.delegateStake.form.amount')}
            format={{
              decimalScale: 3,
              fixedDecimalScale: true,
            }}
            helperText={
              <AvailableAmountHelper
                asset={maxPowerDelegation}
                tooltip={formatAsset(maxVestsDelegation)}
                form={form}
                target={'amount'}
              />
            }
            error={form.formState.errors.amount}
          />
        </Box>

        {existingDelegation && existingDelegationShares && (
          <FormAlert severity={'info'}>
            <Tooltip title={formatAsset(existingDelegationShares)}>
              {translate('dashboard.staking.delegateStake.delegationUpdate', {
                username: formatUsername(existingDelegation.delegatee),
                current: formatAsset(getHivePower(existingDelegationShares)),
              })}
            </Tooltip>
          </FormAlert>
        )}

        <FormAction>
          <Button type={'submit'} variant={'contained'} size={'small'} disabled={!form.formState.isValid}>
            {translate('dashboard.staking.delegateStake.form.delegate')}
          </Button>
        </FormAction>
      </form>
    </DashboardCard>
  );
};
