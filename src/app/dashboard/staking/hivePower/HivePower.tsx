import { Box } from '@mui/material';
import { equationComponentsStyles, totalStyles } from 'app/dashboard/staking/hivePower/HivePower.styles';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { Asset, AssetFootnote, AssetLabel, AssetValue } from 'ui/data/asset/Asset';
import { AssetLabelVariant, AssetValueSize } from 'ui/data/asset/Asset.types';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export const HivePower = () => {
  const translate = useTranslator();
  const { formatAsset } = useLocalization();

  const { wallet } = useActiveAccount();
  const { getHivePower } = useHivePower();

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.staking.hivePower.title')}</DashboardCardTitle>

      <Box sx={equationComponentsStyles}>
        <Asset>
          <AssetLabel variant={AssetLabelVariant.secondary}>
            {translate('dashboard.staking.hivePower.balances.staked')}
          </AssetLabel>
          <AssetValue
            asset={getHivePower(wallet.staked.staked)}
            tooltip={formatAsset(wallet.staked.staked)}
            size={AssetValueSize.mini}
          />
        </Asset>
        <Asset>
          <AssetLabel variant={AssetLabelVariant.secondary}>
            {translate('dashboard.staking.hivePower.balances.received')}
          </AssetLabel>
          <AssetValue
            asset={getHivePower(wallet.staked.received)}
            tooltip={formatAsset(wallet.staked.received)}
            size={AssetValueSize.mini}
            color={'typography.addition'}
            sign={'+'}
          />
        </Asset>
        <Asset>
          <AssetLabel variant={AssetLabelVariant.secondary}>
            {translate('dashboard.staking.hivePower.balances.delegated')}
          </AssetLabel>
          <AssetValue
            asset={getHivePower(wallet.staked.delegated)}
            tooltip={formatAsset(wallet.staked.delegated)}
            size={AssetValueSize.mini}
            color={'typography.subtraction'}
            sign={'-'}
          />
        </Asset>
      </Box>

      <Box sx={totalStyles}>
        <Asset>
          <AssetValue
            asset={getHivePower(wallet.staked.total)}
            tooltip={formatAsset(wallet.staked.total)}
            size={AssetValueSize.big}
          />
          <AssetFootnote>{translate('dashboard.staking.hivePower.balances.total')}</AssetFootnote>
        </Asset>
      </Box>
    </DashboardCard>
  );
};
