import { Styles } from 'styles/theme.types';

export const equationComponentsStyles: Styles = {
  display: 'flex',
  justifyContent: 'space-between',
  borderBottom: (theme) => `1px solid ${theme.palette.ui.separator}`,
  paddingBottom: '20px',
  marginBottom: '18px',
};

export const totalStyles: Styles = {
  display: 'flex',
  justifyContent: 'end',
};
