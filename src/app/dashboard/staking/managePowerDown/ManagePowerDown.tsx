import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';
import { useCallback } from 'react';
import { ReactComponent as TrashcanIcon } from 'assets/images/icons/trashcan.svg';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { castAsset } from 'modules/hive/asset/castAsset/castAsset';
import { createEmptyAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getRemainingPayouts } from 'modules/hive/hivePower/getRemainingPayouts/getRemainingPayouts';
import { withdrawVesting } from 'modules/hive/operation/withdrawVesting/withdrawVesting';
import { AssetCell } from 'ui/data/table/cells/assetCell/AssetCell';
import { IconButtonCell } from 'ui/data/table/cells/iconButtonCell/IconButtonCell';
import { TextCell } from 'ui/data/table/cells/textCell/TextCell';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';
import { parseChainApiDate } from 'utils/date/parseChainApiDate/parseChainApiDate';

export const ManagePowerDown = () => {
  const translate = useTranslator();
  const { wallet, details, username } = useActiveAccount();
  const { getHivePower } = useHivePower();
  const { formatAdaptableRelativeTime, formatDate, formatAsset } = useLocalization();
  const { runOperation } = useTransactionModal();

  const nextPayoutDate = parseChainApiDate(details.next_vesting_withdrawal);

  const handlePowerDownCancel = useCallback(() => {
    runOperation(
      withdrawVesting({
        account: username,
        vestingShares: createEmptyAsset(ChainAssetSymbol.vests),
      }),
    );
  }, [runOperation, username]);

  if (
    wallet.withdraws.toWithdraw.amount === 0 ||
    wallet.withdraws.toWithdraw.amount === wallet.withdraws.withdrawn.amount
  ) {
    return null;
  }

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.staking.managePowerDown.title')}</DashboardCardTitle>

      <Table>
        <TableHead>
          <TableRow>
            <TableCell>{translate('dashboard.staking.managePowerDown.table.toWithdraw')}</TableCell>
            <TableCell>{translate('dashboard.staking.managePowerDown.table.withdrawn')}</TableCell>
            <TableCell>{translate('dashboard.staking.managePowerDown.table.nextPayout')}</TableCell>
            <TableCell>{translate('dashboard.staking.managePowerDown.table.amount')}</TableCell>
            <TableCell>{translate('dashboard.staking.managePowerDown.table.remaining')}</TableCell>
            <TableCell>&nbsp;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <AssetCell
              asset={getHivePower(wallet.withdraws.toWithdraw)}
              tooltip={formatAsset(wallet.withdraws.toWithdraw)}
            />
            <AssetCell
              asset={getHivePower(wallet.withdraws.withdrawn)}
              tooltip={formatAsset(wallet.withdraws.withdrawn)}
            />
            <TextCell>
              <Tooltip title={formatDate(nextPayoutDate, { dateStyle: 'long', timeStyle: 'medium' })}>
                {formatAdaptableRelativeTime(parseChainApiDate(details.next_vesting_withdrawal))}
              </Tooltip>
            </TextCell>
            <AssetCell asset={castAsset(getHivePower(wallet.withdraws.vestingWithdrawRate), ChainAssetSymbol.hive)} />
            <TextCell>{getRemainingPayouts(wallet.withdraws)}</TextCell>
            <IconButtonCell
              onClick={() => handlePowerDownCancel()}
              tooltip={translate('dashboard.staking.managePowerDown.cancelTooltip')}
            >
              <TrashcanIcon />
            </IconButtonCell>
          </TableRow>
        </TableBody>
      </Table>
    </DashboardCard>
  );
};
