import { Styles } from 'styles/theme.types';

export const rewardsWrapper: Styles = {
  display: 'flex',
  justifyContent: 'space-between',
};

export const actionAreaStyles: Styles = {
  marginTop: '33px',
  display: 'flex',
  justifyContent: 'flex-end',
};
