import { Box, Button } from '@mui/material';
import { actionAreaStyles, rewardsWrapper } from 'app/dashboard/staking/stakingRewards/StakingRewards.styles';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useExchange } from 'context/exchange/hooks/useExchange';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { castAsset } from 'modules/hive/asset/castAsset/castAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { claimRewardBalance } from 'modules/hive/operation/claimRewardBalance/claimRewardBalance';
import { Asset, AssetConversions, AssetValue } from 'ui/data/asset/Asset';
import { AssetConversionSize } from 'ui/data/asset/Asset.types';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export const StakingRewards = () => {
  const translate = useTranslator();
  const { formatAsset } = useLocalization();

  const { wallet, username } = useActiveAccount();
  const { convertToComparable } = useExchange();
  const { getHivePower } = useHivePower();
  const { runOperation } = useTransactionModal();

  const hiveReward = wallet.stakingRewards.hive;
  const hiveRewardConversions = convertToComparable(hiveReward);

  const hbdReward = wallet.stakingRewards.hbd;
  const hbdRewardConversions = convertToComparable(hbdReward);

  const vestsReward = wallet.stakingRewards.vests;
  const hpReward = getHivePower(vestsReward);
  const hpRewardConversions = convertToComparable(castAsset(hpReward, ChainAssetSymbol.hive));

  const handleRewardsClaim = () => {
    runOperation(
      claimRewardBalance({
        account: username,
        hiveReward,
        hbdReward,
        vestsReward,
      }),
    );
  };

  const hasNoReward = [hiveReward, hbdReward, vestsReward].every((asset) => {
    return asset.amount === 0;
  });

  if (hasNoReward) {
    return null;
  }

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.staking.stakingRewards.title')}</DashboardCardTitle>
      <Box sx={rewardsWrapper}>
        <Asset>
          <AssetValue asset={hiveReward} />
          <AssetConversions
            conversions={[hiveRewardConversions.usd, hiveRewardConversions.btc]}
            size={AssetConversionSize.small}
          />
        </Asset>
        <Asset>
          <AssetValue asset={hbdReward} />
          <AssetConversions
            conversions={[hbdRewardConversions.usd, hbdRewardConversions.btc]}
            size={AssetConversionSize.small}
          />
        </Asset>
        <Asset>
          <AssetValue asset={hpReward} tooltip={formatAsset(vestsReward)} />
          <AssetConversions
            conversions={[hpRewardConversions.usd, hpRewardConversions.btc]}
            size={AssetConversionSize.small}
          />
        </Asset>
      </Box>
      <Box sx={actionAreaStyles}>
        <Button variant={'contained'} onClick={() => handleRewardsClaim()}>
          {translate('dashboard.staking.stakingRewards.claim')}
        </Button>
      </Box>
    </DashboardCard>
  );
};
