import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { Banner } from 'ui/message/banner/Banner';

export const StakingBanner = () => {
  const translate = useTranslator();
  const { getHivePowerApr } = useHivePower();
  const { formatNumber } = useLocalization();

  const apr = formatNumber(getHivePowerApr(), { maximumFractionDigits: 2 });

  return <Banner>{translate('dashboard.staking.banner', { apr })}</Banner>;
};
