import { VestingDelegation } from '@hiveio/dhive';
import { TableRow } from '@mui/material';
import { ReactComponent as TrashcanIcon } from 'assets/images/icons/trashcan.svg';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { AssetCell } from 'ui/data/table/cells/assetCell/AssetCell';
import { IconButtonCell } from 'ui/data/table/cells/iconButtonCell/IconButtonCell';
import { TextCell } from 'ui/data/table/cells/textCell/TextCell';

export type ActiveDelegationRow = {
  delegation: VestingDelegation;
  onCancel: () => void;
};

export const ActiveDelegationRow = ({ delegation, onCancel }: ActiveDelegationRow) => {
  const translate = useTranslator();
  const { getHivePower } = useHivePower();
  const { formatAsset } = useLocalization();

  const delegatedShares = createAsset(delegation.vesting_shares, ChainAssetSymbol.vests);

  return (
    <TableRow>
      <TextCell>{formatUsername(delegation.delegatee)}</TextCell>
      <AssetCell asset={getHivePower(delegatedShares)} tooltip={formatAsset(delegatedShares)} />
      <IconButtonCell
        onClick={() => onCancel()}
        tooltip={translate('dashboard.staking.activeDelegations.cancelButton')}
      >
        <TrashcanIcon />
      </IconButtonCell>
    </TableRow>
  );
};
