import { VestingDelegation } from '@hiveio/dhive';
import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';
import { ActiveDelegationRow } from 'app/dashboard/staking/activeDelegations/activeDelegationRow/ActiveDelegationRow';
import { titleStyles } from 'app/dashboard/staking/activeDelegations/ActiveDelegations.styles';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { createEmptyAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { delegateVesting } from 'modules/hive/operation/delegateVesting/delegateVesting';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export const ActiveDelegations = () => {
  const translate = useTranslator();
  const { vestingDelegations } = useActiveAccount();
  const { runOperation } = useTransactionModal();

  const handleDelegationCancel = (delegation: VestingDelegation) => {
    runOperation(
      delegateVesting({
        vestingShares: createEmptyAsset(ChainAssetSymbol.vests),
        delegator: delegation.delegator,
        delegatee: delegation.delegatee,
      }),
    );
  };

  if (vestingDelegations.active.length === 0) {
    return null;
  }

  return (
    <DashboardCard>
      <DashboardCardTitle sx={titleStyles}>{translate('dashboard.staking.activeDelegations.title')}</DashboardCardTitle>

      <Table>
        <TableHead>
          <TableRow>
            <TableCell>{translate('dashboard.staking.activeDelegations.table.to')}</TableCell>
            <TableCell>{translate('dashboard.staking.activeDelegations.table.amount')}</TableCell>
            <TableCell>&nbsp;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {vestingDelegations.active.map((delegation, index) => (
            <ActiveDelegationRow
              delegation={delegation}
              onCancel={() => handleDelegationCancel(delegation)}
              key={index}
            />
          ))}
        </TableBody>
      </Table>
    </DashboardCard>
  );
};
