import { TableRow, Tooltip } from '@mui/material';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { ExpiringVestingDelegation } from 'modules/hive/account/account';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { AssetCell } from 'ui/data/table/cells/assetCell/AssetCell';
import { TextCell } from 'ui/data/table/cells/textCell/TextCell';
import { parseChainApiDate } from 'utils/date/parseChainApiDate/parseChainApiDate';

export type ExpiringDelegationRowProps = {
  delegation: ExpiringVestingDelegation;
};

export const ExpiringDelegationRow = ({ delegation }: ExpiringDelegationRowProps) => {
  const { formatAdaptableRelativeTime, formatDate, formatAsset } = useLocalization();
  const { getHivePower } = useHivePower();

  const expirationDate = parseChainApiDate(delegation.expiration);
  const delegatedVests = createAsset(delegation.vesting_shares, ChainAssetSymbol.vests);

  return (
    <TableRow>
      <AssetCell asset={getHivePower(delegatedVests)} tooltip={formatAsset(delegatedVests)} />
      <TextCell>
        <Tooltip
          title={formatDate(expirationDate, {
            dateStyle: 'long',
            timeStyle: 'medium',
          })}
        >
          <span>{formatAdaptableRelativeTime(expirationDate)}</span>
        </Tooltip>
      </TextCell>
    </TableRow>
  );
};
