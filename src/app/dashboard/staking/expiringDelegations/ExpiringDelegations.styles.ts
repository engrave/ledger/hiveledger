import { Styles } from 'styles/theme.types';

export const titleStyles: Styles = {
  marginBottom: '10px',
};

export const lastHeadCellStyles: Styles = {
  '&:last-of-type': {
    textAlign: 'left',
  },
};
