import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';
import { ExpiringDelegationRow } from 'app/dashboard/staking/expiringDelegations/expiringDelegationRow/ExpiringDelegationRow';
import { lastHeadCellStyles, titleStyles } from 'app/dashboard/staking/expiringDelegations/ExpiringDelegations.styles';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export const ExpiringDelegations = () => {
  const translate = useTranslator();
  const { vestingDelegations } = useActiveAccount();

  if (vestingDelegations.expiring.length === 0) {
    return null;
  }

  return (
    <DashboardCard>
      <DashboardCardTitle sx={titleStyles}>
        {translate('dashboard.staking.expiringDelegations.title')}
      </DashboardCardTitle>

      <Table>
        <TableHead>
          <TableRow>
            <TableCell>{translate('dashboard.staking.expiringDelegations.table.amount')}</TableCell>
            <TableCell sx={lastHeadCellStyles}>
              {translate('dashboard.staking.expiringDelegations.table.expiring')}
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {vestingDelegations.expiring.map((delegation, index) => (
            <ExpiringDelegationRow delegation={delegation} key={index} />
          ))}
        </TableBody>
      </Table>
    </DashboardCard>
  );
};
