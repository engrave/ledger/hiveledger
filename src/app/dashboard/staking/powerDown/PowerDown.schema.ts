import { number, object, ObjectSchema } from 'yup';
import { PowerDownForm } from 'app/dashboard/staking/powerDown/PowerDown.types';
import { HpAsset } from 'modules/hive/asset/asset';
import { getRoundedAmount } from 'modules/hive/asset/getRoundedAmount/getRoundedAmount';
import { SchemaCreatorParams } from 'validation/useSchemaCreator/useSchemaCreator';

export const createPowerDownSchema = ({ translate }: SchemaCreatorParams) => {
  return (maxPowerDown: HpAsset): ObjectSchema<PowerDownForm> => {
    return object({
      amount: number<number>()
        .required()
        .positive()
        .max(getRoundedAmount(maxPowerDown))
        .label(translate('dashboard.staking.powerDown.form.amount')),
    }).required();
  };
};
