import { yupResolver } from '@hookform/resolvers/yup/dist/yup';
import { Box, Button } from '@mui/material';
import { useForm } from 'react-hook-form';
import { createPowerDownSchema } from 'app/dashboard/staking/powerDown/PowerDown.schema';
import { PowerDownForm } from 'app/dashboard/staking/powerDown/PowerDown.types';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { createAsset } from 'modules/hive/asset/createAsset/createAsset';
import { getLowestAsset } from 'modules/hive/asset/getLowestAsset/getLowestAsset';
import { ExternalAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { getMaxPowerDown } from 'modules/hive/hivePower/getMaxPowerDown/getMaxPowerDown';
import { withdrawVesting } from 'modules/hive/operation/withdrawVesting/withdrawVesting';
import { FormAction } from 'ui/form/action/FormAction';
import { AvailableAmountHelper } from 'ui/form/availableAmountHelper/AvailableAmountHelper';
import { FormNumber } from 'ui/form/number/FormNumber';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { useSchemaCreator } from 'validation/useSchemaCreator/useSchemaCreator';

export const PowerDown = () => {
  const translate = useTranslator();
  const { formatAsset } = useLocalization();

  const { getVests, getHivePower } = useHivePower();
  const { username, wallet } = useActiveAccount();
  const { runOperation } = useTransactionModal();

  const formSchema = useSchemaCreator(createPowerDownSchema);

  const [maxVestsWithdraw, maxHivePowerWithdraw] = getMaxPowerDown(wallet, getHivePower);

  const form = useForm<PowerDownForm>({
    defaultValues: { amount: 0 },
    mode: 'onChange',
    resolver: yupResolver(formSchema(maxHivePowerWithdraw)),
  });

  const handlePowerDownFormSubmit = async (data: PowerDownForm) => {
    const givenHivePower = createAsset(data.amount, ExternalAssetSymbol.hp);

    await runOperation(
      withdrawVesting({
        account: username,
        vestingShares: getLowestAsset(getVests(givenHivePower), maxVestsWithdraw),
      }),
    );
    form.reset();
  };

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.staking.powerDown.title')}</DashboardCardTitle>

      <form onSubmit={form.handleSubmit(handlePowerDownFormSubmit)}>
        <Box>
          <FormNumber
            name={'amount'}
            control={form.control}
            label={translate('dashboard.staking.powerDown.form.amount')}
            format={{
              decimalScale: 3,
              fixedDecimalScale: true,
            }}
            helperText={
              <AvailableAmountHelper
                asset={maxHivePowerWithdraw}
                tooltip={formatAsset(maxVestsWithdraw)}
                form={form}
                target={'amount'}
              />
            }
            error={form.formState.errors.amount}
          />
        </Box>

        <FormAction>
          <Button type={'submit'} variant={'contained'} size={'small'} disabled={!form.formState.isValid}>
            {translate('dashboard.staking.powerDown.form.powerDown')}
          </Button>
        </FormAction>
      </form>
    </DashboardCard>
  );
};
