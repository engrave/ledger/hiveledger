import { useTranslator } from 'context/localization/hooks/useTranslator';
import { HeadsUp } from 'ui/message/headsUp/HeadsUp';

export const DelegationInfo = () => {
  const translate = useTranslator();

  return <HeadsUp>{translate('dashboard.staking.delegationInfo')}</HeadsUp>;
};
