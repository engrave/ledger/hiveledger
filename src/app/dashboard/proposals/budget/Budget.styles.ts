import { Styles } from 'styles/theme.types';

export const dividerStyles: Styles = {
  borderBottomColor: 'ui.separator',
  marginTop: 1.5,
  marginBottom: 2,
};
