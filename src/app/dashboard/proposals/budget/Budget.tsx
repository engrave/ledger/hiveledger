import { Divider } from '@mui/material';
import { dividerStyles } from 'app/dashboard/proposals/budget/Budget.styles';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useProposals } from 'context/proposals/hooks/useProposals/useProposals';
import { Asset, AssetLabel, AssetValue } from 'ui/data/asset/Asset';
import { AssetLabelVariant, AssetValueSize } from 'ui/data/asset/Asset.types';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { SectionLoader } from 'ui/progress/sectionLoader/SectionLoader';

export const Budget = () => {
  const translate = useTranslator();

  const { budget } = useProposals();

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.proposals.budget.title')}</DashboardCardTitle>

      {budget === null && <SectionLoader height={161} />}

      {budget !== null && (
        <>
          <Asset>
            <AssetLabel variant={AssetLabelVariant.header}>
              {translate('dashboard.proposals.budget.totalBudget')}
            </AssetLabel>
            <AssetValue asset={budget.total} size={AssetValueSize.big} />
          </Asset>

          <Divider sx={dividerStyles} />

          <Asset>
            <AssetLabel variant={AssetLabelVariant.header}>
              {translate('dashboard.proposals.budget.dailyBudget')}
            </AssetLabel>
            <AssetValue asset={budget.daily} size={AssetValueSize.big} />
          </Asset>
        </>
      )}
    </DashboardCard>
  );
};
