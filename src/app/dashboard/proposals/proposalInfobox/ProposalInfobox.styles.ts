import { Styles } from 'styles/theme.types';

export const miniValueStyles: Styles = {
  fontSize: '0.75rem',
};
