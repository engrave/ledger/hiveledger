import { miniValueStyles } from 'app/dashboard/proposals/proposalInfobox/ProposalInfobox.styles';
import { ReactComponent as CalendarIcon } from 'assets/images/icons/calendar.svg';
import { ReactComponent as CoinsIcon } from 'assets/images/icons/coins.svg';
import { ReactComponent as WalletIcon } from 'assets/images/icons/wallet.svg';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { parseProposalDetails } from 'modules/hive/proposals/parseProposalDetails/parseProposalDetails';
import { ProposalDetails } from 'modules/hive/proposals/proposals';
import { InfoboxGrid, InfoboxParameter, InfoboxParameterLabel, InfoboxParameterValue } from 'ui/data/infobox/Infobox';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export type ProposalInfoboxProps = {
  proposal: ProposalDetails;
};

export const ProposalInfobox = ({ proposal }: ProposalInfoboxProps) => {
  const translate = useTranslator();
  const { formatAsset, formatDate } = useLocalization();

  const { dailyPay, totalPay, startDate, endDate } = parseProposalDetails(proposal);

  return (
    <DashboardCard>
      <DashboardCardTitle>
        {translate('dashboard.proposals.infobox.title', { proposalId: proposal.proposal_id })}
      </DashboardCardTitle>

      <InfoboxGrid>
        <InfoboxParameter>
          <InfoboxParameterLabel>{translate('dashboard.proposals.infobox.creator')}</InfoboxParameterLabel>
          <InfoboxParameterValue>{formatUsername(proposal.creator)}</InfoboxParameterValue>
        </InfoboxParameter>

        <InfoboxParameter>
          <InfoboxParameterLabel>{translate('dashboard.proposals.infobox.receiver')}</InfoboxParameterLabel>
          <InfoboxParameterValue>{formatUsername(proposal.receiver)}</InfoboxParameterValue>
        </InfoboxParameter>

        <InfoboxParameter>
          <InfoboxParameterLabel>{translate('dashboard.proposals.infobox.dailyPayout')}</InfoboxParameterLabel>
          <InfoboxParameterValue>
            <CoinsIcon />
            {formatAsset(dailyPay, { minimumFractionDigits: 0, notation: 'compact' })}
          </InfoboxParameterValue>
        </InfoboxParameter>

        <InfoboxParameter>
          <InfoboxParameterLabel>{translate('dashboard.proposals.infobox.totalPayout')}</InfoboxParameterLabel>
          <InfoboxParameterValue>
            <WalletIcon />
            {formatAsset(totalPay, { minimumFractionDigits: 0, notation: 'compact' })}
          </InfoboxParameterValue>
        </InfoboxParameter>

        <InfoboxParameter>
          <InfoboxParameterLabel>{translate('dashboard.proposals.infobox.startDate')}</InfoboxParameterLabel>
          <InfoboxParameterValue sx={miniValueStyles}>
            <CalendarIcon />
            {formatDate(startDate, { dateStyle: 'short', timeStyle: 'medium' })}
          </InfoboxParameterValue>
        </InfoboxParameter>

        <InfoboxParameter>
          <InfoboxParameterLabel>{translate('dashboard.proposals.infobox.endDate')}</InfoboxParameterLabel>
          <InfoboxParameterValue sx={miniValueStyles}>
            <CalendarIcon />
            {formatDate(endDate, { dateStyle: 'short', timeStyle: 'medium' })}
          </InfoboxParameterValue>
        </InfoboxParameter>
      </InfoboxGrid>
    </DashboardCard>
  );
};
