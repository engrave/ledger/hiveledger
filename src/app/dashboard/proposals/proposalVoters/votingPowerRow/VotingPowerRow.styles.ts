import { Styles } from 'styles/theme.types';

export const votingPowerRowStyles: Styles = {
  display: 'flex',
  fontSize: '0.875rem',
  gap: '0.5rem',
  marginBottom: '0.4rem',
};

export const usernameStyles: Styles = {
  fontWeight: 500,
  width: '40%',
  flexShrink: 0,
};

export const powerStyles: Styles = {
  color: 'typography.lightText',
};
