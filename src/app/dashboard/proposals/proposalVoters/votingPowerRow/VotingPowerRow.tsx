import { Box } from '@mui/material';
import { memo } from 'react';
import { FormatNumberOptions } from 'react-intl';
import {
  powerStyles,
  usernameStyles,
  votingPowerRowStyles,
} from 'app/dashboard/proposals/proposalVoters/votingPowerRow/VotingPowerRow.styles';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { VotePower } from 'modules/hive/hivePower/hivePower';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';

export type VotingPowerRowProps = {
  votingPower: VotePower;
};

export const VotingPowerRow = memo(({ votingPower }: VotingPowerRowProps) => {
  const translate = useTranslator();
  const { formatAsset } = useLocalization();
  const { getHivePower } = useHivePower();

  const formatConfig: FormatNumberOptions = { notation: 'compact', maximumSignificantDigits: 2 };

  const formattedShares = formatAsset(votingPower.vestingShares, formatConfig);
  const formattedProxiedShares = formatAsset(votingPower.proxiedVestingShares, formatConfig);

  const directVotingPower = getHivePower(votingPower.vestingShares);
  const proxiedVotingPower = getHivePower(votingPower.proxiedVestingShares);

  const formattedVotingPower = formatAsset(directVotingPower, formatConfig);
  const formattedProxiedVotingPower = formatAsset(proxiedVotingPower, formatConfig);

  return (
    <Box sx={votingPowerRowStyles}>
      <Box sx={usernameStyles}>{formatUsername(votingPower.username)}</Box>

      {votingPower.proxiedVestingShares.amount > 0 && (
        <Tooltip
          title={translate('dashboard.proposals.voters.powerWithProxies', {
            votingPower: formattedShares,
            proxiedVotingPower: formattedProxiedShares,
          })}
        >
          <Box sx={powerStyles}>
            {translate('dashboard.proposals.voters.powerWithProxies', {
              votingPower: formattedVotingPower,
              proxiedVotingPower: formattedProxiedVotingPower,
            })}
          </Box>
        </Tooltip>
      )}

      {votingPower.proxiedVestingShares.amount === 0 && (
        <Tooltip title={translate('dashboard.proposals.voters.power', { votingPower: formattedShares })}>
          <Box sx={powerStyles}>
            {translate('dashboard.proposals.voters.power', { votingPower: formattedVotingPower })}
          </Box>
        </Tooltip>
      )}
    </Box>
  );
});
