import { Box, Button, CircularProgress, Dialog, DialogContent, DialogTitle } from '@mui/material';
import { useMemo, useState } from 'react';
import { Waypoint } from 'react-waypoint';
import {
  buttonWrapperStyles,
  proposalVotersCardStyles,
  votersGridStyles,
  votesLoadingAnimationStyles,
} from 'app/dashboard/proposals/proposalVoters/ProposalVoters.styles';
import { VotingPowerRow } from 'app/dashboard/proposals/proposalVoters/votingPowerRow/VotingPowerRow';
import { proposalsConfig } from 'config/config';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useVotesPower } from 'context/proposals/hooks/useVotesPower/useVotesPower';
import { compareAssets } from 'modules/hive/asset/compareAssets/compareAssets';
import { ProposalDetails } from 'modules/hive/proposals/proposals';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { DialogCloseButton } from 'ui/modal/dialogCloseButton/DialogCloseButton';
import { SectionLoader } from 'ui/progress/sectionLoader/SectionLoader';

export type ProposalVotersProps = {
  proposal: ProposalDetails;
};

export const ProposalVoters = (props: ProposalVotersProps) => {
  const translate = useTranslator();

  const { votesPower, isVotesPowerLoading, isVotesPowerCompleted, fetchMoreVotesPower } = useVotesPower(props.proposal);

  const sortedVotesPower = useMemo(() => {
    return [...votesPower].sort((votingPowerA, votingPowerB) => {
      return compareAssets(votingPowerA.totalVestingShares, votingPowerB.totalVestingShares, true);
    });
  }, [votesPower]);

  const [isModalOpen, setModalOpen] = useState(false);

  return (
    <DashboardCard sx={proposalVotersCardStyles}>
      <DashboardCardTitle>{translate('dashboard.proposals.voters.title')}</DashboardCardTitle>

      {sortedVotesPower.slice(0, proposalsConfig.votersInPreview).map((votePower) => {
        return <VotingPowerRow votingPower={votePower} key={votePower.username} />;
      })}

      {isVotesPowerLoading && sortedVotesPower.length === 0 && <SectionLoader height={147} />}

      {sortedVotesPower.length > proposalsConfig.votersInPreview && (
        <Box sx={buttonWrapperStyles}>
          <Button variant={'microText'} onClick={() => setModalOpen(true)}>
            {translate('dashboard.proposals.voters.showMore')}
          </Button>
        </Box>
      )}

      <Dialog open={isModalOpen} onClose={() => setModalOpen(false)} maxWidth={'xl'}>
        <DialogTitle>{translate('dashboard.proposals.voters.modalTitle')}</DialogTitle>
        <DialogCloseButton onClick={() => setModalOpen(false)} />

        <DialogContent>
          <Box sx={votersGridStyles}>
            {sortedVotesPower.map((votePower) => {
              return <VotingPowerRow votingPower={votePower} key={votePower.username} />;
            })}
          </Box>

          {!isVotesPowerCompleted && (
            <Box sx={votesLoadingAnimationStyles}>
              <Waypoint onEnter={() => fetchMoreVotesPower()} fireOnRapidScroll={true} />
              <CircularProgress />
            </Box>
          )}
        </DialogContent>
      </Dialog>
    </DashboardCard>
  );
};
