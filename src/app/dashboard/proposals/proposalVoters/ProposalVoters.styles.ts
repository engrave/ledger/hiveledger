import { Styles } from 'styles/theme.types';

export const proposalVotersCardStyles: Styles = {
  paddingBottom: '1rem',
};

export const buttonWrapperStyles: Styles = {
  textAlign: 'center',
  marginTop: '1rem',
};

export const votersGridStyles: Styles = {
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
  gap: '0 48px',
  width: '765px',
};

export const votesLoadingAnimationStyles: Styles = {
  marginTop: 4,
  textAlign: 'center',
};
