import { witnessesConfig } from 'config/config';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useHtmlTranslator } from 'context/localization/hooks/useHtmlTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { HeadsUp, HeadsUpPosition } from 'ui/message/headsUp/HeadsUp';

export const ProxyBanner = () => {
  const translate = useHtmlTranslator();
  const { details } = useActiveAccount();

  const isProxySet = details.proxy !== '';

  if (isProxySet) {
    return null;
  }

  return (
    <HeadsUp position={HeadsUpPosition.top}>
      {translate('dashboard.proposals.proxyExplanation.proxyPropose', {
        account: formatUsername(witnessesConfig.proposedProxyAccount),
      })}
    </HeadsUp>
  );
};
