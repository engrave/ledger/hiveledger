import { Box } from '@mui/material';
import { useCallback, useEffect } from 'react';
import { RankingCard } from 'app/dashboard/proposals/ranking/rankingCard/RankingCard';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useProposals } from 'context/proposals/hooks/useProposals/useProposals';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { updateProposalVotes } from 'modules/hive/operation/updateProposalVotes/updateProposalVotes';
import { Proposal, ProposalFundingStatus } from 'modules/hive/proposals/proposals';
import { LoadMoreButton } from 'ui/button/loadMoreButton/LoadMoreButton';
import { SectionLoader } from 'ui/progress/sectionLoader/SectionLoader';
import { headingStyles } from './Ranking.styles';

export const Ranking = () => {
  const translate = useTranslator();
  const { runOperation } = useTransactionModal();
  const { username, details } = useActiveAccount();

  const isVotingDisabled = details.proxy !== '';

  const {
    ranking,
    fetchPage,
    page,
    isRankingLoading,
    isFirstLoading,
    isRankingCompleted,
    votes,
    setVotesLoadingEnabled,
  } = useProposals();

  const handleVoteChange = useCallback(
    async (proposal: Proposal, vote: boolean) => {
      const operation = updateProposalVotes({
        voter: username,
        proposalId: proposal.details.proposal_id,
        approve: vote,
      });

      await runOperation(operation);
    },
    [runOperation, username],
  );

  useEffect(() => {
    setVotesLoadingEnabled(true);

    if (page === 0) {
      fetchPage();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (isFirstLoading) {
    return <SectionLoader />;
  }

  const fundingProposals = ranking.filter((item) => {
    return [ProposalFundingStatus.funded, ProposalFundingStatus.futureFunded].includes(item.fundStatus);
  });

  const notFundingProposals = ranking.filter((item) => {
    return item.fundStatus === ProposalFundingStatus.notFunded;
  });

  return (
    <>
      {fundingProposals.map((item) => {
        return (
          <RankingCard
            proposal={item}
            isVoted={votes[item.details.proposal_id] ?? null}
            onVoteChange={handleVoteChange}
            key={item.details.proposal_id}
            isDisabled={isVotingDisabled}
          />
        );
      })}

      {notFundingProposals.length > 0 && (
        <Box sx={headingStyles}>{translate('dashboard.proposals.ranking.insufficientVotes')}</Box>
      )}

      {notFundingProposals.map((item) => {
        return (
          <RankingCard
            proposal={item}
            isVoted={votes[item.details.proposal_id] ?? null}
            onVoteChange={handleVoteChange}
            key={item.details.proposal_id}
            isDisabled={isVotingDisabled}
          />
        );
      })}

      {!isRankingCompleted && <LoadMoreButton isLoading={isRankingLoading} onClick={() => fetchPage()} />}
    </>
  );
};
