import { Box, Link, Skeleton } from '@mui/material';
import { memo } from 'react';
import { generatePath, Link as RouterLink } from 'react-router-dom';
import { ProposalCreator } from 'app/dashboard/proposals/ranking/rankingCard/proposalCreator/ProposalCreator';
import { ProposalInfo } from 'app/dashboard/proposals/ranking/rankingCard/proposalInfo/ProposalInfo';
import { ProposalState } from 'app/dashboard/proposals/ranking/rankingCard/proposalState/ProposalState';
import {
  proposalIdStyles,
  rankingCardStyles,
  titleStyles,
  votePlaceholderStyles,
  voteStyles,
} from 'app/dashboard/proposals/ranking/rankingCard/RankingCard.styles';
import { ProposalsRoute } from 'config/routes';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { Proposal } from 'modules/hive/proposals/proposals';
import { BaseCheckbox } from 'ui/form/checkbox/BaseCheckbox';
import { DashboardCard } from 'ui/layout/dashboardCard/DashboardCard';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';

export type ProposalCardProps = {
  proposal: Proposal;
  isVoted: boolean | null;
  onVoteChange: (proposal: Proposal, vote: boolean) => void;
  isDisabled: boolean;
};

export const RankingCard = memo((props: ProposalCardProps) => {
  const { proposal, isVoted, onVoteChange, isDisabled } = props;

  const translate = useTranslator();

  const proposalPath = generatePath(ProposalsRoute.details, {
    id: proposal.details.proposal_id.toString(),
  });

  return (
    <DashboardCard sx={rankingCardStyles}>
      <ProposalState proposalDetails={proposal.details} fundStatus={proposal.fundStatus} />
      <Link component={RouterLink} to={proposalPath} sx={proposalIdStyles}>
        {translate('dashboard.proposals.ranking.card.proposalId', { id: proposal.details.proposal_id })}
      </Link>
      <Link component={RouterLink} to={proposalPath} sx={titleStyles}>
        {proposal.details.subject}
      </Link>
      <ProposalInfo proposalDetails={proposal.details} />
      <ProposalCreator proposalDetails={proposal.details} />

      <Box sx={voteStyles}>
        {isVoted === null && (
          <Skeleton width={24} height={24} variant={'rectangular'} animation={'pulse'} sx={votePlaceholderStyles} />
        )}

        {isVoted !== null && (
          <Tooltip title={isDisabled ? translate('dashboard.proposals.ranking.card.proxyEnabled') : ''} wrap={'span'}>
            <BaseCheckbox value={isVoted} disabled={isDisabled} onClick={() => onVoteChange(proposal, !isVoted)} />
          </Tooltip>
        )}
      </Box>
    </DashboardCard>
  );
});
