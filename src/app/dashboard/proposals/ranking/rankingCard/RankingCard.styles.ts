import { Styles } from 'styles/theme.types';

export const rankingCardStyles: Styles = {
  display: 'grid',
  gridTemplateAreas: `
    "state    id"
    "title    title"
    "details  details"
    "creator  vote"
  `,
  gridTemplateColumns: '1fr 50px',
  gridTemplateRows: 'auto auto auto 42px',
  marginBottom: '10px',
};

export const proposalIdStyles: Styles = {
  gridArea: 'id',
  fontSize: '0.75rem',
  fontWeight: 600,
  color: 'typography.header',
  textAlign: 'right',
};

export const titleStyles: Styles = {
  gridArea: 'title',
  fontSize: '1.5rem',
  fontWeight: 'bold',
  color: 'typography.header',
  lineHeight: '1.5rem',
  marginBottom: 2,
};

export const voteStyles: Styles = {
  gridArea: 'vote',
  display: 'flex',
  justifyContent: 'flex-end',

  '& > *': {
    position: 'relative',
    right: '-9px',
    bottom: '-9px',
  },
};

export const votePlaceholderStyles: Styles = {
  margin: '9px',
  borderRadius: '3px',
};
