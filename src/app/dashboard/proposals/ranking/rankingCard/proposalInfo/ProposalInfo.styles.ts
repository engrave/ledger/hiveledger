import { Styles } from 'styles/theme.types';

export const detailsStyles: Styles = {
  gridArea: 'details',
  display: 'grid',
  gridTemplateColumns: '30% 30% 30%',
};

export const detailsItemStyles: Styles = {
  display: 'flex',
  alignItems: 'center',
  gap: 1,
  fontSize: '0.875rem',
  color: 'typography.lightText',
};
