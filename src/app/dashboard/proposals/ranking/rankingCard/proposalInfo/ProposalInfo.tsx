import { Box } from '@mui/material';
import { memo } from 'react';
import {
  detailsItemStyles,
  detailsStyles,
} from 'app/dashboard/proposals/ranking/rankingCard/proposalInfo/ProposalInfo.styles';
import { ReactComponent as CalendarIcon } from 'assets/images/icons/calendar.svg';
import { ReactComponent as CoinsIcon } from 'assets/images/icons/coins.svg';
import { ReactComponent as WalletIcon } from 'assets/images/icons/wallet.svg';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { parseProposalDetails } from 'modules/hive/proposals/parseProposalDetails/parseProposalDetails';
import { ProposalDetails, ProposalStatus } from 'modules/hive/proposals/proposals';

export type ProposalDetailsProps = {
  proposalDetails: ProposalDetails;
};

export const ProposalInfo = memo(({ proposalDetails }: ProposalDetailsProps) => {
  const translate = useTranslator();
  const { formatAsset, formatAdaptableRelativeTime, formatNumber } = useLocalization();

  const { startDate, endDate, durationInDays, dailyPay, totalPay } = parseProposalDetails(proposalDetails);

  return (
    <Box sx={detailsStyles}>
      <Box sx={detailsItemStyles}>
        <CalendarIcon />
        {proposalDetails.status === ProposalStatus.active && (
          <>
            {translate('dashboard.proposals.ranking.card.durationWithTimeLeft', {
              duration: formatNumber(durationInDays, { style: 'unit', unit: 'day' }),
              timeLeft: formatAdaptableRelativeTime(endDate, new Date(), { addSuffix: false }),
            })}
          </>
        )}
        {proposalDetails.status === ProposalStatus.inactive && (
          <>
            {translate('dashboard.proposals.ranking.card.durationAndWhen', {
              duration: formatNumber(durationInDays, { style: 'unit', unit: 'day' }),
              when: formatAdaptableRelativeTime(startDate, new Date(), { addSuffix: false }),
            })}
          </>
        )}
      </Box>

      <Box sx={detailsItemStyles}>
        <CoinsIcon />
        {translate('dashboard.proposals.ranking.card.hbdDaily', {
          amount: formatAsset(dailyPay, { minimumFractionDigits: 0, notation: 'compact' }),
        })}
      </Box>

      <Box sx={detailsItemStyles}>
        <WalletIcon />
        {translate('dashboard.proposals.ranking.card.hbdTotal', {
          amount: formatAsset(totalPay, { minimumFractionDigits: 0, notation: 'compact' }),
        })}
      </Box>
    </Box>
  );
});
