import { Styles } from 'styles/theme.types';

export const creatorStyles: Styles = {
  gridArea: 'creator',
  display: 'flex',
  alignItems: 'flex-end',
  fontSize: '0.875rem',
  fontWeight: 300,
  color: 'typography.lightText',
  whiteSpace: 'pre',

  '& > strong': {
    fontWeight: 600,
  },
};
