import { Box } from '@mui/material';
import { memo } from 'react';
import { creatorStyles } from 'app/dashboard/proposals/ranking/rankingCard/proposalCreator/ProposalCreator.styles';
import { useHtmlTranslator } from 'context/localization/hooks/useHtmlTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { ProposalDetails } from 'modules/hive/proposals/proposals';

export type ProposalCreatorProps = {
  proposalDetails: ProposalDetails;
};

export const ProposalCreator = memo(({ proposalDetails }: ProposalCreatorProps) => {
  const translateHtml = useHtmlTranslator();

  return (
    <Box sx={creatorStyles}>
      {proposalDetails.creator === proposalDetails.receiver
        ? translateHtml('dashboard.proposals.ranking.card.creator', {
            creator: formatUsername(proposalDetails.creator),
          })
        : translateHtml('dashboard.proposals.ranking.card.creatorWithReceiver', {
            creator: formatUsername(proposalDetails.creator),
            receiver: formatUsername(proposalDetails.receiver),
          })}
    </Box>
  );
});
