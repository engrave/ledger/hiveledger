import { Styles } from 'styles/theme.types';

export const stateStyles: Styles = {
  gridArea: 'state',
  display: 'flex',
  gap: 4,
  marginBottom: 3,
};

export const votesTotalStyles: Styles = {
  fontSize: '0.625rem',
  fontWeight: 500,
  color: 'typography.lightText',
  lineHeight: 0,
  display: 'flex',
  alignItems: 'center',
  gap: 0.5,
};
