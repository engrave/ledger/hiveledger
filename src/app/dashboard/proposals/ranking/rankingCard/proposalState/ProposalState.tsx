import { Box } from '@mui/material';
import { memo } from 'react';
import { ReactComponent as VoteIcon } from 'assets/images/icons/vote.svg';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { createFromIntegerString } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { ProposalDetails, ProposalFundingStatus, ProposalStatus } from 'modules/hive/proposals/proposals';
import { StatusDot } from 'ui/label/statusDot/StatusDot';
import { StatusDotColor } from 'ui/label/statusDot/StatusDot.types';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';
import { stateStyles, votesTotalStyles } from './ProposalState.style';

export type ProposalStateProps = {
  proposalDetails: ProposalDetails;
  fundStatus: ProposalFundingStatus;
};

export const ProposalState = memo(({ proposalDetails, fundStatus }: ProposalStateProps) => {
  const translate = useTranslator();
  const { formatAsset } = useLocalization();
  const { getHivePower } = useHivePower();

  const totalVotesInVests = createFromIntegerString(proposalDetails.total_votes, ChainAssetSymbol.vests);
  const totalVotes = getHivePower(totalVotesInVests);

  return (
    <Box sx={stateStyles}>
      {proposalDetails.status === ProposalStatus.active && fundStatus === ProposalFundingStatus.funded && (
        <StatusDot
          color={StatusDotColor.ok}
          label={translate('dashboard.proposals.ranking.card.status.active.label')}
          tooltip={translate('dashboard.proposals.ranking.card.status.active.tooltip')}
        />
      )}

      {proposalDetails.status === ProposalStatus.active && fundStatus === ProposalFundingStatus.notFunded && (
        <StatusDot
          color={StatusDotColor.error}
          label={translate('dashboard.proposals.ranking.card.status.inactive.label')}
          tooltip={translate('dashboard.proposals.ranking.card.status.inactive.tooltip')}
        />
      )}

      {proposalDetails.status === ProposalStatus.inactive && (
        <StatusDot
          color={StatusDotColor.alert}
          label={translate('dashboard.proposals.ranking.card.status.upcoming.label')}
          tooltip={translate('dashboard.proposals.ranking.card.status.upcoming.tooltip')}
        />
      )}

      <Tooltip title={formatAsset(totalVotesInVests, { notation: 'compact' })}>
        <Box sx={votesTotalStyles}>
          <VoteIcon />
          {translate('dashboard.proposals.ranking.card.totalVotes', {
            votes: formatAsset(totalVotes, {
              notation: 'compact',
              minimumFractionDigits: 0,
              maximumFractionDigits: 0,
            }),
          })}
        </Box>
      </Tooltip>
    </Box>
  );
});
