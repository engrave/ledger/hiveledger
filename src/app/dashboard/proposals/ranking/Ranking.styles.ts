import { Styles } from 'styles/theme.types';

export const headingStyles: Styles = {
  fontSize: '0.75rem',
  fontWeight: 500,
  textTransform: 'uppercase',
  color: 'typography.lightText',
  marginTop: 3.5,
  marginBottom: 1.5,
};
