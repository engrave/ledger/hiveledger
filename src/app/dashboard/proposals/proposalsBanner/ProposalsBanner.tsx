import { useTranslator } from 'context/localization/hooks/useTranslator';
import { Banner } from 'ui/message/banner/Banner';

export const ProposalsBanner = () => {
  const translate = useTranslator();

  return <Banner>{translate('dashboard.proposals.banner')}</Banner>;
};
