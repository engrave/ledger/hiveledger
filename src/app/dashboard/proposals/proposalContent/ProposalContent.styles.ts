import { Styles } from 'styles/theme.types';

export const proposalTitleStyles: Styles = {
  fontSize: '1.5rem',
  fontWeight: 'bold',
  marginBottom: '1.5rem',
};
