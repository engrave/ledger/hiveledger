import { Box } from '@mui/material';
import { proposalTitleStyles } from 'app/dashboard/proposals/proposalContent/ProposalContent.styles';
import { Post } from 'modules/hive/post/post';
import { ProposalDetails } from 'modules/hive/proposals/proposals';
import { DashboardCard } from 'ui/layout/dashboardCard/DashboardCard';
import { PostContent } from 'ui/post/postContent/PostContent';

export type ProposalContentProps = {
  proposal: ProposalDetails;
  post: Post;
};

export const ProposalContent = ({ proposal, post }: ProposalContentProps) => {
  return (
    <DashboardCard>
      <Box sx={proposalTitleStyles}>{proposal.subject}</Box>
      <PostContent content={post.body} />
    </DashboardCard>
  );
};
