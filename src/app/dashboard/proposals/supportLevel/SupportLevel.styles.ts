import { Styles } from 'styles/theme.types';

export const voteCheckboxStyles: Styles = {
  position: 'absolute',
  right: '22px',
  top: '14px',
};

export const votePlaceholderStyles: Styles = {
  margin: '9px',
  borderRadius: '3px',
};

export const progressBarStyles: Styles = {
  width: '75%',
  margin: 'auto',
};
