import { Box, Skeleton } from '@mui/material';
import { useCallback, useEffect } from 'react';
import {
  progressBarStyles,
  voteCheckboxStyles,
  votePlaceholderStyles,
} from 'app/dashboard/proposals/supportLevel/SupportLevel.styles';
import { SupportLevelProps } from 'app/dashboard/proposals/supportLevel/SupportLevel.types';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useHivePower } from 'context/hive/hooks/useHivePower';
import { useLocalization } from 'context/localization/hooks/useLocalization';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useProposals } from 'context/proposals/hooks/useProposals/useProposals';
import { useSingleProposalVotes } from 'context/proposals/hooks/useProposalVotes/useSingleProposalVotes';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { createFromIntegerString } from 'modules/hive/asset/createAsset/createAsset';
import { ChainAssetSymbol } from 'modules/hive/assetSymbol/assetSymbol.enum';
import { updateProposalVotes } from 'modules/hive/operation/updateProposalVotes/updateProposalVotes';
import { Infobox, InfoboxParameter, InfoboxParameterLabel, InfoboxParameterValue } from 'ui/data/infobox/Infobox';
import { BaseCheckbox } from 'ui/form/checkbox/BaseCheckbox';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';
import { ProgressBar } from 'ui/progress/progressBar/ProgressBar';
import { clamp } from 'utils/number/clamp/clamp';

export const SupportLevel = ({ proposal }: SupportLevelProps) => {
  const translate = useTranslator();
  const account = useActiveAccount();
  const { formatAsset } = useLocalization();
  const { getHivePower } = useHivePower();
  const { runOperation } = useTransactionModal();

  const { fetchPage, lastFundedProposal, isRankingCompleted } = useProposals();

  const { isVoted, isVoteLoading } = useSingleProposalVotes(account, proposal);

  const isVotingDisabled = account.details.proxy !== '';

  useEffect(() => {
    if (lastFundedProposal === null && !isRankingCompleted) {
      fetchPage();
    }
  }, [fetchPage, isRankingCompleted, lastFundedProposal]);

  const onVoteChange = useCallback(async () => {
    const operation = updateProposalVotes({
      voter: account.username,
      proposalId: proposal.proposal_id,
      approve: !isVoted,
    });

    await runOperation(operation);
  }, [account.username, isVoted, proposal.proposal_id, runOperation]);

  const requiredToFund =
    lastFundedProposal !== null
      ? createFromIntegerString(lastFundedProposal.details.total_votes, ChainAssetSymbol.vests)
      : null;

  const totalVotes = createFromIntegerString(proposal.total_votes, ChainAssetSymbol.vests);

  const requiredToFundPower = requiredToFund !== null ? getHivePower(requiredToFund) : null;
  const totalVotesPower = getHivePower(totalVotes);

  const fundPercent = requiredToFund !== null ? clamp(totalVotes.amount / requiredToFund.amount, 0, 1) : null;

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.proposals.supportLevel.title')}</DashboardCardTitle>

      <Box sx={voteCheckboxStyles}>
        {(isVoteLoading || isVoted === null) && (
          <Skeleton width={24} height={24} variant={'rectangular'} animation={'pulse'} sx={votePlaceholderStyles} />
        )}
        {!isVoteLoading && isVoted !== null && (
          <Tooltip
            title={isVotingDisabled ? translate('dashboard.proposals.supportLevel.proxyEnabled') : ''}
            wrap={'span'}
          >
            <BaseCheckbox value={isVoted} disabled={isVotingDisabled} onClick={() => onVoteChange()} />
          </Tooltip>
        )}
      </Box>

      <Infobox>
        <InfoboxParameter>
          <InfoboxParameterLabel>{translate('dashboard.proposals.supportLevel.requiredToFund')}</InfoboxParameterLabel>
          <InfoboxParameterValue>
            {requiredToFund === null && <Skeleton variant={'text'} />}
            {requiredToFund !== null && requiredToFundPower !== null && (
              <Tooltip title={formatAsset(requiredToFund)}>{formatAsset(requiredToFundPower)}</Tooltip>
            )}
          </InfoboxParameterValue>
        </InfoboxParameter>

        <InfoboxParameter>
          <InfoboxParameterLabel>{translate('dashboard.proposals.supportLevel.totalVotes')}</InfoboxParameterLabel>
          <InfoboxParameterValue>
            <Tooltip title={formatAsset(totalVotes)}>{formatAsset(totalVotesPower)}</Tooltip>
          </InfoboxParameterValue>
        </InfoboxParameter>
      </Infobox>

      <ProgressBar
        title={translate('dashboard.proposals.supportLevel.progress')}
        percent={fundPercent}
        sx={progressBarStyles}
      />
    </DashboardCard>
  );
};
