import { ProposalDetails } from 'modules/hive/proposals/proposals';

export type SupportLevelProps = {
  proposal: ProposalDetails;
};
