import { Styles } from 'styles/theme.types';

export const containerStyles: Styles = {
  marginX: '-30px',
};

export const sectionStyles: Styles = {
  display: 'grid',
  gridTemplateColumns: '14% 1fr',
  padding: '20px 30px',

  '&:nth-of-type(2n)': {
    backgroundColor: 'ui.cardElement',

    '.inverse-borders': {
      borderLeftColor: 'white',

      '&:not(:last-of-type)': {
        borderBottomColor: 'white',
      },
    },
  },
};

export const sectionLabelStyles: Styles = {
  fontWeight: '500',
  fontSize: '0.875rem',
  padding: '6px 6px 6px 0',
};

export const sectionContentStyles: Styles = {
  fontWeight: 'bolder',

  '.MuiFormControl-root': {
    marginBottom: '0 !important',
  },
};

export const actionsStyles: Styles = {
  padding: '0 30px',
  display: 'flex',
  justifyContent: 'flex-end',
};
