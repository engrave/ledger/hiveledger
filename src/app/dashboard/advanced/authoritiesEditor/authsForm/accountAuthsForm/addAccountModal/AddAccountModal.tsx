import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { useForm } from 'react-hook-form';
import { createAddAccountFormSchema } from 'app/dashboard/advanced/authoritiesEditor/authsForm/accountAuthsForm/addAccountModal/AddAccountModal.schema';
import { dialogContentStyles } from 'app/dashboard/advanced/authoritiesEditor/authsForm/accountAuthsForm/addAccountModal/AddAccountModal.styles';
import {
  AddAccountForm,
  AddAccountModalProps,
} from 'app/dashboard/advanced/authoritiesEditor/authsForm/accountAuthsForm/addAccountModal/AddAccountModal.types';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { FormText } from 'ui/form/text/FormText';
import { DialogCloseButton } from 'ui/modal/dialogCloseButton/DialogCloseButton';
import { stopSubmitPropagation } from 'utils/events/stopSubmitPropagation';
import { useSchemaCreator } from 'validation/useSchemaCreator/useSchemaCreator';

export const AddAccountModal = (props: AddAccountModalProps) => {
  const translate = useTranslator();

  const formSchema = useSchemaCreator(createAddAccountFormSchema);

  const form = useForm<AddAccountForm>({
    resolver: yupResolver(formSchema()),
    defaultValues: { username: '' },
  });

  const handleFormSubmit = (values: AddAccountForm) => {
    props.onSave(values.username);
    form.reset();
  };

  const handleModalClose = () => {
    props.onCancel();
    form.reset();
  };

  return (
    <Dialog open={props.isOpen} onClose={() => handleModalClose()} fullWidth={true}>
      <form onSubmit={stopSubmitPropagation(form.handleSubmit(handleFormSubmit))}>
        <DialogTitle>{translate('dashboard.advanced.authoritiesEditor.addAccountModal.title')}</DialogTitle>
        <DialogCloseButton onClick={() => handleModalClose()} />

        <DialogContent sx={dialogContentStyles}>
          <FormText
            control={form.control}
            name={'username'}
            label={translate('dashboard.advanced.authoritiesEditor.addAccountModal.username')}
            error={form.formState.errors.username}
            prefix={'@'}
          />
        </DialogContent>

        <DialogActions>
          <Button variant={'contained'} type={'submit'}>
            {translate('dashboard.advanced.authoritiesEditor.addAccountModal.save')}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
