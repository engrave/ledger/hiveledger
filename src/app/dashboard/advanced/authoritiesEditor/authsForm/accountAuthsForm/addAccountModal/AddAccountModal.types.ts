export type AddAccountModalProps = {
  isOpen: boolean;
  onSave: (username: string) => void;
  onCancel: () => void;
};

export type AddAccountForm = {
  username: string;
};
