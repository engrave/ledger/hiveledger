import { object, ObjectSchema, string } from 'yup';
import { AddAccountForm } from 'app/dashboard/advanced/authoritiesEditor/authsForm/accountAuthsForm/addAccountModal/AddAccountModal.types';
import { SchemaCreatorParams } from 'validation/useSchemaCreator/useSchemaCreator';
import { isBadActor } from 'validation/validators/isBadActor/isBadActor';
import { isValidUsername } from 'validation/validators/isValidUsername/isValidUsername';

export const createAddAccountFormSchema = ({ translate }: SchemaCreatorParams) => {
  return (): ObjectSchema<AddAccountForm> => {
    return object({
      username: string()
        .required()
        .test({
          test: isValidUsername,
          message: translate('validation.custom.invalidUsername'),
        })
        .test({
          test: isBadActor,
          message: translate('validation.custom.badActor'),
        })
        .label(translate('dashboard.advanced.authoritiesEditor.addAccountModal.username')),
    }).required();
  };
};
