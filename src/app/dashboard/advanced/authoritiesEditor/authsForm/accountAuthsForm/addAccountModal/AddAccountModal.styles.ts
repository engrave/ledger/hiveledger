import { Styles } from 'styles/theme.types';

export const dialogContentStyles: Styles = {
  '.MuiFormControl-root, .MuiFormControl-root:last-of-type': {
    marginBottom: 0,

    input: {
      width: '500px',
    },
  },
};
