import { Box, IconButton } from '@mui/material';
import { useState } from 'react';
import { useFormContext } from 'react-hook-form';
import { AuthoritiesEditorForm } from 'app/dashboard/advanced/authoritiesEditor/AuthoritiesEditor.types';
import { AccountRowProps } from 'app/dashboard/advanced/authoritiesEditor/authsForm/accountAuthsForm/accountRow/AccountRow.types';
import {
  authsFormEntryActionsStyles,
  authsFormEntryStyles,
  authsFormEntryValueStyles,
} from 'app/dashboard/advanced/authoritiesEditor/authsForm/AuthsForm.styles';
import { ReactComponent as TrashcanIcon } from 'assets/images/icons/trashcan.svg';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { FormText } from 'ui/form/text/FormText';
import { ConfirmationModal } from 'ui/modal/confirmationModal/ConfirmationModal';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';

export const AccountRow = (props: AccountRowProps) => {
  const translate = useTranslator();
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);

  const form = useFormContext<AuthoritiesEditorForm>();

  const handleDeletingRequest = () => {
    setDeleteModalOpen(true);
  };

  const handleDeletingConfirm = () => {
    setDeleteModalOpen(false);
    props.onDelete();
  };

  const handleDeletingCancel = () => {
    setDeleteModalOpen(false);
  };

  return (
    <>
      <Box sx={authsFormEntryStyles} key={props.field.id}>
        <Box sx={authsFormEntryValueStyles}>{props.field.value}</Box>
        <Box sx={authsFormEntryActionsStyles}>
          <FormText
            name={`accountAuths.${props.index}.weight`}
            control={form.control}
            type={'number'}
            error={form.formState.errors.accountAuths?.[props.index]?.weight}
            min={1}
            step={1}
          />
          <Tooltip
            title={translate('dashboard.advanced.authoritiesEditor.accountAuths.delete.tooltip')}
            placement={'top'}
            arrow
          >
            <IconButton onClick={() => handleDeletingRequest()}>
              <TrashcanIcon />
            </IconButton>
          </Tooltip>
        </Box>
      </Box>

      <ConfirmationModal
        isOpen={isDeleteModalOpen}
        onConfirm={() => handleDeletingConfirm()}
        onCancel={() => handleDeletingCancel()}
        title={translate('dashboard.advanced.authoritiesEditor.accountAuths.delete.title')}
      >
        {translate('dashboard.advanced.authoritiesEditor.accountAuths.delete.description', {
          username: formatUsername(props.field.value),
        })}
      </ConfirmationModal>
    </>
  );
};
