import { Box, Button } from '@mui/material';
import { useState } from 'react';
import { useFieldArray, useFormContext } from 'react-hook-form';
import { AuthoritiesEditorForm } from 'app/dashboard/advanced/authoritiesEditor/AuthoritiesEditor.types';
import { AccountRow } from 'app/dashboard/advanced/authoritiesEditor/authsForm/accountAuthsForm/accountRow/AccountRow';
import { AddAccountModal } from 'app/dashboard/advanced/authoritiesEditor/authsForm/accountAuthsForm/addAccountModal/AddAccountModal';
import {
  authsFormEntriesListStyles,
  authsFormHeaderItemStyles,
  authsFormHeaderRowStyles,
  authsFormStyles,
  authsListFooterStyles,
} from 'app/dashboard/advanced/authoritiesEditor/authsForm/AuthsForm.styles';
import { useTranslator } from 'context/localization/hooks/useTranslator';

export const AccountAuthsForm = () => {
  const translate = useTranslator();

  const [isAddModalOpen, setAddModalOpen] = useState(false);

  const form = useFormContext<AuthoritiesEditorForm>();

  const accountAuthsArray = useFieldArray({
    control: form.control,
    name: 'accountAuths',
  });

  const handleAddingAccount = (username: string) => {
    setAddModalOpen(false);
    accountAuthsArray.append({ value: username, weight: 1 });
  };

  const handleDeletingAccount = (index: number) => {
    accountAuthsArray.remove(index);
  };

  return (
    <Box sx={authsFormStyles}>
      {accountAuthsArray.fields.length > 0 && (
        <Box sx={authsFormHeaderRowStyles}>
          <Box sx={authsFormHeaderItemStyles}>
            {translate('dashboard.advanced.authoritiesEditor.accountAuths.accountColumn')}
          </Box>

          <Box sx={authsFormHeaderItemStyles}>
            {translate('dashboard.advanced.authoritiesEditor.accountAuths.weightColumn')}
          </Box>
        </Box>
      )}

      <Box sx={authsFormEntriesListStyles}>
        {accountAuthsArray.fields.map((field, index) => (
          <AccountRow field={field} index={index} key={index} onDelete={() => handleDeletingAccount(index)} />
        ))}
      </Box>

      <Box sx={authsListFooterStyles}>
        <Button variant={'outlined'} size={'small'} onClick={() => setAddModalOpen(true)}>
          {translate('dashboard.advanced.authoritiesEditor.accountAuths.addAccountButton')}
        </Button>
      </Box>

      <AddAccountModal
        isOpen={isAddModalOpen}
        onCancel={() => setAddModalOpen(false)}
        onSave={(username) => handleAddingAccount(username)}
      />
    </Box>
  );
};
