import { AuthorityType } from 'modules/hive/account/account';

export type KeyAuthsFormProps = {
  authorityType: AuthorityType;
};
