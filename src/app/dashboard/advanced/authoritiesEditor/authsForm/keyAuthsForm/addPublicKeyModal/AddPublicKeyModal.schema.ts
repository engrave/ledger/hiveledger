import { object, ObjectSchema, string } from 'yup';
import { AddPublicKeyModalForm } from 'app/dashboard/advanced/authoritiesEditor/authsForm/keyAuthsForm/addPublicKeyModal/AddPublicKeyModal.types';
import { SchemaCreatorParams } from 'validation/useSchemaCreator/useSchemaCreator';
import { isValidPublicKey } from 'validation/validators/isValidPublicKey/isValidPublicKey';

export const createAddKeyFormSchema = ({ translate }: SchemaCreatorParams) => {
  return (): ObjectSchema<AddPublicKeyModalForm> => {
    return object({
      publicKey: string()
        .required()
        .test({
          test: isValidPublicKey,
          message: translate('validation.custom.invalidPublicKey'),
        })
        .label(translate('dashboard.advanced.authoritiesEditor.addPublicKeyModal.publicKey')),
    }).required();
  };
};
