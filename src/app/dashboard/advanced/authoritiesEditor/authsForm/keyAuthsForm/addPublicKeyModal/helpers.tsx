import { ReactNode } from 'react';
import { KeyConfirmationError } from 'context/ledger/hooks/useKeyConfirmation/useKeyConfirmation';
import { isRpcError } from 'modules/ledger/rpcError/rpcError';
import { FormattedRpcError } from 'ui/message/formattedRpcError/FormattedRpcError';
import { TranslatedMessage } from 'ui/message/translatedMessage/TranslatedMessage';

export const getErrorMessage = (error: unknown): ReactNode => {
  if (error instanceof Error) {
    if (error.message === KeyConfirmationError.cancelled) {
      return <TranslatedMessage id={'dashboard.advanced.authoritiesEditor.addPublicKeyModal.errors.cancelled'} />;
    }

    if (isRpcError(error)) {
      return <FormattedRpcError error={error} />;
    }
  }

  return <TranslatedMessage id={'dashboard.advanced.authoritiesEditor.addPublicKeyModal.errors.deviceError'} />;
};
