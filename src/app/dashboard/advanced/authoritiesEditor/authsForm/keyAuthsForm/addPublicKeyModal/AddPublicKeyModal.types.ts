import { AuthorityType } from 'modules/hive/account/account';

export type AddPublicKeyModalProps = {
  isOpen: boolean;
  onSave: (username: string) => void;
  onCancel: () => void;
  authorityType: AuthorityType;
  bannedKeys: string[];
};

export type AddPublicKeyModalForm = {
  publicKey: string;
};
