import { yupResolver } from '@hookform/resolvers/yup';
import { LoadingButton } from '@mui/lab';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { ReactNode, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createAddKeyFormSchema } from 'app/dashboard/advanced/authoritiesEditor/authsForm/keyAuthsForm/addPublicKeyModal/AddPublicKeyModal.schema';
import { dialogContentStyles } from 'app/dashboard/advanced/authoritiesEditor/authsForm/keyAuthsForm/addPublicKeyModal/AddPublicKeyModal.styles';
import {
  AddPublicKeyModalForm,
  AddPublicKeyModalProps,
} from 'app/dashboard/advanced/authoritiesEditor/authsForm/keyAuthsForm/addPublicKeyModal/AddPublicKeyModal.types';
import { getErrorMessage } from 'app/dashboard/advanced/authoritiesEditor/authsForm/keyAuthsForm/addPublicKeyModal/helpers';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useUnusedAccountKey } from 'context/auth/hooks/useUnusedAccountKey';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { SlipRole } from 'modules/hive/accountDiscovery/accountDiscovery';
import { parsePath } from 'modules/hive/accountDiscovery/parsePath/parsePath';
import { FormText } from 'ui/form/text/FormText';
import { DialogCloseButton } from 'ui/modal/dialogCloseButton/DialogCloseButton';
import { DialogVerticalError } from 'ui/modal/dialogVerticalError/DialogVerticalError';
import { stopSubmitPropagation } from 'utils/events/stopSubmitPropagation';
import { useSchemaCreator } from 'validation/useSchemaCreator/useSchemaCreator';

export const AddPublicKeyModal = (props: AddPublicKeyModalProps) => {
  const translate = useTranslator();
  const getUnusedKey = useUnusedAccountKey();
  const { publicKeys } = useActiveAccount();

  const [isFetchingFromLedger, setFetchingFromLedger] = useState(false);
  const [errorMessage, setErrorMessage] = useState<ReactNode>(null);

  const formSchema = useSchemaCreator(createAddKeyFormSchema);

  const form = useForm<AddPublicKeyModalForm>({
    resolver: yupResolver(formSchema()),
    defaultValues: { publicKey: '' },
  });

  const handleFormSubmit = (values: AddPublicKeyModalForm) => {
    props.onSave(values.publicKey);
    form.reset();
  };

  const handleGettingKeyFromLedger = async () => {
    form.reset();
    setFetchingFromLedger(true);
    setErrorMessage(null);

    try {
      const pathDetails = parsePath(publicKeys[0].path);

      if (pathDetails === null) {
        return;
      }

      const unusedKey = await getUnusedKey(SlipRole[props.authorityType], pathDetails.accountIndex, props.bannedKeys);

      form.setValue('publicKey', unusedKey.publicKey);
    } catch (error) {
      setErrorMessage(getErrorMessage(error));
    } finally {
      setFetchingFromLedger(false);
    }
  };

  const handleModalClose = () => {
    props.onCancel();
    setErrorMessage(null);
    form.reset({ publicKey: '' });
  };

  return (
    <Dialog open={props.isOpen} onClose={() => handleModalClose()} fullWidth={true}>
      <form onSubmit={stopSubmitPropagation(form.handleSubmit(handleFormSubmit))}>
        <DialogTitle>{translate('dashboard.advanced.authoritiesEditor.addPublicKeyModal.title')}</DialogTitle>
        <DialogCloseButton onClick={() => handleModalClose()} />

        <DialogContent sx={dialogContentStyles}>
          <FormText
            control={form.control}
            label={translate('dashboard.advanced.authoritiesEditor.addPublicKeyModal.publicKey')}
            name={'publicKey'}
            error={form.formState.errors.publicKey}
            disabled={isFetchingFromLedger}
          />
        </DialogContent>

        {errorMessage && <DialogVerticalError>{errorMessage}</DialogVerticalError>}

        <DialogActions>
          <LoadingButton
            variant={'outlined'}
            disabled={isFetchingFromLedger}
            loading={isFetchingFromLedger}
            onClick={() => handleGettingKeyFromLedger()}
          >
            {translate('dashboard.advanced.authoritiesEditor.addPublicKeyModal.getFromLedger')}
          </LoadingButton>
          <Button variant={'contained'} type={'submit'} disabled={isFetchingFromLedger}>
            {translate('dashboard.advanced.authoritiesEditor.addPublicKeyModal.save')}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
