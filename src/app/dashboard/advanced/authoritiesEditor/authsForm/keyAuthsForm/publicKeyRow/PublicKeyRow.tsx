import { Box, IconButton } from '@mui/material';
import { useState } from 'react';
import { useFormContext } from 'react-hook-form';
import { AuthoritiesEditorForm } from 'app/dashboard/advanced/authoritiesEditor/AuthoritiesEditor.types';
import {
  authsFormEntryActionsStyles,
  authsFormEntryStyles,
  authsFormEntryValueStyles,
} from 'app/dashboard/advanced/authoritiesEditor/authsForm/AuthsForm.styles';
import { PublicKeyRowProps } from 'app/dashboard/advanced/authoritiesEditor/authsForm/keyAuthsForm/publicKeyRow/PublicKeyRow.types';
import { ReactComponent as TrashcanIcon } from 'assets/images/icons/trashcan.svg';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { FormText } from 'ui/form/text/FormText';
import { ConfirmationModal } from 'ui/modal/confirmationModal/ConfirmationModal';
import { Tooltip } from 'ui/popover/tooltip/Tooltip';

export const PublicKeyRow = (props: PublicKeyRowProps) => {
  const translate = useTranslator();
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);

  const form = useFormContext<AuthoritiesEditorForm>();

  const handleDeletingRequest = () => {
    setDeleteModalOpen(true);
  };

  const handleDeletingConfirm = () => {
    setDeleteModalOpen(false);
    props.onDelete();
  };

  const handleDeletingCancel = () => {
    setDeleteModalOpen(false);
  };

  return (
    <>
      <Box sx={authsFormEntryStyles} className={'inverse-borders'} key={props.field.id}>
        <Box sx={authsFormEntryValueStyles}>{props.field.value}</Box>
        <Box sx={authsFormEntryActionsStyles}>
          <FormText
            name={`keyAuths.${props.index}.weight`}
            control={form.control}
            type={'number'}
            error={form.formState.errors.keyAuths?.[props.index]?.weight}
            min={1}
            step={1}
          />
          <Tooltip
            title={translate('dashboard.advanced.authoritiesEditor.keyAuths.delete.tooltip')}
            placement={'top'}
            arrow
          >
            <IconButton onClick={() => handleDeletingRequest()}>
              <TrashcanIcon />
            </IconButton>
          </Tooltip>
        </Box>
      </Box>

      <ConfirmationModal
        isOpen={isDeleteModalOpen}
        onConfirm={() => handleDeletingConfirm()}
        onCancel={() => handleDeletingCancel()}
        title={translate('dashboard.advanced.authoritiesEditor.keyAuths.delete.title')}
      >
        <>
          {translate('dashboard.advanced.authoritiesEditor.keyAuths.delete.description')}

          <code>{props.field.value}</code>
        </>
      </ConfirmationModal>
    </>
  );
};
