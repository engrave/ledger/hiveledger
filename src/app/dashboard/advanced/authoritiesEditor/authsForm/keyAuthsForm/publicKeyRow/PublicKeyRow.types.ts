import { FieldArrayWithId } from 'react-hook-form/dist/types/fieldArray';
import { AuthoritiesSet } from 'modules/hive/account/account';

export type PublicKeyRowProps = {
  field: FieldArrayWithId<AuthoritiesSet, 'keyAuths'>;
  index: number;
  onDelete: () => void;
};
