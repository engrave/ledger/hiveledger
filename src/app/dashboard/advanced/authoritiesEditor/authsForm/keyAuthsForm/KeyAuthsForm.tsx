import { Box, Button } from '@mui/material';
import { useState } from 'react';
import { useFieldArray, useFormContext } from 'react-hook-form';
import { AuthoritiesEditorForm } from 'app/dashboard/advanced/authoritiesEditor/AuthoritiesEditor.types';
import {
  authsFormEntriesListStyles,
  authsFormHeaderItemStyles,
  authsFormHeaderRowStyles,
  authsFormStyles,
  authsListFooterStyles,
} from 'app/dashboard/advanced/authoritiesEditor/authsForm/AuthsForm.styles';
import { KeyAuthsFormProps } from 'app/dashboard/advanced/authoritiesEditor/authsForm/AuthsForm.types';
import { AddPublicKeyModal } from 'app/dashboard/advanced/authoritiesEditor/authsForm/keyAuthsForm/addPublicKeyModal/AddPublicKeyModal';
import { PublicKeyRow } from 'app/dashboard/advanced/authoritiesEditor/authsForm/keyAuthsForm/publicKeyRow/PublicKeyRow';
import { useTranslator } from 'context/localization/hooks/useTranslator';

export const KeyAuthsForm = (props: KeyAuthsFormProps) => {
  const translate = useTranslator();
  const [isAddModalOpen, setAddModalOpen] = useState(false);

  const form = useFormContext<AuthoritiesEditorForm>();

  const keyAuthsArray = useFieldArray({
    control: form.control,
    name: 'keyAuths',
  });

  const handleAddingPublicKey = (publicKey: string) => {
    setAddModalOpen(false);
    keyAuthsArray.append({ value: publicKey, weight: 1 });
  };

  const handleDeletingPublicKey = (index: number) => {
    keyAuthsArray.remove(index);
  };

  return (
    <Box sx={authsFormStyles} className={'inverse-borders'}>
      {keyAuthsArray.fields.length > 0 && (
        <Box sx={authsFormHeaderRowStyles}>
          <Box sx={authsFormHeaderItemStyles}>
            {translate('dashboard.advanced.authoritiesEditor.keyAuths.publicKeyColumn')}
          </Box>

          <Box sx={authsFormHeaderItemStyles}>
            {translate('dashboard.advanced.authoritiesEditor.keyAuths.weightColumn')}
          </Box>
        </Box>
      )}

      <Box sx={authsFormEntriesListStyles}>
        {keyAuthsArray.fields.map((field, index) => (
          <PublicKeyRow field={field} index={index} key={index} onDelete={() => handleDeletingPublicKey(index)} />
        ))}
      </Box>

      <Box sx={authsListFooterStyles}>
        <Button variant={'outlined'} size={'small'} onClick={() => setAddModalOpen(true)}>
          {translate('dashboard.advanced.authoritiesEditor.keyAuths.addPublicKeyButton')}
        </Button>
      </Box>

      <AddPublicKeyModal
        authorityType={props.authorityType}
        isOpen={isAddModalOpen}
        onCancel={() => setAddModalOpen(false)}
        onSave={(username) => handleAddingPublicKey(username)}
        bannedKeys={keyAuthsArray.fields.map((field) => field.value)}
      />
    </Box>
  );
};
