import { Styles } from 'styles/theme.types';

export const authsFormStyles: Styles = {
  padding: '6px 0 6px 20px',
  borderLeftWidth: '2px',
  borderLeftStyle: 'solid',
  borderLeftColor: 'ui.cardElement',
};

export const authsFormEntriesListStyles: Styles = {
  marginBottom: 1,
};

export const authsFormEntryStyles: Styles = {
  display: 'grid',
  gridTemplateColumns: '1fr 110px',

  '&:not(:last-of-type)': {
    borderBottomWidth: '1px',
    borderBottomStyle: 'solid',
    borderBottomColor: 'ui.cardElement',
  },
};

export const authsFormHeaderRowStyles: Styles = {
  display: 'grid',
  gridTemplateColumns: '1fr 110px',
};

export const authsFormHeaderItemStyles: Styles = {
  fontWeight: '500',
  fontSize: '0.875rem',
};

export const authsFormEntryValueStyles: Styles = {
  fontFamily: '"Courier", monospace',
  fontWeight: 500,
  fontSize: '0.875rem',
  margin: '14px 0',
  display: 'flex',
  alignItems: 'center',
  height: '35px',
};

export const authsFormEntryActionsStyles: Styles = {
  fontWeight: 'bolder',
  margin: '14px 0',
  display: 'flex',
  alignItems: 'start',
  justifyContent: 'space-between',
  gap: 1,

  '.MuiFormControl-root': {
    width: '100px',
    marginBottom: '0 !important',
  },

  '.MuiButtonBase-root': {
    width: '35px',
    height: '35px',
  },
};

export const authsListFooterStyles: Styles = {};
