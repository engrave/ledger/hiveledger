import { array, number, object, ObjectSchema, string } from 'yup';
import { AuthoritiesEditorForm } from 'app/dashboard/advanced/authoritiesEditor/AuthoritiesEditor.types';
import { SchemaCreatorParams } from 'validation/useSchemaCreator/useSchemaCreator';
import { isBadActor } from 'validation/validators/isBadActor/isBadActor';
import { isValidUsername } from 'validation/validators/isValidUsername/isValidUsername';

export const createAuthoritiesEditorFormSchema = ({ translate }: SchemaCreatorParams) => {
  return (): ObjectSchema<AuthoritiesEditorForm> => {
    return object({
      threshold: number().min(1).required().label(translate('dashboard.advanced.authoritiesEditor.threshold.label')),
      keyAuths: array()
        .of(
          object({
            value: string().required(),
            weight: number()
              .min(1)
              .required()
              .label(translate('dashboard.advanced.authoritiesEditor.accountAuths.weightColumn')),
          }).required(),
        )
        .required(),
      accountAuths: array()
        .of(
          object({
            value: string()
              .test({
                test: isValidUsername,
                message: translate('validation.custom.invalidUsername'),
              })
              .test({
                test: isBadActor,
                message: translate('validation.custom.badActor'),
              })
              .required(),
            weight: number()
              .min(1)
              .required()
              .label(translate('dashboard.advanced.authoritiesEditor.accountAuths.weightColumn')),
          }).required(),
        )
        .required(),
    }).required();
  };
};
