import { AuthoritiesSet, AuthorityType } from 'modules/hive/account/account';

export type AuthoritiesEditorProps = {
  type: AuthorityType;
};

export type AuthEntry = {
  value: string;
  weight: number;
};

export type AuthoritiesEditorForm = AuthoritiesSet;
