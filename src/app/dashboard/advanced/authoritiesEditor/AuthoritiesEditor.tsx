import { yupResolver } from '@hookform/resolvers/yup';
import { Box, Button } from '@mui/material';
import { equals } from 'ramda';
import { useEffect } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { createAuthoritiesEditorFormSchema } from 'app/dashboard/advanced/authoritiesEditor/AuthoritiesEditor.schema';
import {
  actionsStyles,
  containerStyles,
  sectionContentStyles,
  sectionLabelStyles,
  sectionStyles,
} from 'app/dashboard/advanced/authoritiesEditor/AuthoritiesEditor.styles';
import {
  AuthoritiesEditorForm,
  AuthoritiesEditorProps,
} from 'app/dashboard/advanced/authoritiesEditor/AuthoritiesEditor.types';
import { AccountAuthsForm } from 'app/dashboard/advanced/authoritiesEditor/authsForm/accountAuthsForm/AccountAuthsForm';
import { KeyAuthsForm } from 'app/dashboard/advanced/authoritiesEditor/authsForm/keyAuthsForm/KeyAuthsForm';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useNavigationLock } from 'context/navigationLock/hooks/useNavigationLock/useNavigationLock';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { authoritiesUpdate } from 'modules/hive/operation/authoritiesUpdate/authoritiesUpdate';
import { FormText } from 'ui/form/text/FormText';
import { useSchemaCreator } from 'validation/useSchemaCreator/useSchemaCreator';

export const AuthoritiesEditor = (props: AuthoritiesEditorProps) => {
  const translate = useTranslator();
  const { runOperation } = useTransactionModal();
  const { addNavigationLock } = useNavigationLock();
  const { authorities, username, details } = useActiveAccount();

  const formSchema = useSchemaCreator(createAuthoritiesEditorFormSchema);

  const form = useForm<AuthoritiesEditorForm>({
    defaultValues: authorities[props.type],
    resolver: yupResolver(formSchema()),
  });

  const handleFormSubmit = async (values: AuthoritiesEditorForm) => {
    await runOperation(
      authoritiesUpdate({
        username,
        memoKey: details.memo_key,
        type: props.type,
        authorities: values,
      }),
    );
  };

  useEffect(() => {
    if (form.formState.isDirty) {
      const removeLock = addNavigationLock({
        title: translate('dashboard.advanced.authoritiesEditor.navigationLock.title'),
        description: translate('dashboard.advanced.authoritiesEditor.navigationLock.description'),
      });

      return () => removeLock();
    }
  }, [translate, form.formState.isDirty, addNavigationLock]);

  useEffect(() => {
    // eslint-disable-next-line no-underscore-dangle
    if (!equals(form.control._defaultValues, authorities[props.type])) {
      form.reset(authorities[props.type]);
    }
  }, [authorities, form, props.type]);

  return (
    <FormProvider {...form}>
      <form onSubmit={form.handleSubmit(handleFormSubmit)}>
        <Box sx={containerStyles}>
          <Box sx={sectionStyles}>
            <Box sx={sectionLabelStyles}>{translate('dashboard.advanced.authoritiesEditor.threshold.label')}</Box>
            <Box sx={sectionContentStyles}>
              <FormText
                name={'threshold'}
                control={form.control}
                type={'number'}
                error={form.formState.errors.threshold}
                min={1}
                step={1}
              />
            </Box>
          </Box>

          <Box sx={sectionStyles}>
            <Box sx={sectionLabelStyles}>{translate('dashboard.advanced.authoritiesEditor.keyAuths.label')}</Box>

            <Box sx={sectionContentStyles}>
              <KeyAuthsForm authorityType={props.type} />
            </Box>
          </Box>

          <Box sx={sectionStyles}>
            <Box sx={sectionLabelStyles}>{translate('dashboard.advanced.authoritiesEditor.accountAuths.label')}</Box>
            <Box sx={sectionContentStyles}>
              <AccountAuthsForm />
            </Box>
          </Box>

          <Box sx={actionsStyles}>
            <Button type={'submit'} variant={'contained'} disabled={!form.formState.isDirty}>
              {translate('dashboard.advanced.authoritiesEditor.saveButton')}
            </Button>
          </Box>
        </Box>
      </form>
    </FormProvider>
  );
};
