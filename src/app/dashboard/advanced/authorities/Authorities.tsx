import { AuthoritiesEditor } from 'app/dashboard/advanced/authoritiesEditor/AuthoritiesEditor';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { AuthorityType } from 'modules/hive/account/account';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export const Authorities = () => {
  const translate = useTranslator();

  return (
    <>
      <DashboardCard>
        <DashboardCardTitle>{translate('dashboard.advanced.authorities.ownerTitle')}</DashboardCardTitle>
        <AuthoritiesEditor type={AuthorityType.owner} />
      </DashboardCard>
      <DashboardCard>
        <DashboardCardTitle>{translate('dashboard.advanced.authorities.activeTitle')}</DashboardCardTitle>
        <AuthoritiesEditor type={AuthorityType.active} />
      </DashboardCard>
      <DashboardCard>
        <DashboardCardTitle>{translate('dashboard.advanced.authorities.postingTitle')}</DashboardCardTitle>
        <AuthoritiesEditor type={AuthorityType.posting} />
      </DashboardCard>
    </>
  );
};
