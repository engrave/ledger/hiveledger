import { useTranslator } from 'context/localization/hooks/useTranslator';
import { Banner } from 'ui/message/banner/Banner';

export const AdvancedBanner = () => {
  const translate = useTranslator();

  return <Banner>{translate('dashboard.advanced.banner')}</Banner>;
};
