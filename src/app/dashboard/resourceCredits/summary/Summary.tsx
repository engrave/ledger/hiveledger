import { Box } from '@mui/material';
import { useIntl } from 'react-intl';
import {
  detailsStyles,
  progressBarStyles,
  rechargeInfoStyles,
} from 'app/dashboard/resourceCredits/summary/Summary.styles';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useHtmlTranslator } from 'context/localization/hooks/useHtmlTranslator';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { getRechargeRate } from 'modules/hive/resourceCredits/getRechargeRate/getRechargeRate';
import { Infobox, InfoboxParameter, InfoboxParameterHeader, InfoboxParameterValue } from 'ui/data/infobox/Infobox';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { ProgressBar } from 'ui/progress/progressBar/ProgressBar';

export const Summary = () => {
  const translate = useTranslator();
  const translateHtml = useHtmlTranslator();
  const { formatNumber } = useIntl();

  const { resourceCredits } = useActiveAccount();

  const { mana, account } = resourceCredits;

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.resourceCredits.summary.title')}</DashboardCardTitle>

      <ProgressBar percent={mana.percentage / 100 / 100} sx={progressBarStyles} />

      <Infobox sx={detailsStyles}>
        <InfoboxParameter>
          <InfoboxParameterHeader>{translate('dashboard.resourceCredits.summary.maxMana')}</InfoboxParameterHeader>
          <InfoboxParameterValue>{formatNumber(mana.max_mana)}</InfoboxParameterValue>
        </InfoboxParameter>

        <InfoboxParameter>
          <InfoboxParameterHeader>{translate('dashboard.resourceCredits.summary.currentMana')}</InfoboxParameterHeader>
          <InfoboxParameterValue>{formatNumber(mana.current_mana)}</InfoboxParameterValue>
        </InfoboxParameter>

        {typeof account.received_delegated_rc !== 'undefined' && (
          <InfoboxParameter>
            <InfoboxParameterHeader>
              {translate('dashboard.resourceCredits.summary.receivedDelegation')}
            </InfoboxParameterHeader>
            <InfoboxParameterValue>{formatNumber(Number(account.received_delegated_rc))}</InfoboxParameterValue>
          </InfoboxParameter>
        )}

        {typeof account.delegated_rc !== 'undefined' && (
          <InfoboxParameter>
            <InfoboxParameterHeader>{translate('dashboard.resourceCredits.summary.delegated')}</InfoboxParameterHeader>
            <InfoboxParameterValue>{formatNumber(Number(account.delegated_rc))}</InfoboxParameterValue>
          </InfoboxParameter>
        )}
      </Infobox>

      <Box sx={rechargeInfoStyles}>
        {translateHtml('dashboard.resourceCredits.summary.rechargeRate', {
          amount: formatNumber(getRechargeRate(account), { notation: 'compact' }),
        })}
      </Box>
    </DashboardCard>
  );
};
