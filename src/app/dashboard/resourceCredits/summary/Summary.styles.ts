import { Styles } from 'styles/theme.types';

export const progressBarStyles: Styles = {
  marginTop: '30px',
};

export const detailsStyles: Styles = {
  display: 'grid',
  gridTemplateColumns: '1fr 1fr 1fr 1fr',
  marginTop: '30px',
};

export const rechargeInfoStyles: Styles = {
  fontSize: '0.875rem',
};
