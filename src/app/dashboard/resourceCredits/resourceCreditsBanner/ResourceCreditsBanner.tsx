import { useTranslator } from 'context/localization/hooks/useTranslator';
import { Banner } from 'ui/message/banner/Banner';

export const ResourceCreditsBanner = () => {
  const translate = useTranslator();

  return <Banner>{translate('dashboard.resourceCredits.banner')}</Banner>;
};
