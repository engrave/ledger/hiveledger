import { number, object, ObjectSchema, string } from 'yup';
import { DelegationForm } from 'app/dashboard/staking/delegateStake/DelegateStake.types';
import { SchemaCreatorParams } from 'validation/useSchemaCreator/useSchemaCreator';
import { isBadActor } from 'validation/validators/isBadActor/isBadActor';
import { isValidUsername } from 'validation/validators/isValidUsername/isValidUsername';

export const createResourceCreditsDelegationSchema = ({ translate }: SchemaCreatorParams) => {
  return (maxDelegation: number): ObjectSchema<DelegationForm> => {
    return object({
      delegatee: string()
        .required()
        .test({
          test: isValidUsername,
          message: translate('validation.custom.invalidUsername'),
        })
        .test({
          test: isBadActor,
          message: translate('validation.custom.badActor'),
        })
        .label(translate('dashboard.resourceCredits.delegateResourceCredits.form.delegate')),
      amount: number()
        .transform((value) => (Number.isNaN(value) ? undefined : value))
        .required()
        .positive()
        .max(maxDelegation)
        .label(translate('dashboard.resourceCredits.delegateResourceCredits.form.amount')),
    }).required();
  };
};
