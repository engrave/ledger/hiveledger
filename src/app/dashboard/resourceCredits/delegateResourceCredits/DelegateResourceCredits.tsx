import { yupResolver } from '@hookform/resolvers/yup';
import { Box, Button } from '@mui/material';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useIntl } from 'react-intl';
import { helpTooltipStyles } from 'app/dashboard/balance/powerUp/PowerUp.styles';
import { createResourceCreditsDelegationSchema } from 'app/dashboard/resourceCredits/delegateResourceCredits/DelegateResourceCredits.schema';
import { DelegationForm } from 'app/dashboard/resourceCredits/delegateResourceCredits/DelegateResourceCredits.types';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { delegateResourceCredits } from 'modules/hive/operation/delegateResourceCredits/delegateResourceCredits';
import { getMaxDelegation } from 'modules/hive/resourceCredits/getMaxDelegation/getMaxDelegation';
import { ResourceCreditsDelegation } from 'modules/hive/resourceCredits/resourceCredits';
import { FormAction } from 'ui/form/action/FormAction';
import { FormAlert } from 'ui/form/alert/FormAlert';
import { AvailableAmountHelper } from 'ui/form/availableAmountHelper/AvailableAmountHelper';
import { FormNumber } from 'ui/form/number/FormNumber';
import { FormText } from 'ui/form/text/FormText';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';
import { HelpTooltip } from 'ui/popover/helpTooltip/HelpTooltip';
import { useSchemaCreator } from 'validation/useSchemaCreator/useSchemaCreator';

export const DelegateResourceCredits = () => {
  const translate = useTranslator();
  const { formatNumber } = useIntl();

  const [existingDelegation, setExisingDelegation] = useState<ResourceCreditsDelegation | null>(null);

  const { username, resourceCredits, resourceCreditsDelegations } = useActiveAccount();
  const { runOperation } = useTransactionModal();

  const formSchema = useSchemaCreator(createResourceCreditsDelegationSchema);

  const availableMana = getMaxDelegation(resourceCredits, existingDelegation);

  const form = useForm<DelegationForm>({
    defaultValues: { delegatee: '', amount: 0 },
    mode: 'onChange',
    resolver: yupResolver(formSchema(availableMana)),
  });

  const [currentDelegatee, currentAmount] = form.watch(['delegatee', 'amount']);

  useEffect(() => {
    const matchingDelegation = resourceCreditsDelegations.find((delegation) => {
      return delegation.to === currentDelegatee;
    });

    setExisingDelegation(matchingDelegation ?? null);
  }, [currentDelegatee, resourceCreditsDelegations]);

  useEffect(() => {
    if (currentAmount > 0) {
      form.trigger('amount');
    }
  }, [existingDelegation, form, currentAmount]);

  const handleDelegationFormSubmit = async (data: DelegationForm) => {
    await runOperation(
      delegateResourceCredits({
        delegator: username,
        delegatee: data.delegatee,
        resourceCredits: data.amount,
      }),
    );
    form.reset();
  };

  return (
    <DashboardCard>
      <DashboardCardTitle>{translate('dashboard.resourceCredits.delegateResourceCredits.title')}</DashboardCardTitle>
      <HelpTooltip title={translate('dashboard.resourceCredits.delegateResourceCredits.help')} sx={helpTooltipStyles} />

      <form onSubmit={form.handleSubmit(handleDelegationFormSubmit)}>
        <Box>
          <FormText
            name={'delegatee'}
            label={translate('dashboard.resourceCredits.delegateResourceCredits.form.delegatee')}
            placeholder={translate('dashboard.resourceCredits.delegateResourceCredits.form.delegateePlaceholder')}
            control={form.control}
            error={form.formState.errors.delegatee}
            prefix={'@'}
          />
          <FormNumber
            name={'amount'}
            control={form.control}
            label={translate('dashboard.resourceCredits.delegateResourceCredits.form.amount')}
            format={{
              decimalScale: 0,
            }}
            helperText={<AvailableAmountHelper value={availableMana} form={form} target={'amount'} />}
            error={form.formState.errors.amount}
          />
        </Box>

        {existingDelegation && (
          <FormAlert severity={'info'}>
            {translate('dashboard.resourceCredits.delegateResourceCredits.delegationUpdate', {
              username: formatUsername(existingDelegation.to),
              current: formatNumber(existingDelegation.delegated_rc),
            })}
          </FormAlert>
        )}

        <FormAction>
          <Button type={'submit'} variant={'contained'} size={'small'} disabled={!form.formState.isValid}>
            {translate('dashboard.resourceCredits.delegateResourceCredits.form.delegate')}
          </Button>
        </FormAction>
      </form>
    </DashboardCard>
  );
};
