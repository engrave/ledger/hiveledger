export type DelegationForm = {
  delegatee: string;
  amount: number;
};
