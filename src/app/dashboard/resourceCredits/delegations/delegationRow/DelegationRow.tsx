import { TableRow } from '@mui/material';
import { ReactComponent as TrashcanIcon } from 'assets/images/icons/trashcan.svg';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { formatUsername } from 'modules/hive/account/formatUsername/formatUsername';
import { ResourceCreditsDelegation } from 'modules/hive/resourceCredits/resourceCredits';
import { IconButtonCell } from 'ui/data/table/cells/iconButtonCell/IconButtonCell';
import { NumberCell } from 'ui/data/table/cells/numberCell/NumberCell';
import { TextCell } from 'ui/data/table/cells/textCell/TextCell';

export type DelegationRow = {
  delegation: ResourceCreditsDelegation;
  onCancel: () => void;
};

export const DelegationRow = ({ delegation, onCancel }: DelegationRow) => {
  const translate = useTranslator();

  return (
    <TableRow>
      <TextCell>{formatUsername(delegation.to)}</TextCell>
      <NumberCell value={delegation.delegated_rc} />
      <IconButtonCell
        onClick={() => onCancel()}
        tooltip={translate('dashboard.resourceCredits.delegations.cancelButton')}
      >
        <TrashcanIcon />
      </IconButtonCell>
    </TableRow>
  );
};
