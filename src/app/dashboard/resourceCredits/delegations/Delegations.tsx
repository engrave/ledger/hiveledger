import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';
import { DelegationRow } from 'app/dashboard/resourceCredits/delegations/delegationRow/DelegationRow';
import { titleStyles } from 'app/dashboard/resourceCredits/delegations/Delegations.styles';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useTransactionModal } from 'context/transaction/hooks/useTransactionModal/useTransactionModal';
import { delegateResourceCredits } from 'modules/hive/operation/delegateResourceCredits/delegateResourceCredits';
import { ResourceCreditsDelegation } from 'modules/hive/resourceCredits/resourceCredits';
import { DashboardCard, DashboardCardTitle } from 'ui/layout/dashboardCard/DashboardCard';

export const Delegations = () => {
  const translate = useTranslator();
  const { resourceCreditsDelegations } = useActiveAccount();
  const { runOperation } = useTransactionModal();

  const handleDelegationCancel = (delegation: ResourceCreditsDelegation) => {
    runOperation(
      delegateResourceCredits({
        resourceCredits: 0,
        delegator: delegation.from,
        delegatee: delegation.to,
      }),
    );
  };

  if (resourceCreditsDelegations.length === 0) {
    return null;
  }

  return (
    <DashboardCard>
      <DashboardCardTitle sx={titleStyles}>
        {translate('dashboard.resourceCredits.delegations.title')}
      </DashboardCardTitle>

      <Table>
        <TableHead>
          <TableRow>
            <TableCell>{translate('dashboard.resourceCredits.delegations.table.to')}</TableCell>
            <TableCell>{translate('dashboard.resourceCredits.delegations.table.amount')}</TableCell>
            <TableCell>&nbsp;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {resourceCreditsDelegations.map((delegation, index) => (
            <DelegationRow delegation={delegation} onCancel={() => handleDelegationCancel(delegation)} key={index} />
          ))}
        </TableBody>
      </Table>
    </DashboardCard>
  );
};
