import { Helmet } from 'react-helmet';
import detailsCloseIcon from 'assets/images/icons/detailsClose.svg';
import detailsOpenIcon from 'assets/images/icons/detailsOpen.svg';

export const Preloads = () => {
  return (
    <Helmet>
      <link rel="preload" href={detailsCloseIcon} as="image" />
      <link rel="preload" href={detailsOpenIcon} as="image" />
    </Helmet>
  );
};
