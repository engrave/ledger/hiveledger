import { Navigate, Route, Routes as ReactRoutes } from 'react-router-dom';
import { AuthRoute, DashboardRoute, GeneralRoute, ProposalsRoute } from 'config/routes';
import { AddAccount } from 'pages/auth/addAccount/AddAccount';
import { Login } from 'pages/auth/login/Login';
import { Logout } from 'pages/auth/logout/Logout';
import { Advanced } from 'pages/dashboard/advanced/Advanced';
import { Balance } from 'pages/dashboard/balance/Balance';
import { Dashboard } from 'pages/dashboard/Dashboard';
import { NotFound } from 'pages/dashboard/notFound/NotFound';
import { Overview } from 'pages/dashboard/overview/Overview';
import { ProposalDetails } from 'pages/dashboard/proposals/proposalDetails/ProposalDetails';
import { Proposals } from 'pages/dashboard/proposals/Proposals';
import { ProposalsBrowser } from 'pages/dashboard/proposals/proposalsBrowser/ProposalsBrowser';
import { ResourceCredits } from 'pages/dashboard/resourceCredits/ResourceCredits';
import { Savings } from 'pages/dashboard/savings/Savings';
import { Staking } from 'pages/dashboard/staking/Staking';
import { Witnesses } from 'pages/dashboard/witnesses/Witnesses';
import { Document } from 'pages/Document';
import { Exchange } from 'pages/exchange/Exchange';

export const Routing = () => {
  return (
    <ReactRoutes>
      <Route path={GeneralRoute.root} element={<Document />}>
        <Route path={GeneralRoute.auth}>
          <Route path={AuthRoute.login} element={<Login />} />
          <Route path={AuthRoute.addAccount} element={<AddAccount />} />
          <Route path={AuthRoute.logout} element={<Logout />} />
        </Route>
        <Route path={GeneralRoute.dashboard} element={<Dashboard />}>
          <Route path={DashboardRoute.overview} element={<Overview />} />
          <Route path={DashboardRoute.balance} element={<Balance />} />
          <Route path={DashboardRoute.staking} element={<Staking />} />
          <Route path={DashboardRoute.savings} element={<Savings />} />
          <Route path={DashboardRoute.proposals} element={<Proposals />}>
            <Route path={ProposalsRoute.browser} element={<ProposalsBrowser />} />
            <Route path={ProposalsRoute.details} element={<ProposalDetails />} />
          </Route>
          <Route path={DashboardRoute.witnesses} element={<Witnesses />} />
          <Route path={DashboardRoute.resourceCredits} element={<ResourceCredits />} />
          <Route path={DashboardRoute.advanced} element={<Advanced />} />
          <Route path={'*'} element={<NotFound />} />
        </Route>
        <Route path={GeneralRoute.exchange} element={<Exchange />} />
        <Route index element={<Navigate to={AuthRoute.login} />} />
      </Route>
    </ReactRoutes>
  );
};
