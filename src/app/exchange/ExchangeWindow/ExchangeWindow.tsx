import { exchangeWindowStyles } from 'app/exchange/ExchangeWindow/ExchangeWindow.styles';
import { exchangeWidgetConfig } from 'config/config';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { DashboardCard } from 'ui/layout/dashboardCard/DashboardCard';

export const ExchangeWindow = () => {
  const translate = useTranslator();

  const searchParams = new URLSearchParams({
    from: '*',
    to: '*',
    amount: '0.1',
    address: '',
    fromDefault: 'btc',
    toDefault: 'hive',
    theme: 'default',
    merchant_id: exchangeWidgetConfig.merchantId,
    payment_id: '',
    v: '3',
  });

  return (
    <DashboardCard sx={exchangeWindowStyles}>
      <iframe
        width="100%"
        height="100%"
        frameBorder="none"
        src={`https://widget.changelly.com?${searchParams.toString()}`}
      >
        {translate('exchange.loadingError')}
      </iframe>
    </DashboardCard>
  );
};
