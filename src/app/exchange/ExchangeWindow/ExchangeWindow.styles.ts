import { Styles } from 'styles/theme.types';

export const exchangeWindowStyles: Styles = {
  height: '650px',
  padding: 2,
  background: '#fafafa',
};
