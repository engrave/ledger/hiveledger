import { LoadingButton } from '@mui/lab';
import { Box, Button, Dialog, DialogContent, DialogTitle } from '@mui/material';
import { ReactNode, useEffect, useState } from 'react';
import {
  actionsStyles,
  hashedTransactionModalStyles,
} from 'app/transaction/hashedTransactionModal/HashedTransactionModal.styles';
import {
  TransactionModalProps,
  TransactionModalStatus,
} from 'app/transaction/hashedTransactionModal/HashedTransactionModal.types';
import { getErrorMessage } from 'app/transaction/transactionModal/helpers';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useAuth } from 'context/auth/hooks/useAuth';
import { useLedger } from 'context/ledger/hooks/useLedger/useLedger';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { useTransaction } from 'context/transaction/hooks/useTransaction/useTransaction';
import { useToast } from 'hooks/useToast/useToast';
import {
  ActionParameter,
  ActionParameterExpandableValue,
  ActionParameterLabel,
  ActionParameterValue,
} from 'ui/data/actionParameter/ActionParameter';
import { DialogCloseButton } from 'ui/modal/dialogCloseButton/DialogCloseButton';
import { DialogVerticalError } from 'ui/modal/dialogVerticalError/DialogVerticalError';
import { downloadContent } from 'utils/file/downloadContent/downloadContent';

export const HashedTransactionModal = ({ transaction, isOpen, onClose }: TransactionModalProps) => {
  const translate = useTranslator();
  const account = useActiveAccount();
  const { refreshAccounts } = useAuth();
  const { connectLedger } = useLedger();

  const { operations } = transaction;

  const { showError, showSuccess } = useToast();
  const { start, done } = useGlobalProgressBar();

  const [status, setStatus] = useState<TransactionModalStatus>(TransactionModalStatus.idle);
  const [errorMessage, setErrorMessage] = useState<ReactNode>(null);
  const [isHashSigningDisabled, setHashSigningDisabled] = useState(false);

  const { path } = account.publicKeys[0];
  const { getTransactionDigest, signTransactionHash, broadcastSignedTransaction, confirmTransaction } =
    useTransaction();

  const transactionHash = getTransactionDigest(transaction);

  useEffect(() => {
    if (isOpen) {
      setStatus(TransactionModalStatus.idle);
      setErrorMessage(null);

      (async () => {
        const connection = await connectLedger();
        const { hashSignPolicy } = await connection.device.getSettings();

        if (!hashSignPolicy) {
          setHashSigningDisabled(true);
          setErrorMessage(translate('transaction.transactionModal.errors.hashSigningDisabled'));
        }

        await connection.closeDevice();
      })();
    }
  }, [connectLedger, isOpen, translate]);

  const handleModalClose = () => {
    if (status !== TransactionModalStatus.idle) {
      showError('transaction.transactionModal.errors.transactionInProgress');
    } else {
      onClose();
    }
  };

  const handleTransactionDownload = () => {
    downloadContent(`hive-${Date.now()}.json`, JSON.stringify(transaction));
  };

  const handleBroadcast = async () => {
    setErrorMessage(null);

    try {
      setStatus(TransactionModalStatus.awaitingSignup);
      const signedTransaction = await signTransactionHash(transaction, path);

      start();

      setStatus(TransactionModalStatus.broadcasting);
      const response = await broadcastSignedTransaction(signedTransaction);

      await confirmTransaction(response.id);
    } catch (error) {
      setErrorMessage(getErrorMessage(error));

      showError('transaction.transactionModal.errors.failed');

      setStatus(TransactionModalStatus.idle);
      done();

      return;
    }

    await refreshAccounts();
    showSuccess('transaction.transactionModal.completed');
    done();

    handleModalClose();
    setStatus(TransactionModalStatus.idle);
  };

  const buttonLabelMapper = {
    [TransactionModalStatus.idle]: translate('transaction.transactionModal.button.signIn'),
    [TransactionModalStatus.awaitingSignup]: translate('transaction.transactionModal.button.confirmOnDevice'),
    [TransactionModalStatus.broadcasting]: translate('transaction.transactionModal.button.broadcasting'),
  };

  return (
    <Dialog open={isOpen} onClose={handleModalClose} maxWidth={'xl'}>
      <DialogTitle>{translate('transaction.transactionModal.title')}</DialogTitle>
      <DialogCloseButton onClick={() => handleModalClose()} />

      <DialogContent sx={hashedTransactionModalStyles}>
        <ActionParameter>
          <ActionParameterLabel>{translate('transaction.transactionModal.signingKeyPath')}</ActionParameterLabel>
          <ActionParameterValue>{path}</ActionParameterValue>
        </ActionParameter>

        <ActionParameter>
          <ActionParameterLabel>{translate('transaction.transactionModal.operations')}</ActionParameterLabel>

          {operations.map((operation, index) => (
            <ActionParameterExpandableValue heading={operation[0]} key={index}>
              {JSON.stringify(operation[1], null, 4)}
            </ActionParameterExpandableValue>
          ))}
        </ActionParameter>

        <ActionParameter>
          <ActionParameterLabel>{translate('transaction.transactionModal.hash')}</ActionParameterLabel>
          <ActionParameterValue>{transactionHash}</ActionParameterValue>
        </ActionParameter>

        <Box sx={actionsStyles}>
          <LoadingButton
            variant={'contained'}
            onClick={() => handleBroadcast()}
            loading={status !== TransactionModalStatus.idle}
            disabled={status !== TransactionModalStatus.idle || isHashSigningDisabled}
            loadingPosition={'start'}
          >
            {buttonLabelMapper[status]}
          </LoadingButton>

          <Button variant={'text'} size={'small'} onClick={() => handleTransactionDownload()}>
            {translate('transaction.transactionModal.button.downloadJson')}
          </Button>
        </Box>

        {errorMessage && <DialogVerticalError>{errorMessage}</DialogVerticalError>}
      </DialogContent>
    </Dialog>
  );
};
