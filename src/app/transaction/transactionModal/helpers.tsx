import { ReactNode } from 'react';
import { TransactionError } from 'context/transaction/hooks/useTransaction/useTransaction.types';
import { TransactionStatus } from 'modules/hive/transaction/transaction';
import { isRpcError } from 'modules/ledger/rpcError/rpcError';
import { FormattedRpcError } from 'ui/message/formattedRpcError/FormattedRpcError';
import { TranslatedMessage } from 'ui/message/translatedMessage/TranslatedMessage';

export const getErrorMessage = (error: unknown): ReactNode => {
  if (error instanceof Error) {
    if (error.message === TransactionError.deviceLocked) {
      return <TranslatedMessage id={'transaction.transactionModal.errors.deviceLocked'} />;
    }

    if (error.message === TransactionError.applicationNotRunning) {
      return <TranslatedMessage id={'transaction.transactionModal.errors.applicationNotRunning'} />;
    }

    if (error.message === TransactionError.transactionRejectedByUser) {
      return <TranslatedMessage id={'transaction.transactionModal.errors.transactionRejected'} />;
    }

    if (error.message === TransactionError.transactionNotIncludedInBlock) {
      return <TranslatedMessage id={'transaction.transactionModal.errors.transactionNotAccepted'} />;
    }

    if (isRpcError(error)) {
      return <FormattedRpcError error={error} />;
    }
  }

  return <TranslatedMessage id={'transaction.transactionModal.errors.failed'} />;
};

export const isTransactionAwaiting = (status: string) => {
  return [TransactionStatus.withinMempool, TransactionStatus.unknown].includes(status as TransactionStatus);
};

export const isTransactionConfirmed = (status: string) => {
  return [TransactionStatus.withinReversibleBlock, TransactionStatus.withinIrreversibleBlock].includes(
    status as TransactionStatus,
  );
};
