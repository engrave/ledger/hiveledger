import { Transaction } from 'modules/hive/operation/operation';

export type TransactionModalProps = {
  transaction: Transaction;
  isOpen: boolean;
  onClose: () => void;
};

export enum TransactionModalStatus {
  idle = 'idle',
  awaitingSignup = 'awaiting_signup',
  broadcasting = 'broadcasting',
}
