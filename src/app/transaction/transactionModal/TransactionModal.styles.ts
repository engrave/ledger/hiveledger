import { Styles } from 'styles/theme.types';

export const transactionModalStyles: Styles = {
  width: '420px',
};

export const actionsStyles: Styles = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  gap: 1,
  textAlign: 'center',
  marginTop: '30px',
};
