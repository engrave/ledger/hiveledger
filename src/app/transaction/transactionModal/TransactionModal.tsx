import { LoadingButton } from '@mui/lab';
import { Box, Button, CircularProgress, Dialog, DialogContent, DialogTitle } from '@mui/material';
import { ReactNode, useEffect, useState } from 'react';
import { getErrorMessage } from 'app/transaction/transactionModal/helpers';
import { actionsStyles, transactionModalStyles } from 'app/transaction/transactionModal/TransactionModal.styles';
import { TransactionModalProps, TransactionModalStatus } from 'app/transaction/transactionModal/TransactionModal.types';
import { parametersOrders } from 'config/transactions';
import { useActiveAccount } from 'context/auth/hooks/useActiveAccount';
import { useAuth } from 'context/auth/hooks/useAuth';
import { useTranslator } from 'context/localization/hooks/useTranslator';
import { useGlobalProgressBar } from 'context/progress/hooks/useGlobalProgressBar';
import { useTransaction } from 'context/transaction/hooks/useTransaction/useTransaction';
import { useToast } from 'hooks/useToast/useToast';
import { serializeParameter } from 'modules/hive/transaction/serializeParameter/serializeParameter';
import { ActionParameter, ActionParameterLabel, ActionParameterValue } from 'ui/data/actionParameter/ActionParameter';
import { DialogCloseButton } from 'ui/modal/dialogCloseButton/DialogCloseButton';
import { DialogVerticalError } from 'ui/modal/dialogVerticalError/DialogVerticalError';
import { downloadContent } from 'utils/file/downloadContent/downloadContent';
import { toSentenceCase } from 'utils/string/toSentenceCase';

export const TransactionModal = ({ transaction, isOpen, onClose }: TransactionModalProps) => {
  const translate = useTranslator();
  const { refreshAccounts } = useAuth();
  const account = useActiveAccount();

  const operation = transaction.operations[0];
  const [operationName, operationParameters] = operation;

  const { showError, showSuccess } = useToast();
  const { start, done } = useGlobalProgressBar();

  const [status, setStatus] = useState<TransactionModalStatus>(TransactionModalStatus.idle);
  const [errorMessage, setErrorMessage] = useState<ReactNode>(null);

  const { path } = account.publicKeys[0];
  const { signTransaction, broadcastSignedTransaction, confirmTransaction } = useTransaction();

  useEffect(() => {
    if (isOpen) {
      setStatus(TransactionModalStatus.idle);
      setErrorMessage(null);
    }
  }, [isOpen]);

  const handleModalClose = () => {
    if (status !== TransactionModalStatus.idle) {
      showError('transaction.transactionModal.errors.transactionInProgress');
    } else {
      onClose();
    }
  };

  const handleTransactionDownload = () => {
    downloadContent(`hive-${Date.now()}.json`, JSON.stringify(transaction));
  };

  const handleBroadcast = async () => {
    setErrorMessage(null);

    try {
      setStatus(TransactionModalStatus.awaitingSignup);
      const signedTransaction = await signTransaction(transaction, path);

      start();

      setStatus(TransactionModalStatus.broadcasting);
      const response = await broadcastSignedTransaction(signedTransaction);

      await confirmTransaction(response.id);
    } catch (error) {
      setErrorMessage(getErrorMessage(error));

      showError('transaction.transactionModal.errors.failed');

      setStatus(TransactionModalStatus.idle);
      done();

      return;
    }

    await refreshAccounts();
    showSuccess('transaction.transactionModal.completed');
    done();

    handleModalClose();
    setStatus(TransactionModalStatus.idle);
  };

  const buttonLabelMapper = {
    [TransactionModalStatus.idle]: translate('transaction.transactionModal.button.signIn'),
    [TransactionModalStatus.awaitingSignup]: translate('transaction.transactionModal.button.confirmOnDevice'),
    [TransactionModalStatus.broadcasting]: translate('transaction.transactionModal.button.broadcasting'),
  };

  return (
    <Dialog open={isOpen} onClose={handleModalClose} maxWidth={'md'}>
      <DialogTitle>{translate('transaction.transactionModal.title')}</DialogTitle>
      <DialogCloseButton onClick={() => handleModalClose()} />

      <DialogContent sx={transactionModalStyles}>
        <ActionParameter>
          <ActionParameterLabel>{translate('transaction.transactionModal.signingKeyPath')}</ActionParameterLabel>
          <ActionParameterValue>{path}</ActionParameterValue>
        </ActionParameter>

        <ActionParameter>
          <ActionParameterLabel>{translate('transaction.transactionModal.operation')}</ActionParameterLabel>
          <ActionParameterValue>{operationName}</ActionParameterValue>
        </ActionParameter>

        {parametersOrders[operationName].map((parameter, index) => (
          <ActionParameter key={index}>
            <ActionParameterLabel>{toSentenceCase(parameter)}</ActionParameterLabel>
            <ActionParameterValue>{serializeParameter(operationParameters[parameter])}</ActionParameterValue>
          </ActionParameter>
        ))}

        <Box sx={actionsStyles}>
          <LoadingButton
            variant={'contained'}
            onClick={() => handleBroadcast()}
            loading={status !== TransactionModalStatus.idle}
            disabled={status !== TransactionModalStatus.idle}
            loadingPosition={'start'}
            startIcon={<CircularProgress size={'xs'} />}
          >
            {buttonLabelMapper[status]}
          </LoadingButton>

          <Button variant={'text'} size={'small'} onClick={() => handleTransactionDownload()}>
            {translate('transaction.transactionModal.button.downloadJson')}
          </Button>
        </Box>

        {errorMessage && <DialogVerticalError>{errorMessage}</DialogVerticalError>}
      </DialogContent>
    </Dialog>
  );
};
