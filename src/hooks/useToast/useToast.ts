import { useMemo } from 'react';
import { toast } from 'react-toastify';
import { TranslationKey, TranslationValues } from 'context/localization/dictionaries/dictionaries';
import { useHtmlTranslator } from 'context/localization/hooks/useHtmlTranslator';

export const useToast = () => {
  const translateHtml = useHtmlTranslator();

  return useMemo(
    () => ({
      showSuccess: (message: TranslationKey, values: TranslationValues = {}) => {
        toast.success(translateHtml(message, values));
      },
      showError: (message: TranslationKey, values: TranslationValues = {}) => {
        toast.error(translateHtml(message, values));
      },
      showWarning: (message: TranslationKey, values: TranslationValues = {}) => {
        toast.warning(translateHtml(message, values));
      },
      showInfo: (message: TranslationKey, values: TranslationValues = {}) => {
        toast.info(translateHtml(message, values));
      },
      clearToasts: () => {
        toast.clearWaitingQueue();
      },
    }),
    [translateHtml],
  );
};
