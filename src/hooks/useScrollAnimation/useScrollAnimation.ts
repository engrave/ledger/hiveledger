import { useEffect, useState } from 'react';
import { throttle } from 'utils/function/throttle/throttle';

export type ScrollAnimation = {
  isAnimatedIn: boolean;
};

export type ScrollAnimationSettings = {
  element: HTMLElement;
  threshold: number;
};

export const useScrollAnimation = (options: Partial<ScrollAnimationSettings>) => {
  const settings: ScrollAnimationSettings = {
    threshold: 10,
    element: window.document.documentElement,
    ...options,
  };

  const [animationState, setAnimationState] = useState<ScrollAnimation>({
    isAnimatedIn: false,
  });

  useEffect(() => {
    const scrollHandler = throttle(
      () => {
        const isBeyondThreshold = settings.element.scrollTop > settings.threshold;

        if (isBeyondThreshold !== animationState.isAnimatedIn) {
          setAnimationState({
            isAnimatedIn: isBeyondThreshold,
          });
        }
      },
      10,
      { trailer: true },
    );

    scrollHandler();
    document.addEventListener('scroll', scrollHandler);

    return () => document.removeEventListener('scroll', scrollHandler);
  }, [animationState.isAnimatedIn, settings.element, settings.threshold]);

  return animationState;
};
