FROM node:18.14.0 AS builder
WORKDIR /app
COPY . .

RUN npm ci --no-audit --silent
RUN npm run build

FROM nginx:1.25.2-alpine as production

COPY --from=builder /app/build/. /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/
