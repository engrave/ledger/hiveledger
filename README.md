# Hive Ledger App

## Setup

Copy the .env file and adjust the environmental variables if needed.
```
cp .env.mainnet .env
```
Install dependencies using NPM 
```
npm install
```

## Development

To run local development server:
```
npm run start
```
Also run this command after each .env change:
```
npm run pregenerate
```

## Production

```
npm run build
```

## Code Quality

Check code quality violations

```
npm run lint 
```

Fix automatically some of them

```
npm run lint:fix 
```

## Testing

Run tests in an interactive live mode
```
npm test
```
To just run all tests
```
npm test -- --watchAll=false
```

## Environmental variables

| Name                                  | Description                           | Dev only |
|---------------------------------------|---------------------------------------|:--------:|
| **DISABLE_ESLINT_PLUGIN**             | Disable CRAs eslint config            |    X     |
| **ESLINT_NO_DEV_ERRORS**              | Disable eslint errors in console      |    X     |
| **REACT_APP_ENVIRONMENT**             | Environment name (e.g.: mainnet)      |          |
| **REACT_APP_HIVE_API**                | Hive API base URL                     |          |
| **REACT_APP_HIVE_ADDRESS_PREFIX**     | Hive API address prefix               |          |
| **REACT_APP_HIVE_CHAIN_ID**           | API chainId parameter value           |          |
| **REACT_APP_COINGECKO_API**           | Coingecko API base URL                |          |
| **REACT_APP_BLOCK_EXPLORER_URL**      | Block explorer domain                 |          |
| **REACT_APP_LIQUID_TICKER**           | Liquid ticker (HIVE, TESTS)           |          |
| **REACT_APP_DEBT_TICKER**             | Debt ticker (HBD, SBD)                |          |
| **REACT_APP_VESTS_TICKER**            | Vests ticker (VESTS)                  |          |
| **REACT_APP_VESTS_TICKER**            | Vests ticker (VESTS)                  |          |
| **REACT_APP_GOOGLE_ANALYTICS_ID**     | Google Analytics ID                   |          |

## License

[MIT](LICENSE)
